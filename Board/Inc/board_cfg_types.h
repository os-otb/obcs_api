/*
 * board_cfg_types.h
 *
 *  Created on: Aug 29, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef BOARD_CFG_TYPES_H_
#define BOARD_CFG_TYPES_H_

// HAL includes ---------------------------------------------------------------
#include "stm32f4xx_hal.h"

#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_gpio.h"
#include "arm_math.h"

// GPIOs ----------------------------------------------------------------------
#define BOARD_GPIO_STRUCT_INIT(bp, gport, gpin, p, m, s, iv) {.board_port=bp, .gpio_port=gport, .gpio_pin=gpin, .pull=p, .mode=m, .speed=s, .init_val=iv}

typedef struct
{
	uint32_t board_port;	/*<! An ID for the GPIO.*/
	GPIO_TypeDef *gpio_port;
	uint16_t gpio_pin;
	uint32_t pull;			/*<! GPIO_NOPULL, GPIO_PULLUP or GPIO_PULLDOWN. */
	uint32_t mode;			/*<! GPIO_MODE_INPUT, GPIO_MODE_OUTPUT_PP, GPIO_MODE_OUTPUT_OD, GPIO_MODE_ANALOG, etc. */
	uint32_t speed; 		/*<! GPIO_SPEED_FREQ_LOW, GPIO_SPEED_FREQ_MEDIUM, GPIO_SPEED_FREQ_HIGH or GPIO_SPEED_FREQ_VERY_HIGH.*/
	uint32_t init_val;		/*<! GPIO_PIN_RESET (LOW) or GPIO_PIN_SET (HIGH). */
}Board_GPIO_Handler_T;

// USARTs ---------------------------------------------------------------------
#define BOARD_USART_STRUCT_INIT(bp, pusartx, clkusart, baud, txport, txpin, txclk, txaf, rxport, rxpin, rxclk, rxaf, i) \
{\
	.port=bp, .pusart=pusartx, .usart_clk=clkusart, .baudrate=baud,\
	.tx_gpio.port=txport, .tx_gpio.pin=txpin, .tx_gpio.clk=txclk, .tx_gpio.alternate_func=txaf,\
	.rx_gpio.port=rxport, .rx_gpio.pin=rxpin, .rx_gpio.clk=rxclk, .rx_gpio.alternate_func=rxaf,\
	.irqn = i\
}

typedef struct
{
	uint32_t port;		/*!< An ID for the USART. */
	USART_TypeDef *pusart;				/*!< USARTx instance used for that port. */
	uint32_t usart_clk;					/*!< For example: LL_APB2_GRP1_PERIPH_USART1. */
	uint32_t baudrate;

	/* TX Pin configuration. */
	struct
	{
		GPIO_TypeDef *port;			/*!< For example: GPIOB. */
		uint16_t pin;				/*!< For example: LL_GPIO_PIN_6. */
		uint32_t clk; 				/*!< For example: LL_AHB1_GRP1_PERIPH_GPIOB if rx pin is in GPIO B port. */
		uint32_t alternate_func;	/*!< For example: LL_GPIO_AF_7. */
	}tx_gpio;

	/* RX Pin configuration. */
	struct
	{
		GPIO_TypeDef *port;			/*!< For example: GPIOA. */
		uint16_t pin;				/*!< For example: LL_GPIO_PIN_10. */
		uint32_t clk; 				/*!< For example: LL_AHB1_GRP1_PERIPH_GPIOA if rx pin is in GPIO B port. */
		uint32_t alternate_func;	/*!< For example: LL_GPIO_AF_7. */
	}rx_gpio;

	IRQn_Type irqn; /*!< For example: USART1_IRQn. */
}Board_USART_Handler_T;

// LMT85 ----------------------------------------------------------------------
#define BOARD_LMT85_MAX_SENSORS	12	/*!< Max sensors that can be used. */
#define BOARD_LMT85_FIR_FILTER_ORDER 32		/*!< The order of the filter applied to the temp sensors. */
#define BOARD_LMT85_FIR_FILTER_COEFFS_QTY	(BOARD_LMT85_FIR_FILTER_ORDER+1) /*!< The qty of coeffs is N+1 (N = order of the filter). */
#define BOARD_LMT85_FIR_FILTER_SAMPLES_QTY	(BOARD_LMT85_FIR_FILTER_ORDER+1) /*!< The quantiy of samples needed to implement the filter is N+1 (N = order of the filter).*/
#define BOARD_LMT85_INIT_STRUCT(ap_id, ad_id) { .api_id = ap_id, .adc_input_id = ad_id, .samples_mv = {0}, .last_temp = 0, .fir = {0}}

typedef struct
{
	uint32_t api_id; 			/*!< ID used by the API to identify the sensor. NOTE: This ID must start from 0 (the first sensor must have id = 0). */
	uint32_t adc_input_id;	/*!< ID used to get the masure from the ADC conversions. */
	float samples_mv[BOARD_LMT85_FIR_FILTER_SAMPLES_QTY]; /*!< Here will be stored the previous and the actual sample of the adc. */
	float last_temp;	/*!< Last temperature measured. */
	float last_filtered; /*!< Last filtered value. */
	arm_fir_instance_f32 fir; /*!< FIR filter instance used for this sensor. */
} board_lmt85_t;

extern board_lmt85_t board_lmt85[BOARD_LMT85_MAX_SENSORS];	/*!< Table with all the sensors in the board. */
/*!< FIR coefficients for each sensor. The coefficients must be in reversed order:
 * c[0] => coefficient of order BOARD_LMT85_FIR_FILTER_ORDER,
 * c[BOARD_LMT85_FIR_FILTER_ORDER]  => coefficient of order 0. */
extern float board_lmt85_fir_coeffs[BOARD_LMT85_FIR_FILTER_COEFFS_QTY];

// ADC inputs -----------------------------------------------------------------
#define BOARD_ADC_MAX_INPUTS_PER_ADC	6	/*!< Maximum number of inputs that each ADC can measure. */
#define BOARD_ADC_MAX_ADCS_USED			2	/*!< Maximun number of ADCs utilized. */

#define BOARD_ADC_INPUT_INIT_STRUCT(id, ch, gpo, gpi)\
{\
	.sensor_id = id, .channel = ch,\
	.gport = gpo, .gpin = gpi, \
}

typedef struct
{
	GPIO_TypeDef *gport;		/*!< The port of the GPIO of this input. */
	uint16_t gpin;				/*!< The pin number of the GPIO port of this input. */
	uint32_t sensor_id;			/*!<  An ID for the sensor. */
	uint32_t channel; 			/*!< The ADC channel used to measure the sensor. */
}board_adc_input_t;

extern board_adc_input_t board_adc1_inputs[BOARD_ADC_MAX_INPUTS_PER_ADC];
extern board_adc_input_t board_adc3_inputs[BOARD_ADC_MAX_INPUTS_PER_ADC];

// 23k256 RAMs ----------------------------------------------------------------
#define BOARD_23K256_MAX_RAMS_USED 9	/*!< Maximun number of ADCs utilized. */

#define BOARD_23K256_INIT_STRUCT(cgpo, cgpi, i)\
{\
	.cs.gpio_port = cgpo, .cs.gpio_pin = cgpi,\
	.ram_id = i \
}

typedef struct
{
	struct
	{
		GPIO_TypeDef *gpio_port;		/*!< The port of the GPIO of this CS. */
		uint16_t gpio_pin;				/*!< The pin number of the GPIO port of this cs. */
	}cs;
	uint32_t ram_id;			/*!<  An ID for the sensor. */
}board_23k256_t;

extern board_23k256_t board_23k256[BOARD_23K256_MAX_RAMS_USED];

#endif /* BOARD_CFG_TYPES_H_ */
