/*
 * board.h
 *
 *  Created on: March 15, 2021
 *      Authors:
 *      Lucas Mancini
 *      Julian Rodriguez aka marifante
 */

#ifndef BOARD_H
#define BOARD_H

// Includes -------------------------------------------------------------------
#include "stm32f407xx.h"
#include "stm32f4xx_hal.h"
#include "ff_gen_drv.h"

#include "api_iface_board.h"

// Macros & Constants ---------------------------------------------------------
#define BOARD_SET_VTOR_ADDRESS(addr)			SCB->VTOR = addr
#define BOARD_SET_MAIN_STACK_POINTER_ADDR(addr)	__set_MSP(*(__IO uint32_t*)addr)

// Public variable declaration ------------------------------------------------

// Public functions declaration  ----------------------------------------------
ret_code_t Board_Init(void);
void Board_Prepare_For_IAP_Jump(uint32_t app_address);

// GPIOs functions --------------------------------------------------
ret_code_t Board_GPIO_Init(void);
ret_code_t Board_GPIO_Toggle(uint32_t board_gpio);
ret_code_t Board_GPIO_Get_State(uint32_t board_gpio, bool *state);
ret_code_t Board_GPIO_Set_State(uint32_t board_gpio, bool state);

// USART functions --------------------------------------------------
#define BOARD_USART_MAX_PACKET_SIZE		1024 /*<! Size (in bytes) of largest packet that can be sent through USART. */
#define BOARD_USART_RX_CIRC_BUFFER_SIZE	BOARD_USART_MAX_PACKET_SIZE*5
ret_code_t Board_USART_Init(void);
ret_code_t Board_USART_Send_Packet(uint32_t port_id, char *packet, size_t packet_size);
ret_code_t Board_USART_TX_Is_Busy(uint32_t port_id);
void Board_USARTx_IRQnHandler(USART_TypeDef *USARTx);
ret_code_t Board_USART_Get_Char(uint32_t port_id, char *data, bool lock);

// FLASH functions --------------------------------------------------
typedef enum
{
	BOARD_FLASH_WR_BYTE = FLASH_TYPEPROGRAM_BYTE,
//	BOARD_FLASH_WR_HALF_WORD = FLASH_TYPEPROGRAM_HALFWORD,
	BOARD_FLASH_WR_WORD = FLASH_TYPEPROGRAM_WORD,
//	BOARD_FLASH_WR_DOUBLE_WORD = FLASH_TYPEPROGRAM_DOUBLE_WORD,
	BOARD_FLASH_WR_MAX_TYPES /* Last one. */
} Board_FLASH_WR_Unit_T;

ret_code_t Board_FLASH_Init(void);
ret_code_t Board_FLASH_Unlock(void);
ret_code_t Board_FLASH_Lock(void);
ret_code_t Board_FLASH_Unlock_Option_Bytes(void);
ret_code_t Board_FLASH_Lock_Option_Bytes(void);

ret_code_t Board_FLASH_Erase_Sector(int nsector, bool unlock_lock);
ret_code_t Board_FLASH_Read_Offset(	int nsector, uint32_t *data, int offset, bool unlock_lock );
ret_code_t Board_FLASH_Write_Sector(int nsector, uint32_t data);
ret_code_t Board_FLASH_Write_Offset(	int nsector, Board_FLASH_WR_Unit_T unit_type,
										uint32_t data, int offset, bool unlock_lock);
int32_t Board_Flash_Get_Sector_Size(int nsector);
uint32_t Board_FLASH_Get_Sector_Address(int nsector);
bool Board_FLASH_Is_There_A_Valid_App(int nsector);

// RNG (Random Number Generator) functions --------------------------
ret_code_t Board_Random_Number_Init(void);
ret_code_t Board_Random_Number_Generate(uint32_t *pulValue);

// Ethernet functions -----------------------------------------------
ret_code_t Board_Eth_Init(ETH_HandleTypeDef* xETHHandle);

// SD diskio functions ----------------------------------------------
#ifdef API_FS_USE_SD
ret_code_t Board_SD_Diskio_Init( void );
void Board_SD_TimerA_ISR_Callback( void );
#endif

#ifdef API_FS_USE_MB85RS
ret_code_t Board_MB85RS_Diskio_Init( void );
#endif

// ADC --------------------------------------------------------------
ret_code_t board_adc_init(void);
ret_code_t board_adc_start_all(void);
ret_code_t board_adc_stop_all(void);
ret_code_t board_adc_measure(ADC_TypeDef *adc, float *buff, size_t buff_size, bool block);

// LMT85 ------------------------------------------------------------
ret_code_t board_lmt85_init(void);
ret_code_t board_lmt85_get_temp(api_temp_sensor_measure_t *buff_measure, int buff_size);

// 23K256 RAMs ------------------------------------------------------
ret_code_t board_23k256_init(void);
ret_code_t board_23k256_send(uint8_t mem_num, uint8_t *buff, size_t buff_size, uint32_t timeout_ms);
ret_code_t board_23k256_receive(uint8_t mem_num, uint8_t *buff, size_t buff_size, uint32_t timeout_ms);

// CRC --------------------------------------------------------------
ret_code_t Board_CRC_init(void);
uint32_t Board_CRC_calculate8(uint8_t *buff, size_t buff_size, bool reset);

#endif /* BOARD_INC_BOARD_H_ */
