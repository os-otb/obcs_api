/*
 * board.c
 *
 *  Created on: March 15, 2021
 *      Authors:
 *      Lucas Mancini
 *      Julian Rodriguez aka marifante
 */

// Include --------------------------------------------------------------------
#include "board_cfg_private.h"
#include "board_private.h"

// Macros & constants ---------------------------------------------------------

// Private variables declaration ----------------------------------------------

// Private variables definition -----------------------------------------------

// Private functions declaration ----------------------------------------------
ret_code_t board_system_clock_config(void);

// Public functions definition  -----------------------------------------------
/**
 *
 * @brief init core board peripherals.
 * Just the necessary peripherals to run the Boot code.
 *
 * @return RET_OK if success.
 *
 */
ret_code_t Board_Init(void)
{
	ret_code_t ret = RET_OK;
	HAL_StatusTypeDef hal_ret = HAL_OK;

	SystemInit(); // Important! This function relocalizates the Vector Table

	hal_ret = HAL_Init();
	if( HAL_OK != hal_ret )
	{
		/* TODO handle this error. Maybe not using HAL at all? */
		ret = RET_ERR;
		return ret;
	}

	__HAL_RCC_SYSCFG_CLK_ENABLE();
	__HAL_RCC_PWR_CLK_ENABLE();

	/* System interrupt init*/
	/* PendSV_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(PendSV_IRQn, 15, 0);

	/* Configure the system clock */
	ret = board_system_clock_config();
	if( RET_OK != ret )
	{
		/* TODO: What we must to do when clock configuration fails? */
		return ret;
	}

	/* Initialize all core configured peripherals for Boot. */
	Board_GPIO_Init();

	ret = Board_FLASH_Init();
	if( RET_OK != ret )
	{
		/* TODO: What we must to do when flash configuration fails? goodbye */
		return ret;
	}

	ret = Board_USART_Init();
	if( RET_OK != ret )
	{
		/* TODO: What we must to do when the SD configuration fails? */
		return ret;
	}

	/* The random number generator is needed if the network APIs are in use. */
	ret = Board_Random_Number_Init();
	if( RET_OK != ret )
	{
		/* TODO: What we must to do when the RNG configuration fails? */
		return ret;
	}

#ifdef API_FS_USE_SD
	ret = Board_SD_Diskio_Init();
#endif

#ifdef API_FS_USE_MB85RS
	ret = Board_MB85RS_Diskio_Init();
#endif
	return ret;
}

/**
 * @brief System Clock Configuration
 *
 * @return RET_OK if success.
 *
 */
ret_code_t board_system_clock_config(void)
{
	ret_code_t ret = RET_OK;
	RCC_OscInitTypeDef RCC_OscInitStruct = {0};
	RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

	/* Configure the main internal regulator output voltage. */
	__HAL_RCC_PWR_CLK_ENABLE();
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	/* Initializes the RCC Oscillators according to the specified parameters
	 * in the RCC_OscInitTypeDef structure. */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLM = 8;
	RCC_OscInitStruct.PLL.PLLN = 168;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 4;
	if( HAL_OK != HAL_RCC_OscConfig(&RCC_OscInitStruct))
	{
		ret = RET_ERR;
		return ret;
	}
	/* Initializes the CPU, AHB and APB buses clocks. */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
						  	  	  |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
	{
		ret = RET_ERR;
	}

	return ret;
}

/*
 * @brief prepare the board to make an IAP jump to application.
 *
 * @param app_address the initial address of the application code.
 *
 * */
void Board_Prepare_For_IAP_Jump(uint32_t app_address)
{
	HAL_RCC_DeInit(); // De-init clock configuration
	HAL_DeInit(); // De-init peripherals handled by hal

    /* Stop the SysTick. */
    SysTick->CTRL = 0;
    SysTick->LOAD = 0;
    SysTick->VAL  = 0;

	__DMB(); /* ARM says to use a DMB instruction before relocating VTOR. */
	BOARD_SET_VTOR_ADDRESS(app_address);
	__DSB(); /* ARM says to use a DSB instruction just after relocating VTOR. */

	BOARD_SET_MAIN_STACK_POINTER_ADDR(app_address);
}
