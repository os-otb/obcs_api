/*
 * board_mb85rs_diskio.c
 *
 *	This MB85RS SPI driver was designed to be used with a FAT filesystem stack
 *	(FATFS or FreeRTOS+FAT).
 *	This driver handles the SPI communication with a MB85RS with the SPI
 *	peripheral of the STM32 and one hardware timers.
 *
 *  Created on: Jun 22, 2021
 *      Author: Julian Rodriguez aka Marifante 
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Includes -------------------------------------------------------------------
#include "board_cfg_private.h"

#ifdef API_FS_USE_MB85RS
// Marcos & Constants ---------------------------------------------------------

/* This timeout is the timeout that the TX/RX functions of the SPI peripheral
 * blocks until a response is received. Must be smaller */
#define BOARD_MB85RS_DISKIO_SPI_TIMEOUT_MS	2000 /* !< SPI transmit and receive timeout in ms. Is not counted with a timer. */

#define BOARD_MB85RS_DISKIO_STATUS_IS_NO_INIT		((0 != (board_mb85rs_diskio.mb85rs.stat & STA_NOINIT)) ? true : false)  /* !< true if the SD is not initialized yet. */
#define BOARD_MB85RS_DISKIO_STATUS_IS_NO_DISK		((0 != (board_mb85rs_diskio.mb85rs.stat & STA_NODISK)) ? true : false)  /* !< true if there is no SD inserted. */
#define BOARD_MB85RS_DISKIO_STATUS_IS_PROTECT		((0 != (board_mb85rs_diskio.mb85rs.stat & STA_PROTECT)) ? true : false) /* !< true if the SD is in WR protect mode. */

/* This driver uses the GPIOB_BSRR register.
 * This is a 32-bit register which allows the application to set and reset each
 * individual bit in the GPIOB_ODR.
 * Each bit in GPIOB_ODR correspond two control bits in GPIOB_BSRR:
 * 	- BSRR(i)
 * 	- BSRR(i+SIZE)
 * 	When a 1 is written to GPIOB_BSRR(i) then a 1 is written to GPIOB_ODR(i).
 * 	When a 1 is written to GPIOB_BSRR(i+SIZE) then a 0 is written to GPIOB_ODR(i).
 * 	Managing the state of a gpio with this register allows the state of the
 * 	gpio to be imposed atomically.
 *
 * Macros to set MB85RS SPI Chip Select high or low. Used by FATFS to BOARD_MB85RS_DISKIO_SPI_CS_LOW or BOARD_MB85RS_DISKIO_SPI_CS_HIGH
 * the disk. */
#define BOARD_MB85RS_DISKIO_SPI_CS_HIGH()		BOARD_MB85RS_CS_GPORT->BSRR |= (1<<BOARD_MB85RS_CS_GPIN_NUMBER)		//MB85RS CS H
#define BOARD_MB85RS_DISKIO_SPI_CS_LOW()		BOARD_MB85RS_CS_GPORT->BSRR |= (1<<(16+BOARD_MB85RS_CS_GPIN_NUMBER))	//MB85RS CS L

/* Macros to set the frequency of the SPI communication.
 * NOTE: the macro for set the slow clock to the SPI must be tuned to emit a clock rate between 100kHz and 400kHz. */
#define BOARD_MB85RS_DISKIO_SPI_SET_SLOW_FREQ() LL_SPI_SetBaudRatePrescaler(board_mb85rs_diskio.hspi.Instance, LL_SPI_BAUDRATEPRESCALER_DIV128)
#define BOARD_MB85RS_DISKIO_SPI_SET_FAST_FREQ() LL_SPI_SetBaudRatePrescaler(board_mb85rs_diskio.hspi.Instance, LL_SPI_BAUDRATEPRESCALER_DIV64)

/* Commands sent to the MB85RS Card and responses that the MB85RS Card sent to the MCU.
 * Each command have a determinated response. There can be 3 types of responses: R1, R1b, R3 and R7.
 *
 * CMD		|	RESP	|	Description
 * -----------------------------------------------------------------------------------------------------------------------
 * CMD_WREN	|	N/A				|	Sets the WEL (Write Enable Latch). The WEL must be set before a writing operation. (NOTE 3)
 * CMD_WRDI	|	N/A				|	Reset the WEL (Write Enable Latch). Write operations are not performed when WEL is reset.
 * CMD_RDSR	| Status Register	|	Reads the Status Register data. After send this command, the MB85RS will send the contet of this register.
 * CMD_WDSR |	N/A				|	Write data to the Status Register of the memory.
 * CMD_READ |	data			|	Reads FRAM memory cell array data. Send this command and a 24 bit address to read. (NOTE 1)
 * CMD_WRITE|	N/A				|	Writes data to the FRAM memory cell array. Send this command and a 24 bit addres to write. (NOTE 2)
 * CMD_RDID	|	Device Info		|	Reads fixed Device ID. After sending this command, the memory send to the uC a 32bit response. See CMD_RDID_RES_xxx
 * CMD_FSTRD|	data			|	Its similar to READ command (TODO: research).
 * CMD_SLEEP|	N/A				|	TODO: research the sleep cmd.
 *
 * NOTE 1: With the READ command, we can send this command one time and continue giving clock cycles to the FRAM
 * and the FRAM will increment the start address sent by us and keep sending the data of that address continously
 * until we assert the Chip Select GPIO.
 * NOTE 2: The same mechanical occurs with the WRITE command. See NOTE 1.
 * NOTE 3: The Write Enable Latch is reset after Power On. For this reason, must to be set with WREN when the user
 * initializes the driver.
 */
#define CMD_WREN	(0x06)	/*!< WREN command. Set write enable latch. */
#define CMD_WRDI	(0x04)	/*!< WRDI command. Reset write enable latch. */
#define CMD_RDSR	(0x05)	/*!< RDSR command. Read status register. */
#define CMD_WDSR	(0x01)	/*!< WDSR command. Write status register. */
#define CMD_READ	(0x03)	/*!< READ command. Read memory code. */
#define CMD_WRITE	(0x02)	/*!< WRITE command. Write memory code. */
#define CMD_RDID	(0x9F)	/*!< RDID command. Read device ID command. */
#define CMD_FSTRD	(0x0B)	/*!< FSTRD command. Fast Read memory command. */
#define CMD_SLEEP	(0xB9)	/*!< SLEEP command. Set to sleep mode. */

/* CMD_RDID response:
 * The first byte sent by the FRAM is the manufacturer ID.
 * The second byte sent by the FRAM is the continuation code.
 * The third & four byte sent by the FRAM is the product ID.
 * */
#define FUJITSU_MAN_ID	(0x04)		/*!< Fujitsu manufacturer ID. */
#define CMD_RDID_CONT_CODE	(0x7F)	/*!< Continuation code */

#define BOARD_MB85RS_DISKIO_SECTOR_SIZE		512	/*!< This driver is designed in that manner that the sector size is 512 bytes. */
#define BOARD_MB85RS_ADDR_LEN				3	/*!< The leng address of each memory register of the FRAM. */

/* Address definitions of the MB85RS4 particular memory. */
#define BOARD_MB85RS_DISKIO_MAX_WORDS		524288	/*!< This FRAM is divided in 524288 words of 1 byte. */
#define BOARD_MB85RS_DISKIO_INITIAL_ADDRESS	0	/*!< Initial address of the FRAM. */
#define BOARD_MB85RS_DISKIO_END_ADDRESS		(BOARD_MB85RS_DISKIO_INITIAL_ADDRESS+BOARD_MB85RS_DISKIO_MAX_WORDS)	/*!< End address of the FRAM. */
#define BOARD_MB85RS_DISKIO_SECTOR_COUNT	(BOARD_MB85RS_DISKIO_MAX_WORDS/BOARD_MB85RS_DISKIO_SECTOR_SIZE)

// Private variables declaration ----------------------------------------------
typedef struct
{
	SPI_HandleTypeDef hspi;
	volatile uint32_t main_tim_ms_left;	/* !< Variable used to implement timeouts with the main timer. */

	struct
	{
		volatile DSTATUS stat;						/* Disk status Flag. */
	}mb85rs;
} board_mb85rs_diskio_t;

// Private variables definition -----------------------------------------------
static board_mb85rs_diskio_t board_mb85rs_diskio;

// Private functions declaration ----------------------------------------------
static void board_mb85rs_diskio_spi_tx_byte( uint8_t data );
static uint8_t board_mb85rs_diskio_spi_rx_byte( void );
static uint32_t board_mb85rs_diskio_sector_to_address(uint32_t sector);
static ret_code_t board_mb85rs_diskio_send_cmd( uint8_t cmd,
												uint8_t *ptx_data, size_t tx_data_size,
												uint8_t *prx_data, size_t rx_data_size );
static ret_code_t board_mb85rs_diskio_rx_sector( uint8_t *buff, uint32_t sector );
static ret_code_t board_mb85rs_diskio_tx_sector( const uint8_t *buff , uint32_t sector );
ret_code_t board_mb85rs_diskio_init_spi(void);
ret_code_t board_mb85rs_diskio_init_main_timer(void);
ret_code_t board_mb85rs_diskio_init_secondary_timer(void);
ret_code_t board_mb85rs_diskio_init_timeout(uint32_t timeout_ms, TIM_TypeDef *timer);
ret_code_t board_mb85rs_diskio_deinit_timeout(TIM_TypeDef *timer);
static void board_mb85rs_diskio_deselect(void);
static ret_code_t board_mb85rs_diskio_select(void);

/* Functions used to interact with the drive, this are accesses by the upper level
 * file system modules. */
DSTATUS	board_mb85rs_diskio_initialize( uint8_t pdrv );
DSTATUS	board_mb85rs_diskio_status( uint8_t pdrv );
Diskio_DRESULT_t board_mb85rs_diskio_read( uint8_t pdrv, uint8_t *buff, uint32_t sector, uint32_t count );
Diskio_DRESULT_t board_mb85rs_diskio_write( uint8_t pdrv, const uint8_t *buff, uint32_t sector, uint32_t count );
Diskio_DRESULT_t board_mb85rs_diskio_ioctl( uint8_t pdrv, uint8_t cmd, void *buff );
Diskio_DRESULT_t board_mb85rs_diskio_get_sectors_count(uint8_t drv, uint32_t *sectors);

// Public variables definition ------------------------------------------------
#ifdef API_FS_USE_FATFS
Diskio_drvTypeDef MB85RS_Disk_Driver =
{
	board_mb85rs_diskio_initialize,
	board_mb85rs_diskio_status,
	board_mb85rs_diskio_read,
#if  _USE_WRITE
	board_mb85rs_diskio_write,
#endif  /* _USE_WRITE == 1 */
#if  _USE_IOCTL == 1
	board_mb85rs_diskio_ioctl,
#endif /* _USE_IOCTL == 1 */
};
#endif

#ifdef API_FS_USE_FF_FAT
API_FF_FAT_Diskio_t MB85RS_Disk_Driver =
{
	board_mb85rs_diskio_initialize,
	board_mb85rs_diskio_status,
	board_mb85rs_diskio_read,
	board_mb85rs_diskio_write,
	board_mb85rs_diskio_ioctl,
};
#endif

// Public functions definition ------------------------------------------------
/*
 * @brief init FS MB85RS driver. Assign GPIO to CS and config timers to handle
 * time of spi transactions.
 * */
ret_code_t Board_MB85RS_Diskio_Init( void )
{
	ret_code_t ret = RET_OK;
	memset(&board_mb85rs_diskio, 0, sizeof(board_mb85rs_diskio));

	ret = board_mb85rs_diskio_init_spi();
	if( RET_OK != ret )
	{
		/* TODO Handle error. */
	}

	return ret;
}

// Public functions definitions used by FATFS file ----------------------------
/**
  * @brief  Initializes a Drive with the MB85RS card.
  * @param  pdrv: Physical drive number (0..) to identify the drive
  * @return DSTATUS: Operation status
  */
DSTATUS board_mb85rs_diskio_initialize( uint8_t drv )
{
	DSTATUS dstatus = STA_NOINIT;
	ret_code_t retv = RET_OK;
	uint8_t rdid_res[4] = {0, 0, 0, 0};

	/* TODO make a function to translate the drive number to the CS GPIO of that memory. */
	if( 0 != drv ) return dstatus; 	/* Only one drive is supported by this driver (0). */

	retv = board_mb85rs_diskio_send_cmd(CMD_RDID, NULL, 0, rdid_res, sizeof(rdid_res));
	if( RET_OK == retv )
	{	/* Read the manufacturer ID and check if it is okey. */
		if( FUJITSU_MAN_ID == rdid_res[0] )
		{	/* If the manufacturer ID is okey, then set the Write Enable Latch */
			retv = board_mb85rs_diskio_send_cmd(CMD_WREN, NULL, 0, NULL, 0);
			if( RET_OK == retv ) dstatus &= ~STA_NOINIT; // Succesfull initialization
		}
	}

	board_mb85rs_diskio.mb85rs.stat = dstatus;

	return board_mb85rs_diskio.mb85rs.stat;
}

/**
  * @brief  Gets Disk board_mb85rs_diskio.mb85rs.status
  * @param  pdrv: Physical drive number (0..) of the drive.
  * @retval DSTATUS: Operation status
  * @details Only one drive is supported. In this case, always gonna be the
  * drive 0.
  */
DSTATUS board_mb85rs_diskio_status( uint8_t drv )
{
	if( 0 != drv ) return STA_NOINIT;
	return board_mb85rs_diskio.mb85rs.stat;
}

/**
  * @brief  Reads Sector(s)
  * @param  pdrv: Physical drive number (0..)
  * @param  *buff: Data buffer to store read data
  * @param  sector: Sector address (LBA)
  * @param  count: Number of sectors to read (1..128)
  * @return Diskio_DRESULT_t: Operation result
  */
Diskio_DRESULT_t board_mb85rs_diskio_read( uint8_t pdrv, uint8_t *buff, uint32_t sector, uint32_t count )
{
	ret_code_t ret = RET_OK;
	if( NULL == buff ) return RES_ERROR;

	for(int i = 0; i<count; i++)
	{
		ret = board_mb85rs_diskio_rx_sector(buff+BOARD_MB85RS_DISKIO_SECTOR_SIZE*i, sector+i);
		if( RET_OK != ret ) break;
	}

	return (RET_OK != ret) ? RES_ERROR : RES_OK;
}

/**
  * @brief  Writes Sector(s)
  * @param  pdrv: Physical drive number (0..) of the drive to write
  * @param  *buff: Data to be written
  * @param  sector: Sector address (LBA)
  * @param  count: Number of sectors to write (1..128)
  * @retval Diskio_DRESULT_t: Operation result
  */
Diskio_DRESULT_t board_mb85rs_diskio_write( uint8_t pdrv, const uint8_t *buff, uint32_t sector, uint32_t count )
{
	ret_code_t ret = RET_OK;
	if( NULL == buff ) return RES_ERROR;

	for(int i = 0; i<count; i++)
	{
		ret = board_mb85rs_diskio_tx_sector(buff+BOARD_MB85RS_DISKIO_SECTOR_SIZE*i, sector+i);
		if( RET_OK != ret ) break;
	}

	return (RET_OK != ret) ? RES_ERROR : RES_OK;
}

/**
  * @brief  I/O control operation
  * @param  pdrv: Physical drive number (0..)
  * @param  cmd: Control code
  * @param  *buff: Buffer to send/receive control data
  * @retval Diskio_DRESULT_t: Operation result
  */
Diskio_DRESULT_t board_mb85rs_diskio_ioctl( uint8_t drv, uint8_t cmd, void *buff )
{
	Diskio_DRESULT_t res = RES_ERROR;

	if( 0 != drv ) return RES_PARERR; /* Check parameter */
	if(true == BOARD_MB85RS_DISKIO_STATUS_IS_NO_INIT) return RES_NOTRDY; /* Check if drive is ready */

	switch(cmd)
	{
	case GET_SECTOR_COUNT:	/* Get drive capacity in unit of sector (uint32_t) */
		*(WORD*) buff = BOARD_MB85RS_DISKIO_SECTOR_COUNT;
		res = RES_OK;
		break;

	case GET_SECTOR_SIZE:
		*(WORD*) buff = BOARD_MB85RS_DISKIO_SECTOR_SIZE;	/* Sector size is 512 for memory cards. */
		res = RES_OK;
		break;

	case CTRL_SYNC:
		/* TODO implement a way to sync with multiple memories (maybe with hold?)*/
		res = RES_OK;
		break;

	default:
		res = RES_PARERR;
	}

	board_mb85rs_diskio_deselect();
	return res;
}

// Private functions definition -----------------------------------------------
/*
 * @brief reads N bytes from the FRAM.
 * @param buff the buffer where the data will be stored.
 * @param sector the sector to read.
 * @return RET_OK if success.
 * */
static ret_code_t board_mb85rs_diskio_rx_sector( uint8_t *buff, uint32_t sector )
{
	uint32_t addr = 0;
	ret_code_t retv = RET_OK;
	if( NULL == buff ) return RET_ERR_NULL_POINTER;

	addr = board_mb85rs_diskio_sector_to_address(sector);

	retv = board_mb85rs_diskio_select();
	if(RET_OK == retv)
	{
		board_mb85rs_diskio_spi_tx_byte(CMD_READ); // send command byte
		/* Send three byte address: */
		board_mb85rs_diskio_spi_tx_byte( (addr & 0xFF0000) >> 16 ); // first byte
		board_mb85rs_diskio_spi_tx_byte( (addr & 0x00FF00) >> 8 ); // second byte
		board_mb85rs_diskio_spi_tx_byte( (addr & 0x0000FF) >> 0 ); // third byte

		for(int i = 0; i<BOARD_MB85RS_DISKIO_SECTOR_SIZE; i++)
		{
			buff[i] = board_mb85rs_diskio_spi_rx_byte(); // write data to sector
		}
	}

	board_mb85rs_diskio_deselect(); // always de-select to leave in the same state
	return retv;
}

/*
 * @brief write a sector.
 * @param buff the buffer that contains the data that will be written.
 * @param sector the sector to write.
 * @return RET_OK if success.
 * */
static ret_code_t board_mb85rs_diskio_tx_sector( const uint8_t *buff , uint32_t sector )
{
	uint32_t addr = 0;
	ret_code_t retv = RET_OK;
	if( NULL == buff ) return RET_ERR_NULL_POINTER;

	addr = board_mb85rs_diskio_sector_to_address(sector);

	retv = board_mb85rs_diskio_select();
	if(RET_OK == retv)
	{
		board_mb85rs_diskio_spi_tx_byte(CMD_WRITE); // send command byte
		/* Send three byte address: */
		board_mb85rs_diskio_spi_tx_byte( (addr & 0xFF0000) >> 16 ); // first byte
		board_mb85rs_diskio_spi_tx_byte( (addr & 0x00FF00) >> 8 ); // second byte
		board_mb85rs_diskio_spi_tx_byte( (addr & 0x0000FF) >> 0 ); // third byte

		for(int i = 0; i<BOARD_MB85RS_DISKIO_SECTOR_SIZE; i++)
		{
			board_mb85rs_diskio_spi_tx_byte(buff[i]); // write data to sector
		}
	}

	board_mb85rs_diskio_deselect(); // always de-select to leave in the same state
	return retv;
}

/*
 * @brief Send a CMD to the MB85RS.
 * @param cmd the command to be sent
 * @param ptx_data a buffer with the data to send after sending the command. Can
 * be NULL if you don't want to sent data to the FRAM after the command.
 * @param tx_data_size the size of the data to send.
 * @param prx_data a buffer where the data received from the FRAM will be stored. Can
 * be NULL if the response of the FRAM is ignored.
 * @param rx_data_size the size of the buffer where the data received will be stored.
 * @param res where the response will be stored. Note, if the command response have more than one byte
 * (i.e. R7 response) then the following bytes (from the second onwards) of the response must be
 * captured with the board_mb85rs_diskio_spi_rx_byte() after using this function.
 * @return RET_OK if success.
 * */
static ret_code_t board_mb85rs_diskio_send_cmd( uint8_t cmd,
												uint8_t *ptx_data, size_t tx_data_size,
												uint8_t *prx_data, size_t rx_data_size )
{
	ret_code_t retv = RET_OK;
	retv = board_mb85rs_diskio_select();
	if(RET_OK == retv)
	{
		board_mb85rs_diskio_spi_tx_byte(cmd); // send command byte
		/* If a tx buffer is passed as argument, then send tx_data_size
		 * bytes to the FRAM. */
		if( NULL != ptx_data )
		{
			for(int i = 0; i<tx_data_size; i++)
			{
				board_mb85rs_diskio_spi_tx_byte(ptx_data[i]); // send command byte
			}
		}
		/* If rx buffer is passed as argument, then receive rx_data_size
		 * bytes from the FRAM. */
		if( NULL != prx_data )
		{
			for(int i = 0; i<rx_data_size; i++)
			{
				prx_data[i] = board_mb85rs_diskio_spi_rx_byte();
			}
		}
	}

	board_mb85rs_diskio_deselect(); // always de-select to leave in the same state
	return retv;
}

/*
 * @brief de-select the memory. Also make a dummy clock cycle
 * to force the Hi-Z on the MISO line (this can be useful
 * if multiple slaves are on the bus).
 * */
static void board_mb85rs_diskio_deselect(void)
{
	BOARD_MB85RS_DISKIO_SPI_CS_HIGH();
	board_mb85rs_diskio_spi_rx_byte(); /* Dummy clock, force MISO enabled. */
}

/*
 * @brief select the memory and wait until is ready.
 * @details this function asserts the CS pin and then waits until the memory
 * is read. The memory is ready when it holds the MISO line high (i.e. when
 * the MCU receives a 0xFF.
 * @return RET_OK if the card was selected successfully.
 * */
static ret_code_t board_mb85rs_diskio_select(void)
{
	ret_code_t res = RET_OK;
	BOARD_MB85RS_DISKIO_SPI_CS_LOW();
	return res;
}

/*
 * @brief translates a logical sector to a memory address.
 * @param sector the sector number to be translated.
 * @return the initial address of that sector.
 * */
static uint32_t board_mb85rs_diskio_sector_to_address(uint32_t sector)
{
	uint32_t addr = 0;
	addr = BOARD_MB85RS_DISKIO_INITIAL_ADDRESS + sector*BOARD_MB85RS_DISKIO_SECTOR_SIZE; // transform sector to address.

	/* Limit the address to not overflow. */
	if( BOARD_MB85RS_DISKIO_END_ADDRESS < addr ) addr = BOARD_MB85RS_DISKIO_END_ADDRESS;
	return addr;
}

// SPI base functions -----------------------------------------------
/*
 * @brief SPI data transmission.
 *
 * @param data the byte to send.
 *
 * */
static void board_mb85rs_diskio_spi_tx_byte( uint8_t data )
{
	uint8_t dummy = 0;
	HAL_StatusTypeDef ret_hal = HAL_OK;

	while(HAL_SPI_GetState(&board_mb85rs_diskio.hspi) !=  HAL_SPI_STATE_READY)
	{
		// TODO remove this and put a timeout with the HAL. Implement an error return code to identify the timeout
		asm("NOP");
	}

	ret_hal = HAL_SPI_TransmitReceive(&board_mb85rs_diskio.hspi, &data, &dummy, 1, BOARD_MB85RS_DISKIO_SPI_TIMEOUT_MS);
	if( HAL_OK != ret_hal )
	{
		asm("NOP"); // For debugging
	}
}

/*
 * @brief SPI data transmission/reception return type function.
 *
 * @return the byte received.
 *
 * */
static uint8_t board_mb85rs_diskio_spi_rx_byte( void )
{
	HAL_StatusTypeDef ret_hal = HAL_OK;
	uint8_t dummy, data;
	dummy = 0xFF ;
	data = 0 ;

	while(HAL_SPI_GetState(&board_mb85rs_diskio.hspi) != HAL_SPI_STATE_READY)
	{
		// TODO remove this and put a timeout with the HAL. Implement an error return code to identify the timeout
		asm("NOP");
	}

	ret_hal = HAL_SPI_TransmitReceive(&board_mb85rs_diskio.hspi, &dummy, &data, 1, BOARD_MB85RS_DISKIO_SPI_TIMEOUT_MS);
	if( HAL_OK != ret_hal )
	{
		asm("NOP"); // For debugging
	}

	return data;
}

/*
 * @brief init SPI peripheral.
 *
 * @return RET_OK if success.
 *
 * */
ret_code_t board_mb85rs_diskio_init_spi(void)
{
	ret_code_t ret = RET_OK;

	/* SPI clock enable. */
	BOARD_MB85RS_DISKIO_SPI_CLK_ENABLE();

	/* Configure and initialize SPI. */
	board_mb85rs_diskio.hspi.Instance = BOARD_MB85RS_DISKIO_SPI_INSTANCE;
	board_mb85rs_diskio.hspi.Init.Mode = SPI_MODE_MASTER;
	board_mb85rs_diskio.hspi.Init.Direction = SPI_DIRECTION_2LINES;
	board_mb85rs_diskio.hspi.Init.DataSize = SPI_DATASIZE_8BIT;
	board_mb85rs_diskio.hspi.Init.CLKPolarity = SPI_POLARITY_LOW;
	board_mb85rs_diskio.hspi.Init.CLKPhase = SPI_PHASE_1EDGE;
	board_mb85rs_diskio.hspi.Init.NSS = SPI_NSS_SOFT;
	board_mb85rs_diskio.hspi.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;//SPI_BAUDRATEPRESCALER_128;
	board_mb85rs_diskio.hspi.Init.FirstBit = SPI_FIRSTBIT_MSB;
	board_mb85rs_diskio.hspi.Init.TIMode = SPI_TIMODE_DISABLE;
	board_mb85rs_diskio.hspi.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	board_mb85rs_diskio.hspi.Init.CRCPolynomial = 10;
	if( HAL_OK != HAL_SPI_Init(&board_mb85rs_diskio.hspi) )
	{
		ret = RET_ERR;
	}

	/* Make the initialization of the GPIOs here and not in the HAL
	 * HAL_SPI_MspInit() function. */
	GPIO_InitTypeDef  GPIO_InitStruct = {0};

	/* SPI GPIOs clock enable */
	BOARD_MB85RS_DISKIO_MISO_GPIO_CLK_ENABLE();
	BOARD_MB85RS_DISKIO_MOSI_GPIO_CLK_ENABLE();
	BOARD_MB85RS_DISKIO_CLK_GPIO_CLK_ENABLE();

	/* Config CS GPIO. */
	GPIO_InitStruct.Pin = BOARD_MB85RS_CS_GPIN;
 	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	HAL_GPIO_Init(BOARD_MB85RS_CS_GPORT, &GPIO_InitStruct);

	HAL_GPIO_WritePin(BOARD_MB85RS_CS_GPORT, BOARD_MB85RS_CS_GPIN, GPIO_PIN_SET);

	/* Config MOSI GPIO. */
	GPIO_InitStruct.Pin = BOARD_MB85RS_DISKIO_MOSI_GPIN;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;//GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = BOARD_MB85RS_DISKIO_GPIO_AF;
	HAL_GPIO_Init(BOARD_MB85RS_DISKIO_MOSI_GPORT, &GPIO_InitStruct);

	/* Config MISO GPIO. */
	GPIO_InitStruct.Pin = BOARD_MB85RS_DISKIO_MISO_GPIN;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;//GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = BOARD_MB85RS_DISKIO_GPIO_AF;
	HAL_GPIO_Init(BOARD_MB85RS_DISKIO_MISO_GPORT, &GPIO_InitStruct);

	/* Config CLK GPIO. */
	GPIO_InitStruct.Pin = BOARD_MB85RS_DISKIO_CLK_GPIN;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = BOARD_MB85RS_DISKIO_GPIO_AF;
	HAL_GPIO_Init(BOARD_MB85RS_DISKIO_CLK_GPORT, &GPIO_InitStruct);

	return ret;
}

#endif
