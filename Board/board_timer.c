/*
 * board_timers.c
 *
 *  Created on: Jun 23, 2021
 *      Author: Julian Rodriguez aka Marifante 
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Include --------------------------------------------------------------------
#include "board_cfg_private.h"
#include "board_private.h"

// Macros & constants ---------------------------------------------------------

// Public functions definition  -----------------------------------------------
/**
 *  @brief enable timer clock.
 *  @param apbSrc timer source clock (APB1 or APB2).
 *  @param grp1_periph grp1_periph of the timer.
 */
void Board_Timer_Enable_Clock( APBClockSource_t apbSrc, uint32_t periph )
{
	if( apbSrc == APB1 )
		LL_APB1_GRP1_EnableClock(periph);
	else
		LL_APB2_GRP1_EnableClock(periph);
}

/**
 *  @brief get APB timer input clock source frequency.
 *  @param apbSrc choose between APB1 or APB2.
 */
uint32_t Board_Timer_Get_APB_Freq( APBClockSource_t apbSrc )
{
  /* Get PCLK frequency */
  uint32_t pclk = (apbSrc == APB1 ? HAL_RCC_GetPCLK1Freq() : HAL_RCC_GetPCLK2Freq());

  /* Get PCLK prescaler */
  uint32_t cfgPPRE = (apbSrc == APB1 ? RCC_CFGR_PPRE1 : RCC_CFGR_PPRE2);
  if((RCC->CFGR & cfgPPRE) == 0)
  {	/* PCLK prescaler equal to 1 => TIMCLK = PCLK */
    return (pclk);
  }
  else
  {	/* PCLK prescaler different from 1 => TIMCLK = 2 * PCLK */
    return(2 * pclk);
  }
}
