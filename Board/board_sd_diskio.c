/*
 * board_sd_spi.c
 *
 *	This is micro SD SPI driver intended to be used with elm-chan FATFS
 *	(http://elm-chan.org/fsw/ff/00index_e.html).
 *	This driver handles the SPI communication with a SD card with the SPI
 *	peripheral of the STM32 and two hardware timers.
 *	The TIMER A measures the timeouts of the DISKIO functions. This functions
 *	are public and are used by the FATFS library.
 *	The TIMER B measures the timeouts of the private functions of the driver.
 *	In fact, measures the timeouts of the SPI communicacion.
 *
 *  Created on: Jun 22, 2021
 *      Author: Julian Rodriguez aka Marifante 
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Includes -------------------------------------------------------------------
#include "board_cfg_private.h"
#include "board_private.h"

#ifdef API_FS_USE_SD
// Marcos & Constants ---------------------------------------------------------
/* Priorities of the IRQn of the timers of this module. */
#define BOARD_SD_DISKIO_MAIN_TIM_IRQn_PRIOTY		0
#define BOARD_SD_DISKIO_SECONDARY_TIM_IRQn_PRIOTY	0

/* Frequencies of the ISRs of the timers: */
#define BOARD_SD_DISKIO_MAIN_TIM_ISR_FREQ			10		// Frequency of the ISR, Hz
#define BOARD_SD_DISKIO_MAIN_TIM_ISR_PERIOD			(1000/BOARD_SD_DISKIO_MAIN_TIM_ISR_FREQ)
#define BOARD_SD_DISKIO_SECONDARY_TIM_ISR_FREQ		10		// Frequency of the ISR, Hz
#define BOARD_SD_DISKIO_SECONDARY_TIM_ISR_PERIOD	(1000/BOARD_SD_DISKIO_SECONDARY_TIM_ISR_FREQ)

#define BOARD_SD_DISKIO_WAIT_CMD_RESPONSE_MAX_RETRIES 10 /* !< The quantity of retries that the send_cmd functions do to get a response from the SD. */

/* This timeout is the timeout that the TX/RX functions of the SPI peripheral
 * blocks until a response is received. Must be smaller */
#define BOARD_SD_DISKIO_SPI_TIMEOUT_MS	2000 /* !< SPI transmit and receive timeout in ms. Is not counted with a timer. */

/* Timeouts counted with the timer, in ms: */
#define BOARD_SD_DISKIO_POWER_SEQ_TIMEOUT_MS			BOARD_SD_DISKIO_SPI_TIMEOUT_MS+1000
#define BOARD_SD_DISKIO_SELECT_TIMEOUT_MS				BOARD_SD_DISKIO_SPI_TIMEOUT_MS+500
#define BOARD_SD_DISKIO_TX_DATABLOCK_READY_TIMEOUT_MS	BOARD_SD_DISKIO_SPI_TIMEOUT_MS+500
#define BOARD_SD_DISKIO_RX_DATABLOCK_READY_TIMEOUT_MS	BOARD_SD_DISKIO_SPI_TIMEOUT_MS+500
#define BOARD_SD_DISKIO_INIT_TIMEOUT_MS					BOARD_SD_DISKIO_SPI_TIMEOUT_MS+1000 /* Theorically, the minimum time is 1s, but can be increased if is needed. */

#define BOARD_SD_DISKIO_MAIN_TIMER_TIMEOUT		((board_sd_diskio.main_tim_ms_left > 0) ? false : true)
#define BOARD_SD_DISKIO_SECONDARY_TIMER_TIMEOUT	((board_sd_diskio.secondary_tim_ms_left > 0) ? false : true)
#define BOARD_SD_DISKIO_STATUS_IS_NO_INIT		((0 != (board_sd_diskio.sd.stat & STA_NOINIT)) ? true : false)  /* !< true if the SD is not initialized yet. */
#define BOARD_SD_DISKIO_STATUS_IS_NO_DISK		((0 != (board_sd_diskio.sd.stat & STA_NODISK)) ? true : false)  /* !< true if there is no SD inserted. */
#define BOARD_SD_DISKIO_STATUS_IS_PROTECT		((0 != (board_sd_diskio.sd.stat & STA_PROTECT)) ? true : false) /* !< true if the SD is in WR protect mode. */

/* This driver uses the GPIOB_BSRR register.
 * This is a 32-bit register which allows the application to set and reset each
 * individual bit in the GPIOB_ODR.
 * Each bit in GPIOB_ODR correspond two control bits in GPIOB_BSRR:
 * 	- BSRR(i)
 * 	- BSRR(i+SIZE)
 * 	When a 1 is written to GPIOB_BSRR(i) then a 1 is written to GPIOB_ODR(i).
 * 	When a 1 is written to GPIOB_BSRR(i+SIZE) then a 0 is written to GPIOB_ODR(i).
 * 	Managing the state of a gpio with this register allows the state of the
 * 	gpio to be imposed atomically.
 *
 * Macros to set SD SPI Chip Select high or low. Used by FATFS to BOARD_SD_DISKIO_SPI_CS_LOW or BOARD_SD_DISKIO_SPI_CS_HIGH
 * the disk. */
#define BOARD_SD_DISKIO_SPI_CS_HIGH()		BOARD_SD_CS_GPORT->BSRR |= (1<<BOARD_SD_CS_GPIN_NUMBER)		//SD CS H
#define BOARD_SD_DISKIO_SPI_CS_LOW()		BOARD_SD_CS_GPORT->BSRR |= (1<<(16+BOARD_SD_CS_GPIN_NUMBER))	//SD CS L

/* Macros to set the frequency of the SPI communication.
 * NOTE: the macro for set the slow clock to the SPI must be tuned to emit a clock rate between 100kHz and 400kHz. */
#define BOARD_SD_DISKIO_SPI_SET_SLOW_FREQ() LL_SPI_SetBaudRatePrescaler(board_sd_diskio.hspi.Instance, LL_SPI_BAUDRATEPRESCALER_DIV128)
#define BOARD_SD_DISKIO_SPI_SET_FAST_FREQ() LL_SPI_SetBaudRatePrescaler(board_sd_diskio.hspi.Instance, LL_SPI_BAUDRATEPRESCALER_DIV64)

/* Commands sent to the SD Card and responses that the SD Card sent to the MCU.
 * Each command have a determinated response. There can be 3 types of responses: R1, R1b, R3 and R7.
 *
 * CMD							|	RESP	|	Description
 * -----------------------------------------------------------------------------------------------------------------------
 * CMD0 = GO_IDLE_STATE			|	R1		|	Resets the SD Card.
 * CMD1 = SEND_OP_COND			|	R1		|	Send Host Capacity Support (HCS) information and activates SD Card initialization process.
 * CMD8	= SEND_IF_CONF			|	R7		|	Send Host Supply Voltage (VHS) information and asks if the SD Card can operate in supplied voltage.
 * CMD9 = SEND_CSD				|	R1		|	Asks the SD Card to send its Card-Specific Data (CSD).
 * CMD10 = SEND_CID				|	R1		|	Asks the SD Card to send its Card-Identification Data (CID).
 * CMD12 = STOP_TRANSMISSION	|	R1b		|	Forces the card to stop transmission in Multiple Block Read Operation.
 * CMD16 = SET_BLOCKLEN			|	R1		|	Sets a block length (in bytes) for all following block commands (read & write) for Standard Capacity Card (for SDHC and SDXc this is fixed to 512 bytes)
 * CMD17 = READ_SINGLE_BLOCK	|	R1		|	Reads a block of the size selected by SET_BLOCKLEN command.
 * CMD18 = READ_MULTPLE_BLOCK	|	R1		|	Continously transfer data blocks from car to host until interrupted by a STOP_TRANSMISSION command.
 * CMD23 = SET_WR_BLK_ERASE_CNT	|	R1		|	Set the number of write blocks to be pre-erased before writing.
 * CMD24 = WRITE_BLOCK			|	R1		|	Writes a block of the size selected by the SET_BLOCKLEN command.
 * CMD25 = WRITE_MULTIPLE_BLOCK	|	R1		|	Continuously writes blocks of data until 'Stop Tran' token is sent.
 * CMD41 = SP_SEND_OP_COND		|	R1		|	Sends host capacity support information and activates the card's initialization process.
 * CMD55 = APP_CMD				|	R1		|	Defines to the card that the next command is an application specific command rather than a standard command.
 * CMD58 = READ_OCR				|	R3		|	Reads the OCR register of a card. CCS bit is assigned to OCR[30]
 *
 */
#define ACMD_FLAG	(0x80)	 /* Internal flag used to identify when to send an ACMD (CMD55 and then the command). */
#define CMD0		(0)      /* GO_IDLE_STATE */
#define CMD1		(1)      /* SEND_OP_COND */
#define CMD8		(8)      /* SEND_IF_COND */
#define CMD9		(9)      /* SEND_CSD */
#define ACMD9		(ACMD_FLAG | CMD9)
#define CMD10		(10)     /* SEND_CID */
#define CMD12		(12)     /* STOP_TRANSMISSION */
#define CMD13		(13)
#define ACMD13		(ACMD_FLAG | CMD13)
#define CMD16		(16)     /* SET_BLOCKLEN */
#define CMD17		(17)     /* READ_SINGLE_BLOCK */
#define CMD18		(18)     /* READ_MULTIPLE_BLOCK */
#define CMD23		(23)     /* SET_BLOCK_COUNT */
#define ACMD23		(ACMD_FLAG | CMD23)
#define CMD24		(24)     /* WRITE_BLOCK */
#define CMD25		(25)     /* WRITE_MULTIPLE_BLOCK */
#define CMD32		(32)
#define CMD33		(33)
#define CMD38		(38)
#define CMD41		(41)     /* SEND_OP_COND*/
#define ACMD41		(ACMD_FLAG | CMD41)	/* First CMD55 and then a CMD41. */
#define CMD55		(55)     /* APP_CMD */
#define CMD58		(58)     /* READ_OCR */
#define CLEAN_ACMD_FLAG(cmd)	(cmd = cmd & (~ACMD_FLAG)) /* !< Clean the ACMD flag from a command. */

#define BOARD_SD_DISKIO_START_BIT_CMD 0x40 /* !< When each command is sent, this bit is set to indicate the start of a command. */

/* Responses of each command. */
/* R1 Response: have only 1 byte. Each bit is a status flag. */
#define R1_SUCCESS				(0x00)		/* Successful response from SD card. */
#define R1_FLAG_PARAM_ERR		(1 << 6)	/* When this bit is set in R1 response, then there was a parameter error in the previous command. */
#define R1_FLAG_ADDR_ERR		(1 << 5)	/* When this bit is set in R1 response, then there was a misaligned address that did not match the block length was used in the command.. */
#define R1_FLAG_ERASE_SEQ_ERR	(1 << 4)	/* When this bit is set in R1 response, then there was an error in the sequence of erase commands occurred. */
#define R1_FLAG_CMD_CRC_ERR		(1 << 3)	/* When this bit is set in R1 response, the CRC of the last command failed. */
#define R1_FLAG_ILLEGAL_CMD_ERR	(1 << 2)	/* When this bit is set in R1 response, an illegal command was sent to the card. */
#define R1_FLAG_ERASE_RST		(1 << 1)	/* When this bit is set in R1 response, an erase sequence was cleared before executing because an out of erase sequence command was received. */
#define R1_FLAG_IDLE_STATE		(1 << 0)	/* When this bit is set in R1 response, the card is in idle state and running the initializating process. */

#define BOARD_SD_DISKIO_CMD_RESPONSE_HAVE_FLAG(flag, sd_cmd_response)	(((sd_cmd_response & flag) != 0x00)? true : false)
#define BOARD_SD_DISKIO_CMD_RESPONSE_IS_SUCCESS(sd_cmd_response)		((sd_cmd_response == R1_SUCCESS)? true : false)
#define BOARD_SD_DISKIO_R1_IS_IDLE_STATE(sd_cmd_response)				((sd_cmd_response == R1_FLAG_IDLE_STATE)? true : false)

// Private variables declaration ----------------------------------------------
typedef uint32_t LBA_t; // TODO check this typedef!

typedef enum
{
	BOARD_SD_DISKIO_CARD_TYPE_NONE		= 0,
	BOARD_SD_DISKIO_CARD_TYPE_MMC_VER3	= 0x01,	/* !< MMC Ver 3. */
	BOARD_SD_DISKIO_CARD_TYPE_SDC1		= 0x02,	/* !< SD Ver 1. */
	BOARD_SD_DISKIO_CARD_TYPE_MMC		= 0x03,	/* !< MMC. */
	BOARD_SD_DISKIO_CARD_TYPE_SDC2		= 0x04,	/* !< SD Ver 2+. */
	BOARD_SD_DISKIO_CARD_TYPE_SDC		= 0x0C,	/* !< SD. */
	BOARD_SD_DISKIO_CARD_TYPE_BLOCK		= 0x10,	/* !< Block addressing. */
} board_sd_diskio_card_types_t;

/* Macro used to see if the card type is SDC. */
#define BOARD_SD_DISKIO_CARD_TYPE_IS_SDC(type) (((type == BOARD_SD_DISKIO_CARD_TYPE_SDC1) || (type == BOARD_SD_DISKIO_CARD_TYPE_SDC2)) ? true : false)

/* Macro used to see if the card of some particular type. */
#define BOARD_SD_DISKIO_CARD_TYPE_IS(compare_val, actual_type) (((actual_type & compare_val) != 0) ? true : false)

typedef struct
{
	SPI_HandleTypeDef hspi;
	volatile uint32_t main_tim_ms_left;	/* !< Variable used to implement timeouts with the main timer. */
	volatile uint32_t secondary_tim_ms_left;	/* !< Variable used to implement timeouts with the secondary timer. */

	struct
	{
		volatile DSTATUS stat;						/* Disk status Flag. */
		board_sd_diskio_card_types_t card_type;
	}sd;
} board_sd_diskio_t;

// Private variables definition -----------------------------------------------
static board_sd_diskio_t board_sd_diskio;

// Private functions declaration ----------------------------------------------
static void board_sd_diskio_spi_tx_byte( BYTE data );
static BYTE board_sd_diskio_spi_rx_byte( void );
static void board_sd_diskio_rx_byte_ptr( uint8_t *buff );
static BYTE board_sd_diskio_send_cmd( BYTE cmd, DWORD arg, BYTE *cmd_response );
static bool board_sd_diskio_tx_data_block( const BYTE *buff , BYTE token );
static bool board_sd_diskio_rx_data_block( BYTE *buff, UINT btr );
ret_code_t board_sd_diskio_init_spi(void);
ret_code_t board_sd_diskio_init_main_timer(void);
ret_code_t board_sd_diskio_init_secondary_timer(void);
ret_code_t board_sd_diskio_init_timeout(uint32_t timeout_ms, TIM_TypeDef *timer);
ret_code_t board_sd_diskio_deinit_timeout(TIM_TypeDef *timer);
static ret_code_t board_sd_diskio_wait_until_ready(uint32_t timeout_ms);
static void board_sd_diskio_deselect_card(void);
static ret_code_t board_sd_diskio_select_card(void);
static void board_sd_diskio_block_delay( uint32_t timeout_ms, TIM_TypeDef *timer );

/* Functions used to interact with the drive, this are accesses by the upper level
 * file system modules. */
DSTATUS	board_sd_diskio_initialize( BYTE pdrv );
DSTATUS	board_sd_diskio_status( BYTE pdrv );
Diskio_DRESULT_t board_sd_diskio_read( BYTE pdrv, BYTE *buff, DWORD sector, UINT count );
Diskio_DRESULT_t	board_sd_diskio_write( BYTE pdrv, const BYTE *buff, DWORD sector, UINT count );
Diskio_DRESULT_t	board_sd_diskio_ioctl( BYTE pdrv, BYTE cmd, void *buff );
Diskio_DRESULT_t board_sd_diskio_get_sectors_count(BYTE drv, uint32_t *sectors);


// Public variables definition ------------------------------------------------
#ifdef API_FS_USE_FATFS
Diskio_drvTypeDef SD_Disk_Driver =
{
	board_sd_diskio_initialize,
	board_sd_diskio_status,
	board_sd_diskio_read,
#if  _USE_WRITE
	board_sd_diskio_write,
#endif  /* _USE_WRITE == 1 */
#if  _USE_IOCTL == 1
	board_sd_diskio_ioctl,
#endif /* _USE_IOCTL == 1 */
};
#endif

#ifdef API_FS_USE_FF_FAT
API_FF_FAT_Diskio_t SD_Disk_Driver =
{
	board_sd_diskio_initialize,
	board_sd_diskio_status,
	board_sd_diskio_read,
	board_sd_diskio_write,
	board_sd_diskio_ioctl,
};
#endif

// Public functions definition ------------------------------------------------
/*
 * @brief init FATFS SD driver. Assign GPIO to CS and config timers to handle
 * time of spi transactions.
 * */
ret_code_t Board_SD_Diskio_Init( void )
{
	ret_code_t ret = RET_OK;
	memset(&board_sd_diskio, 0, sizeof(board_sd_diskio));

	ret = board_sd_diskio_init_spi();
	if( RET_OK != ret )
	{
		/* TODO Handle error. */
	}

	ret = board_sd_diskio_init_main_timer();
	if( RET_OK != ret )
	{
		/* TODO Handle error. */
	}

	ret = board_sd_diskio_init_secondary_timer();
	if( RET_OK != ret )
	{
		/* TODO Handle error. */
	}

	return ret;
}

/* @brief ISR of the secondary timer. Just decrement the var board_sd_diskio.main_tim_ms_left. */
void Board_SD_Diskio_Main_TIM_IRQHandler(void)
{
	/* Check whether update interrupt is pending */
	if(LL_TIM_IsActiveFlag_UPDATE(BOARD_SD_DISKIO_MAIN_TIM) == 1)
	{	/* Clear the update interrupt flag*/
		LL_TIM_ClearFlag_UPDATE(BOARD_SD_DISKIO_MAIN_TIM);
	}

	if( board_sd_diskio.main_tim_ms_left < BOARD_SD_DISKIO_MAIN_TIM_ISR_PERIOD )
	{	// If time left its less than TIM_A_ISR_PERIOD then set to 0 the variable
		board_sd_diskio.main_tim_ms_left = 0;
	}
	else
	{
		board_sd_diskio.main_tim_ms_left -= BOARD_SD_DISKIO_MAIN_TIM_ISR_PERIOD;
	}
}

/* @brief ISR of the secondary timer. Just decrement the var board_sd_diskio.secondary_tim_ms_left. */
void Board_SD_Diskio_Secondary_TIM_IRQHandler( void )
{
	/* Check whether update interrupt is pending */
	if(LL_TIM_IsActiveFlag_UPDATE(BOARD_SD_DISKIO_SECONDARY_TIM) == 1)
	{	/* Clear the update interrupt flag*/
		LL_TIM_ClearFlag_UPDATE(BOARD_SD_DISKIO_SECONDARY_TIM);
	}

	if( board_sd_diskio.secondary_tim_ms_left < BOARD_SD_DISKIO_SECONDARY_TIM_ISR_PERIOD )
	{	// If time left its less than TIM_A_ISR_PERIOD then set to 0 the variable
		board_sd_diskio.secondary_tim_ms_left = 0;
	}
	else
	{
		board_sd_diskio.secondary_tim_ms_left -= BOARD_SD_DISKIO_SECONDARY_TIM_ISR_PERIOD;
	}
}

// Public functions definitions used by FATFS file ----------------------------
/**
  * @brief  Initializes a Drive with the SD card.
  *
  * @param  pdrv: Physical drive number (0..) to identify the drive
  *
  * @return DSTATUS: Operation status
  *
  */
DSTATUS board_sd_diskio_initialize( BYTE drv )
{
	DSTATUS dstatus = STA_NOINIT;
	ret_code_t retv = RET_OK;
	uint8_t n, ocr[4];
	BYTE sd_cmd_response = 0;
	board_sd_diskio_card_types_t type = BOARD_SD_DISKIO_CARD_TYPE_NONE;

	if( 0 != drv ) return dstatus; 	/* Only one drive is supported by this driver (0). */
	if( true == BOARD_SD_DISKIO_STATUS_IS_NO_DISK ) return board_sd_diskio.sd.stat; 	/* SD card not inserted. */

	/* Wait 1 ms. Most cards take 1 ms to initialize from when
	 * they were powered. */
	BOARD_SD_DISKIO_SPI_CS_HIGH();
	board_sd_diskio_block_delay(BOARD_SD_DISKIO_POWER_SEQ_TIMEOUT_MS, BOARD_SD_DISKIO_MAIN_TIM);

	/* Set the SPI clock in a rate between 100kHz and 400kHz and send at least
	 * 74 dummy clocks to set the card in SPI mode. */
	BOARD_SD_DISKIO_SPI_SET_SLOW_FREQ();
	for( int i = 0; i < 10;  i++ ) board_sd_diskio_spi_rx_byte();

	/* Init timeout and start the init process. First send CMD0 until receives an R1 response
	 * with the idle flag set. */
	board_sd_diskio_init_timeout(BOARD_SD_DISKIO_INIT_TIMEOUT_MS, BOARD_SD_DISKIO_MAIN_TIM);
	do
	{
		retv = board_sd_diskio_send_cmd(CMD0, 0, &sd_cmd_response);
		if( (RET_OK == retv) && (true == BOARD_SD_DISKIO_R1_IS_IDLE_STATE(sd_cmd_response)) ) break;
	}while( false == BOARD_SD_DISKIO_MAIN_TIMER_TIMEOUT );

	if( (RET_OK == retv) && (BOARD_SD_DISKIO_R1_IS_IDLE_STATE(sd_cmd_response)) )
	{
		retv = board_sd_diskio_send_cmd(CMD8, 0x1AA, &sd_cmd_response);
		if( RET_OK == retv )
		{
			if( true == BOARD_SD_DISKIO_R1_IS_IDLE_STATE(sd_cmd_response) )
			{	/* The card is SDC Ver2+. Receive the next 4 bytes of the R7 response. */
				for( n = 0; n < 4; n++ ) ocr[n] = board_sd_diskio_spi_rx_byte();
				/* Check the lower 12 bits of the R7 response. If this 12 bits are 0x1AA then the
				 * card accepts a voltage range between 2.7V and 3.6V. */
				if( (ocr[2] == 0x01) && (ocr[3] == 0xAA) )
				{
					do{	/* Send ACMD41 to initialize the initialization process of the card. Re-send
						 * this command until the card finish. The card will sent the IDLE_STATE flag
						 * in the R1 response until finish this process. */
						retv = board_sd_diskio_send_cmd(ACMD41, (1UL << 30), &sd_cmd_response);
						if( (RET_OK == retv) && (true == BOARD_SD_DISKIO_CMD_RESPONSE_IS_SUCCESS(sd_cmd_response))) break;
					}while( false == BOARD_SD_DISKIO_MAIN_TIMER_TIMEOUT );

					if( false == BOARD_SD_DISKIO_MAIN_TIMER_TIMEOUT )
					{
						retv = board_sd_diskio_send_cmd(CMD58, 0, &sd_cmd_response);	/* Read OCR register with CMD58. */
						if( (RET_OK == retv) && (true == BOARD_SD_DISKIO_CMD_RESPONSE_IS_SUCCESS(sd_cmd_response)) )
						{
							/* Receive the next four bytes. In the next four bytes will be
							 * stored the value of the OCR. Check the bit 30 (CCS bit)
							 * to verify if the card is a high-capacity card known as SDHC/SDXC.
							 * The SDHC/SDXC are SDC2 cards but that have block addressing capabilities. */
							for(n = 0; n < 4; n++) ocr[n] = board_sd_diskio_spi_rx_byte();
							type = (ocr[0] & 0x40) ? BOARD_SD_DISKIO_CARD_TYPE_SDC2|BOARD_SD_DISKIO_CARD_TYPE_BLOCK : BOARD_SD_DISKIO_CARD_TYPE_SDC2;
						}
					}
				}
			}
			else
			if( RET_OK == retv )
			{
				uint8_t cmd = 0;
				/* If the CMD8 command is rejected then the card isn't a SD card V2.*/
				retv = board_sd_diskio_send_cmd(ACMD41, 0, &sd_cmd_response);
				if( RET_OK == retv )
				{
					if( sd_cmd_response <= (R1_SUCCESS|R1_FLAG_IDLE_STATE) )
					{	/* !< The card is a SD Ver 1. */
						type = BOARD_SD_DISKIO_CARD_TYPE_SDC1;
						cmd = ACMD41;
					}
					else
					{	/* !< The card is a MMC Ver 3. */
						type = BOARD_SD_DISKIO_CARD_TYPE_MMC_VER3;
						cmd = CMD1;
					}

					do{
						retv = board_sd_diskio_send_cmd(cmd, 0, &sd_cmd_response);
						if( (RET_OK == retv) && (true == BOARD_SD_DISKIO_R1_IS_IDLE_STATE(sd_cmd_response))) break;
					}while( false == BOARD_SD_DISKIO_MAIN_TIMER_TIMEOUT );

					/* Force block size to 512 bytes to work with FAT file system if the card doesn't
					 * have native block addressing. */
					do{
						retv = board_sd_diskio_send_cmd(CMD16, 512, &sd_cmd_response);
						if( (RET_OK == retv) && (true == BOARD_SD_DISKIO_R1_IS_IDLE_STATE(sd_cmd_response))) break;
					}while( false == BOARD_SD_DISKIO_MAIN_TIMER_TIMEOUT );
				}
			}
		}
	}

	board_sd_diskio_deinit_timeout(BOARD_SD_DISKIO_MAIN_TIM);
	board_sd_diskio_deselect_card();
	BOARD_SD_DISKIO_SPI_SET_FAST_FREQ(); // return to fast freq

	/* If the card type of the SD was identified clear the STA_NOINIT flag from status. */
	board_sd_diskio.sd.stat = (BOARD_SD_DISKIO_CARD_TYPE_NONE != type) ? (board_sd_diskio.sd.stat & ~STA_NOINIT) : STA_NOINIT;
	board_sd_diskio.sd.card_type = type;
	return board_sd_diskio.sd.stat;
}

/**
  * @brief  Gets Disk board_sd_diskio.sd.status
  * @param  pdrv: Physical drive number (0..) of the drive.
  * @retval DSTATUS: Operation status
  *
  * @details Only one drive is supported. In this case, always gonna be the
  * drive 0.
  */
DSTATUS board_sd_diskio_status( BYTE drv )
{
	if( 0 != drv ) return STA_NOINIT;
	return board_sd_diskio.sd.stat;
}

/**
  * @brief  Reads Sector(s)
  *
  * @param  pdrv: Physical drive number (0..)
  * @param  *buff: Data buffer to store read data
  * @param  sector: Sector address (LBA)
  * @param  count: Number of sectors to read (1..128)
  *
  * @return Diskio_DRESULT_t: Operation result
  */
Diskio_DRESULT_t board_sd_diskio_read( BYTE pdrv, BYTE *buff, DWORD sector, UINT count )
{
	ret_code_t retv = RET_OK;
	BYTE sd_cmd_response = 0;
	if( pdrv || !count ) return RES_PARERR;	/* !< Chekc parameter. */
	if( true == BOARD_SD_DISKIO_STATUS_IS_NO_INIT ) return RES_NOTRDY; /* !< Check if drive is ready. */

	if( false == BOARD_SD_DISKIO_CARD_TYPE_IS(BOARD_SD_DISKIO_CARD_TYPE_BLOCK, board_sd_diskio.sd.card_type) )
	{
		sector *= 512; /* LBA ==> BA conversion (byte addressing cards) */
	}

	if( count == 1 )
	{	/* Read single block */
		retv = board_sd_diskio_send_cmd(CMD17, sector, &sd_cmd_response);
		if( (retv == RET_OK) && (true == BOARD_SD_DISKIO_CMD_RESPONSE_IS_SUCCESS(sd_cmd_response)) )
		{
			if( true == board_sd_diskio_rx_data_block(buff, 512) ) count = 0;
		}
	}
	else
	{	/* Read multiple blocks */
		retv = board_sd_diskio_send_cmd(CMD18, sector, &sd_cmd_response);
		if( (retv == RET_OK) && (true == BOARD_SD_DISKIO_CMD_RESPONSE_IS_SUCCESS(sd_cmd_response)) )
		{
			do{
				if( !board_sd_diskio_rx_data_block( buff, 512 ) ) break;
				buff += 512;
			}while( --count );
			/* STOP_TRANSMISSION, request to stop transmission after reading all blocks */
			retv = board_sd_diskio_send_cmd(CMD12, 0, &sd_cmd_response);
		}
	}

	board_sd_diskio_deselect_card();

	return (count != 0) ? RES_ERROR : RES_OK;
}

/**
  * @brief  Writes Sector(s)
  * @param  pdrv: Physical drive number (0..) of the drive to write
  * @param  *buff: Data to be written
  * @param  sector: Sector address (LBA)
  * @param  count: Number of sectors to write (1..128)
  * @retval Diskio_DRESULT_t: Operation result
  */
Diskio_DRESULT_t board_sd_diskio_write( BYTE pdrv, const BYTE *buff, DWORD sector, UINT count )
{
	ret_code_t retv = RET_OK;
	BYTE sd_cmd_response = 0;

	if( pdrv || !count ) return RES_PARERR;	/* Check parameter */
	if( true == BOARD_SD_DISKIO_STATUS_IS_NO_INIT ) return RES_NOTRDY; /* Check drive status */
	if( true == BOARD_SD_DISKIO_STATUS_IS_PROTECT ) return RES_WRPRT; /* Check write protect */

	if( false == BOARD_SD_DISKIO_CARD_TYPE_IS(BOARD_SD_DISKIO_CARD_TYPE_BLOCK, board_sd_diskio.sd.card_type) )
	{
		sector *= 512; /* LBA ==> BA conversion (byte addressing cards) */
	}

	if( count == 1 )
	{
		/* write a single block */
		retv = board_sd_diskio_send_cmd(CMD24, sector, &sd_cmd_response);
		if( (retv == RET_OK) && (true == BOARD_SD_DISKIO_CMD_RESPONSE_IS_SUCCESS(sd_cmd_response)) )
		{
			if( true == board_sd_diskio_tx_data_block(buff, 0xFE) ) count = 0;
		}
	}
	else
	{	/* Write multiple blocks */
		if( true == BOARD_SD_DISKIO_CARD_TYPE_IS_SDC(board_sd_diskio.sd.card_type) )
		{
			retv = board_sd_diskio_send_cmd(ACMD23, count, &sd_cmd_response); /* Predefine the number of sectors */
			// TODO we must to check the response here?
		}

		if( retv == RET_OK ) // retv could be modified if the card is SDC
		{
			retv = board_sd_diskio_send_cmd( CMD25, sector, &sd_cmd_response );
			if( (retv == RET_OK) && (true == BOARD_SD_DISKIO_CMD_RESPONSE_IS_SUCCESS(sd_cmd_response)) )
			{
				do{
					if( !board_sd_diskio_tx_data_block( buff, 0xFC ) ) break;
					buff += 512;
				} while( --count );

				if( !board_sd_diskio_tx_data_block( 0, 0xFD ) ) count = 1;
			}
		}
	}

	board_sd_diskio_deselect_card();

	return (count != 0) ? RES_ERROR : RES_OK;
}

/**
  * @brief  I/O control operation
  * @param  pdrv: Physical drive number (0..)
  * @param  cmd: Control code
  * @param  *buff: Buffer to send/receive control data
  * @retval Diskio_DRESULT_t: Operation result
  */
Diskio_DRESULT_t board_sd_diskio_ioctl( BYTE drv, BYTE cmd, void *buff )
{
	Diskio_DRESULT_t res = RES_ERROR;
	BYTE n, csd[16];
	BYTE *ptr = (BYTE*)buff;
	WORD csize = 0;
	uint8_t sd_cmd_response = 0;
	ret_code_t retv = RET_OK;

	if(drv) return RES_PARERR; /* Check parameter */
	if(true == BOARD_SD_DISKIO_STATUS_IS_NO_INIT) return RES_NOTRDY; /* Check if drive is ready */

	switch(cmd)
	{
	case GET_SECTOR_COUNT:	/* Get drive capacity in unit of sector (DWORD) */
		retv = board_sd_diskio_send_cmd(CMD9, 0, &sd_cmd_response);
		if( (retv == RET_OK) && (true == BOARD_SD_DISKIO_CMD_RESPONSE_IS_SUCCESS(sd_cmd_response)) )
		{
			if( true == board_sd_diskio_rx_data_block(csd, 16) )
			{
				if((csd[0] >> 6) == 1)
				{	/* SDC CSD ver 2 */
					csize = csd[9] + ((WORD)csd[8] << 8) + ((DWORD)(csd[7] & 63) << 16) + 1;
					*(LBA_t*)buff = csize << 10;
				}
				else
				{					/* SDC CSD ver 1 or MMC */
					n = (csd[5] & 15) + ((csd[10] & 128) >> 7) + ((csd[9] & 3) << 1) + 2;
					csize = (csd[8] >> 6) + ((WORD)csd[7] << 2) + ((WORD)(csd[6] & 3) << 10) + 1;
					*(LBA_t*)buff = csize << (n - 9);
				}
				res = RES_OK;
			}
		}
		break;

	case GET_SECTOR_SIZE:
		*(WORD*) buff = 512;	/* Sector size is 512 for memory cards. */
		res = RES_OK;
		break;

	case CTRL_SYNC:
		if( RET_OK == board_sd_diskio_wait_until_ready(500) ) res = RES_OK;
		break;

	case MMC_GET_CSD:		/* Receive CSD information (16 bytes) */
		retv = board_sd_diskio_send_cmd(CMD9, 0, &sd_cmd_response);
		if( (retv == RET_OK) && (true == BOARD_SD_DISKIO_CMD_RESPONSE_IS_SUCCESS(sd_cmd_response)) )
		{
			if( true == board_sd_diskio_rx_data_block(ptr, 16) ) res = RES_OK;
		}
	break;

	case MMC_GET_CID:		/* Receive CID information (16 bytes) */
		retv = board_sd_diskio_send_cmd(CMD10, 0, &sd_cmd_response);
		if( (retv == RET_OK) && (true == BOARD_SD_DISKIO_CMD_RESPONSE_IS_SUCCESS(sd_cmd_response)) )
		{
			if( true == board_sd_diskio_rx_data_block(ptr, 16) ) res = RES_OK;
		}
	break;

	case MMC_GET_OCR:		/* Receive OCR information (4 bytes) */
		retv = board_sd_diskio_send_cmd(CMD58, 0, &sd_cmd_response);
		if( (retv == RET_OK) && (true == BOARD_SD_DISKIO_CMD_RESPONSE_IS_SUCCESS(sd_cmd_response)) )
		{
			for( n = 0; n < 4; n++ )
			{
				*ptr++ = board_sd_diskio_spi_rx_byte();
			}
			res = RES_OK;
		}
	default:
		res = RES_PARERR;
	}

	board_sd_diskio_deselect_card();

	return res;
}

// Private functions definition -----------------------------------------------
/*
 * @brief init main timer hardware peripheral.
 *
 * */
ret_code_t board_sd_diskio_init_main_timer(void)
{
	ret_code_t ret = RET_OK;

	// Enable the timer peripheral clock on APBx
	LL_APB1_GRP1_EnableClock(BOARD_SD_DISKIO_MAIN_TIM_APB1_GRP1);

	// Init Timer A
	uint32_t apb1Freq = Board_Timer_Get_APB_Freq(BOARD_SD_DISKIO_MAIN_TIM_APB);	// Timers used in this driver must be sourced from APB1
	uint32_t apb1Prescaler = 1; // Not pre-scale the APB1 input clock
	LL_TIM_InitTypeDef TIM_InitStruct = {0};
	TIM_InitStruct.Prescaler = apb1Prescaler;
	TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_DOWN;
	TIM_InitStruct.Autoreload = __LL_TIM_CALC_ARR(apb1Freq, apb1Prescaler, BOARD_SD_DISKIO_MAIN_TIM_ISR_FREQ);
	TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;	// CK_INT = 1
	TIM_InitStruct.RepetitionCounter = (uint8_t)0x00;
	LL_TIM_Init(BOARD_SD_DISKIO_MAIN_TIM, &TIM_InitStruct);
	if(LL_TIM_IsActiveFlag_UPDATE(BOARD_SD_DISKIO_MAIN_TIM) == 1)
	{	/* If update event flag triggers after initialization, clears it. */
		LL_TIM_ClearFlag_UPDATE(BOARD_SD_DISKIO_MAIN_TIM);
	}
	LL_TIM_DisableCounter(BOARD_SD_DISKIO_MAIN_TIM);
	LL_TIM_SetClockSource(BOARD_SD_DISKIO_MAIN_TIM, BOARD_SD_DISKIO_MAIN_TIM_CLK_SOURCE);

	// Enable the update interrupt
	LL_TIM_EnableIT_UPDATE(BOARD_SD_DISKIO_MAIN_TIM);

	// Configure the NVIC to handle TIM_A update interrupt
	NVIC_SetPriority(BOARD_SD_DISKIO_MAIN_TIM_IRQn, BOARD_SD_DISKIO_MAIN_TIM_IRQn_PRIOTY);
	NVIC_EnableIRQ(BOARD_SD_DISKIO_MAIN_TIM_IRQn);

	return ret;
}

/*
 * @brief init secondary timer hardware peripheral.
 *
 * */
ret_code_t board_sd_diskio_init_secondary_timer(void)
{
	ret_code_t ret = RET_OK;

	// Enable the timer peripheral clock on APBx
	LL_APB1_GRP1_EnableClock(BOARD_SD_DISKIO_SECONDARY_TIM_APB1_GRP1);

	// Init Timer A
	uint32_t apb1Freq = Board_Timer_Get_APB_Freq(BOARD_SD_DISKIO_SECONDARY_TIM_APB);	// Timers used in this driver must be sourced from APB1
	uint32_t apb1Prescaler = 1; // Not pre-scale the APB1 input clock
	LL_TIM_InitTypeDef TIM_InitStruct = {0};
	TIM_InitStruct.Prescaler = apb1Prescaler;
	TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_DOWN;
	TIM_InitStruct.Autoreload = __LL_TIM_CALC_ARR(apb1Freq, apb1Prescaler, BOARD_SD_DISKIO_SECONDARY_TIM_ISR_FREQ);
	TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;	// CK_INT = 1
	TIM_InitStruct.RepetitionCounter = (uint8_t)0x00;
	LL_TIM_Init(BOARD_SD_DISKIO_SECONDARY_TIM, &TIM_InitStruct);
	if(LL_TIM_IsActiveFlag_UPDATE(BOARD_SD_DISKIO_SECONDARY_TIM) == 1)
	{	/* If update event flag triggers after initialization, clears it. */
		LL_TIM_ClearFlag_UPDATE(BOARD_SD_DISKIO_SECONDARY_TIM);
	}
	LL_TIM_DisableCounter(BOARD_SD_DISKIO_SECONDARY_TIM);
	LL_TIM_SetClockSource(BOARD_SD_DISKIO_SECONDARY_TIM, BOARD_SD_DISKIO_SECONDARY_TIM_CLK_SOURCE);

	// Enable the update interrupt
	LL_TIM_EnableIT_UPDATE(BOARD_SD_DISKIO_SECONDARY_TIM);

	// Configure the NVIC to handle TIM_A update interrupt
	NVIC_SetPriority(BOARD_SD_DISKIO_SECONDARY_TIM_IRQn, BOARD_SD_DISKIO_SECONDARY_TIM_IRQn_PRIOTY);
	NVIC_EnableIRQ(BOARD_SD_DISKIO_SECONDARY_TIM_IRQn);

	return ret;
}

/*
 *
 * @brief used to init a timeout handled with a hardware timer.
 *
 * @param timeout_ms the timeout in ms to initialize.
 * @param timer the timer peripheral instance of the timer used.
 *
 * @return RET_OK if success
 *
 * */
ret_code_t board_sd_diskio_init_timeout(uint32_t timeout_ms, TIM_TypeDef *timer)
{
	ret_code_t ret = RET_OK;
	if(NULL == timer)
	{
		ret = RET_ERR_NULL_POINTER;
	}
	else
	{
		if( BOARD_SD_DISKIO_MAIN_TIM == timer )
		{
			board_sd_diskio.main_tim_ms_left = timeout_ms;
		}
		else
		{
			board_sd_diskio.secondary_tim_ms_left = timeout_ms;
		}

		LL_TIM_EnableCounter(timer);
	}
	return ret;
}

/*
 *
 * @brief used to de-init a timeout handled with a hardware timer.
 *
 * @param timer the timer peripheral instance of the timer used.
 *
 * @return RET_OK if success
 *
 * */
ret_code_t board_sd_diskio_deinit_timeout(TIM_TypeDef *timer)
{
	ret_code_t ret = RET_OK;
	if(NULL == timer)
	{
		ret = RET_ERR_NULL_POINTER;
	}
	else
	{
		if( BOARD_SD_DISKIO_MAIN_TIM == timer )
		{
			board_sd_diskio.main_tim_ms_left = 0;
		}
		else
		{
			board_sd_diskio.secondary_tim_ms_left = 0;
		}

		LL_TIM_DisableCounter(timer);
	}
	return ret;
}

/*
 * @brief make a little delay with one of the timers of the MCU used in this module
 *
 * @param timer timer instance used
 * @param timeout total time of the delay in ms
 *
 * */
static void board_sd_diskio_block_delay( uint32_t timeout_ms, TIM_TypeDef *timer )
{
	board_sd_diskio_init_timeout(timeout_ms, timer);
	if( BOARD_SD_DISKIO_SECONDARY_TIM == timer )
	{
		while( false == BOARD_SD_DISKIO_SECONDARY_TIMER_TIMEOUT) asm("NOP");
	}
	else
	{
		while( false == BOARD_SD_DISKIO_MAIN_TIMER_TIMEOUT) asm("NOP");
	}
	board_sd_diskio_deinit_timeout(timer);
}

/*
 * @brief SPI data sending/receiving pointer type function
 *
 * TODO add more documentation
 * */
static void board_sd_diskio_rx_byte_ptr( uint8_t *buff )
{
  *buff = board_sd_diskio_spi_rx_byte();
}

/*
 * @brief receive data packet.
 * @param buff buffer where the readed data will be stored.
 * @param btr the number of bytes to be readed from the memory.
 * @return true if success
 * */
static bool board_sd_diskio_rx_data_block( BYTE *buff, UINT btr )
{
	uint8_t token;

	/* 100ms timer */
	board_sd_diskio_init_timeout(BOARD_SD_DISKIO_RX_DATABLOCK_READY_TIMEOUT_MS, BOARD_SD_DISKIO_SECONDARY_TIM);

	/* Wait for response */
	do{
		token = board_sd_diskio_spi_rx_byte();
	} while( (token == 0xFF) && (false == BOARD_SD_DISKIO_SECONDARY_TIMER_TIMEOUT) );

	board_sd_diskio_deinit_timeout(BOARD_SD_DISKIO_SECONDARY_TIM);

	/* Error handling when receiving a token other than 0xFE */
	if( token !=  0xFE ) return false;

	/* Receive data into buffer */
	do{
		board_sd_diskio_rx_byte_ptr( buff++ );
		board_sd_diskio_rx_byte_ptr( buff++ );
	} while( btr -=  2 );

	board_sd_diskio_spi_rx_byte();  /* Ignore CRC */
	board_sd_diskio_spi_rx_byte();

	return true;
}


#if _READONLY ==  0
/*
 * @brief data transfer packet.
 *
 * @param
 * @param
 *
 * @return
 *	TODO add documentation
 * */
static bool board_sd_diskio_tx_data_block( const BYTE *buff , BYTE token )
{
	uint8_t resp, wc;
	uint8_t i = 0;

	/* Waiting for SD card ready */
	if( RET_OK != board_sd_diskio_wait_until_ready(BOARD_SD_DISKIO_TX_DATABLOCK_READY_TIMEOUT_MS) ) return false;

	/* Send token */
	board_sd_diskio_spi_tx_byte( token );

	/* For data tokens */
	if( token != 0xFD )
	{
		wc = 0;

		/* 512 bytes data transfer */
		do {
			board_sd_diskio_spi_tx_byte( *buff++ );
			board_sd_diskio_spi_tx_byte( *buff++ );
		} while( --wc );

		board_sd_diskio_spi_rx_byte();        /* Ignore CRC */
		board_sd_diskio_spi_rx_byte();

		/* Receive date response */
		while ( i <= 64 )
		{
			resp = board_sd_diskio_spi_rx_byte();
			/* Error response processing */
			if( ( resp & 0x1F )  ==  0x05 ) break;
			i ++;
		}

		/* Clear SPI receive buffer */
		while( board_sd_diskio_spi_rx_byte() == 0 )
		{
			asm("NOP");
		}
	}

	if( ( resp & 0x1F ) == 0x05 )
		return true;
	else
		return false;
}
#endif /* _READONLY */

/*
 * @brief Send a CMD to the SD.
 *
 * @param cmd the command to be sent
 * @param arg an argument for the command
 * @param res where the response will be stored. Note, if the command response have more than one byte
 * (i.e. R7 response) then the following bytes (from the second onwards) of the response must be
 * captured with the board_sd_diskio_spi_rx_byte() after using this function.
 *
 * @return RET_OK if success.
 *
 * @details the functions is prepared to handle the ACMD commands. When first
 * send CMD55 and then the cmd defined by the ACMDx "x". The argument
 * of the CMD55 is assumed to be zero.
 *
 * */
static ret_code_t board_sd_diskio_send_cmd( BYTE cmd, DWORD arg, BYTE *res )
{
	ret_code_t retv = RET_OK;
	uint8_t crc = 0;
	BYTE cmds[2] = {cmd, 0}; /* By default, send the raw cmd passed first. */
	DWORD args[2] = {arg, 0};
	BYTE cmds_to_send = 1; /* Always assume there is only one command. */
	if( NULL == res ) return RET_ERR_NULL_POINTER;

	/* If it is an ACMD then send a CMD55 before to send the command. */
	if( 0 != (ACMD_FLAG & cmd) )
	{
		CLEAN_ACMD_FLAG(cmd);
		cmds[0] = CMD55;
		args[0] = 0;
		cmds[1] = cmd;
		args[1] = arg;
		cmds_to_send++; /* In a ACMD cmd 2 cmds are sent. */
	}

	for(int c = 0; c < cmds_to_send; c++)
	{
		/* Select the card and wait until is ready until we are stopping
		 * a multiple block read (CMD12). */
		if( CMD12 != cmds[c] )
		{
			retv = board_sd_diskio_select_card();
			if( RET_OK != retv ) break;
		}

		/* Send command packet. The first byte is the command ID and the next four
		 * are the argument of the command. */
		board_sd_diskio_spi_tx_byte( BOARD_SD_DISKIO_START_BIT_CMD | cmds[c] );	/* Start + Command index */
		board_sd_diskio_spi_tx_byte( (BYTE) (args[c] >> 24));  	/* Argument[31..24] */
		board_sd_diskio_spi_tx_byte( (BYTE) (args[c] >> 16));  	/* Argument[23..16] */
		board_sd_diskio_spi_tx_byte( (BYTE) (args[c] >> 8));  	/* Argument[15..8] */
		board_sd_diskio_spi_tx_byte( (BYTE) args[c] );  		/* Argument[7..0] */

		/* Prepare CRC and send it. When the card enters the SPI mode, the CRC feature is disabled
		 * so, the command transmission routine can be written with the hardcoded CRC value
		 * valid only for CMD0 and CMD8 used in the initialization process. */
		crc = 0x01;							/* Dummy CRC + Stop */
		if( cmds[c] == CMD0 ) crc = 0x95;	/* CRC for CMD0(0) */
		if( cmds[c] == CMD8 ) crc = 0x87;	/* CRC for CMD8(0x1AA) */
		board_sd_diskio_spi_tx_byte( crc ); /* CRC transmission */

		/* In case of CMD12 Stop Reading command, one response byte is discarded */
		if( cmds[c] == CMD12 ) board_sd_diskio_spi_rx_byte();

		/* Receive normal data within 10 times.
		 * We know a response has come when the received byte have
		 * the MSB different equal to 0. As the specification,
		 * the MSB of a R1 response is always zero. */
		uint8_t n = BOARD_SD_DISKIO_WAIT_CMD_RESPONSE_MAX_RETRIES;
		do
		{
			*res = board_sd_diskio_spi_rx_byte();
		}while( (0 != (*res & 0x80)) && --n );

		/* Check command response. If the command sent is a ACMD cmd, check
		 * if there was a response from the card. If not, break. */
		if( (cmds[c] == CMD55) && (*res > (R1_SUCCESS|R1_FLAG_IDLE_STATE)) ) retv = RET_ERR;
		if( retv != RET_OK ) break;
	}

	return retv;
}

/*
 * @brief receive bytes until the card holds high the line (0xFF bytes received).
 * This function uses the secondary timer of the module.
 *
 * @param timeout_ms the timeout to wait until the card is ready.
 *
 * @return RET_OK if the card is ready. RET_ERR_TIMEOUT otherwise.
 *
 * */
static ret_code_t board_sd_diskio_wait_until_ready(uint32_t timeout_ms)
{
	BYTE b_rx = 0x00;
	board_sd_diskio_init_timeout(timeout_ms, BOARD_SD_DISKIO_SECONDARY_TIM);
	do
	{
		b_rx = board_sd_diskio_spi_rx_byte();
	}while((b_rx != 0xFF) && (false == BOARD_SD_DISKIO_SECONDARY_TIMER_TIMEOUT));
	board_sd_diskio_deinit_timeout(BOARD_SD_DISKIO_SECONDARY_TIM);

	return ((b_rx != 0xFF) ? RET_ERR_TIMEOUT : RET_OK);
}

/*
 * @brief de-select the card. Also make a dummy clock cycle
 * to force the Hi-Z on the MISO line (this can be useful
 * if multiple slaves are on the bus).
 *
 * */
static void board_sd_diskio_deselect_card(void)
{
	BOARD_SD_DISKIO_SPI_CS_HIGH();
	board_sd_diskio_spi_rx_byte(); /* Dummy clock, force MISO enabled. */
}

/*
 * @brief select the card and wait until is ready.
 *
 * @details this function asserts the CS pin and then waits until the card
 * is read. The card is ready when it holds the MISO line high (i.e. when
 * the MCU receives a 0xFF.
 *
 * @return RET_OK if the card was selected successfully.
 *
 * */
static ret_code_t board_sd_diskio_select_card(void)
{
	ret_code_t res = RET_OK;
	BOARD_SD_DISKIO_SPI_CS_LOW();
	board_sd_diskio_spi_rx_byte(); /* Dummy clock, force MISO enabled. */
	res = board_sd_diskio_wait_until_ready(BOARD_SD_DISKIO_SELECT_TIMEOUT_MS);
	if( res != RET_OK ) board_sd_diskio_deselect_card();
	return res;
}


// SPI base functions -----------------------------------------------
/*
 * @brief SPI data transmission.
 *
 * @param data the byte to send.
 *
 * */
static void board_sd_diskio_spi_tx_byte( BYTE data )
{
	uint8_t dummy = 0;
	HAL_StatusTypeDef ret_hal = HAL_OK;

	while( HAL_SPI_GetState(&board_sd_diskio.hspi) !=  HAL_SPI_STATE_READY )
	{
		// TODO remove this and put a timeout with the HAL. Implement an error return code to identify the timeout
		asm("NOP");
	}

	ret_hal = HAL_SPI_TransmitReceive(&board_sd_diskio.hspi, &data, &dummy, 1, BOARD_SD_DISKIO_SPI_TIMEOUT_MS);
	if( HAL_OK != ret_hal )
	{
		asm("NOP"); // For debugging
	}
}

/*
 * @brief SPI data transmission/reception return type function.
 *
 * @return the byte received.
 *
 * */
static BYTE board_sd_diskio_spi_rx_byte( void )
{
	HAL_StatusTypeDef ret_hal = HAL_OK;
	uint8_t dummy, data;
	dummy = 0xFF ;
	data = 0 ;

	while( HAL_SPI_GetState( &board_sd_diskio.hspi ) != HAL_SPI_STATE_READY )
	{
		// TODO remove this and put a timeout with the HAL. Implement an error return code to identify the timeout
		asm("NOP");
	}

	ret_hal = HAL_SPI_TransmitReceive(&board_sd_diskio.hspi, &dummy, &data, 1, BOARD_SD_DISKIO_SPI_TIMEOUT_MS);
	if( HAL_OK != ret_hal )
	{
		asm("NOP"); // For debugging
	}

	return data;
}

/*
 * @brief init SPI peripheral.
 *
 * @return RET_OK if success.
 *
 * */
ret_code_t board_sd_diskio_init_spi(void)
{
	ret_code_t ret = RET_OK;

	/* SPI clock enable. */
	BOARD_SD_DISKIO_SPI_CLK_ENABLE();

	/* Configure and initialize SPI. */
	board_sd_diskio.hspi.Instance = BOARD_SD_DISKIO_SPI_INSTANCE;
	board_sd_diskio.hspi.Init.Mode = SPI_MODE_MASTER;
	board_sd_diskio.hspi.Init.Direction = SPI_DIRECTION_2LINES;
	board_sd_diskio.hspi.Init.DataSize = SPI_DATASIZE_8BIT;
	board_sd_diskio.hspi.Init.CLKPolarity = SPI_POLARITY_LOW;
	board_sd_diskio.hspi.Init.CLKPhase = SPI_PHASE_1EDGE;
	board_sd_diskio.hspi.Init.NSS = SPI_NSS_SOFT;
	board_sd_diskio.hspi.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_8;
	board_sd_diskio.hspi.Init.FirstBit = SPI_FIRSTBIT_MSB;
	board_sd_diskio.hspi.Init.TIMode = SPI_TIMODE_DISABLE;
	board_sd_diskio.hspi.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	board_sd_diskio.hspi.Init.CRCPolynomial = 10;
	if( HAL_OK != HAL_SPI_Init(&board_sd_diskio.hspi) )
	{
		ret = RET_ERR;
	}

	/* Make the initialization of the GPIOs here and not in the HAL
	 * HAL_SPI_MspInit() function. */
	GPIO_InitTypeDef  GPIO_InitStruct = {0};

	/* SPI GPIOs clock enable */
	BOARD_SD_DISKIO_MISO_GPIO_CLK_ENABLE();
	BOARD_SD_DISKIO_MOSI_GPIO_CLK_ENABLE();
	BOARD_SD_DISKIO_CLK_GPIO_CLK_ENABLE();

	/* Config CS GPIO. */
	GPIO_InitStruct.Pin = BOARD_SD_CS_GPIN;
 	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	HAL_GPIO_Init(BOARD_SD_CS_GPORT, &GPIO_InitStruct);

	HAL_GPIO_WritePin(BOARD_SD_CS_GPORT, BOARD_SD_CS_GPIN, GPIO_PIN_RESET);

	/* Config MOSI GPIO. */
	GPIO_InitStruct.Pin = BOARD_SD_DISKIO_MOSI_GPIN;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = BOARD_SD_DISKIO_GPIO_AF;
	HAL_GPIO_Init(BOARD_SD_DISKIO_MOSI_GPORT, &GPIO_InitStruct);

	/* Config MISO GPIO. */
	GPIO_InitStruct.Pin = BOARD_SD_DISKIO_MISO_GPIN;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = BOARD_SD_DISKIO_GPIO_AF;
	HAL_GPIO_Init(BOARD_SD_DISKIO_MISO_GPORT, &GPIO_InitStruct);

	/* Config CLK GPIO. */
	GPIO_InitStruct.Pin = BOARD_SD_DISKIO_CLK_GPIN;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = BOARD_SD_DISKIO_GPIO_AF;
	HAL_GPIO_Init(BOARD_SD_DISKIO_CLK_GPORT, &GPIO_InitStruct);

	return ret;
}

#endif
