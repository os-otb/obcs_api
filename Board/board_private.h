/*
 * boot_board_private.h
 *
 *  Created on: Jun 22, 2021
 *      Author: Julian Rodriguez aka Marifante 
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef BOARD_PRIVATE_H
#define BOARD_PRIVATE_H

// Include --------------------------------------------------------------------
#include "board.h"
#include "board_cfg_types.h"

#include "api_config.h"
#include "board_config.h"

/* MCU include specific.*/
#include "stm32f407xx.h"

/* STM32Cube HAL includes. */
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal_rng.h"
#include "stm32f4xx_hal_flash.h"
#include "stm32f4xx_hal_rcc.h"

/* STM32Cube Low Level Drivers includes. */
#include "stm32f4xx_ll_gpio.h"
#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_tim.h"
#include "stm32f4xx_ll_usart.h"
#include "stm32f4xx_ll_spi.h"

#ifdef API_FS_USE_FATFS
#include "ff_gen_drv.h"
#endif

#include "api_iface_board.h"


// Macros & constants ---------------------------------------------------------

// Public variables declaration -----------------------------------------------
/**
 * @brief Data type used to distinguish between APB1 and APB2 clock
 * source of the timers.
 */
typedef enum
{
	APB1 = 1,
	APB2 = 2,
} APBClockSource_t;

// Public functions declaration -----------------------------------------------
void Board_Timer_Enable_Clock( APBClockSource_t apbSrc, uint32_t periph );
uint32_t Board_Timer_Get_APB_Freq( APBClockSource_t apbSrc );

#endif /* BOARD_PRIVATE_H_ */
