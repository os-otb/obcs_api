/*
 * board_usart.c
 *
 *  Created on: Apr 17, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Include --------------------------------------------------------------------
#include "board_cfg_private.h"
#include "board_private.h"

// Macros and constants -------------------------------------------------------

// Private functions declaration ----------------------------------------------

// Public functions definition  -----------------------------------------------

/*
 *
 * @brief init Ethernet driver on the board. If FreeRTOS+TCP IP Stack is used
 * this function is called by the stack. The user applicaion don't must to
 * call this function.
 *
 */
ret_code_t Board_Eth_Init( ETH_HandleTypeDef* xETHHandle )
{
	ret_code_t ret = RET_OK;
	GPIO_InitTypeDef GPIO_InitStruct;

	if( xETHHandle->Instance == ETH )
	{
		/* Peripheral clock enable. */
		__ETH_CLK_ENABLE();			/* defined as __HAL_RCC_ETH_CLK_ENABLE. */
		__ETHMACRX_CLK_ENABLE();	/* defined as __HAL_RCC_ETHMACRX_CLK_ENABLE. */
		__ETHMACTX_CLK_ENABLE();	/* defined as __HAL_RCC_ETHMACTX_CLK_ENABLE. */

		/* Just in case that the GPIOs clocks are not enabled before. */
	    __HAL_RCC_GPIOC_CLK_ENABLE();
	    __HAL_RCC_GPIOA_CLK_ENABLE();
	    __HAL_RCC_GPIOB_CLK_ENABLE();

		if(	(NULL != BOARD_ETH_RMII_CLK_GPORT)		&&
			(NULL != BOARD_ETH_RMII_MDIO_GPORT)	&&
			(NULL != BOARD_ETH_RMII_MDC_GPORT)		&&
			(NULL != BOARD_ETH_RMII_CRSDV_GPORT)	&&
			(NULL != BOARD_ETH_RMII_RX0_GPORT) 	&&
			(NULL != BOARD_ETH_RMII_RX1_GPORT) 	&&
			(NULL != BOARD_ETH_RMII_TX0_GPORT) 	&&
			(NULL != BOARD_ETH_RMII_TX1_GPORT) 	&&
			(NULL != BOARD_ETH_RMII_TXEN_GPORT) )
		{
#if 0
		    GPIO_InitStruct.Pin = BOARD_ETH_RMII_MDC_GPIN|BOARD_ETH_RMII_RX1_GPIN|BOARD_ETH_RMII_RX0_GPIN;
		    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		    GPIO_InitStruct.Pull = GPIO_NOPULL;
		    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		    GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
		    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

		    GPIO_InitStruct.Pin = BOARD_ETH_RMII_CLK_GPIN|BOARD_ETH_RMII_MDIO_GPIN|BOARD_ETH_RMII_CRSDV_GPIN;
		    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		    GPIO_InitStruct.Pull = GPIO_NOPULL;
		    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		    GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
		    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

		    GPIO_InitStruct.Pin = BOARD_ETH_RMII_RX1_GPIN|BOARD_ETH_RMII_TX0_GPIN|BOARD_ETH_RMII_TX1_GPIN|BOARD_ETH_RMII_TXEN_GPIN;
		    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		    GPIO_InitStruct.Pull = GPIO_NOPULL;
		    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		    GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
		    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
#else
			/* Init RMII CLK GPIO. */
			GPIO_InitStruct.Pin = BOARD_ETH_RMII_CLK_GPIN;
			GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
			GPIO_InitStruct.Pull = GPIO_NOPULL;
			GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
			GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
			HAL_GPIO_Init(BOARD_ETH_RMII_CLK_GPORT, &GPIO_InitStruct);

			/* Init RMII MDIO GPIO. */
			GPIO_InitStruct.Pin = BOARD_ETH_RMII_MDIO_GPIN;
			GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
			GPIO_InitStruct.Pull = GPIO_NOPULL;
			GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
			GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
			HAL_GPIO_Init( BOARD_ETH_RMII_MDIO_GPORT, &GPIO_InitStruct );

			/* Init RMII MDC GPIO. */
			GPIO_InitStruct.Pin = BOARD_ETH_RMII_MDC_GPIN;
			GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
			GPIO_InitStruct.Pull = GPIO_NOPULL;
			GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
			GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
			HAL_GPIO_Init( BOARD_ETH_RMII_MDC_GPORT, &GPIO_InitStruct );

			/* Init RMII CRSDV GPIO. */
			GPIO_InitStruct.Pin = BOARD_ETH_RMII_CRSDV_GPIN;
			GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
			GPIO_InitStruct.Pull = GPIO_NOPULL;
			GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
			GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
			HAL_GPIO_Init( BOARD_ETH_RMII_CRSDV_GPORT, &GPIO_InitStruct );

			/* Init RMII RX GPIOs. */
			GPIO_InitStruct.Pin = BOARD_ETH_RMII_RX0_GPIN;
			GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
			GPIO_InitStruct.Pull = GPIO_NOPULL;
			GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
			GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
			HAL_GPIO_Init( BOARD_ETH_RMII_RX0_GPORT, &GPIO_InitStruct );

			GPIO_InitStruct.Pin = BOARD_ETH_RMII_RX1_GPIN;
			GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
			GPIO_InitStruct.Pull = GPIO_NOPULL;
			GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
			GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
			HAL_GPIO_Init( BOARD_ETH_RMII_RX1_GPORT, &GPIO_InitStruct );

			/* Init RMII TX GPIOs. */
			GPIO_InitStruct.Pin = BOARD_ETH_RMII_TX0_GPIN;
			GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
			GPIO_InitStruct.Pull = GPIO_NOPULL;
			GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
			GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
			HAL_GPIO_Init( BOARD_ETH_RMII_TX0_GPORT, &GPIO_InitStruct );

			GPIO_InitStruct.Pin = BOARD_ETH_RMII_TX1_GPIN;
			GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
			GPIO_InitStruct.Pull = GPIO_NOPULL;
			GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
			GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
			HAL_GPIO_Init( BOARD_ETH_RMII_TX1_GPORT, &GPIO_InitStruct );

			/* Init RMII TXEN GPIO. */
			GPIO_InitStruct.Pin = BOARD_ETH_RMII_TXEN_GPIN;
			GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
			GPIO_InitStruct.Pull = GPIO_NOPULL;
			GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
			GPIO_InitStruct.Alternate = GPIO_AF11_ETH;
			HAL_GPIO_Init( BOARD_ETH_RMII_TXEN_GPORT, &GPIO_InitStruct );
#endif

			if( NULL != BOARD_ETH_RMII_RXERR_GPORT )
			{
				/* Init RMII RXERR GPIO. */
				/* Note about RMII RXERR on STM32:
				 * There's no RMII_RX_ERR in the ETH/MAC implementation in STM32.
				 * Rx error results in corrupted bits and it's likely (although not guaranteed)
				 * that the received frame will be discarded on invalid CRC.
				 * Other option is to connect the signal to any pin set as external interrupt
				 * (this is probably the intention of the connection on said Discovery board).
				 * Discussion on st forum:
				 * https://community.st.com/s/question/0D50X00009XkXqd/ethernet-rmii-rx-error
				 * */
				// TODO implement this for ksz8041rnl dev board
			}

			if( NULL != BOARD_ETH_RST_GPORT )
			{
				/* Init PHY reset GPIO. */
				// TODO implement this for ksz8041rnl dev board
			}

			if( NULL != BOARD_ETH_INT_GPORT )
			{
				/* Init PHY interrupt GPIO. */
				// TODO implement this for ksz8041rnl dev board
			}

			HAL_NVIC_SetPriority( ETH_IRQn, configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY, 0 );
			HAL_NVIC_EnableIRQ( ETH_IRQn );
		}
		else
		{
			ret = RET_ERR;
		}
	}
	else
	{
		ret = RET_ERR;
	}

	return ret;
}

///*
// * @brief read Basic Mode Control Register of the PHY.
// *
// * */
//ret_code_t Eth_Phy_Read_BMCR(uint32_t *p_reg_val)
//{
//	ret_code_t ret = RET_OK;
//	if( NULL == p_reg_val )
//	{
//		ret = RET_ERR_NULL_POINTER;
//	}
//	else
//	{
//		xSTM32_PhyRead(ETH_PHY_ADDRESS, phyREG_00_BMCR, &reg_val);
//	}
//	return ret;
//}
//
///*
// * @brief read Basic Mode Status Register of the PHY.
// *
// * */
//ret_code_t Eth_Phy_Read_BMSR(uint32_t *p_reg_val)
//{
//	ret_code_t ret = RET_OK;
//	if( NULL == p_reg_val )
//	{
//		ret = RET_ERR_NULL_POINTER;
//	}
//	else
//	{
//		xSTM32_PhyRead(ETH_PHY_ADDRESS, phyREG_01_BMSR, &reg_val);
//	}
//	return ret;
//}
