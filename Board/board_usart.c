/*
 * board_usart.c
 *
 *  Created on: Apr 17, 2021
 *      Author: Julian Rodriguez aka Marifante 
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef BOARD_BOARD_USART_C_
#define BOARD_BOARD_USART_C_

// Include --------------------------------------------------------------------
#include "board_cfg_private.h"
#include "board_private.h"

// Macros & constants ---------------------------------------------------------

// Private variables declaration  ---------------------------------------------
typedef struct
{
	uint8_t *tail;	/*!< The tail of the circular buffer that recollect the bytes. */
	uint8_t *head;	/*!< The head of the circular buffer that recollect the bytes. */
	uint8_t buf[BOARD_USART_RX_CIRC_BUFFER_SIZE];	/*<! RX circular buffer. */
} circ_buffer_t;

typedef struct
{
	uint32_t id;
	USART_TypeDef *pusart;
	struct
	{
		uint32_t pkt_len;	/*<! Size (in bytes) of the packet being transmitted. */
		uint8_t buf[BOARD_USART_MAX_PACKET_SIZE];	/*<! TX buffer. */
		uint32_t byte_to_send;	/*<! Byte of the buffer that must be sent. */
		bool busy;				/*<! True if the port is sending a packet currently. */
		SemaphoreHandle_t mutex;	/*!< A mutex to synchronize the access to send things through this usart. */
	}tx;
	struct
	{
		circ_buffer_t circ_buff;
		SemaphoreHandle_t mutex;	/*!< A mutex to synchronize the access to the rx buffer of the USART. */
	}rx;
}board_usart_port_t;

typedef struct
{
	board_usart_port_t port[API_USART_MAX_PORTS];
}board_usart_t;

// Private variables definition  ----------------------------------------------
extern const Board_USART_Handler_T board_usart_handler_table[BOARD_USART_SIZEOF_TABLE];
board_usart_t board_usart;

// Private functions declaration  ---------------------------------------------
void board_USARTx_send_byte(USART_TypeDef *USARTx);
ret_code_t board_USARTx_send_string(uint8_t *str, size_t str_size);
void board_USARTx_tx_empty_callback(USART_TypeDef *USARTx);
void board_USARTx_char_transmit_complete_callback(USART_TypeDef *USARTx);
void board_USARTx_error_callback(USART_TypeDef *USARTx);
void board_USARTx_char_reception_callback(USART_TypeDef *USARTx);

// Circular buffer functions
static ret_code_t board_usart_circ_buffer_push( circ_buffer_t *circ_buff, uint8_t data );
static ret_code_t board_usart_circ_buffer_pop( circ_buffer_t *circ_buff, char *data );

// Public functions definition  -----------------------------------------------
/**
  * @brief  initialize the USART assigned to and usart_port.
  * @return  RET_OK if success.
  */
ret_code_t Board_USART_Init(void)
{
	LL_USART_InitTypeDef USART_init_struct = {0};
	LL_GPIO_InitTypeDef GPIO_init_struct = {0};

	memset(&board_usart, 0, sizeof(board_usart)); // Clear ctrl struct
	for(int c = 0; c<BOARD_USART_SIZEOF_TABLE; c++)
	{
		if( (NULL ==  board_usart_handler_table[c].pusart) ||
			(NULL ==  board_usart_handler_table[c].tx_gpio.port) ||
			(NULL ==  board_usart_handler_table[c].rx_gpio.port))
		{
			/* TODO See how to handle this error. But is an error on configuration
			 * in board_cfg.h*/
			break;
		}

		memset(&GPIO_init_struct, 0, sizeof(GPIO_init_struct));
		memset(&USART_init_struct, 0, sizeof(USART_init_struct));
		board_usart.port[c].id = board_usart_handler_table[c].port;
		board_usart.port[c].pusart = board_usart_handler_table[c].pusart;

		/* Enable usart peripheral clock. */
		if( (USART1 == board_usart_handler_table[c].pusart) ||
			(USART6 == board_usart_handler_table[c].pusart) /*||*/
//			(UART9 == board_usart_handler_table[c].pusart)  ||
//			(UART10 == board_usart_handler_table[c].pusart)
			)
		{
			LL_APB2_GRP1_EnableClock(board_usart_handler_table[c].usart_clk);
		}
		else
		if( (USART2 == board_usart_handler_table[c].pusart) ||
			(USART3 == board_usart_handler_table[c].pusart) ||
			(UART4 == board_usart_handler_table[c].pusart)  ||
			(UART5 == board_usart_handler_table[c].pusart)  /*||*/
//			(UART7 == board_usart_handler_table[c].pusart)  ||
//			(UART8 == board_usart_handler_table[c].pusart)
			)
		{
			LL_APB1_GRP1_EnableClock(board_usart_handler_table[c].usart_clk);
		}

		/* Enable gpio pins peripheral clock. */
		LL_AHB1_GRP1_EnableClock(board_usart_handler_table[c].tx_gpio.clk);
		LL_AHB1_GRP1_EnableClock(board_usart_handler_table[c].rx_gpio.clk);

		/* Configure tx pin. */
		GPIO_init_struct.Pin = board_usart_handler_table[c].tx_gpio.pin;
		GPIO_init_struct.Mode = LL_GPIO_MODE_ALTERNATE;
		GPIO_init_struct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_init_struct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
		GPIO_init_struct.Pull = LL_GPIO_PULL_NO;
		GPIO_init_struct.Alternate = board_usart_handler_table[c].tx_gpio.alternate_func;
		LL_GPIO_Init(board_usart_handler_table[c].tx_gpio.port, &GPIO_init_struct);

		/* Configure rx pin. */
		GPIO_init_struct.Pin = board_usart_handler_table[c].rx_gpio.pin;
		GPIO_init_struct.Mode = LL_GPIO_MODE_ALTERNATE;
		GPIO_init_struct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_init_struct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
		GPIO_init_struct.Pull = LL_GPIO_PULL_NO;
		GPIO_init_struct.Alternate = board_usart_handler_table[c].rx_gpio.alternate_func;
		LL_GPIO_Init(board_usart_handler_table[c].rx_gpio.port, &GPIO_init_struct);

		/* Interrupt Init */
		NVIC_SetPriority(board_usart_handler_table[c].irqn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),5, 0));
		NVIC_EnableIRQ(board_usart_handler_table[c].irqn);

		/* Configure USART peripheral. */
		USART_init_struct.BaudRate = board_usart_handler_table[c].baudrate;
		USART_init_struct.DataWidth = LL_USART_DATAWIDTH_8B;
		USART_init_struct.StopBits = LL_USART_STOPBITS_1;
		USART_init_struct.Parity = LL_USART_PARITY_NONE;
		USART_init_struct.TransferDirection = LL_USART_DIRECTION_TX_RX;
		USART_init_struct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
		USART_init_struct.OverSampling = LL_USART_OVERSAMPLING_16;
		LL_USART_Init(board_usart_handler_table[c].pusart, &USART_init_struct);
		LL_USART_ConfigAsyncMode(board_usart_handler_table[c].pusart);
		LL_USART_Enable(board_usart_handler_table[c].pusart);

		// Initialize the circular buffer pointers of the reception
		board_usart.port[c].rx.circ_buff.tail = board_usart.port[c].rx.circ_buff.buf;
		board_usart.port[c].rx.circ_buff.head = board_usart.port[c].rx.circ_buff.buf;
		LL_USART_EnableIT_RXNE(board_usart.port[c].pusart);
		MUTEX_INIT(board_usart.port[c].rx.mutex);
		MUTEX_INIT(board_usart.port[c].tx.mutex);
	}

	return RET_OK;
}

/**
  * @brief  checks if the USART TX assigned to a port_id number is busy or not.
  * @return  RET_ERR_BUSY if the USART is busy or RET_OK if it is not busy.
  */
ret_code_t Board_USART_TX_Is_Busy(uint32_t port_id)
{
	ret_code_t ret = RET_OK;

	if(API_USART_MAX_PORTS <= port_id)
	{
		ret = RET_ERR_INVALID_PARAMS;
	}
	else
	{
		for(int c = 0; c< API_USART_MAX_PORTS; c++)
		{
			if( port_id == board_usart.port[c].id )
			{
				if(true == board_usart.port[port_id].tx.busy)
				{
					ret = RET_ERR_BUSY;
				}
				else
				{
					ret = RET_OK;
				}
			}
		}
	}
	return ret;
}

/**
  * @brief  Start the transmission of a packet through a USART port
  * @param  port_id, the usart port used to send the character
  * @param	str, the string to send,
  * @param	str_size, the size of the packet to send
  */
ret_code_t Board_USART_Send_Packet(uint32_t port_id, char *packet, size_t packet_size)
{
	ret_code_t ret = RET_OK;
	if(NULL == packet)
	{
		ret = RET_ERR_NULL_POINTER;
		return ret;
	}

	if((API_USART_MAX_PORTS <= port_id) || (BOARD_USART_MAX_PACKET_SIZE < packet_size))
	{
		ret = RET_ERR_INVALID_PARAMS;
		return ret;
	}

	// Search what port was queries to send a packet
	uint32_t c = 0;
	for(c = 0; c< API_USART_MAX_PORTS; c++)
	{
		if( port_id == board_usart.port[c].id )
		{
			if( NULL == board_usart.port[c].pusart )
			{
				// TODO handle this null pointer
				ret = RET_ERR_NULL_POINTER;
				break;
			}

			if( taskSCHEDULER_RUNNING == xTaskGetSchedulerState() )
			{
				MUTEX_UNLOCK(board_usart.port[c].tx.mutex);
			}

			// This port was queried, init transmission
			board_usart.port[c].tx.pkt_len = packet_size;
			memcpy(board_usart.port[c].tx.buf, packet, packet_size);

		    /* Start USART transmission : Will initiate TXE interrupt after DR register is empty */
		    board_usart.port[c].tx.byte_to_send = 0;
		    LL_USART_TransmitData8(board_usart.port[c].pusart, board_usart.port[c].tx.buf[board_usart.port[c].tx.byte_to_send]);
		    board_usart.port[c].tx.byte_to_send++;
		    /* Enable TXE interrupt */
		    LL_USART_EnableIT_TXE(board_usart.port[c].pusart);

		    /* Set busy flag and wait until transmission is done. */
		    board_usart.port[c].tx.busy = true;
		    while( true == board_usart.port[c].tx.busy ) {};

		    if( taskSCHEDULER_RUNNING == xTaskGetSchedulerState() )
			{
				MUTEX_LOCK(board_usart.port[c].tx.mutex);
			}

		    break;
		}
	}

	if( API_USART_MAX_PORTS <= c )
	{
		ret = RET_ERR; // USART port wasnt initializated!
	}
    return ret;
}

/**
 * @brief get a char from the uart reception buffer.
 * @param  port_id, the usart port where we want to get a char
 * @param  data, a var where the byte readed will be stored
 * @return RET_OK if succes. If the reception buffer is empty then
 * returns RET_EMPTY.
 */
ret_code_t Board_USART_Get_Char(uint32_t port_id, char *data, bool lock)
{
	if( (NULL == data) || (API_USART_MAX_PORTS <= port_id) ) return RET_ERR_NULL_POINTER;
	ret_code_t ret = RET_OK;

	// Search what port was queries to send a packet
	uint32_t c = 0;
	for(c = 0; c< API_USART_MAX_PORTS; c++)
	{
		if( port_id == board_usart.port[c].id )
		{
			if( NULL == board_usart.port[c].pusart )
			{	// TODO handle this null pointer
				ret = RET_ERR;
			}
			else
			{
				if( lock == true ) MUTEX_UNLOCK(board_usart.port[c].rx.mutex);
				ret = board_usart_circ_buffer_pop(&board_usart.port[c].rx.circ_buff, data);
				if( lock == true ) MUTEX_LOCK(board_usart.port[c].rx.mutex);
			}
		    break;
		}
	}

	if( API_USART_MAX_PORTS <= c ) ret = RET_ERR_INVALID_PARAMS; // USART port wasnt configured!
	return ret;
}

/**
  * @brief  Interrupt handler used on the USART interrupt.
  * @param  USARTx, the uart that will be used in this interrupt handler.
  */
void Board_USARTx_IRQnHandler(USART_TypeDef *USARTx)
{
	if(LL_USART_IsEnabledIT_TXE(USARTx) && LL_USART_IsActiveFlag_TXE(USARTx))
	{
		/* TXE flag will be automatically cleared when writing new data in DR register
		 * Call function in charge of handling empty DR => will lead to transmission of next character */
		board_USARTx_tx_empty_callback(USARTx);
	}

	if(LL_USART_IsEnabledIT_TC(USARTx) && LL_USART_IsActiveFlag_TC(USARTx))
	{
		/* Clear TC flag */
		LL_USART_ClearFlag_TC(USARTx);

		/* Call function in charge of handling end of transmission of sent character
		   and prepare next character transmission */
		board_USARTx_char_transmit_complete_callback(USARTx);
	}

	if(LL_USART_IsEnabledIT_RXNE(USARTx) && LL_USART_IsActiveFlag_RXNE(USARTx))
	{
		/* RXNE flag will be cleared by reading of DR register (done in call) */
		/* Call function in charge of handling Character reception */
		board_USARTx_char_reception_callback(USARTx);
	}

	if(LL_USART_IsEnabledIT_ERROR(USARTx) && LL_USART_IsActiveFlag_NE(USARTx))
	{
		/* Call Error function */
		board_USARTx_error_callback(USARTx);
	}
}

// Private functions definition  ----------------------------------------------
/**
  * @brief  Function called for achieving next TX Byte sending
  * @param  USARTx the usart interface that is involved
  */
void board_USARTx_tx_empty_callback(USART_TypeDef *USARTx)
{
	for(int c = 0; c<BOARD_USART_SIZEOF_TABLE; c++)
	{
		if( USARTx == board_usart.port[c].pusart )
		{
			/* If only left one byte to send, disable the TXE
			 * interrupt and enable TC interrupt. */
			if( board_usart.port[c].tx.pkt_len == (board_usart.port[c].tx.byte_to_send - 1) )
			{
			    LL_USART_DisableIT_TXE(USARTx);							/* Disable TXE interrupt */
			    LL_USART_EnableIT_TC(USARTx);							/* Enable TC interrupt */
			}
			/* Fill DR with a new char */
			LL_USART_TransmitData8(USARTx, board_usart.port[c].tx.buf[ board_usart.port[c].tx.byte_to_send ]);
			board_usart.port[c].tx.byte_to_send++;
		}
	}
}

/**
  * @brief  Function called at completion of last byte transmission
  * @param  USARTx the usart interface that is involved
  */
void board_USARTx_char_transmit_complete_callback(USART_TypeDef *USARTx)
{
	for(int c = 0; c<BOARD_USART_SIZEOF_TABLE; c++)
	{
		if( USARTx == board_usart.port[c].pusart )
		{
			if( board_usart.port[c].tx.byte_to_send >= board_usart.port[c].tx.pkt_len)
			{
				/* If the transmit complete interrupt (TC) was triggered then all the
				 * packet was sent. So, clear the counter and disable TC interrupt. */
				board_usart.port[c].tx.byte_to_send = 0; // clear the counter
				board_usart.port[c].tx.busy = false; // clear busy flag
			    LL_USART_DisableIT_TC(USARTx);
			    memset(board_usart.port[c].tx.buf, 0, sizeof(board_usart.port[c].tx.buf));
			}
		}
	}
}

/**
  * @brief  Function called at completion of a received byte. When this happen
  * the byte is inserted in the circular buffer assigned to a specific usart
  * channel.
  * @param  USARTx the usart interface that is involved
  */
void board_USARTx_char_reception_callback(USART_TypeDef *USARTx)
{
	for(int c = 0; c<BOARD_USART_SIZEOF_TABLE; c++)
	{
		if( USARTx == board_usart.port[c].pusart )
		{
			uint8_t received_byte = LL_USART_ReceiveData8(USARTx);
			board_usart_circ_buffer_push(&board_usart.port[c].rx.circ_buff, received_byte);
		}
	}
}

void board_USARTx_error_callback(USART_TypeDef *USARTx)
{
	/* TODO handle this error. */
}

/**
 * @brief circular buffer push function. Push a byte it into the buffer and increment
 * the head (write pointer).
 * @param circ_buffer the circular buffer where the operation will be made.
 * @param data the byte to push into the circ buffer
 * @return RET_OK if success or RET_FULL if it s full.
 **/
static ret_code_t board_usart_circ_buffer_push( circ_buffer_t *circ_buff, uint8_t data )
{
	ret_code_t ret = RET_OK;
	if( NULL == circ_buff ) return RET_ERR_NULL_POINTER;
    uint8_t *next;

    next = circ_buff->head + 1; // Next is where head will point to after this write.

    /* Verify if the next it is the end of the buffer. */
    if( next >= &(circ_buff->buf[sizeof(circ_buff->buf)-1]) )
    {	/* If it is the max, completes one round and return the next head to
    	 * the initial position of the buffer.*/
        next = circ_buff->buf;
    }

    /* If the head + 1 == tail, circular buffer is full. We don-t will
     * push any data. */
    if( next == circ_buff->tail )
    {
    	ret = RET_FULL;
    }
    else
    {
    	*(circ_buff->head) = data;	// Load data and then move
    	circ_buff->head = next;	// Head to next data offset.
    }

    return ret;  // Return success to indicate successful push.
}

/**
 * @brief circular buffer pop function. Takes a byte from the buffer and increment
 * the tail (read pointer).
 * @param circ_buff the circular buffer where the operation will be done.
 * @param data var where the byte popped will be stored.
 * @return RET_OK if succes ort RET_EMPTY if the buffer is empty.
 **/
static ret_code_t board_usart_circ_buffer_pop( circ_buffer_t *circ_buff, char *data )
{
	ret_code_t ret = RET_OK;
	if( NULL == circ_buff ) return RET_ERR_NULL_POINTER;
	uint8_t *next;

    /* If the head == tail, we don't have any data, the buffer is empty. */
    if(circ_buff->head == circ_buff->tail)
    {
    	ret = RET_EMPTY;
    }
    else
    {
        next = circ_buff->tail + 1;  // Next is where tail will point to after this read.
        if( next >= &(circ_buff->buf[sizeof(circ_buff->buf)-1]) )
        {	/* If it is the max, complete one round and return the next head to
	 	 	 * the initial position of the buffer.*/
        	next = circ_buff->buf;
        }

        *data = *circ_buff->tail;	// Read data and then move
        circ_buff->tail = next;		// tail to next offset.
    }

    return ret;
}

#endif /* BOARD_USART_C_ */
