/*
 * board_flash.c
 *
 *  Created on: Apr 30, 2021
 *      Author: Julian Rodriguez aka Marifante 
 *      		Lucas Mancini
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *				https://gitlab.com/lucasmancini95
 *		Github: https://github.com/Marifante
 *				https://github.com/lucasmancini95
 */

// Include --------------------------------------------------------------------
#include "board_cfg_private.h"

// Macros & constants ---------------------------------------------------------
#define BOARD_FLASH_NSECTOR_MAX 11

#define ADDR_FLASH_ADDR_SECTOR_0     ((uint32_t)0x08000000) /* Base address of Sector 0, 16 Kbytes */
#define ADDR_FLASH_ADDR_SECTOR_1     ((uint32_t)0x08004000) /* Base address of Sector 1, 16 Kbytes */
#define ADDR_FLASH_ADDR_SECTOR_2     ((uint32_t)0x08008000) /* Base address of Sector 2, 16 Kbytes */
#define ADDR_FLASH_ADDR_SECTOR_3     ((uint32_t)0x0800C000) /* Base address of Sector 3, 16 Kbytes */
#define ADDR_FLASH_ADDR_SECTOR_4     ((uint32_t)0x08010000) /* Base address of Sector 4, 64 Kbytes */
#define ADDR_FLASH_ADDR_SECTOR_5     ((uint32_t)0x08020000) /* Base address of Sector 5, 128 Kbytes */
#define ADDR_FLASH_ADDR_SECTOR_6     ((uint32_t)0x08040000) /* Base address of Sector 6, 128 Kbytes */
#define ADDR_FLASH_ADDR_SECTOR_7     ((uint32_t)0x08060000) /* Base address of Sector 7, 128 Kbytes */
#define ADDR_FLASH_ADDR_SECTOR_8     ((uint32_t)0x08080000) /* Base address of Sector 8, 128 Kbytes */
#define ADDR_FLASH_ADDR_SECTOR_9     ((uint32_t)0x080A0000) /* Base address of Sector 9, 128 Kbytes */
#define ADDR_FLASH_ADDR_SECTOR_10    ((uint32_t)0x080C0000) /* Base address of Sector 10, 128 Kbytes */
#define ADDR_FLASH_ADDR_SECTOR_11    ((uint32_t)0x080E0000) /* Base address of Sector 11, 128 Kbytes */

#define ADDR_FLASH_SIZE_SECTOR_0     16000
#define ADDR_FLASH_SIZE_SECTOR_1     16000
#define ADDR_FLASH_SIZE_SECTOR_2     16000
#define ADDR_FLASH_SIZE_SECTOR_3     16000
#define ADDR_FLASH_SIZE_SECTOR_4     64000
#define ADDR_FLASH_SIZE_SECTOR_5     128000
#define ADDR_FLASH_SIZE_SECTOR_6     128000
#define ADDR_FLASH_SIZE_SECTOR_7     128000
#define ADDR_FLASH_SIZE_SECTOR_8     128000
#define ADDR_FLASH_SIZE_SECTOR_9     128000
#define ADDR_FLASH_SIZE_SECTOR_10    128000
#define ADDR_FLASH_SIZE_SECTOR_11    128000

#if 0 //WIP
/* protection type */
enum{
  FLASHIF_PROTECTION_NONE         = 0,
  FLASHIF_PROTECTION_PCROPENABLED = 0x1,
  FLASHIF_PROTECTION_WRPENABLED   = 0x2,
  FLASHIF_PROTECTION_RDPENABLED   = 0x4,
};

/* protection update */
enum {
  FLASHIF_WRP_ENABLE,
  FLASHIF_WRP_DISABLE
};
#endif

// Private variables declaration ----------------------------------------------

// Private variables definition -----------------------------------------------


// Private functions declaration ----------------------------------------------

static uint32_t get_sector_macro(int nsector){

	switch (nsector) {
		case 0:
				return FLASH_SECTOR_0;
		case 1:
				return FLASH_SECTOR_1;
		case 2:
				return FLASH_SECTOR_2;
		case 3:
				return FLASH_SECTOR_3;
		case 4:
				return FLASH_SECTOR_4;
		case 5:
				return FLASH_SECTOR_5;
		case 6:
				return FLASH_SECTOR_6;
		case 7:
				return FLASH_SECTOR_7;
		case 8:
				return FLASH_SECTOR_8;
		case 9:
				return FLASH_SECTOR_9;
		case 10:
				return FLASH_SECTOR_10;
		case 11:
				return FLASH_SECTOR_11;
		default:
			return FLASH_SECTOR_11;
			break;
	}
	return FLASH_SECTOR_11;
}

// Public functions definition  -----------------------------------------------
/**
 * @brief init the FLASH of the board.
 * @return RET_OK if success. TODO: add error codes.
 */
ret_code_t Board_FLASH_Init(void)
{
	FLASH_OBProgramInitTypeDef OBInit;
	/* Get the bank configuration status */
	HAL_FLASHEx_OBGetConfig(&OBInit);

	//CHECK (manual stmf4 page 93/1751)
	OBInit.RDPLevel = OB_RDP_LEVEL_0;

	OBInit.WRPSector = OB_WRP_SECTOR_All;
	OBInit.WRPState = OB_WRPSTATE_DISABLE;

	OBInit.OptionType = OPTIONBYTE_WRP | OPTIONBYTE_RDP | OPTIONBYTE_USER ;

	Board_FLASH_Unlock();
	Board_FLASH_Unlock_Option_Bytes();

	HAL_FLASHEx_OBProgram(&OBInit);

	Board_FLASH_Lock_Option_Bytes();
	Board_FLASH_Lock();

	return RET_OK;
}

/**
 * @brief unlock the flash of the MCU.
 * @return RET_OK if success.
 */
ret_code_t Board_FLASH_Unlock(void)
{
	 if( HAL_OK != HAL_FLASH_Unlock())
	 {
		 return RET_ERR;
	 }
	 return RET_OK;
}

/**
 * @brief lock the flash of the MCU.
 * @return RET_OK if success.
 */
ret_code_t Board_FLASH_Lock(void)
{

	 if( HAL_OK != HAL_FLASH_Lock())
	 {
		 return RET_ERR;
	 }
	 return RET_OK;
}

/**
 * @brief unlock the option bytes of the MCU.
 * @return RET_OK if success.
 */
ret_code_t Board_FLASH_Unlock_Option_Bytes(void)
{
	 if( HAL_OK != HAL_FLASH_OB_Unlock())
	 {
		 return RET_ERR;
	 }
	 return RET_OK;
}

/**
 * @brief lock the option bytes of the MCU.
 * @return RET_OK if success.
 */
ret_code_t Board_FLASH_Lock_Option_Bytes(void)
{
	 if( HAL_OK != HAL_FLASH_OB_Lock()){
		 return RET_ERR;
	 }
	 return RET_OK;
}

/*
 * @brief erase a sector.
 * @param nsector the sector that will be erased.
 * @param unlock_lock unlock the flash before erasing it and lock it after.
 * @return RET_OK if success.
 * */
ret_code_t Board_FLASH_Erase_Sector(int nsector, bool unlock_lock)
{
	ret_code_t ret = RET_OK;
	HAL_StatusTypeDef hal_ret = HAL_OK;
	uint32_t SECTORError = 0;
	FLASH_EraseInitTypeDef EraseInitStruct = {0};

	if( nsector > BOARD_FLASH_NSECTOR_MAX )
	{
		ret = RET_ERR_INVALID_PARAMS;
	}
	else
	{
		if(true == unlock_lock)
		{
			HAL_FLASH_Unlock();
		}

		/* FIXME: See if the paralellism flag is set and clear it. This is only a workaround, see
		 * why its corromped this flag! */
		if(__HAL_FLASH_GET_FLAG(FLASH_FLAG_PGPERR) != RESET ) __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_PGPERR);
		if(__HAL_FLASH_GET_FLAG(FLASH_FLAG_PGSERR) != RESET ) __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_PGSERR);

		/* Erase Flash sectors selected by the user.
		 * Fill EraseInit structure. */
		EraseInitStruct.TypeErase     = FLASH_TYPEERASE_SECTORS; //erase by sectors
		EraseInitStruct.VoltageRange  = FLASH_VOLTAGE_RANGE_3;
		EraseInitStruct.Sector        = get_sector_macro(nsector);
		EraseInitStruct.NbSectors     = 1;//NbOfSectors;//is 1

		hal_ret = HAL_FLASHEx_Erase(&EraseInitStruct, &SECTORError);
		if( HAL_OK != hal_ret )
		{
			// Error occurred while sector erase
			ret = RET_ERR;
		}

		if(true == unlock_lock)
		{
			HAL_FLASH_Lock();
		}
	}
	return ret;
}


/**
 * @brief read a word from the flash in the sector "nsector" with an
 * offset "offset".
 *
 * @param nsector the sector where the data will be readed.
 * @param data buffer where the data readed will be stored.
 * @param offset address offset from the sector origin.
 *
 * @return RET_OK success.
 */
ret_code_t Board_FLASH_Read_Offset(	int nsector, uint32_t *data, int offset, bool unlock_lock )
{
	ret_code_t ret = RET_OK;
	if( NULL == data ) return RET_ERR_NULL_POINTER;

	uint32_t Address = Board_FLASH_Get_Sector_Address(nsector)+offset;

	*data = *(uint32_t *)Address;
	return ret;
}

/*
 * @brief write a data of size "unit_type" (see the typedef Board_FLASH_WR_Unit_T)
 * into the flash in the sector "nsector" with an offset "offset".
 *
 * @param nsector the sector where the data will be written.
 * @param unit_type the type of data that will be written (byte or word). If there is a byte,
 * only the first byte of the word passed as data are burned to the flash.
 * @param data the data that will be stored. Must be an uint32_t.
 * @param offset address offset from the sector origin.
 * @param unlock_lock set to true if wants to unlock and lock the flash.
 *
 * @return RET_OK success.
 * */
ret_code_t Board_FLASH_Write_Offset(	int nsector, Board_FLASH_WR_Unit_T unit_type,
										uint32_t data, int offset, bool unlock_lock )
{
	ret_code_t ret = RET_OK;
	HAL_StatusTypeDef hal_ret = HAL_OK;

	if( (nsector > BOARD_FLASH_NSECTOR_MAX) || (unit_type > BOARD_FLASH_WR_MAX_TYPES) )
	{
		ret = RET_ERR_INVALID_PARAMS;
	}
	else
	{
		if(true == unlock_lock)
		{
			HAL_FLASH_Unlock();
		}

		/* FIXME: See if the paralellism flag is set and clear it. This is only a workaround, see
		 * why its corromped this flag! */
//		if(__HAL_FLASH_GET_FLAG(FLASH_FLAG_PGPERR) != RESET ) __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_PGPERR);
//		if(__HAL_FLASH_GET_FLAG(FLASH_FLAG_PGSERR) != RESET ) __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_PGSERR);

		/* Keep only the first byte if the data is a byte, keep four bytes if the data is a word. */
		data = ( unit_type == BOARD_FLASH_WR_BYTE ) ? (0xFF & data) : (0xFFFF & data);

		/* FLASH PROGRAM --> WRITE. Write one byte in the flash. */
		hal_ret = HAL_FLASH_Program(unit_type, Board_FLASH_Get_Sector_Address(nsector) + offset, data);
		if( hal_ret != HAL_OK )
		{
			ret = RET_ERR;
		}

		if(true == unlock_lock)
		{
			HAL_FLASH_Lock();
		}
	}

	return ret;
}

/*
 * @brief get the size of a sector of the flash.
 *
 * @param nsector the sector that was queried.
 *
 * @return the size of the sector.
 *
 * */
int32_t Board_Flash_Get_Sector_Size(int nsector)
{

	switch (nsector) {
		case 0:
				return ADDR_FLASH_SIZE_SECTOR_0;
		case 1:
				return ADDR_FLASH_SIZE_SECTOR_1;
		case 2:
				return ADDR_FLASH_SIZE_SECTOR_2;
		case 3:
				return ADDR_FLASH_SIZE_SECTOR_3;
		case 4:
				return ADDR_FLASH_SIZE_SECTOR_4;
		case 5:
				return ADDR_FLASH_SIZE_SECTOR_5;
		case 6:
				return ADDR_FLASH_SIZE_SECTOR_6;
		case 7:
				return ADDR_FLASH_SIZE_SECTOR_7;
		case 8:
				return ADDR_FLASH_SIZE_SECTOR_8;
		case 9:
				return ADDR_FLASH_SIZE_SECTOR_9;
		case 10:
				return ADDR_FLASH_SIZE_SECTOR_10;
		case 11:
				return ADDR_FLASH_SIZE_SECTOR_11;
		default:
			return ADDR_FLASH_SIZE_SECTOR_11;
			break;
	}
	return ADDR_FLASH_SIZE_SECTOR_11;
}

/*
 * @brief get the address of a sector.
 *
 * */
uint32_t Board_FLASH_Get_Sector_Address(int nsector)
{

	switch (nsector) {
		case 0:
				return ADDR_FLASH_ADDR_SECTOR_0;
		case 1:
				return ADDR_FLASH_ADDR_SECTOR_1;
		case 2:
				return ADDR_FLASH_ADDR_SECTOR_2;
		case 3:
				return ADDR_FLASH_ADDR_SECTOR_3;
		case 4:
				return ADDR_FLASH_ADDR_SECTOR_4;
		case 5:
				return ADDR_FLASH_ADDR_SECTOR_5;
		case 6:
				return ADDR_FLASH_ADDR_SECTOR_6;
		case 7:
				return ADDR_FLASH_ADDR_SECTOR_7;
		case 8:
				return ADDR_FLASH_ADDR_SECTOR_8;
		case 9:
				return ADDR_FLASH_ADDR_SECTOR_9;
		case 10:
				return ADDR_FLASH_ADDR_SECTOR_10;
		case 11:
				return ADDR_FLASH_ADDR_SECTOR_11;
		default:
			return ADDR_FLASH_ADDR_SECTOR_11;
			break;
	}
	return ADDR_FLASH_ADDR_SECTOR_11;
}
