/*
 * board_random_number.c
 *
 *  Created on: May 24, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */


// Include --------------------------------------------------------------------
#include "board_cfg_private.h"
#include "board_private.h"

// Macros & constants ---------------------------------------------------------

// Private variables declaration  ---------------------------------------------
typedef struct
{
	RNG_HandleTypeDef RngHandle;
}board_rng_t;

// Private variables definition  ----------------------------------------------
//static RNG_HandleTypeDef RngHandle;
static board_rng_t board_rng;

// Private functions declaration  ---------------------------------------------

// Public functions definition  -----------------------------------------------
/*
 * @brief Initiate the random number generator.
 * */
ret_code_t Board_Random_Number_Init(void)
{
	ret_code_t ret = RET_OK;
	board_rng.RngHandle.Instance = RNG;

	if(HAL_OK != HAL_RNG_Init(&board_rng.RngHandle))
	{
		ret = RET_ERR;
	}

	return ret;
}

/*
 * @brief Generate a random number using RNG.
 * Some APIs will need a random number to work.
 * */
ret_code_t Board_Random_Number_Generate(uint32_t *pulValue)
{
	ret_code_t ret = RET_OK;
	uint32_t ulValue;

	if( HAL_OK == HAL_RNG_GenerateRandomNumber( &(board_rng.RngHandle), &ulValue ) )
	{
		*pulValue = ulValue;
	}
	else
	{
		ret = RET_ERR;
	}
	return ret;
}

/*
 * @brief init low level hw for the HAL of the RNG.
 * */
void HAL_RNG_MspInit(RNG_HandleTypeDef *hrng)
{
	__RNG_CLK_ENABLE();/* RNG Peripheral clock enable */
}
