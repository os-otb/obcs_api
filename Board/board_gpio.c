/*
 * board_gpio.c
 *
 *  Created on: May 4, 2021
 *      Author: Julian Rodriguez aka Marifante 
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Include --------------------------------------------------------------------
#include "board_cfg_private.h"

// Private variables declaration ----------------------------------------------
extern const Board_GPIO_Handler_T board_gpio_handler_table[BOARD_GPIO_SIZEOF_TABLE];

// Public functions definition  -----------------------------------------------
ret_code_t Board_GPIO_Init(void)
{
	ret_code_t ret = RET_OK;
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	GPIO_PinState gpio_state = GPIO_PIN_RESET;

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();
	__HAL_RCC_GPIOE_CLK_ENABLE();
	__HAL_RCC_GPIOH_CLK_ENABLE();
#if defined(STM32F407Z) || defined(STM32F407I)
	__HAL_RCC_GPIOF_CLK_ENABLE();
	__HAL_RCC_GPIOG_CLK_ENABLE();
#ifdef STM32F407I
	__HAL_RCC_GPIOI_CLK_ENABLE();
	__HAL_RCC_GPIOH_CLK_ENABLE();
#endif
#endif

	for(int c = 0; c < BOARD_GPIO_SIZEOF_TABLE; c++)
	{
		GPIO_InitStruct.Pin = board_gpio_handler_table[c].gpio_pin;
		GPIO_InitStruct.Mode = board_gpio_handler_table[c].mode;
		GPIO_InitStruct.Pull = board_gpio_handler_table[c].pull;
		GPIO_InitStruct.Speed = board_gpio_handler_table[c].speed;
		HAL_GPIO_Init(board_gpio_handler_table[c].gpio_port, &GPIO_InitStruct);

		/* If it is an output then set initial value. */
		if(	(GPIO_MODE_OUTPUT_PP == board_gpio_handler_table[c].mode) ||
			(GPIO_MODE_OUTPUT_OD == board_gpio_handler_table[c].mode) )
		{
			gpio_state = (board_gpio_handler_table[c].init_val == true) ? GPIO_PIN_SET : GPIO_PIN_RESET;
		  HAL_GPIO_WritePin(  board_gpio_handler_table[c].gpio_port,
							  board_gpio_handler_table[c].gpio_pin,
							  gpio_state);
		}
	}

	return ret;
}

/*
 * @brief toggle board GPIO.
 * @param board_gpio.
 * @return RET_OK if success.
 * */
ret_code_t Board_GPIO_Toggle(uint32_t board_gpio)
{
	ret_code_t ret = RET_OK;
	int c = 0;

	for(c = 0; c<BOARD_GPIO_SIZEOF_TABLE; c++)
	{
		if( board_gpio_handler_table[c].board_port == board_gpio )
		{
			HAL_GPIO_TogglePin(board_gpio_handler_table[c].gpio_port, board_gpio_handler_table[c].gpio_pin);
			break;
		}
	}

	if( c > BOARD_GPIO_SIZEOF_TABLE ) ret = RET_ERR_INVALID_PARAMS;
	return ret;
}

/*
 * @brief get board GPIO state.
 * @param board_gpio gpio used. Its defined in the gpios table defined by the user.
 * @param state bool where the state of the GPIO will be stored. SET = true & RESET = false.
 * @return RET_OK if success.
 * */
ret_code_t Board_GPIO_Get_State(uint32_t board_gpio, bool *state)
{
	ret_code_t ret = RET_OK;
	GPIO_PinState gpio_state = GPIO_PIN_RESET;
	int c = 0;

	for(c = 0; c<BOARD_GPIO_SIZEOF_TABLE; c++)
	{
		if( board_gpio_handler_table[c].board_port == board_gpio )
		{
			gpio_state = HAL_GPIO_ReadPin(board_gpio_handler_table[c].gpio_port, board_gpio_handler_table[c].gpio_pin);
			*state = (GPIO_PIN_RESET == gpio_state ) ? false : true;
			break;
		}
	}

	if( c > BOARD_GPIO_SIZEOF_TABLE ) ret = RET_ERR_INVALID_PARAMS; // gpio not defined in table
	return ret;
}

/*
 * @brief set board GPIO state.
 * @param board_gpio gpio used. Its defined in the gpios table defined by the user.
 * @param state bool with the state to set on the gpio. true = SET & false = RESET..
 * @return RET_OK if success.
 * */
ret_code_t Board_GPIO_Set_State(uint32_t board_gpio, bool state)
{
	ret_code_t ret = RET_OK;
	int c = 0;
	GPIO_PinState gpio_state = GPIO_PIN_RESET;
	gpio_state = (state == false) ? GPIO_PIN_RESET : GPIO_PIN_SET;

	for(c = 0; c<BOARD_GPIO_SIZEOF_TABLE; c++)
	{
		if( board_gpio_handler_table[c].board_port == board_gpio )
		{
			HAL_GPIO_WritePin(board_gpio_handler_table[c].gpio_port, board_gpio_handler_table[c].gpio_pin, gpio_state);
			break;
		}
	}

	if( c > BOARD_GPIO_SIZEOF_TABLE ) ret = RET_ERR_INVALID_PARAMS; // gpio not defined in table
	return ret;
}
