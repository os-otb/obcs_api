/*
 * board_lmt85.c
 *
 *  This module implements a measurement system with the ADCs of the STM3 and 
 *  LMT85 sensors.
 *  Datasheet of the sensor: https://www.ti.com/lit/ds/symlink/lmt85.pdf?ts=1630699403863&ref_url=https%253A%252F%252Fwww.google.com%252F
 *	Also a FIR filter is implemented with the STM32 ARM Cortex M4
 *	Microprocessor.
 *	This code was implemented taken in count this AN from STM:
 *	https://www.st.com/resource/en/application_note/dm00273990-digital-signal-processing-for-stm32-microcontrollers-using-cmsis-stmicroelectronics.pdf
 *
 *	Principle of working:
 *	In the init function, the module will init:
 *	- A timer that uses to sample the sensors at a given frequency when
 *	the applications needs it with the function board_lmt85_get_temp.
 *	- The ADCs needed to measure the sensors.
 *	- The FIR filter of each sensor. In this case, we are using the CMSIS-DSP
 *	library. We initialize the filter instance in the init and when we measure the temp
 *	we filter the values.
 *
 *	In this case, the board_adc_measure function will retrieve an array of values
 *	where each value can be mapped to a specific sensor. This map between the values
 *	on this array and the sensors must be done in the configuration by the user defining
 *	the array board_lmt85[]. Each element of this array will have two IDs:
 *	- One ID for the API, this will be used by the API to identify the sensors.
 *	- One ID for the ADC module. The ADC module handle two different ADCs. The sensors
 *	are split in two groups: one group that is measured by the ADC1 and another group that is
 *	measured by the ADC3. The ADC id of each sensor is the order in which the board_adc_measure(ADCx)
 *	function retrieve us the values.
 *
 *  Created on: Apr 30, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Includes -------------------------------------------------------------------
#include "board_cfg_private.h"

#ifdef BOARD_USE_LMT85
/* ARM Math module */
#include "arm_math.h"

// Macros & Constants ---------------------------------------------------------
/* Timer used to sample the sensor. This time marks the sampling frequency
 * of the sensors: */
#define BOARD_LMT85_SAMPLING_TIM_IRQn_PRIOTY		0		/*!< Priority of the interrupt of the timer. */

// Private variables declaration ----------------------------------------------

// Private variables definition -----------------------------------------------
static bool board_lmt85_must_sample_now;

// Private functions declaration ----------------------------------------------
static float board_lmt85_voltage_to_temp(float voltage_mv);
static ret_code_t board_lmt85_init_sampling_timer(void);
static ret_code_t board_lmt85_measure_adc1_group(bool must_filter, bool block);
static ret_code_t board_lmt85_measure_adc3_group(bool must_filter, bool block);

// Public funtions definition -------------------------------------------------
/**
 * @brief init the things that only will be configured one time in the code 
 * (i.e. the gpios configuration of the ADC inputs)
 * @return RET_OK if success.
*/
ret_code_t board_lmt85_init(void)
{
    ret_code_t ret = RET_OK;
    /* Initialize the sampling timer. */
    ret = board_lmt85_init_sampling_timer();
    if( RET_OK == ret )
    {	/* Initialize the ADC. */
        ret = board_adc_init();
        if( RET_OK == ret )
        {
        	/* Look for each sensor in the ADC1 and initialize its FIR filter. */
        	for( int i = 0; i < BOARD_ADC1_MAX_ANALOG_INPUTS; i++ )
        	{
        		for( int c = BOARD_LMT85_TS_T0; c < BOARD_LMT85_MAX; c++ )
        		{
        			if( board_lmt85[c].adc_input_id == i )
        			{
        				arm_fir_init_f32(&board_lmt85[c].fir,
        						BOARD_LMT85_FIR_FILTER_ORDER,
    							board_lmt85_fir_coeffs,
    							board_lmt85[c].samples_mv, 1);
        				break;
        			}
        		}
        	}

        	/* Look for each sensor in the ADC3 and initialize its FIR filter. */
        	for( int i = 0; i < BOARD_ADC3_MAX_ANALOG_INPUTS; i++ )
        	{ 	/* The ID of each sensor for the ADC module is the order
        	 	 * in which they give to us the values */
        		for( int c = BOARD_LMT85_TS_N0; c < BOARD_LMT85_MAX; c++ )
        		{
        			if( board_lmt85[c].adc_input_id == i )
        			{	/* Do a shift of the last values and store this one. */
        				arm_fir_init_f32(&board_lmt85[c].fir,
        						BOARD_LMT85_FIR_FILTER_ORDER,
    							board_lmt85_fir_coeffs,
    							board_lmt85[c].samples_mv, 1);
        				break;
        			}
        		}
        	}

        	/* Measure with each sensor to fill the buffer. We don't must to
        	 * filter now, we only must to fill the buffer because the FIR filter
        	 * will not work if the buffer is empty. Also, this dont must be a blocking call!
        	 * Break if a timeout occurs.*/
        	for(int i = 0; i < BOARD_LMT85_FIR_FILTER_SAMPLES_QTY; i++) board_lmt85_measure_adc1_group(false, false);
        	for(int i = 0; i < BOARD_LMT85_FIR_FILTER_SAMPLES_QTY; i++) board_lmt85_measure_adc3_group(false, false);
        }
    }
    return ret;
}

/**
 * @brief measure with all the sensors initialized and returns up to sens_qty measures.
 * @param buff_measure a buffer where the measures with the correspondent sensor id will be
 * stored.
 * @param buff_len the length of the buffer passed by the API.
 * @return RET_OK if success
 */
ret_code_t board_lmt85_get_temp(api_temp_sensor_measure_t *buff_measure, int buff_len)
{
	if( NULL == buff_measure ) return RET_ERR_NULL_POINTER;
	ret_code_t ret = RET_OK;

	/* Measure with the ADC1 with a given sampling frequency filtering the values. */
	for(int c = 0; c < BOARD_LMT85_SAMPLES_QTY; c++)
	{
		LL_TIM_EnableCounter(BOARD_LMT85_SAMPLING_TIM);
		while( true != board_lmt85_must_sample_now ) {};
		ret = board_lmt85_measure_adc1_group(true, true); // measure filtering
		board_lmt85_must_sample_now = false;
	}

	/* Measure with the ADC3 with a given sampling frequency filtering the values. */
	for(int c = 0; c < BOARD_LMT85_SAMPLES_QTY; c++)
	{
		LL_TIM_EnableCounter(BOARD_LMT85_SAMPLING_TIM);
		while( true != board_lmt85_must_sample_now ) {};
		ret = board_lmt85_measure_adc3_group(true, true); // measure filtering
		board_lmt85_must_sample_now = false;
	}

	/* Pass the values to the API. */
	for( int i = 0; i < BOARD_LMT85_MAX; i++ )
	{
		buff_measure[i].temp = board_lmt85_voltage_to_temp(board_lmt85[i].last_filtered);
		buff_measure[i].sensor_id = board_lmt85[i].api_id;
		if( i >= buff_len ) break;
	}
	return ret;
}

// Private functions definition -----------------------------------------------
/**
 * @brief measure all the sensors attached to adc1.
 * @param must_filter if it is set to true, then the FIR filter will be applied and
 * the value will be stored in the last_filtered value of the sensor struct.
 * @param block if true, the adc measure function will block forever until a timeout
 * is reached. If not, the adc measure function will break after a timeout.
 * @details in this function we are not converting the filtered values to temperature
 * becase this takes too much time and delays a lot the sampling-filter process. The
 * best approach is convert the value when the user wants to get the temperature
 * value.
 */
static ret_code_t board_lmt85_measure_adc1_group(bool must_filter, bool block)
{
	ret_code_t ret = RET_OK;

	float aux_mv;	/* Aux var to compute the temperature with the voltage. */
	/* Measure the sensors that are attached to the ADC1. */
	float adc1_voltages[BOARD_ADC1_MAX_ANALOG_INPUTS];
	ret = board_adc_measure(ADC1, adc1_voltages, sizeof(adc1_voltages), block);
	for( int i = 0; i < BOARD_ADC1_MAX_ANALOG_INPUTS; i++ )
	{ 	/* The ID of each sensor for the ADC module is the order
	 	 * in which they give to us the values */
		for( int c = BOARD_LMT85_TS_T0; c < BOARD_LMT85_MAX; c++ )
		{
			if( board_lmt85[c].adc_input_id == i )
			{	/* Do a shift of the last values and store this one. */
				for( int k = 0; k < BOARD_LMT85_FIR_FILTER_SAMPLES_QTY-1; k++ )
				{
					board_lmt85[c].samples_mv[k] = board_lmt85[c].samples_mv[k+1];
				}
				board_lmt85[c].samples_mv[BOARD_LMT85_FIR_FILTER_SAMPLES_QTY-1] = adc1_voltages[i];
				aux_mv = board_lmt85[c].samples_mv[BOARD_LMT85_FIR_FILTER_SAMPLES_QTY-1];
				if( true == must_filter ) arm_fir_f32(&board_lmt85[c].fir, board_lmt85[c].samples_mv, &aux_mv, 1); // Filter!
				board_lmt85[c].last_filtered = aux_mv;
				break;
			}
		}
	}

	return ret;
}

/**
 * @brief measure all the sensors attached to adc3.
 * @param must_filter if it is set to true, then the FIR filter will be applied and
 * the value will be stored in the last_filtered value of the sensor struct.
 * @param block if true, the adc measure function will block forever until a timeout
 * is reached. If not, the adc measure function will break after a timeout.
 * @details in this function we are not converting the filtered values to temperature
 * becase this takes too much time and delays a lot the sampling-filter process. The
 * best approach is convert the value when the user wants to get the temperature
 * value.
 */
static ret_code_t board_lmt85_measure_adc3_group(bool must_filter, bool block)
{
	ret_code_t ret = RET_OK;
	float aux_mv;	/* Aux var to compute the temperature with the voltage. */
	/* Measure the sensors that are attached to the ADC3. */
	float adc3_voltages[BOARD_ADC3_MAX_ANALOG_INPUTS];
	ret = board_adc_measure(ADC3, adc3_voltages, sizeof(adc3_voltages), block);
	for( int i = 0; i < BOARD_ADC3_MAX_ANALOG_INPUTS; i++ )
	{ 	/* The ID of each sensor for the ADC module is the order
	 	 * in which they give to us the values */
		for( int c = BOARD_LMT85_TS_N0; c < BOARD_LMT85_MAX; c++ )
		{
			if( board_lmt85[c].adc_input_id == i )
			{	/* Do a shift of the last values and store this one. */
				for( int k = 0; k < BOARD_LMT85_FIR_FILTER_SAMPLES_QTY-1; k++ )
				{
					board_lmt85[c].samples_mv[k] = board_lmt85[c].samples_mv[k+1];
				}
				board_lmt85[c].samples_mv[BOARD_LMT85_FIR_FILTER_SAMPLES_QTY-1] = adc3_voltages[i];
				aux_mv = board_lmt85[c].samples_mv[BOARD_LMT85_FIR_FILTER_SAMPLES_QTY-1];
				if( true == must_filter ) arm_fir_f32(&board_lmt85[c].fir, board_lmt85[c].samples_mv, &aux_mv, 1); // Filter!
				board_lmt85[c].last_filtered = aux_mv;
				break;
			}
		}
	}
	return ret;
}

/**
 * @brief convert the voltage measured to temperature.
 * @param voltage_ms the voltage measured in the sensor in mV.
 * @return the temperature in celsius grades. TODO: we can change this to kelvin?
 * */
static float board_lmt85_voltage_to_temp(float voltage_mv)
{
	float temp, sqrt, aux;
	aux = 67.141636 +  0.01048 * (1324 - voltage_mv);
	if( ARM_MATH_SUCCESS != arm_sqrt_f32(aux, &sqrt) )
	{
		asm("nop"); /* Error in the sqrt calculation! */
	}
	temp = ((sqrt-8.194)/ 0.00524) + 30;
	return temp;
}

/*
 * @brief init the timer that will trigger the measurements
 * when the API wants to get temperature. This timer will give to the
 * driver the correct sampling frequency for all the sensors. This would not be
 * the exact sampling frequency because we filter between each conversion, but will be
 * good enough.
 * */
static ret_code_t board_lmt85_init_sampling_timer(void)
{
	ret_code_t ret = RET_OK;

	// Enable the timer peripheral clock on APBx
	LL_APB1_GRP1_EnableClock(BOARD_LMT85_SAMPLING_TIM_APB_GRP);

	// Init Timer A
	uint32_t apbFreq = Board_Timer_Get_APB_Freq(BOARD_LMT85_SAMPLING_TIM_APB);	// Timers used in this driver must be sourced from APB1
	uint32_t apbPrescaler = 1; // Not pre-scale the APB1 input clock
	LL_TIM_InitTypeDef TIM_InitStruct = {0};
	TIM_InitStruct.Prescaler = apbPrescaler;
	TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_DOWN;
	TIM_InitStruct.Autoreload = __LL_TIM_CALC_ARR(apbFreq, apbPrescaler, BOARD_LMT85_SAMPLING_TIM_ISR_FREQ);
	TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;	// CK_INT = 1
	TIM_InitStruct.RepetitionCounter = (uint8_t)0x00;
	LL_TIM_Init(BOARD_LMT85_SAMPLING_TIM, &TIM_InitStruct);
	if(LL_TIM_IsActiveFlag_UPDATE(BOARD_LMT85_SAMPLING_TIM) == 1)
	{	/* If update event flag triggers after initialization, clears it. */
		LL_TIM_ClearFlag_UPDATE(BOARD_LMT85_SAMPLING_TIM);
	}
	LL_TIM_DisableCounter(BOARD_LMT85_SAMPLING_TIM);
	LL_TIM_SetClockSource(BOARD_LMT85_SAMPLING_TIM, BOARD_LMT85_SAMPLING_TIM_CLK_SOURCE);

	// Set one pulse mode (one shot)
	LL_TIM_SetOnePulseMode(BOARD_LMT85_SAMPLING_TIM, LL_TIM_ONEPULSEMODE_SINGLE);

	// Enable the update interrupt
	LL_TIM_EnableIT_UPDATE(BOARD_LMT85_SAMPLING_TIM);

	// Configure the NVIC to handle TIM_A update interrupt
	NVIC_SetPriority(BOARD_LMT85_SAMPLING_TIM_IRQn, BOARD_LMT85_SAMPLING_TIM_IRQn_PRIOTY);
	NVIC_EnableIRQ(BOARD_LMT85_SAMPLING_TIM_IRQn);

	return ret;
}


/* @brief when this ISR is reached, the must_sample_now flag will be set to true and
 * the board_lmt85_measure_all function will need to measure. */
void Board_LMT85_Sampling_TIM_IRQHandler(void)
{
	/* Check whether update interrupt is pending */
	if(LL_TIM_IsActiveFlag_UPDATE(BOARD_LMT85_SAMPLING_TIM) == 1)
	{	/* Clear the update interrupt flag*/
		LL_TIM_ClearFlag_UPDATE(BOARD_LMT85_SAMPLING_TIM);
	}
	board_lmt85_must_sample_now = true;
}
#endif
