/*
 * board_23k256.c
 *
 *  This module implements a driver to interface with a RAM memory called
 * 
 *
 *  Created on: Sep 25, 2021
 *      Author: Federico Olivero
 */

// Includes -------------------------------------------------------------------
#include "board_cfg_private.h"
#define BOARD_USE_23K256
#ifdef BOARD_USE_23K256

// Macros & Constants ---------------------------------------------------------

// Private variables declaration ----------------------------------------------


// Private variables definition -----------------------------------------------
SPI_HandleTypeDef hspi1;

// Private functions declaration ----------------------------------------------
ret_code_t board_23k256_init_spi(void);
void CS_LOW(uint8_t Mem_Num);
void CS_HIGH(uint8_t Mem_Num);
void write_ram (int Mem_Num);
void board_verifify_ram (int *working_memories, size_t buff_size);

void k256_send (uint8_t *buff, uint16_t buff_size, uint32_t timeout_ms);
void k256_receive ( uint8_t *buff, uint16_t buff_size , uint32_t timeout_ms);

// Public funtions definition -------------------------------------------------
/**
 * @brief init the things that only will be configured one time in the code 
	 * (i.e. the gpios configuration of the ADC inputs)
	 * @return RET_OK if success.
	*/
	ret_code_t board_23k256_init(void)
{
    ret_code_t ret = RET_OK;

    ret = board_23k256_init_spi();

    return ret;
}

/**
 * @brief send a buffer to a memory ram.
 * @param buff the data to send
 * @param buff_size the size of the buffer to send.
 * @param timeout_ms the time to wait the transaction.
 * @return RET_OK if success.
 */
	/*ret_code_t board_23k256_send(uint8_t *buff, uint8_t buff_size, uint32_t timeout_ms)
	{
		if( NULL == buff ) return RET_ERR_NULL_POINTER;

		ret_code_t ret = RET_OK;

		HAL_SPI_Transmit(&hspi1, buff, buff_size, timeout_ms);

		return ret;
	}/*

	/**
	 * @brief receive a buffer from a memory ram.
	 * @param buff the data to receive
	 * @param buff_size the size of the buffer passed as param.
	 * @param timeout_ms the time to wait the transaction.
	 * @return RET_OK if success.
	 */
	/*ret_code_t board_23k256_receive( uint8_t *buff, uint8_t buff_size, uint32_t timeout_ms)
	{
		if( NULL == buff ) return RET_ERR_NULL_POINTER;

		ret_code_t ret = RET_OK;

		HAL_SPI_Receive(&hspi1, buff, buff_size, timeout_ms);

		return ret;
	}/*


// Private functions definition -----------------------------------------------
/**
 * @brief init the spi and the gpios to talk with the RAM memories.
 * */
ret_code_t board_23k256_init_spi(void)
{
	ret_code_t ret = RET_OK;

	__HAL_RCC_SPI1_CLK_ENABLE();
	/* TODO: init only the clocks used.
	 * GPIO Ports Clock Enable */


	/* Init SPI */
	hspi1.Instance = SPI1;
	hspi1.Init.Mode = SPI_MODE_MASTER;
	hspi1.Init.Direction = SPI_DIRECTION_2LINES;
	hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
	hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
	hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
	hspi1.Init.NSS = SPI_NSS_SOFT;
	hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_64;
	hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
	hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
	hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	hspi1.Init.CRCPolynomial = 10;
	if(HAL_SPI_Init(&hspi1) != HAL_OK) ret = RET_ERR;




	/* Init Chip Selects */
	GPIO_InitTypeDef GPIO_InitStruct = {0};


		__HAL_RCC_GPIOB_CLK_ENABLE();

//	// Init all the GPIOs and init them in HIGH state!
	for(int c = 0; c < BOARD_23K256_MAX_RAMS; c++)
	{
		HAL_GPIO_WritePin(	board_23k256[c].cs.gpio_port,
							board_23k256[c].cs.gpio_pin,
							GPIO_PIN_SET);
		GPIO_InitStruct.Pin = board_23k256[c].cs.gpio_pin;
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull = GPIO_PULLUP;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
		HAL_GPIO_Init(board_23k256[c].cs.gpio_port, &GPIO_InitStruct);
	}



	/* Init SPI MISO, MOSI & CLK */
	/* Config MISO GPIO. */
	GPIO_InitStruct.Pin = BOARD_23k256_RAM_MISO_GPIN;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
	HAL_GPIO_Init(BOARD_23k256_RAM_MISO_GPORT, &GPIO_InitStruct);

	/* Config MOSI GPIO. */
	GPIO_InitStruct.Pin = BOARD_23k256_RAM_MOSI_GPIN;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
	HAL_GPIO_Init(BOARD_23k256_RAM_MOSI_GPORT, &GPIO_InitStruct);

	/* Config CLK GPIO. */
	GPIO_InitStruct.Pin = BOARD_23k256_RAM_CLK_GPIN;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
	HAL_GPIO_Init(BOARD_23k256_RAM_CLK_GPORT, &GPIO_InitStruct);





	return ret;
}

/**
 * @brief select a RAM memory with its Chip Select
 */
void CS_LOW(uint8_t Mem_Num)
{
	/* If the ram indexed is not valid then set all the rams in high state. */
	if( Mem_Num > BOARD_23K256_MAX_RAMS_USED )
	{
		for(int c = 0; c < BOARD_23K256_MAX_RAMS_USED; c++)
		{
			HAL_GPIO_WritePin(board_23k256[c].cs.gpio_port, board_23k256[c].cs.gpio_pin, GPIO_PIN_SET);
		}
	}
	else
	{
		HAL_GPIO_WritePin(board_23k256[Mem_Num].cs.gpio_port, board_23k256[Mem_Num].cs.gpio_pin, GPIO_PIN_RESET);
	}
}

/**
 * @brief de-select a RAM memory with its Chip Select
 */
void CS_HIGH(uint8_t Mem_Num)
{
	/* If the ram indexed is not valid then set all the rams in high state. */
	if( Mem_Num > BOARD_23K256_MAX_RAMS_USED )
	{
		for(int c = 0; c < BOARD_23K256_MAX_RAMS_USED; c++)
		{
			HAL_GPIO_WritePin(board_23k256[c].cs.gpio_port, board_23k256[c].cs.gpio_pin, GPIO_PIN_SET);
		}
	}
	else
	{
		HAL_GPIO_WritePin(board_23k256[Mem_Num].cs.gpio_port, board_23k256[Mem_Num].cs.gpio_pin, GPIO_PIN_SET);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////
void board_verifify_ram (int *working_memories, size_t buff_size){
if( NULL == working_memories ) return -1;
HAL_StatusTypeDef hal_ret;
uint8_t Counter_A = 0;
uint8_t Command_M[2];							//Comandos
uint8_t Command;
uint8_t	spiRxVef;								//Verificacion  de funcionamiento
int buffer_loc = 0; 							// indica el lugar del buffer

	for (Counter_A=0;Counter_A<buff_size;Counter_A++)
	{

   		  Command_M[0] = 0x01;	//Write STATUS register
   		  Command_M[1] = 0x80;	//MODE Page
   		  Command_M[2] = 0x05;	// Read STATUS register
   		  CS_LOW(Counter_A);

   		  HAL_SPI_Transmit(&hspi1,(uint8_t *)&Command_M[0],1,200);
   		  HAL_SPI_Transmit(&hspi1,(uint8_t *)&Command_M[1],1,200);

   		  CS_HIGH(Counter_A);

   		  CS_LOW(Counter_A);

   		  HAL_SPI_Transmit(&hspi1,(uint8_t *)&Command_M[2],1,200);

   		  hal_ret = HAL_SPI_Receive(&hspi1,(uint8_t*)&spiRxVef,1, 200);

   		  CS_HIGH(Counter_A);
//////////////////////////////////////////////////////////////////////////////////////////
		  //READ STATUS MODE//
 if (spiRxVef == 0x80){
	 working_memories[buffer_loc] = Counter_A;
     spiRxVef=0;
     buffer_loc++;
   	}
 else {
	 working_memories[buffer_loc] = -1;
     spiRxVef=0;
     buffer_loc++;

 	}
	}

}





void board_write_ram (int Mem_Num){

	uint8_t spiTxBuff[32];
	unsigned char lowdata = 0;
    unsigned char highdata = 0;

	uint8_t num_mem[9];								//Numeros de memorias
	uint16_t Total_pages=1024;						//Numero de paginas a leer
	uint16_t Command_A;								//Comando de envio
	uint16_t Counter_D;								//Contado de buff_Tx
	uint8_t Counter_A=0;							//Contador de memorias
	uint16_t Counter_B=0;							//Contador de numero de paginas
	uint8_t Command[1];

////////////////////Data to send definition////////////////
for( Counter_D=0; Counter_D<32; Counter_D++){
				spiTxBuff[Counter_D]=0xAA;
}
///////////////////// WRITE MEMORIES //////////////////////

for (Counter_B=0;Counter_B< Total_pages; Counter_B++ ){

Command[0] = 0x02;			//Write command
Command_A = (Counter_B<<5);	//Page Direction
highdata = Command_A >> 8;	//Separa parte alta
lowdata = Command_A;		//Separa parte baja

CS_LOW(Mem_Num);
HAL_SPI_Transmit(&hspi1,(uint8_t*)&Command[0],1,25);
HAL_SPI_Transmit(&hspi1,(uint8_t*)&highdata,1,25);
HAL_SPI_Transmit(&hspi1,(uint8_t*)&lowdata,1,25);
HAL_SPI_Transmit(&hspi1,spiTxBuff,32,25); 	//Write data in 32B bursts
CS_HIGH(Mem_Num);

      			          }
	      		          }

void k256_send( uint8_t *buff, uint16_t buff_size, uint32_t timeout_ms){
	HAL_SPI_Transmit(&hspi1, buff, buff_size, timeout_ms);

}

void k256_receive( uint8_t *buff, uint16_t buff_size, uint32_t timeout_ms){
	HAL_SPI_Receive(&hspi1, buff, buff_size, timeout_ms);




}




#endif
