/*
 * board_crc.c
 *
 *	Note: take in count that the polynomial is fixed to 0x4C11DB7
 *	in STM32F1/F2/L1/F4/F0 series.
 *
 *  Created on: May 24, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */


// Include --------------------------------------------------------------------
#include "board_cfg_private.h"
#include "board_private.h"

// Macros & constants ---------------------------------------------------------

// Private variables declaration  ---------------------------------------------
typedef struct
{
	CRC_HandleTypeDef   CrcHandle;
}board_crc_t;

// Private variables definition  ----------------------------------------------
static board_crc_t board_crc;

// Private functions declaration  ---------------------------------------------

// Public functions definition  -----------------------------------------------
/*
 * @brief Initiate the CRC calculator.
 * */
ret_code_t Board_CRC_init(void)
{
	ret_code_t ret = RET_OK;
	board_crc.CrcHandle.Instance = CRC;

	if( HAL_CRC_Init(&board_crc.CrcHandle) != HAL_OK ) ret = RET_ERR;

	return ret;
}

/*
 * @brief Calculate the CRC of a buffer of bytes.
 * @param buff the buffer to which we will calculate the crc.
 * @param buff_size the size of the buffer
 * @param reset set to true if the crc calculator data register must be reset
 * before iniit the calculation.
 * @return the crc computation
 * @note the CRC hardware peripheral of the STM32 computes 32 bits crcs,
 * because this, we need to feed to the crc calculator a 32 bits block in
 * each computation.
 * */
uint32_t Board_CRC_calculate8(uint8_t *buff, size_t buff_size, bool reset, bool reverse_output, bool invert)
{
	if( NULL == buff ) return RET_ERR_NULL_POINTER;
	uint8_t *pbuff = buff;

	/* If necessary, reset CRC data register. */
	if(true == reset) board_crc.CrcHandle.Instance->CR = CRC_CR_RESET;

	board_crc.CrcHandle.Instance->CR = CRC_CR_RESET;
	/* Calculate the number of 32-bits blocks (divide by 4). */
	uint32_t cnt = buff_size >> 2;

	while( cnt-- )
	{
		board_crc.CrcHandle.Instance->DR = *(uint32_t*) pbuff; /* Set new value. */
		pbuff += 4; /* Go to the next 32 bit block. */
	}

	/* Calculate remainging data as 8-bit. */
	cnt = buff_size % 4; // get the remaining qty of bytes.

	while( cnt-- )
	{
		*((uint8_t*)&board_crc.CrcHandle.Instance->DR) = *pbuff++;
	}

	/* Reverse the order of the bits gived by the CRC hardware calculation unit of the STM32.*/

	return board_crc.CrcHandle.Instance->DR;
}

/*
 * @brief init low level hw for the HAL of the CRC.
 * */
void HAL_CRC_MspInit(CRC_HandleTypeDef *hcrc)
{
	__HAL_RCC_CRC_CLK_ENABLE();
}
