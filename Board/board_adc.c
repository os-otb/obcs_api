/*
 * board_adc.c
 *
 *  Created on: Apr 30, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Includes -------------------------------------------------------------------
#include "board_cfg_private.h"

// Macros & Constants --------------------------------------------------------
#define BOARD_ADC_ENALE_DMA_CLK()       __HAL_RCC_DMA2_CLK_ENABLE()

#define BOARD_ADC1_DMA_CHANNEL	DMA_CHANNEL_0 /* !< In STM32F4xx the ADC1 uses the channel 0 of DMA2. */
#define BOARD_ADC1_DMA_STREAM     DMA2_Stream0 /* !< In STM32F4xx the ADC1 can use the stream 0 of the channel 0 of DMA2. */
#define BOARD_ADC1_DMA_IRQn       DMA2_Stream0_IRQn
#define Board_ADC1_DMA_Stream_IRQHandler DMA2_Stream0_IRQHandler

#define BOARD_ADC3_DMA_CHANNEL	DMA_CHANNEL_2 /* !< In STM32F4xx the ADC3 uses the channel 2 of DMA2. */
#define BOARD_ADC3_DMA_STREAM     DMA2_Stream1 /* !< In STM32F4xx the ADC3 can use the stream 1 of the channel 2 of DMA2. */
#define BOARD_ADC3_DMA_IRQn       DMA2_Stream1_IRQn
#define Board_ADC3_DMA_Stream_IRQHandler DMA2_Stream1_IRQHandler

#define BOARD_ADC_MEASURE_GROUP_TIMEOUT_MS 30 /*!< Time to wait for a conversion! */

// Private variables declaration ---------------------------------------------
typedef struct
{
	struct
	{
	    ADC_HandleTypeDef hadc; /*!< ADC instance used to measure this group of inputs. */
	    DMA_HandleTypeDef hdma;	/*!< DMA instance used for this ADC. */
	    uint32_t samples[BOARD_ADC_MAX_INPUTS_PER_ADC]; /*!< In this buffer the DMA will store the samples of the ADC. */
	    IRQn_Type dma_irqn;
	    board_adc_input_t *inputs;	/*!< This list must be defined by the user in the config. */
	    bool conversion_done; 	/*!< Flag used to notify when a conversion is done with the ADC. */
	}inputs_group[BOARD_ADC_MAX_ADCS_USED];
	SemaphoreHandle_t mutex;	/*!< Mutex to acced to the samples of the ADCs. */
}board_adc_t;

// Private variables definition ----------------------------------------------
board_adc_t board_adc;

// Private functions declaration ---------------------------------------------

// Public functions definition -----------------------------------------------
/**
 * @brief configure the ADCs used to measure in continous mode the inputs
 * configured.
 * @return RET_OK if success.
 * TODO: we need to implement an initialized flag here, because if another API or
 * board module initialize it again can enter in conflict.
 */
ret_code_t board_adc_init(void)
{
	ret_code_t ret = RET_OK;
	/* GPIO Ports Clock Enable. We can init them all because won't hurt anybody :idk: */
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();
	__HAL_RCC_GPIOE_CLK_ENABLE();
	__HAL_RCC_GPIOH_CLK_ENABLE();
#if defined(STM32F407Z) || defined(STM32F407I)
	__HAL_RCC_GPIOF_CLK_ENABLE();
	__HAL_RCC_GPIOG_CLK_ENABLE();
#ifdef STM32F407I
	__HAL_RCC_GPIOI_CLK_ENABLE();
	__HAL_RCC_GPIOH_CLK_ENABLE();
#endif
#endif

	/* Initial config of the group that uses the ADC1 */
    __HAL_RCC_ADC1_CLK_ENABLE();
    board_adc.inputs_group[0].hadc.Instance = ADC1;
    board_adc.inputs_group[0].hdma.Instance = BOARD_ADC1_DMA_STREAM;
    board_adc.inputs_group[0].hdma.Init.Channel = BOARD_ADC1_DMA_CHANNEL;
    board_adc.inputs_group[0].dma_irqn = BOARD_ADC1_DMA_IRQn;
    board_adc.inputs_group[0].hadc.Init.NbrOfConversion = BOARD_ADC1_MAX_ANALOG_INPUTS;
    board_adc.inputs_group[0].inputs = board_adc1_inputs;

	/* Initial config of the group that uses the ADC3 */
	__HAL_RCC_ADC3_CLK_ENABLE();
	board_adc.inputs_group[1].hadc.Instance = ADC3;
	board_adc.inputs_group[1].hdma.Instance = BOARD_ADC3_DMA_STREAM;
	board_adc.inputs_group[1].hdma.Init.Channel = BOARD_ADC3_DMA_CHANNEL;
	board_adc.inputs_group[1].dma_irqn = BOARD_ADC3_DMA_IRQn;
    board_adc.inputs_group[1].hadc.Init.NbrOfConversion = BOARD_ADC3_MAX_ANALOG_INPUTS;
    board_adc.inputs_group[1].inputs = board_adc3_inputs;

	/* Enable the clock needed for the DMA */
	BOARD_ADC_ENALE_DMA_CLK();

    for(int i = 0; i < BOARD_ADC_MAX_ADCS_USED; i++)
    {
    	/* Configuration of the GPIOS of each input. */
    	GPIO_InitTypeDef GPIO_InitStruct;
		for(int c = 0; c < board_adc.inputs_group[i].hadc.Init.NbrOfConversion; c++)
		{
	        GPIO_InitStruct.Pin = board_adc.inputs_group[i].inputs[c].gpin;
	        GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	        GPIO_InitStruct.Pull = GPIO_NOPULL;
	        HAL_GPIO_Init(board_adc.inputs_group[i].inputs[c].gport, &GPIO_InitStruct);
		}

    	/* Configuration of the ADC itself. */
    	board_adc.inputs_group[i].hadc.Init.ClockPrescaler = ADC_CLOCKPRESCALER_PCLK_DIV2;
    	board_adc.inputs_group[i].hadc.Init.Resolution = ADC_RESOLUTION_12B;
    	board_adc.inputs_group[i].hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
    	board_adc.inputs_group[i].hadc.Init.ScanConvMode = ENABLE; /*!< We are measuring in SCAN mode! */
    	board_adc.inputs_group[i].hadc.Init.EOCSelection = ADC_EOC_SEQ_CONV; /*!< The end of the conversion is when all the sequence is done! */
    	board_adc.inputs_group[i].hadc.Init.NbrOfDiscConversion = 1;
    	board_adc.inputs_group[i].hadc.Init.ContinuousConvMode = DISABLE/*ENABLE*/; /*!< ADC will measure the inputs continously. */
    	board_adc.inputs_group[i].hadc.Init.DiscontinuousConvMode = DISABLE;
    	board_adc.inputs_group[i].hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
    	board_adc.inputs_group[i].hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
    	board_adc.inputs_group[i].hadc.Init.DMAContinuousRequests = ENABLE; /* This allows DMA channel to collect converted data from ADC and transfer to RAM. */

    	if(HAL_ADC_Init(&board_adc.inputs_group[i].hadc) != HAL_OK) ret = RET_ERR;
    	else
    	{	/* Now config the channels attached to the ADC. The ADC is configured
    		 * in multichannel scan conversion mode, so will measure each configured channel in a
    		 * defined order. This order is equal to the "Rank" set for the channel. In example, if we want that
    		 * the sensor "M" be measured first we need to set its "Rank" as 1.*/
    		ADC_ChannelConfTypeDef sConfig;
    		for(int c = 0; c < board_adc.inputs_group[i].hadc.Init.NbrOfConversion; c++)
    		{
    			sConfig.Channel = board_adc.inputs_group[i].inputs[c].channel;
    			/*!< the conversion rank (a.k.a. conversion order is the ID of the sensor. Note that
    			 * the adc sensor id must start from 0 (for this reason is the +1 here. */
    			sConfig.Rank = board_adc.inputs_group[i].inputs[c].sensor_id+1;
    			sConfig.SamplingTime = ADC_SAMPLETIME_15CYCLES/*ADC_SAMPLETIME_3CYCLES*/;
    			sConfig.Offset = 0;
    	    	if(HAL_ADC_ConfigChannel(&board_adc.inputs_group[i].hadc, &sConfig) != HAL_OK) ret = RET_ERR;
    		}

			// Configure DMA --------------------
			board_adc.inputs_group[i].hdma.Init.Direction = DMA_PERIPH_TO_MEMORY;
			board_adc.inputs_group[i].hdma.Init.PeriphInc = DMA_PINC_DISABLE;
			board_adc.inputs_group[i].hdma.Init.MemInc = DMA_MINC_ENABLE;
			board_adc.inputs_group[i].hdma.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
			board_adc.inputs_group[i].hdma.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;
			board_adc.inputs_group[i].hdma.Init.Mode = DMA_CIRCULAR;
			board_adc.inputs_group[i].hdma.Init.Priority = DMA_PRIORITY_HIGH;
			board_adc.inputs_group[i].hdma.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
			board_adc.inputs_group[i].hdma.Init.FIFOThreshold = DMA_FIFO_THRESHOLD_FULL/*DMA_FIFO_THRESHOLD_HALFFULL*/;
			board_adc.inputs_group[i].hdma.Init.MemBurst = DMA_MBURST_SINGLE;
			board_adc.inputs_group[i].hdma.Init.PeriphBurst = DMA_PBURST_SINGLE;

			if( HAL_OK != HAL_DMA_Init(&board_adc.inputs_group[i].hdma) ) ret = RET_ERR;
			else
			{
				/* Associate the initialized DMA handle to the the ADC handle */
				__HAL_LINKDMA(&board_adc.inputs_group[i].hadc, DMA_Handle, board_adc.inputs_group[i].hdma);
				 /* NVIC configuration for DMA transfer complete interrupt */
				HAL_NVIC_SetPriority(board_adc.inputs_group[i].dma_irqn, 0, 0);
				HAL_NVIC_EnableIRQ(board_adc.inputs_group[i].dma_irqn);
			}
    	}
    }
	return ret;
}

/**
 * @brief get the last samples from an ADC.
 * @param adc the ADC to get the last samples (ADC1, ADC2 or ADC3).
 * @param buff the buffer where the samples will be stored.
 * @param buff_size the size of the buffer where the samples will be stored.
 * @param block if true, the function will block forever until the ADC conversion is done.
 * If false, the conversion will break when a timeout is reached.
 * @return RET_OK if success.
 */
ret_code_t board_adc_measure(ADC_TypeDef *adc, float *buff, size_t buff_size, bool block)
{
	if( (NULL == buff) || (NULL == adc) ) return RET_ERR;
	HAL_StatusTypeDef hal_ret = HAL_OK;
	ret_code_t ret = RET_OK;

	for( int c = 0; c < BOARD_ADC_MAX_ADCS_USED; c++ )
	{
		if( adc == board_adc.inputs_group[c].hadc.Instance )
		{
			hal_ret = HAL_ADC_Start_DMA(&board_adc.inputs_group[c].hadc, \
										board_adc.inputs_group[c].samples, \
										board_adc.inputs_group[c].hadc.Init.NbrOfConversion);
			if( HAL_OK != hal_ret ) ret = RET_ERR;
			else
			{	/* If the call was non-blocking then wait for conversion until a timeout. */
				if( false == block )
				{
					uint32_t init_time = TICKS_TO_MS(xTaskGetTickCount());
					while( false == board_adc.inputs_group[c].conversion_done )
					{
						if( BOARD_ADC_MEASURE_GROUP_TIMEOUT_MS < (TICKS_TO_MS(xTaskGetTickCount()) - init_time) )
						{
							ret = RET_ERR_TIMEOUT;
							break;
						}
						else
						{
							vTaskDelay(10);
						}
					}
				}
				else
				{ /* If the call was in blocking mode, wait until the conversion is done!. */
					while( false == board_adc.inputs_group[c].conversion_done ) {};
				}

				board_adc.inputs_group[c].conversion_done = false;

				for(int i = 0; i < (buff_size/sizeof(float)); i++)
				{
					buff[i] = __LL_ADC_CALC_DATA_TO_VOLTAGE(	ADC_VREF_MV,\
															board_adc.inputs_group[c].samples[i],\
															LL_ADC_RESOLUTION_12B);
				}
			}
			break;
		}
	}
	return ret;
}

/**
 * @brief start to measuring all the inputs of each ADC.
 * @return RET_OK if success.
 **/
ret_code_t board_adc_start_all(void)
{
	ret_code_t ret = RET_OK;
	HAL_StatusTypeDef hal_ret = HAL_OK;
	for(int i = 0; i < BOARD_ADC_MAX_ADCS_USED; i++)
	{
		hal_ret = HAL_ADC_Start_DMA(&board_adc.inputs_group[i].hadc, \
									board_adc.inputs_group[i].samples, \
									board_adc.inputs_group[i].hadc.Init.NbrOfConversion);
		if( HAL_OK != hal_ret ) ret = RET_ERR;
	}
	return ret;
}

/**
 * @brief stop all the ADCs.
 * @return RET_OK if success.
 **/
ret_code_t board_adc_stop_all(void)
{
	ret_code_t ret = RET_OK;
	HAL_StatusTypeDef hal_ret = HAL_OK;
	for(int i = 0; i < BOARD_ADC_MAX_ADCS_USED; i++)
	{
		hal_ret = HAL_ADC_Stop_DMA(&board_adc.inputs_group[i].hadc);
		if( HAL_OK != hal_ret ) ret = RET_ERR;
	}
	return ret;
}

// Private functions definition -----------------------------------------------
/* IRQn handlers! ---------------------------------------------------------- */
/**
 * @brief  This function handles DMA interrupt request. The stream 0 of the
 * DMA2 is used by the ADC3 in this module.
 */
void Board_ADC1_DMA_Stream_IRQHandler(void)
{
	HAL_DMA_IRQHandler(board_adc.inputs_group[0].hadc.DMA_Handle);
}

/**
 * @brief  This function handles DMA interrupt request. The stream 1 of the
 * DMA2 is used by the ADC3 in this module.
 */
void Board_ADC3_DMA_Stream_IRQHandler(void)
{
	HAL_DMA_IRQHandler(board_adc.inputs_group[1].hadc.DMA_Handle);
}

/**
 * @brief callback executed when a conversion is done!
 */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	for( int c=0; c < BOARD_ADC_MAX_ADCS_USED; c++)
	{

		if( board_adc.inputs_group[c].hadc.Instance == hadc->Instance )
		{
			board_adc.inputs_group[c].conversion_done = true;
		}
	}
}
