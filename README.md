# OBCs API
This API was conceived to be used in the OTB project and the OBC of OpenSpace 1.0 payload. Its divided in two big parts:
* API: must be agnostic of the MCU used. The principle is that this API must be used for various boards.
* Board: are all the things that are linked to the hardware. Mostly, uses the HAL or Low Level drivers of an MCU.

Initially, the target of this API was an STM32F4xx MCU, but was designed to be easily ported changing the Board module.

## Configuration
All the API can be configured with two configuration files that must be created by the user:
* api_config: here is all the configuration of the APIs (i.e. the IP of the server FTP if it is used).
* board_config: here is all the configuration of the board (i.e. the GPIOs pinout of the MCU).

## Network module
In this module are all the APIs relationated with network:
* common: here are all the things that the different protocols implementations have in common (i.e. things relationated with IP layer)
* ftp: a implementation of a FTP server using FreeRTOS+TCP demo and FreeRTOS+FAT as filesystem.
* tcp: a implementation of a TCP server creator using FreeRTOS+TCP. Is used by the other applications protocols in this API.

## Tests!
This API uses Unity to unit tests some modules (in the future must be all!!!). To run the tests just run `make`. If you want to print in the stdout the logs of the modules then type `make CFLAGS+=-DUNIT_TESTS_LOGS_ENABLED`.