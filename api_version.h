/*
 * version.h
 *
 *  Created on: Aug 29, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef OBCS_API_API_VERSION_H_
#define OBCS_API_API_VERSION_H_

// API Version ----------------------------------------------------------------
#define API_VERSION					"100"	/* !< The version of the actual API. */


#endif /* OBCS_API_API_VERSION_H_ */
