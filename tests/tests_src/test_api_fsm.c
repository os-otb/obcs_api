/*
 * test_api_fsm.c
 *
 * In this file we are testing if the api_fsm reproduce the expected results
 * with a simple state machine. The test machine under test is the following:
 *                           ↙―――――――↖ 
 *                  ↙―――( A )―――↘    |
 *                  |           |    |
 *                 ev1         ev2  ev3
 *                  ↓           ↓    |
 *                ( B )――ev4―→( C )――↗
 * 
 * Every state have a "on_entry_value". When the fsm enters to the state, 
 * each event sets this "on_entry_value" in one determined value. When
 * the fsm exits this state, this value is cleared **if the state got a 
 * on_exit handler**. When the fsm make a 'periodic' work, increments the
 * "on_enty_value" in the value passed as parameter to the api_fsm_work 
 * function.
 *  
 *  Created on: Aug 13, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Includes -------------------------------------------------------------------
#include "unity.h"
#include "api_fsm.h"

// Stub functions for Unity ---------------------------------------------------
void setUp(void)
{

}

void tearDown(void)
{

}

// Creation of the fsm --------------------------------------------------------
typedef enum
{
    STATE_A = 0,
    STATE_B,
    STATE_C,
    MAX_STATES /*!< max states on the fsm. */
}test_fsm_state_id_t;

typedef enum
{
    EVENT_1 = 0,
    EVENT_2,
    EVENT_3,
    EVENT_4,
    MAX_EVENTS /*!< max events on the fsm. */
}test_fsm_state_events_t;

/* State A handlers. */
static int state_a_on_entry_value = 0;
ret_code_t test_fsm_state_a_on_entry_handler(void *arg)
{
	if( NULL != arg ) state_a_on_entry_value = (int) arg;
	return RET_OK;
}

ret_code_t test_fsm_state_a_on_exit_handler(void)
{
	state_a_on_entry_value = 0;
	return RET_OK;
}

ret_code_t test_fsm_state_a_ongoing_handler(void *arg)
{
	if( NULL != arg ) state_a_on_entry_value += (int) arg;
	return RET_OK;
}

/* State B handlers. */
static int state_b_on_entry_value = 0;
ret_code_t test_fsm_state_b_on_entry_handler(void *arg)
{
	if( NULL != arg ) state_b_on_entry_value = (int) arg;
	return RET_OK;
}

ret_code_t test_fsm_state_b_ongoing_handler(void *arg)
{
	if( NULL != arg ) state_b_on_entry_value += (int) arg;
	return RET_OK;
}

/* State C handlers. */
static int state_c_on_entry_value = 0;
ret_code_t test_fsm_state_c_on_entry_handler(void *arg)
{
	if( NULL != arg ) state_c_on_entry_value = (int) arg;
	return RET_OK;
}

ret_code_t test_fsm_state_c_on_exit_handler(void)
{
	state_c_on_entry_value = 0;
	return RET_OK;
}

/* Definition of the fsm states. */
api_fsm_state_t test_fsm_states[] = 
{
    API_FSM_DEFINE_STATE(STATE_A,   test_fsm_state_a_on_entry_handler,
                                    test_fsm_state_a_ongoing_handler,
                                    test_fsm_state_a_on_exit_handler ),
    API_FSM_DEFINE_STATE(STATE_B,   test_fsm_state_b_on_entry_handler,
                                    test_fsm_state_b_ongoing_handler,
                                    NULL ),
    API_FSM_DEFINE_STATE(STATE_C,   test_fsm_state_c_on_entry_handler,
                                    NULL,
                                    test_fsm_state_c_on_exit_handler ), 
};

/* Definition of the fsm transition map. */
api_fsm_transition_t test_fsm_transition_map[] = 
{
    API_FSM_DEFINE_TRANSITION_MAP_ITEM(STATE_A, EVENT_1, STATE_B),
    API_FSM_DEFINE_TRANSITION_MAP_ITEM(STATE_A, EVENT_2, STATE_C),
    API_FSM_DEFINE_TRANSITION_MAP_ITEM(STATE_C, EVENT_3, STATE_A),
    API_FSM_DEFINE_TRANSITION_MAP_ITEM(STATE_B, EVENT_4, STATE_C),
};

api_fsm_t test_fsm = 
{
    .state_table = test_fsm_states,
    .states_qty = MAX_STATES,
    .actual_state = &test_fsm_states[0],
    .transition_map = test_fsm_transition_map,
	.transitions_qty = MAX_EVENTS
};

// Test body ------------------------------------------------------------------
/*
 * @brief process event 1 transition. If we are in the state A, then the event
 * is valid and we must go to STATE_B. Otherwise, we must remain in the same
 * state. */
void test_ev1(void)
{
	uint32_t actual_state_id = test_fsm.actual_state->id;
	/* If we are in the state A, then the event is valid and we must go to state B.
	 * Otherwise, the state must be the same. */
	uint32_t expected_next_state_id = actual_state_id;
	if( actual_state_id == STATE_A ) expected_next_state_id = STATE_B;

	api_fsm_process_event(&test_fsm, EVENT_1, (void*) 10);
	actual_state_id = test_fsm.actual_state->id;
	TEST_ASSERT_EQUAL_UINT32_MESSAGE(expected_next_state_id, actual_state_id, "unexpected state!!");
	/* If the transition is valid the test continues. So, we must check if
	 * the values of the state B and A are correct. This ones are modificated
	 * by the on_entry, ongoing and on_exit handlers. */
	if( STATE_B == actual_state_id )
	{
		TEST_ASSERT_EQUAL_UINT32_MESSAGE(10, state_b_on_entry_value, "the on_entry function of state b didn't work!"); // this is the value passed by te event
		TEST_ASSERT_EQUAL_UINT32_MESSAGE(0, state_a_on_entry_value, "the on_exit function of state a didn't work!"); // the on_exit function clears the value
	}
}

/*
 * @brief process event 2 transition. If we are in the state A, then the event
 * is valid and we must go to STATE_C. Otherwise, we must remain in the same
 * state. */
void test_ev2(void)
{
	uint32_t actual_state_id = test_fsm.actual_state->id;
	/* If we are in the state A, then the event is valid and we must go to state B.
	 * Otherwise, the state must be the same. */
	uint32_t expected_next_state_id = actual_state_id;
	if( actual_state_id == STATE_A ) expected_next_state_id = STATE_C;

	api_fsm_process_event(&test_fsm, EVENT_2, (void*) 20);
	actual_state_id = test_fsm.actual_state->id;
	TEST_ASSERT_EQUAL_UINT32_MESSAGE(expected_next_state_id, actual_state_id, "unexpected state!!");
	/* If the transition is valid the test continues. So, we must check if
	 * the values of the state C and A are correct. This ones are modificated
	 * by the on_entry, ongoing and on_exit handlers. */
	if( STATE_C == actual_state_id )
	{
		TEST_ASSERT_EQUAL_UINT32_MESSAGE(20, state_c_on_entry_value, "the on_entry function of state c didn't work!"); // this is the value passed by te event
		TEST_ASSERT_EQUAL_UINT32_MESSAGE(0, state_a_on_entry_value, "the on_exit function of state a didn't work!"); // the on_exit function clears the value
	}
}

/*
 * @brief process event 3 transition. If we are in the state C, then the event
 * is valid and we must go to STATE_A. Otherwise, we must remain in the same
 * state. */
void test_ev3(void)
{
	uint32_t actual_state_id = test_fsm.actual_state->id;
	/* If we are in the state A, then the event is valid and we must go to state B.
	 * Otherwise, the state must be the same. */
	uint32_t expected_next_state_id = actual_state_id;
	if( actual_state_id == STATE_C ) expected_next_state_id = STATE_A;

	api_fsm_process_event(&test_fsm, EVENT_3, (void*) 30);
	actual_state_id = test_fsm.actual_state->id;
	/* Test if the actual state is the correct one. */
	TEST_ASSERT_EQUAL_UINT32_MESSAGE(expected_next_state_id, actual_state_id, "unexpected state!!");
	/* If the transition is valid the test continues. So, we must check if
	 * the values of the state C and A are correct. This ones are modificated
	 * by the on_entry, ongoing and on_exit handlers. */
	if( STATE_A == actual_state_id )
	{
		TEST_ASSERT_EQUAL_UINT32_MESSAGE(30, state_a_on_entry_value, "the on_entry function of state a didn't work!"); // this is the value passed by te event
		TEST_ASSERT_EQUAL_UINT32_MESSAGE(0, state_c_on_entry_value, "the on_exit function of state c didn't work!"); // the on_exit function clears the value	
	}
}

/*
 * @brief process event 4 transition. If we are in the state B, then the event
 * is valid and we must go to STATE_C. Otherwise, we must remain in the same
 * state. */
void test_ev4(void)
{
	uint32_t actual_state_id = test_fsm.actual_state->id;
	/* If we are in the state A, then the event is valid and we must go to state B.
	 * Otherwise, the state must be the same. */
	uint32_t expected_next_state_id = actual_state_id;
	if( actual_state_id == STATE_B ) expected_next_state_id = STATE_C;

	api_fsm_process_event(&test_fsm, EVENT_4, (void*) 40);
	actual_state_id = test_fsm.actual_state->id;
	TEST_ASSERT_EQUAL_UINT32_MESSAGE(expected_next_state_id, actual_state_id, "unexpected state!!");
	/* If the transition is valid the test continues. So, we must check if
	 * the values of the state C and B are correct. This ones are modificated
	 * by the on_entry, ongoing and on_exit handlers. */
	if( STATE_C == actual_state_id )
	{
		TEST_ASSERT_EQUAL_UINT32_MESSAGE(40, state_c_on_entry_value, "the on_entry function of state c didn't work!"); // this is the value passed by te event
		TEST_ASSERT_EQUAL_UINT32_MESSAGE(/*10*/12, state_b_on_entry_value, "the on_exit function of state b didn't work!"); // the state b doesn't have an on_exit function, so the value must not be cleared.
	}
}

void test_fsm_work(void)
{
	int *ptr_state_on_entry_value = NULL;	/* !< a pointer to the on_entry value of the state. */
	uint32_t expected_state_on_entry_value = 0; /* !< expected value of the on_entry var of the state. */
	uint32_t work_arg = 2; /* !< number passed as argument to the work function. */
	switch(test_fsm.actual_state->id)
	{
		case STATE_A:
			ptr_state_on_entry_value = &state_a_on_entry_value;
			TEST_ASSERT_NOT_NULL_MESSAGE(ptr_state_on_entry_value, "NULL pointer in state_a_on_entry_value (test itself error!)");
			expected_state_on_entry_value = *(ptr_state_on_entry_value) + work_arg;
		break;
		case STATE_B:
			ptr_state_on_entry_value = &state_b_on_entry_value;
			TEST_ASSERT_NOT_NULL_MESSAGE(ptr_state_on_entry_value, "NULL pointer in state_b_on_entry_value (test itself error!)");
			expected_state_on_entry_value = *(ptr_state_on_entry_value) + work_arg;
		break;
		case STATE_C:
			ptr_state_on_entry_value = &state_c_on_entry_value;
			TEST_ASSERT_NOT_NULL_MESSAGE(ptr_state_on_entry_value, "NULL pointer in state_c_on_entry_value (test itself error!)");
			expected_state_on_entry_value = *ptr_state_on_entry_value;
		break;
		default:
		/* TODO: Add something to assert the test. */
		break;
	}
	api_fsm_work(&test_fsm, (void*) work_arg);
	TEST_ASSERT_EQUAL_UINT32_MESSAGE(expected_state_on_entry_value, *ptr_state_on_entry_value, "error in api_fsm_work function!");
}

/*
 * @brief the main tries to trigger some events and checks if the current state
 * and value of the state are the expected ones.
 * */
int main(void)
{
UNITY_BEGIN();
RUN_TEST(test_ev1);	/* Trigger event 1. Go from STATE_A to STATE_B. */
RUN_TEST(test_ev3);	/* Trigger event 2. Invalid transition! Must remain in the same state!. */
RUN_TEST(test_fsm_work); /* Test a work loop of the fsm in the STATE_B. */
RUN_TEST(test_ev4);	/* Trigger event 4. Go from STATE_B to STATE_C. */
RUN_TEST(test_ev1);	/* Trigger event 1. Invalid transition! Must remain in the same state!. */
RUN_TEST(test_ev3);	/* Trigger event 3. Go from STATE_C to STATE_A. */
RUN_TEST(test_fsm_work); /* Test a work loop of the fsm in the STATE_A. */
RUN_TEST(test_ev4);	/* Trigger event 4. Invalid transition! Must remain in the same state!. */
RUN_TEST(test_ev2);	/* Trigger event 2. Go from STATE_A to STATE_C. */
RUN_TEST(test_fsm_work); /* Test a work loop of the fsm in the STATE_C. */
return UNITY_END();
}