/*
 * test_api_fsm.c
 *
 * In this file we are testing if the api_fsm reproduce the expected results
 * with a simple state machine. The test machine under test is the following:
 *                           ↙―――――――↖ 
 *                  ↙―――( A )―――↘    |
 *                  |           |    |
 *                 ev1         ev2  ev3
 *                  ↓           ↓    |
 *                ( B )――ev4―→( C )――↗
 * 
 * Every state have a "on_entry_value". When the fsm enters to the state, 
 * each event sets this "on_entry_value" in one determined value. When
 * the fsm exits this state, this value is cleared **if the state got a 
 * on_exit handler**. When the fsm make a 'periodic' work, increments the
 * "on_enty_value" in the value passed as parameter to the api_fsm_work 
 * function.
 *  
 *  Created on: Aug 13, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Includes -------------------------------------------------------------------
#include "unity.h"
#include "api_osotb_commands.h"
#include "api_osotb_types.h"

// Stub functions for Unity ---------------------------------------------------
void setUp(void)
{

}

void tearDown(void)
{

}

// Test OSOTB requests --------------------------------------------------------

/* GET to the OP_MODE resource packet. */
const char *get_opmode_req = "{\"header\":{\"ver\":\"100\",\"method\":\"GET\",\"resource\":\"OP_MODE\"},\"payload\":\"\"}";
/* GET to the UPTIME resource packet. */
const char *get_uptime_req = "{\"header\":{\"ver\":\"100\",\"method\":\"GET\",\"resource\":\"UPTIME\"},\"payload\":\"\"}";
/* PUT to the OP_MODE resource packet. Set the OP_MODE to OP_MODE_B. */
const char *put_opmodeb_req = "{\"header\":{\"ver\":\"100\",\"method\":\"PUT\",\"resource\":\"OP_MODE\"},\"payload\":\"OP_MODE_B\"}";
/* PUT to the UPTIME resource packet. The OSOTB server of the test doesn't have a PUT handler for this resource. */
const char *put_uptime_req = "{\"header\":{\"ver\":\"100\",\"method\":\"PUT\",\"resource\":\"UPTIME\"},\"payload\":\"116000232\"}";
/* Erroneous OSOTB request. */
const char *dummy1_osotb_req = "{\"heider\":{\"ver\":\"100\",\"method\":\"PUT\",\"resource\":\"UPTIME\"},\"payload\":\"116000232\"}";
const char *dummy2_osotb_req = "{\"header\":{\"ver\":\"100\",\"methxd\":\"PUT\",\"resource\":\"UPTIME\"},\"payload\":\"116000232\"}";
const char *dummy3_osotb_req = "{\"header\":{\"ver\":\"100\",\"method\":\"PIT\",\"resource\":\"UPTIME\"},\"payload\":\"116000232\"}";
const char *dummy4_osotb_req = "{\"header\":{\"ver\":\"100\",\"method\":\"PUT\",\"resoxrce\":\"UPTIME\"},\"payload\":\"116000232\"}";
const char *dummy5_osotb_req = "{\"header\":{\"ver\":\"100\",\"method\":\"PUT\",\"resource\":\"APTIME\"},\"payload\":\"116000232\"}";
const char *dummy6_osotb_req = "{\"header\":{\"v\"\"400x0wer\":\"1\"\"da20\"00\",\"met\"\"\"ndas052x2hod\":\"PUT\",\"r\"\"eso0v\"\"urce\":\"UPTIME\"},\"poyload\":\"116000232\"}";
const char *dummy7_osotb_req = "\"\"\"0x00050v\"\"{\"header\":{\"v\"\"400x0wer\":\"1\"\"da20\"00\",\"met\"\"\"ndas052x2hod\":\"PUT\",\"r\"\"eso0v\"\"urce\":\"UPTIME\"},\"poyload\":\"116000232\"}\"\"dasx0x4000200\"";

// Resources of the Device Under Test -----------------------------------------
enum
{
	OP_MODE_A = 0,
	OP_MODE_B,
	OP_MODE_C,
	OP_MODE_D,
	MAX_OP_MODES,	/*!< Max number of op modes. */
};

uint32_t op_mode = OP_MODE_A;

ret_code_t osotb_rx_handler_get_uptime(char *payload, char *payload_response);
ret_code_t osotb_rx_handler_get_uptime(char *payload, char *payload_response)
{
	if( NULL == payload_response ) return RET_ERR_NULL_POINTER;
	ret_code_t ret = RET_OK;
	(void) payload;
	strcpy(payload_response, "1000032");	// The uptime is a well known value forever in this test
	return ret;
}

/**
 * @brief just copy to the buffer payload_response the correspondent operation mode.
 * @param payload the payload received, is a don't care in this case.
 * @param payload_response the response to the request. In this case is the 
 * actual mode of operation (value of op_mode). 
 */
ret_code_t osotb_rx_handler_get_op_mode(char *payload, char *payload_response);
ret_code_t osotb_rx_handler_get_op_mode(char *payload, char *payload_response)
{
	if( NULL == payload_response ) return RET_ERR_NULL_POINTER;
	ret_code_t ret = RET_OK;
	(void) payload;
	switch(op_mode)
	{
		case OP_MODE_A:
		strcpy(payload_response, "OP_MODE_A");
		break;
		case OP_MODE_B:
		strcpy(payload_response, "OP_MODE_B");
		break;
		case OP_MODE_C:
		strcpy(payload_response, "OP_MODE_C");
		break;
		case OP_MODE_D:
		strcpy(payload_response, "OP_MODE_D");
		break;
		default:
		strcpy(payload_response, "ERR: unknown op mode");
		break;
	}
	return ret;
}

/**
 * @brief Set the operation mode depending on the value of the payload.
 * @param payload the payload received, must be a operation mode.
 * @param payload_response the response to the request. In this case is the 
 * actual mode of operation (value of op_mode). 
 */
ret_code_t osotb_rx_handler_put_op_mode(char *payload, char *payload_response);
ret_code_t osotb_rx_handler_put_op_mode(char *payload, char *payload_response)
{
	if( (NULL == payload) || (NULL == payload_response) ) return RET_ERR_NULL_POINTER;
	ret_code_t ret = RET_OK;
	if( 0 == strcmp(payload, "OP_MODE_A") ) {op_mode = OP_MODE_A; strcpy(payload_response, "OP_MODE_A");}
	if( 0 == strcmp(payload, "OP_MODE_B") ) {op_mode = OP_MODE_B; strcpy(payload_response, "OP_MODE_B");}
	if( 0 == strcmp(payload, "OP_MODE_C") ) {op_mode = OP_MODE_C; strcpy(payload_response, "OP_MODE_C");}
	if( 0 == strcmp(payload, "OP_MODE_D") ) {op_mode = OP_MODE_D; strcpy(payload_response, "OP_MODE_D");}
	return ret;
}

api_osotb_rx_handler_table_item_t rx_table[] =
{
	API_OSOTB_RX_HANDLER_TABLE_SET_ITEM(API_OSOTB_METHOD_GET, "UPTIME", osotb_rx_handler_get_uptime),
	API_OSOTB_RX_HANDLER_TABLE_SET_ITEM(API_OSOTB_METHOD_GET, "OP_MODE", osotb_rx_handler_get_op_mode),
	API_OSOTB_RX_HANDLER_TABLE_SET_ITEM(API_OSOTB_METHOD_PUT, "OP_MODE", osotb_rx_handler_put_op_mode),
};

#define OSOTB_RX_SIZEOF_TABLE (sizeof(rx_table) / sizeof(rx_table[0]))

// Test body ------------------------------------------------------------------
/**
 * @brief check if the GET to the OP_MODE resources succeed! This resource will have a
 * well known value.
 */
void process_get_op_mode_cmd(void)
{
	ret_code_t ret = RET_OK;
	char expected_op_mode[10];
	switch(op_mode)
	{
		case OP_MODE_A:
		strcpy(expected_op_mode, "OP_MODE_A");
		break;
		case OP_MODE_B:
		strcpy(expected_op_mode, "OP_MODE_B");
		break;
		case OP_MODE_C:
		strcpy(expected_op_mode, "OP_MODE_C");
		break;
		case OP_MODE_D:
		strcpy(expected_op_mode, "OP_MODE_D");
		break;
		default:
		strcpy(expected_op_mode, "ERR: unknown op mode");
		break;
	}

	char buff_response[256];
	const char *buff = get_opmode_req;
	size_t buff_size = strlen(get_opmode_req);
	ret = api_osotb_rx_process_pkt(&rx_table, buff, buff_size, buff_response);
	TEST_ASSERT_EQUAL_UINT32_MESSAGE(RET_OK, ret, "error in api_osotb_rx!");
//	TEST_ASSERT_EQUAL_STRING_MESSAGE("OP_MODE", target_resource, "the resource detected wasn't the correct!");
//	TEST_ASSERT_EQUAL_STRING_MESSAGE(expected_op_mode, payload_response, "the returned op_mode wasn't the correct!");
}

/**
 * @brief check if the GET to the UPTIME resources succeed! This resource will have a
 * well known value.
 */
void process_get_uptime_cmd(void)
{
	ret_code_t ret = RET_OK;
	char buff_response[256];
	const char *buff = get_uptime_req;
	size_t buff_size = strlen(get_uptime_req);
	ret = api_osotb_rx_process_pkt(&rx_table, buff, buff_size, buff_response);
	TEST_ASSERT_EQUAL_UINT32_MESSAGE(RET_OK, ret, "error in api_osotb_rx!");
//	TEST_ASSERT_EQUAL_STRING_MESSAGE("UPTIME", target_resource, "the resource detected wasn't the correct!");
//	TEST_ASSERT_EQUAL_STRING_MESSAGE("1000032", payload_response, "the returned uptime wasn't the correct!");
}

/**
 * @brief check if a PUT to the OP_MODE resource makes effect. In this case,
 * set the OP_MODE to OP_MODE_B.
 */
void process_set_op_modeb_cmd(void)
{
	ret_code_t ret = RET_OK;
	char buff_response[256];
	const char *buff = put_opmodeb_req;
	size_t buff_size = strlen(put_opmodeb_req);
	ret = api_osotb_rx_process_pkt(&rx_table, buff, buff_size, buff_response);
	TEST_ASSERT_EQUAL_UINT32_MESSAGE(RET_OK, ret, "error in api_osotb_rx!");
//	TEST_ASSERT_EQUAL_STRING_MESSAGE("OP_MODE", target_resource, "the resource detected wasn't the correct!");
//	TEST_ASSERT_EQUAL_STRING_MESSAGE("OP_MODE_B", payload_response, "the returned op_mode wasn't the correct!");
}

/**
 * @brief check if an erroneous GET to the UPTIME resources don't trigger the RX handler!
 */
void process_erroneous_get_uptime_cmd(void)
{
	ret_code_t ret = RET_OK;
	char buff_response[256];
	/* Erroneous msg 1:*/
	const char *buff1 = dummy1_osotb_req;
	size_t buff1_size = strlen(dummy1_osotb_req);
	ret = api_osotb_rx_process_pkt(&rx_table, buff1, buff1_size, buff_response);
	TEST_ASSERT_EQUAL_UINT32_MESSAGE(RET_ERR_INVALID_PARAMS, ret, "error in api_osotb_rx!");
//	TEST_ASSERT_EQUAL_STRING_MESSAGE("", target_resource, "the resource detected wasn't the correct!");
//	TEST_ASSERT_EQUAL_STRING_MESSAGE("", payload_response, "the returned uptime wasn't the correct!");

	/* Erroneous msg 2:*/
	const char *buff2 = dummy2_osotb_req;
	size_t buff2_size = strlen(dummy2_osotb_req);
	ret = api_osotb_rx_process_pkt(&rx_table, buff2, buff2_size, buff_response);
	TEST_ASSERT_EQUAL_UINT32_MESSAGE(RET_ERR_INVALID_PARAMS, ret, "error in api_osotb_rx!");
//	TEST_ASSERT_EQUAL_STRING_MESSAGE("", target_resource, "the resource detected wasn't the correct!");
//	TEST_ASSERT_EQUAL_STRING_MESSAGE("", payload_response, "the returned uptime wasn't the correct!");

	/* Erroneous msg 3:*/
	const char *buff3 = dummy3_osotb_req;
	size_t buff3_size = strlen(dummy3_osotb_req);
	ret = api_osotb_rx_process_pkt(&rx_table, buff3, buff3_size, buff_response);
	TEST_ASSERT_EQUAL_UINT32_MESSAGE(RET_ERR_INVALID_PARAMS, ret, "error in api_osotb_rx!");
//	TEST_ASSERT_EQUAL_STRING_MESSAGE("", target_resource, "the resource detected wasn't the correct!");
//	TEST_ASSERT_EQUAL_STRING_MESSAGE("", payload_response, "the returned uptime wasn't the correct!");

	/* Erroneous msg 4:*/
	const char *buff4 = dummy4_osotb_req;
	size_t buff4_size = strlen(dummy4_osotb_req);
	ret = api_osotb_rx_process_pkt(&rx_table, buff4, buff4_size, buff_response);
	TEST_ASSERT_EQUAL_UINT32_MESSAGE(RET_ERR_INVALID_PARAMS, ret, "error in api_osotb_rx!");
//	TEST_ASSERT_EQUAL_STRING_MESSAGE("", target_resource, "the resource detected wasn't the correct!");
//	TEST_ASSERT_EQUAL_STRING_MESSAGE("", payload_response, "the returned uptime wasn't the correct!");

	/* Erroneous msg 5:*/
	const char *buff5 = dummy5_osotb_req;
	size_t buff5_size = strlen(dummy5_osotb_req);
	ret = api_osotb_rx_process_pkt(&rx_table, buff5, buff5_size, buff_response);
	TEST_ASSERT_EQUAL_UINT32_MESSAGE(RET_ERR_INVALID_PARAMS, ret, "error in api_osotb_rx!");
//	TEST_ASSERT_EQUAL_STRING_MESSAGE("", target_resource, "the resource detected wasn't the correct!");
//	TEST_ASSERT_EQUAL_STRING_MESSAGE("", payload_response, "the returned uptime wasn't the correct!");

	/* Erroneous msg 6:*/
	const char *buff6 = dummy6_osotb_req;
	size_t buff6_size = strlen(dummy6_osotb_req);
	ret = api_osotb_rx_process_pkt(&rx_table, buff6, buff6_size, buff_response);
	TEST_ASSERT_EQUAL_UINT32_MESSAGE(RET_ERR_INVALID_PARAMS, ret, "error in api_osotb_rx!");
//	TEST_ASSERT_EQUAL_STRING_MESSAGE("", target_resource, "the resource detected wasn't the correct!");
//	TEST_ASSERT_EQUAL_STRING_MESSAGE("", payload_response, "the returned uptime wasn't the correct!");

	/* Erroneous msg 7:*/
	const char *buff7 = dummy7_osotb_req;
	size_t buff7_size = strlen(dummy7_osotb_req);
	ret = api_osotb_rx_process_pkt(&rx_table, buff7, buff7_size, buff_response);
	TEST_ASSERT_EQUAL_UINT32_MESSAGE(RET_ERR_INVALID_PARAMS, ret, "error in api_osotb_rx!");
//	TEST_ASSERT_EQUAL_STRING_MESSAGE("", target_resource, "the resource detected wasn't the correct!");
//	TEST_ASSERT_EQUAL_STRING_MESSAGE("", payload_response, "the returned uptime wasn't the correct!");
}

/**
 * @brief check if the PUT to the UPTIME not crash! This resource didn't have
 * a handler for the PUT method.
 */
void process_put_uptime_cmd(void)
{
	ret_code_t ret = RET_OK;
	char buff_response[256];
	const char *buff = put_uptime_req;
	size_t buff_size = strlen(put_uptime_req);
	ret = api_osotb_rx_process_pkt(&rx_table, buff, buff_size, buff_response);
	TEST_ASSERT_EQUAL_UINT32_MESSAGE(RET_ERR_INVALID_PARAMS, ret, "error in api_osotb_rx!");
}

/*
 * @brief the main tries to trigger some events an#if 0
 * */
int main(void)
{
UNITY_BEGIN();
RUN_TEST(process_get_op_mode_cmd); /* Process a GET on this resource. */
RUN_TEST(process_get_uptime_cmd); /* Process a GET on this resource. */
RUN_TEST(process_set_op_modeb_cmd); /* Process a SET on this resource. */
RUN_TEST(process_erroneous_get_uptime_cmd); /* Process erroneous GETs on this resource.*/
RUN_TEST(process_put_uptime_cmd); /* Process a PUT on this resource.*/
return UNITY_END();
}