// Includes -------------------------------------------------------------------
#include "unity.h"
#include "api_crc.h"

// Stub functions for Unity ---------------------------------------------------
void setUp(void)
{

}

void tearDown(void)
{

}

// Tests ----------------------------------------------------------------------
#define CHAR_USED_TO_TEST_CRC_CCITT 'h'
#define CRC_CCITT_OF_TEST_CHAR      0xEDAE  /*https://www.lammertbies.nl/comm/info/crc-calculation*/
#define INITIAL_CRC_TO_TEST_CRC_CCITT_TO_A_BYTE 0

/**
 * @brief test the api_crc16_ccitt_byte function calculating the crc of a 
 * byte that we know its crc. 
 */
void test_api_crc16_ccitt_byte(void)
{
    uint16_t calculated_crc = api_crc16_ccitt_byte(INITIAL_CRC_TO_TEST_CRC_CCITT_TO_A_BYTE, CHAR_USED_TO_TEST_CRC_CCITT);
    TEST_ASSERT_EQUAL_UINT16_MESSAGE(CRC_CCITT_OF_TEST_CHAR, calculated_crc, "unexpected crc16 calculated for test char!!");
}   

#define BUFF_USED_TO_TEST_CRC_CCITT "{\"header\": {\"ver\": \"100\", \"method\": \"GET\", \"resource\": \"uptime\"}, \"payload\": null}"
#define CRC_CCITT_OF_TEST_BUFF      ((uint16_t) 0x3620)//0x4DEB  /*https://www.lammertbies.nl/comm/info/crc-calculation*/

#define BUFF2_USED_TO_TEST_CRC_CCITT "{\"header\": {\"ver\": \"100\", \"method\": \"GET\", \"resource\": \"op_mode\"}, \"payload\": null}"
#define CRC_CCITT_OF_TEST_BUFF2     ((uint16_t) 0xB7F4)//0xEEBB  /*https://www.lammertbies.nl/comm/info/crc-calculation*/

/*
 * @brief test the api_crc16_ccitt_buff function calculating the crc of a 
 * buffer that we kwnow its crc. */
void test_api_crc16_ccitt_buf(void)
{
    uint16_t calculated_crc = api_crc16_ccitt_buf(BUFF_USED_TO_TEST_CRC_CCITT, sizeof(BUFF_USED_TO_TEST_CRC_CCITT));
    TEST_ASSERT_EQUAL_UINT16_MESSAGE(CRC_CCITT_OF_TEST_BUFF, calculated_crc, "unexpected crc16 calculated for test buff!!");

    calculated_crc = api_crc16_ccitt_buf(BUFF2_USED_TO_TEST_CRC_CCITT, sizeof(BUFF2_USED_TO_TEST_CRC_CCITT));
    TEST_ASSERT_EQUAL_UINT16_MESSAGE(CRC_CCITT_OF_TEST_BUFF2, calculated_crc, "unexpected crc16 calculated for test buff2!!");
}

// Body -----------------------------------------------------------------------
int main(void)
{
UNITY_BEGIN();
RUN_TEST(test_api_crc16_ccitt_byte);
RUN_TEST(test_api_crc16_ccitt_buf);
return UNITY_END();
}