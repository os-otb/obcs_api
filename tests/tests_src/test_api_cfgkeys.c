/*
 * test_api_cfgkeys.c
 *
 * At the beginning, the test will try to find this file and will not find it.
 * Then the code will try to copy the default configurations and create the config
 * file.
 * Then the test will to get and set this config files.
 *
 *  Created on: Aug 26, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Includes -------------------------------------------------------------------
#include "unity.h"
#include "api_cfgkeys.h"

#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/statvfs.h>

// Stub functions for Unity ---------------------------------------------------
void setUp(void)
{

}

void tearDown(void)
{

}

// Defines and configuration of the module ------------------------------------
#define API_CFGKEYS_FILE_T FILE
#define API_CFGKEYS_FSTAT_T struct stat
#define API_CFGKEYS_STATVFS_T struct statvfs
#define API_CFGKEYS_mkdir mkdir
#define API_CFGKEYS_fopen fopen
#define API_CFGKEYS_fclose fclose
#define API_CFGKEYS_fread fread
#define API_CFGKEYS_fwrite fwrite
#define API_CFGKEYS_fstat fstat
#define API_CFGKEYS_statvfs statvfs
#define API_CFGKEYS_fgets fgets
#define API_CFGKEYS_DISK_PATH
#define API_CFGKEYS_DIR	API_CFGKEYS_DISK_PATH "/cfgkeys"
#define API_CFGKEYS_FILE "cfg.json"
#define API_CFGKEYS_STR_VAL_MAX_LEN 8

// Default config keys table --------------------------------------------------
enum
{
    CFGKEY_ID_APP_SLOT_DEF = 0,
    CFGKEY_ID_CURRENT_SAMPLING_FREQ_HZ,
    CFGKEY_ID_PWM_DUTY_CYCLE_PER,
    CFGKEY_ID_PD0_MAX_CURRENT_MA,
    CFGKEY_ID_PD1_MAX_CURRENT_MA,
    CFGKEY_ID_PD2_MAX_CURRENT_MA,
    CFGKEY_ID_MAX_KEYS, /*!< max number of keys. */
};

#define CFGKEY_DEF_VALUE_APP_SLOT_DEF               0u
#define CFGKEY_DEF_VALUE_CURRENT_SAMPLING_FREQ_HZ   10u
#define CFGKEY_DEF_VALUE_PWM_DUTY_CYCLE_PER         70u
#define CFGKEY_DEF_VALUE_PD0_MAX_CURRENT_MA         1500u
#define CFGKEY_DEF_VALUE_PD1_MAX_CURRENT_MA         2000u
#define CFGKEY_DEF_VALUE_PD2_MAX_CURRENT_MA         763u

static api_cfgkey_t cfgkeys_table[] =
{
    API_CFGKEYS_DEFINE_TABLE_ITEM(CFGKEY_ID_APP_SLOT_DEF, CFGKEY_DEF_VALUE_APP_SLOT_DEF),
    API_CFGKEYS_DEFINE_TABLE_ITEM(CFGKEY_ID_CURRENT_SAMPLING_FREQ_HZ, CFGKEY_DEF_VALUE_CURRENT_SAMPLING_FREQ_HZ),
    API_CFGKEYS_DEFINE_TABLE_ITEM(CFGKEY_ID_PWM_DUTY_CYCLE_PER, CFGKEY_DEF_VALUE_PWM_DUTY_CYCLE_PER),
    API_CFGKEYS_DEFINE_TABLE_ITEM(CFGKEY_ID_PD0_MAX_CURRENT_MA, CFGKEY_DEF_VALUE_PD0_MAX_CURRENT_MA),
    API_CFGKEYS_DEFINE_TABLE_ITEM(CFGKEY_ID_PD1_MAX_CURRENT_MA, CFGKEY_DEF_VALUE_PD1_MAX_CURRENT_MA),
    API_CFGKEYS_DEFINE_TABLE_ITEM(CFGKEY_ID_PD2_MAX_CURRENT_MA, CFGKEY_DEF_VALUE_PD2_MAX_CURRENT_MA),
};

// Test body ------------------------------------------------------------------

/*
 * @brief
 * */
int main(void)
{
UNITY_BEGIN();
RUN_TEST(test_fsm_work); /* Test a work loop of the fsm in the STATE_C. */
return UNITY_END();
}