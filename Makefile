##
# This Makefile was intended to unit test the OBCS_API.
# Makes use of the Unity library that must be in another repository
# (obcs_tools repository indeed.)
# As a reference, we can see: http://www.throwtheswitch.org/build/make
#
#  Created on: Aug 13, 2021
#      Author: Julian Rodriguez aka Marifante
#		Email: jnrodriguezz@hotmail.com
#		Gitlab: https://gitlab.com/Marifante
#		Github: https://github.com/Marifante
##
#We try to detect the OS we are running on, and adjust commands as needed
ifeq ($(OSTYPE),cygwin)
	CLEANUP = rm -f
	CLEANUP_DIR = rm -rf
	MKDIR = mkdir -p
	TARGET_EXTENSION=.out
elseifeq ($(OSTYPE),msys)
	CLEANUP = rm -f
	CLEANUP_DIR = rm -rf
	MKDIR = mkdir -p
	TARGET_EXTENSION=.exe
elseifeq ($(OS),Windows_NT)
	CLEANUP = del /F /Q
	MKDIR = mkdir
	TARGET_EXTENSION=.exe
else
	CLEANUP = rm -f
	CLEANUP_DIR = rm -rf
	MKDIR = mkdir -p
	TARGET_EXTENSION=.out
endif

COMPILE=gcc

override CFLAGS += -std=c99
override CFLAGS += -Wall
override CFLAGS += -Wextra
#override CFLAGS += -Werror 
override CFLAGS += -Wpointer-arith
override CFLAGS += -Wcast-align
override CFLAGS += -Wwrite-strings
override CFLAGS += -Wswitch-default
override CFLAGS += -Wunreachable-code
override CFLAGS += -Winit-self
override CFLAGS += -Wmissing-field-initializers
override CFLAGS += -Wno-unknown-pragmas
override CFLAGS += -Wstrict-prototypes
override CFLAGS += -Wundef
override CFLAGS += -Wold-style-definition
#override CFLAGS += -Wmissing-prototypes
override CFLAGS += -Wmissing-declarations
override CFLAGS += -DUNITY_FIXTURES
override CFLAGS += -DUNIT_TEST

#####################################################
# PATHS
#tests
PATH_TEST_SRC = tests/tests_src
PATH_TEST_RUNNERS = $(PATH_TEST_SRC)test_runners
#unity
PATH_UNITY_ROOT=../obcs_tools/Unity
#Directories to create
PATH_BUILD          = tests/build
PATH_BUILD_RESULTS  = $(PATH_BUILD)/results
PATH_BUILD_OBJS     = $(PATH_BUILD)/objs #TODO
PATH_BUILD_DEPENDS  = $(PATH_BUILD)/depends #TODO
PATH_DOCS           = tests/docs

#####################################################
# SOURCE CODE
#####################################################
SOURCE_TEST_RUNNERS = $(wildcard $(PATH_TEST_RUNNERS)*.c)

#####################################################
# TEST API FSM
#####################################################
API_FSM_SOURCE = API/fsm/api_fsm.c
API_FSM_SOURCE_TEST = $(PATH_TEST_SRC)/test_api_fsm.c
API_FSM_TARGET_BASE= api_fsm_test
API_FSM_TARGET = $(PATH_BUILD)/$(API_FSM_TARGET_BASE)$(TARGET_EXTENSION)
API_FSM_SRC_FILES=\
  $(PATH_UNITY_ROOT)/src/unity.c \
  $(API_FSM_SOURCE) \
  $(API_FSM_SOURCE_TEST) \
  $(SOURCE_TEST_RUNNERS)
API_FSM_INC = -IAPI/fsm/Inc -IAPI -I.
API_FSM_INC_DIRS =-Isrc -I$(PATH_UNITY_ROOT)/src -I$(PATH_TEST_SRC) $(API_FSM_INC)
API_FSM_RESULTS = $(PATH_BUILD_RESULTS)/api_fsm_results.txt

#####################################################
# TEST API OSOTB RX
#####################################################
API_OSOTB_RX_SOURCE =	API/osotb/api_osotb.c \
					../obcs_tools/cJSON/cJSON.c \
					../obcs_tools/cJSON/cJSON_Utils.c
API_OSOTB_RX_SOURCE_TEST = $(PATH_TEST_SRC)/test_api_osotb_rx.c
API_OSOTB_RX_TARGET_BASE = api_osotb_test_rx
API_OSOTB_RX_TARGET = $(PATH_BUILD)/$(API_OSOTB_TARGET_BASE)$(TARGET_EXTENSION)
API_OSOTB_RX_SRC_FILES =\
  $(PATH_UNITY_ROOT)/src/unity.c \
  $(API_OSOTB_RX_SOURCE) \
  $(API_OSOTB_RX_SOURCE_TEST) \
  $(SOURCE_TEST_RUNNERS)
API_OSOTB_RX_INC = -IAPI/osotb/Inc -IAPI -I. -I../obcs_tools/cJSON
API_OSOTB_RX_INC_DIRS =-Isrc -I$(PATH_UNITY_ROOT)/src -I$(PATH_TEST_SRC) $(API_OSOTB_RX_INC)
API_OSOTB_RX_RESULTS = $(PATH_BUILD_RESULTS)/api_osotb_results.txt


#####################################################
# TEST API CRC
#####################################################
API_CRC_SOURCE = API/crc/api_crc.c
API_CRC_SOURCE_TEST = $(PATH_TEST_SRC)/test_api_crc.c
API_CRC_TARGET_BASE= api_crc_test
API_CRC_TARGET = $(PATH_BUILD)/$(API_CRC_TARGET_BASE)$(TARGET_EXTENSION)
API_CRC_SRC_FILES=\
  $(PATH_UNITY_ROOT)/src/unity.c \
  $(API_CRC_SOURCE) \
  $(API_CRC_SOURCE_TEST) \
  $(SOURCE_TEST_RUNNERS)
API_CRC_INC = -IAPI/crc/Inc -IAPI -I.
API_CRC_INC_DIRS =-Isrc -I$(PATH_UNITY_ROOT)/src -I$(PATH_TEST_SRC) $(API_CRC_INC)
API_CRC_RESULTS = $(PATH_BUILD_RESULTS)/api_crc_results.txt

#####################################################
# RESULTS 
#####################################################
#Variable used during build call
BUILD_THE_PATHS     =\
   $(PATH_BUILD) \
   $(PATH_BUILD_RESULTS)

all: clean default print

RESULTS = $(API_FSM_RESULTS) $(API_OSOTB_RX_RESULTS) $(API_CRC_RESULTS)

default:$(BUILD_THE_PATHS) $(RESULTS)
	@echo "making $(RESULTS)..."

$(PATH_BUILD):
	$(MKDIR) $(PATH_BUILD)

$(PATH_BUILD_RESULTS):
	$(MKDIR) $(PATH_BUILD_RESULTS)

$(API_OSOTB_RX_TARGET):
	@echo "compiling $@..."
	$(COMPILE) $(CFLAGS) $(API_OSOTB_RX_INC_DIRS) $(SYMBOLS) $(API_OSOTB_RX_SRC_FILES) -o $@

$(API_OSOTB_RX_RESULTS): $(API_OSOTB_RX_TARGET)
	- ./$< -v > $@ 2>&1

$(API_FSM_TARGET):
	@echo "compiling $@..."
	$(COMPILE) $(CFLAGS) $(API_FSM_INC_DIRS) $(SYMBOLS) $(API_FSM_SRC_FILES) -o $@

$(API_FSM_RESULTS): $(API_FSM_TARGET)
	- ./$< -v > $@ 2>&1

$(API_CRC_TARGET):
	@echo "compiling $@..."
	$(COMPILE) $(CFLAGS) $(API_CRC_INC_DIRS) $(SYMBOLS) $(API_CRC_SRC_FILES) -o $@

$(API_CRC_RESULTS): $(API_CRC_TARGET)
	- ./$< -v > $@ 2>&1

PASSED = `grep -s PASS $(PATH_BUILD_RESULTS)/*.txt`
FAIL = `grep -s FAIL $(PATH_BUILD_RESULTS)/*.txt`
IGNORE = `grep -s IGNORE $(PATH_BUILD_RESULTS)/*.txt`
	
.PHONY:print
print:
	@echo -e "=============\nRUNNING TESTS:\n============="
	@echo "-----------------------\nIGNORES:\n-----------------------"
	@echo "$(IGNORE)"
	@echo "-----------------------\nFAILURES:\n-----------------------"
	@echo "$(FAIL)"
	@echo "-----------------------\nPASSED:\n-----------------------"
	@echo "$(PASSED)"
	@echo "\nDONE"

.PHONY:clean
clean:
	$(CLEANUP_DIR) $(PATH_BUILD)