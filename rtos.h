/*
 * rtos.h
 *
 *  Created on: Aug 14, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef RTOS_H_
#define RTOS_H_

// Includes  ------------------------------------------------------------------
/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "timers.h"

/* FreeRTOS+TCP includes. */
#include "FreeRTOS_IP.h"
#include "FreeRTOS_Sockets.h"

// Macros & Constants ---------------------------------------------------------
#define osPriorityNormal 24	/*!< Priority: normal, taken from cmsis_os2.h */

/* Used to handle mutexes in the code.*/
#define MUTEX_INIT(x) 	x = xSemaphoreCreateMutex()
#define MUTEX_UNLOCK(x)	xSemaphoreTake( x, ( TickType_t ) portMAX_DELAY )
#define MUTEX_LOCK(x)	xSemaphoreGive( x )

/* Macro to convert ticks to milliseconds. */
#define MS_TO_TICKS(ms) pdMS_TO_TICKS(ms)//(ms/portTICK_RATE_MS)
/* Macro to convert milliseconds to ticks. */
#define TICKS_TO_MS(ticks) ((1000/configTICK_RATE_HZ)*ticks)

#endif /* OBCS_API_RTOS_H_ */
