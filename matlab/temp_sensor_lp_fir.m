% Design of a low pass FIR filter of a fixed order
%
% This filter was designed to block the high component signals that could
% be in the way of the DC signal of the temperature sensors.
%
%  Created on: Ago 26, 2021
%      Author: Julian Rodriguez aka Marifante
%		Email: jnrodriguezz@hotmail.com
%		Gitlab: https://gitlab.com/Marifante
%		Github: https://github.com/Marifante
% %

clear all; clc;

N   = 32;           % FIR filter order
fp  = 1e3;          % [Hz] passband-edge frequency
fs  = 100e3;         % [Hz] Sampling frequency
Rp  = 0.00057565; % Corresponds to 0.01 dB peak-to-peak ripple
Rst = 1e-4;       % Corresponds to 80 dB stopband attenuation

fo = fp/(fs/2); % normalized frequency at the upper of the passband

% Get the coefficients of the filter. Take in count that the coefficients
% will be returned in incremental order. The first element of the array 
% returned is the first coefficient and the last element is the coefficient
% of greatest order.
% Remember: for a filter of order N, we need to have N+1 coefficients
coeff = firceqrip(N, ... % order of the filter                
                 fo, ... % normalized frequency at the upper of the passband
                 [Rp Rst], ... 
                 'passedge'); % specify that the filter must rolloff at fo

%% Visualize the filter with fvtool
hfvt = fvtool(coeff,'fs',fs); % Visualize filter

%%
% Test filter with test data. The dsp.FIRFilter('Numerator', coeff) 
% returns a FIR filter object that can filter data over time.
LP_FIR = dsp.FIRFilter('Numerator',coeff);
SA     = dsp.SpectrumAnalyzer('SampleRate',fs,'SpectralAverages',5);
tic
while toc < 10
    x = randn(256,1); % Generate 256 random noise samples 
    y = LP_FIR(x); % Inject random noise to the filter
    step(SA,y);
end

%% 
% Display in an array form to insert it in C code. Also, reverse the data
% to be implemented easily with arm_fir_f32 function of CMSIS-DSP.
coeff_rev = flip(coeff);
coeffs_str = "";
coeffs_rev_str = "";
for i=(1:N+1)
    coeffs_rev_str = strcat(coeffs_rev_str, num2str(coeff_rev(i)));
    coeffs_str = strcat(coeffs_str, num2str(coeff(i)));
    if i ~= (N+1)
        coeffs_rev_str = strcat(coeffs_rev_str, ", ");
        coeffs_str = strcat(coeffs_str, ", ");
    end
end
fprintf("The coefficients (from coefficient %d to coefficient 0) are: \n",N);
fprintf("%s\n",coeffs_str);

fprintf("The coefficients (from coefficient 0 to coefficient %d) are: \n",N);
fprintf("%s\n",coeffs_rev_str);
