/*
 * api_config_default.h
 *
 *  Created on: Jul 26, 2021
 *      Author: julian
 */

#ifndef API_CONFIG_DEFAULT_H_
#define API_CONFIG_DEFAULT_H_

// File system ----------------------------------------------------------------
/* Selection of the filesystem used. By default use FreeRTOS+FAT. Note that
 * if FATFS is chosen, the FTP protocol implemented by the API cannot be used. */
//#define API_FS_USE_FATFS
#ifndef API_FS_USE_FATFS
#define API_FS_USE_FF_FAT
#endif

// IP -------------------------------------------------------------------------
/* Default MAC address configuration. */
#ifndef configMAC_ADDR0
	#define configMAC_ADDR0		0x00
#endif
#ifndef configMAC_ADDR1
	#define configMAC_ADDR1		0x11
#endif
#ifndef configMAC_ADDR2
	#define configMAC_ADDR2		0x22
#endif
#ifndef configMAC_ADDR3
	#define configMAC_ADDR3		0x33
#endif
#ifndef configMAC_ADDR4
	#define configMAC_ADDR4		0x44
#endif
#ifndef configMAC_ADDR5
	#define configMAC_ADDR5		0x46
#endif

/* Default IP address configuration.  Used in case ipconfigUSE_DHCP is set to 0,
or ipconfigUSE_DHCP is set to 1 but a DHCP server cannot be contacted. */
#ifndef configIP_ADDR0
	#define configIP_ADDR0		10//192
#endif
#ifndef configIP_ADDR1
	#define configIP_ADDR1		0//168
#endif
#ifndef configIP_ADDR2
	#define configIP_ADDR2		0
#endif
#ifndef configIP_ADDR3
	#define configIP_ADDR3		56//83
#endif

/* Default gateway IP address configuration.  Used in case ipconfigUSE_DHCP is
set to 0, or ipconfigUSE_DHCP is set to 1 but a DHCP server cannot be contacted. */
#ifndef configGATEWAY_ADDR0
	#define configGATEWAY_ADDR0	10 //192
#endif
#ifndef configGATEWAY_ADDR1
	#define configGATEWAY_ADDR1	0
#endif
#ifndef configGATEWAY_ADDR2
	#define configGATEWAY_ADDR2	0
#endif
#ifndef configGATEWAY_ADDR3
	#define configGATEWAY_ADDR3	1
#endif

/* Default DNS server configuration.  OpenDNS addresses are 208.67.222.222 and
208.67.220.220.  Used in ipconfigUSE_DHCP is set to 0, or ipconfigUSE_DHCP is set
to 1 but a DHCP server cannot be contacted.*/
#ifndef configDNS_SERVER_ADDR0
	#define configDNS_SERVER_ADDR0 	208
#endif
#ifndef configDNS_SERVER_ADDR1
	#define configDNS_SERVER_ADDR1 	67
#endif
#ifndef configDNS_SERVER_ADDR2
	#define configDNS_SERVER_ADDR2 	222
#endif
#ifndef configDNS_SERVER_ADDR3
	#define configDNS_SERVER_ADDR3 	222
#endif

/* Default netmask configuration.  Used in case ipconfigUSE_DHCP is set to 0,
or ipconfigUSE_DHCP is set to 1 but a DHCP server cannot be contacted. */
#ifndef configNET_MASK0
	#define configNET_MASK0		255
#endif
#ifndef configNET_MASK1
	#define configNET_MASK1		255
#endif
#ifndef configNET_MASK2
	#define configNET_MASK2		255
#endif
#ifndef configNET_MASK3
	#define configNET_MASK3		0
#endif

// FTP ------------------------------------------------------------------------
#ifndef API_FTP_SERVER_PORT
#define API_FTP_SERVER_PORT			49153
#endif

#ifndef API_FTP_SERVER_MAX_CLIENTS
#define API_FTP_SERVER_MAX_CLIENTS		2
#endif

#ifndef API_FTP_SERVER_ROOT_DIR_DISK_PATH
#define API_FTP_SERVER_ROOT_DIR_DISK_PATH API_FS_FRAM0_DISK_PATH
#endif

#ifndef API_FTP_SERVER_ROOT_DIR
#define API_FTP_SERVER_ROOT_DIR	API_FTP_SERVER_ROOT_DIR_DISK_PATH "/ftp"
#endif

#ifndef API_FTP_SERVER_WORK_BLOCKTIME_MS
#define API_FTP_SERVER_WORK_BLOCKTIME_MS 1000
#endif

/* Set to one to use the vApplicationFTPReceivedHook(). This function is called
 * when a file is received. */
#ifndef ipconfigFTP_HAS_RECEIVED_HOOK
#define ipconfigFTP_HAS_RECEIVED_HOOK 1
#endif

/* When the FTP server receives a file. It can send a msg to the client. The max
 * length of this message is determined by this number (the length is in bytes). */
#ifndef API_FTP_REPLY_MSG_MAX_LEN
#define API_FTP_REPLY_MSG_MAX_LEN 64
#endif

// OSOTB ----------------------------------------------------------------------
#ifndef API_OSOTB_SERVER_PORT
#define API_OSOTB_SERVER_PORT			49253
#endif

#ifndef API_OSOTB_SERVER_MAX_CLIENTS
#define API_OSOTB_SERVER_MAX_CLIENTS		2
#endif

#ifndef API_OSOTB_SERVER_WORK_BLOCKTIME_MS
#define API_OSOTB_SERVER_WORK_BLOCKTIME_MS 1000
#endif

// Logger ---------------------------------------------------------------------
/* This is the first message printed by serial by the logger. */
#ifndef API_LOGGER_INITIAL_MSG_TITLE
#define API_LOGGER_INITIAL_MSG_TITLE	"--------------------------------------------------------------------------------\r\n"
#endif

// IAP ------------------------------------------------------------------------
#ifndef API_IAP_BIN_DISK_PATH
#define API_IAP_BIN_DISK_PATH	API_FS_FRAM0_DISK_PATH	/*!< The disk where the bin is stored. */
#endif

#ifndef API_IAP_APP0_FIRST_SECTOR_LOC
#define API_IAP_APP0_FIRST_SECTOR_LOC	API_FLASH_SECTOR_6	/*!< The first sector where the APP0 will be located. */
#endif

#ifndef API_IAP_APP0_SECTORS_LOCATION
#define API_IAP_APP0_SECTORS_LOCATION	( (1 << API_APP0_FIRST_SECTOR_LOC) | (1 << (API_APP0_FIRST_SECTOR_LOC+1)) )
#endif

#ifndef API_IAP_APP1_FIRST_SECTOR_LOC
#define API_IAP_APP1_FIRST_SECTOR_LOC	API_FLASH_SECTOR_8	/*!< The first sector where the APP0 will be located. */
#endif

#ifndef API_IAP_APP1_SECTORS_LOCATION
#define API_IAP_APP1_SECTORS_LOCATION	( (1 << API_APP1_FIRST_SECTOR_LOC) | (1 << (API_APP1_FIRST_SECTOR_LOC+1)) )
#endif

#ifndef API_IAP_BIN_DISK_PATH
#define API_IAP_BIN_DISK_PATH	API_FS_FRAM0_DISK_PATH	/*!< The disk where the bin is stored. */
#endif

#ifndef API_IAP_DEFAULT_APP_SLOT_FLASH_SECTOR
#define API_IAP_DEFAULT_APP_SLOT_FLASH_SECTOR	API_FLASH_SECTOR_11
#endif

#ifndef API_IAP_DEFAULT_APP_SLOT_FLASH_SECTOR_OFFSET
#define API_IAP_DEFAULT_APP_SLOT_FLASH_SECTOR_OFFSET	0x00
#endif

// Config keys ----------------------------------------------------------------
/* This is defined because this types are not defined in the module itself to be
 * unit-tested in another architecture. */
#ifndef API_CFGKEYS_FILE_T
#define API_CFGKEYS_FILE_T API_FS_FILE
#endif

#ifndef API_CFGKEYS_FSTAT_T
#define API_CFGKEYS_FSTAT_T API_FS_fstat_t
#endif

#ifndef API_CFGKEYS_STATVFS_T
#define API_CFGKEYS_STATVFS_T API_FS_statvfs_t
#endif

#ifndef API_CFGKEYS_mkdir
#define API_CFGKEYS_mkdir API_FS_mkdir
#endif

#ifndef API_CFGKEYS_fopen
#define API_CFGKEYS_fopen API_FS_fopen
#endif

#ifndef API_CFGKEYS_fclose
#define API_CFGKEYS_fclose API_FS_fclose
#endif

#ifndef API_CFGKEYS_fread
#define API_CFGKEYS_fread API_FS_fread
#endif

#ifndef API_CFGKEYS_fwrite
#define API_CFGKEYS_fwrite API_FS_fwrite
#endif

#ifndef API_CFGKEYS_fstat
#define API_CFGKEYS_fstat API_FS_fstat
#endif

#ifndef API_CFGKEYS_statvfs
#define API_CFGKEYS_statvfs API_FS_statvfs
#endif

#ifndef API_CFGKEYS_fgets
#define API_CFGKEYS_fgets API_FS_fgets
#endif

#ifndef API_CFGKEYS_feof
#define API_CFGKEYS_feof API_FS_feof
#endif

#ifndef API_CFGKEYS_rename
#define API_CFGKEYS_rename API_FS_rename
#endif

#ifndef API_CFGKEYS_DISK_PATH
#define API_CFGKEYS_DISK_PATH	API_FS_FRAM0_DISK_PATH	/*!< The disk where the bin is stored. */
#endif

#ifndef API_CFGKEYS_DIR
#define API_CFGKEYS_DIR	API_CFGKEYS_DISK_PATH "/cfgkeys"
#endif

#ifndef API_CFGKEYS_FILE
#define API_CFGKEYS_FILE "cfg.csv"
#endif

#ifndef API_CFGKEYS_STR_VAL_MAX_LEN
#define API_CFGKEYS_STR_VAL_MAX_LEN 8
#endif

#ifndef API_CFGKEYS_FILE_LINE_MAX_LEN
#define API_CFGKEYS_FILE_LINE_MAX_LEN API_CFGKEYS_STR_VAL_MAX_LEN+8
#endif

// LMT85 temp sensors ---------------------------------------------------------
#ifndef BOARD_TEMP_SENSORS_MAX
#define BOARD_TEMP_SENSORS_MAX 12
#endif

// ADC ------------------------------------------------------------------------
#ifndef ADC_VREF_MV
#define ADC_VREF_MV 3300
#endif

#endif /* API_CONFIG_DEFAULT_H_ */
