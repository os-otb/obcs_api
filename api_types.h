/*
 * api_types.h
 *
 *  Created on: May 1, 2021
 *      Author: Julian Rodriguez aka Marifante 
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef API_TYPES_H_
#define API_TYPES_H_

// Includes  ------------------------------------------------------------------
/* Standard libraries includes. */
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>

// Public variables declaration  ----------------------------------------------
typedef enum
{
	RET_OK = 0,
	RET_ERR,
	RET_ERR_NULL_POINTER,
	RET_ERR_INVALID_PARAMS,
	RET_ERR_BUSY,
	RET_ERR_TIMEOUT,
	RET_TRUE,		/*!< In some cases we want to return a true/false value or an error. */
	RET_FALSE,		/*!< In some cases we want to return a true/false value or an error. */
	RET_FULL,		/*!< Indicating that a buffer is full. */
	RET_EMPTY,		/*!< Indicating that a buffer is empty. */
	RET_CODE_MAX,		/*<! Number of error codes in the code. */
} ret_code_t;

#endif /* API_TYPES_H_ */
