# Board LMT85

With this module, we can measure the temperature from multiple LMT85 IC temperature sensors filtering with a FIR implemented with the CMSIS-DSP.

## ADC measures

This module uses the module board_adc.c to measure with the ADC *every* sensor configured by the user at a time. 

## FIR filter
The objective of the low pass FIR implemented is filter the noise surrounding the analog signal of the temperature sensors. This filter uses the actual and some previous values of the sample to do some math operations and gives as result the value filtered. As we can see, to implement a filter of N taps (order N) we need at least N+1 coefficients (b[i] in this image).

![fir_filter_equation](../img/fir_filter_equation.svg)

In this case, we are using the MAC (Multiply-Accumulate) instructions of Cortex M4 processor to filter his signals. As the name says, this instructions permits the microcontroller to multiply and accumulate a value in only one instruction cycle. We can acced to this instructions using the CMSIS-DSP library of ARM.

 