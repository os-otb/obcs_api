# OSOTB Serial

The OSOTB Serial protocol its a protocol where OSOTB requests are sent through a serial interface (USART in this case).

## Packet frame

The frame of an OSOTB serial packet is like the following:

| Header (1 byte) | Data (max 1024 bytes) | Trailer (3 bytes) |
| ----------------------- |:---------------------:| ----------------:|
| STX (0x02)              | <OSOTB_JSON>          | '\n'(0x0A) CRC16_HI  CRC16_L |


A more detailed explanaition of the fields are:

* One header of 1 byte that indicate the start of the frame

* One data field that can have an OSOTB json of 1024 bytes at maximum.

* One trailer of 3 bytes:

    * The first byte have a newline char character `\n` (0x0A) that delimits where the data packets ends and when the trailer begins.

    * The second byte is the MSB of a CRC16

    * The third byte is the LSB of a CRC16

## CRC

The protocol uses a 16 bit CRC to check the integrity of the data received. The crc used is the CRC-CCITT (is the CRC used by the xmodem protocol). The CRC is calculated over the data packet and don't take in count the newline delimiter.

## NACK and ACKS

In the OSOTB protocol there are requests and responses. Usually a client make requests to a server and the server sends to the client responses to this requests.

When the server or the client sends a packet, it can wait up to 10 seconds without a NACK or NACK and then that will count as a send error. A send error can be a reception of a NACK or a timeout of this type. If a send error happen, then client will re-send the packet up to 10 times.

If the CRC sent by the sender is the same as the calculated by the receiver, then the receiver sends an ACK. In the other case, the receiver will send a NACK.

The NACK is the character 0x15 and the ACK is the character 0x06 of the ASCII code.


