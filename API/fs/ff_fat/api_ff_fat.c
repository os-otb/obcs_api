/*
 * tcpip.c
 *
 *  Created on: May 23, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Include --------------------------------------------------------------------
#include "api_ff_fat.h"

/* Only include the content of the file on the build if FreeRTOS+FAT is used. */
#ifdef API_FS_USE_FF_FAT
#include "board.h"

// Macros and constants -------------------------------------------------------
#define API_FF_FAT_LOG_ENABLED			1	/*<! Enables module log. */

#define	LOG_TAG							"api_ff_fat"	/*<! Tag assigned to logs for this module. */

#if API_FF_FAT_LOG_ENABLED
	#define API_FF_FAT_LOG_ERROR(format, ...)		LOG_ERROR(LOG_TAG, format, ##__VA_ARGS__)
	#define API_FF_FAT_LOG_WARNING(format, ...)	LOG_WARNING(LOG_TAG, format, ##__VA_ARGS__)
	#define API_FF_FAT_LOG_INFO(format, ...)		LOG_INFO(LOG_TAG, format, ##__VA_ARGS__)
	#define API_FF_FAT_LOG_DEBUG(format, ...)		LOG_DEBUG(LOG_TAG, format, ##__VA_ARGS__)
#else
	#define API_FF_FAT_LOG_ERROR(...)
	#define API_FF_FAT_LOG_WARNING(...)
	#define API_FF_FAT_LOG_INFO(...)
	#define API_FF_FAT_LOG_DEBUG(...)
#endif

#define API_FF_FAT_MAX_VOLUMES	2	/*!< Max number of volumes/disks that can handle the API. */
#ifdef API_FS_USE_SD
#define ptr_SD_DISK_DRIVER		(&SD_Disk_Driver)	/*!< SD disk driver that will be linked to FreeRTOS+FAT. */
#endif

#ifdef API_FS_USE_MB85RS
#define ptr_FRAM_DISK_DRIVER	(&MB85RS_Disk_Driver)
#endif

#define API_FF_FAT_FORMAT_HIDDEN_SECTORS 8 /*!< When a disk is formated, keep this sector count free. */

// Private variables declaration ----------------------------------------------
typedef struct
{
	char disk_path[API_FF_FAT_DRIVE_STR_MAX_LEN];	/*!< Disk logical drive path. */
	FF_Disk_t *ff_disk;	/*!< File system object for disk logical drive. */
	API_FF_FAT_Diskio_t *drv;	/*!< Driver used for this volume. */
}api_ff_fat_vol_table_item_t;

// Private variables definition -----------------------------------------------
#ifdef API_FS_USE_SD
extern API_FF_FAT_Diskio_t SD_Disk_Driver;	/* !< Driver for a SD card. */
#endif

#ifdef API_FS_USE_MB85RS
extern API_FF_FAT_Diskio_t MB85RS_Disk_Driver;	/* !< Driver for a MB85RS FRAM memory. */
#endif

#define API_FF_FAT_INIT_VOLUME(dp, dr) {.disk_path = dp, .ff_disk = 0, .drv = dr}
/*
 * Struct with the volumes that will initialize the API. The first volume in the
 * list is the volume 0, the next is the 1, and so on.
 * Each volume in this table will have a determinated path. The FreeRTOS+FAT stack
 * implements a virtual filesystem and can add to the virtual fs the mounted partition
 * as aonther directory. This is done with the FF_FS_Add function in the API_FF_FAT_Mount
 * function.
 * */
static api_ff_fat_vol_table_item_t api_ff_fat_vol[] =
{
#ifdef API_FS_USE_SD
	API_FF_FAT_INIT_VOLUME(API_FF_FAT_SD_DISK_PATH, &SD_Disk_Driver),
#endif
#ifdef API_FS_USE_MB85RS
	API_FF_FAT_INIT_VOLUME(API_FF_FAT_FRAM0_DISK_PATH, &MB85RS_Disk_Driver),
#endif
};
#define API_FF_FAT_VOLUMES_TABLE_SIZE	(sizeof(api_ff_fat_vol) / sizeof(api_ff_fat_vol[0]))

// Private functions declaration ----------------------------------------------
static ret_code_t api_ff_fat_disk_init(uint8_t vol_num);
static int32_t api_ff_fat_prv_read(	uint8_t *pucDestination,
									uint32_t ulSectorNumber,
									uint32_t ulSectorCount,
									FF_Disk_t *pxDisk);
static int32_t api_ff_fat_prv_write(uint8_t *pucSource,			/* Source of data to be written. */
									uint32_t ulSectorNumber,	/* The first sector being written to. */
									uint32_t ulSectorCount,		/* The number of sectors to write. */
									FF_Disk_t *pxDisk);			/* Describes the disk being written to. */
static ret_code_t api_ff_fat_disk_delete(FF_Disk_t *pxDisk);
uint8_t api_ff_fat_dp_to_vn(char *disk_path);
ret_code_t api_ff_fat_partition_format(uint8_t vol);
//static ret_code_t api_ff_fat_handles_active(uint8_t vol);

// Public functions definition  -----------------------------------------------
/*
 * @brief initialize the FreeRTOS+FAT file system API.
 * @return RET_OK if success.
 * */
ret_code_t API_FF_FAT_Init(void)
{
	ret_code_t ret = RET_OK;

	for(int i = 0; i < API_FF_FAT_VOLUMES_TABLE_SIZE; i++)
	{
		ret = api_ff_fat_disk_init(i);
		if( ret != RET_OK )
		{
			API_FF_FAT_LOG_ERROR("%s: error initializing disk %s", __func__, api_ff_fat_vol[i].disk_path);
			/* TODO Handle this error. */
		}
	}
	return ret;
}

/*
 * @brief mounts a disk that is in a determined path.
 * @param disk_path the path of the disk to be mounted.
 * @return RET_OK if success.
 */
ret_code_t API_FF_FAT_Mount(char *disk_path)
{
	ret_code_t ret = RET_OK;
	FF_Error_t ff_fat_ret = FF_ERR_NONE;
	uint8_t vol_num = 0;
	if( NULL == disk_path ) return RET_ERR_NULL_POINTER;

	vol_num = api_ff_fat_dp_to_vn(disk_path); /* Translate the disk path to the volume number. */

	if( API_FF_FAT_VOLUMES_TABLE_SIZE < vol_num ) return RET_ERR_INVALID_PARAMS; // Path not valid!

	ff_fat_ret = FF_Mount(api_ff_fat_vol[vol_num].ff_disk, 0);
	if( FF_ERR_NONE != ff_fat_ret )
	{
		ret = RET_ERR;
		API_FF_FAT_LOG_INFO("%s: error: %s\n", __func__, FF_GetErrMessage(ff_fat_ret));
		if( (FF_ERR_IOMAN_INVALID_FORMAT == FF_GETERROR(ff_fat_ret)) ||
			(FF_ERR_IOMAN_NOT_FAT_FORMATTED == FF_GETERROR(ff_fat_ret)) ||
			(FF_ERR_IOMAN_NO_MOUNTABLE_PARTITION == FF_GETERROR(ff_fat_ret)))
		{	/* This means the disk is not formatted. So, we will format it to be used later. */
			ret = api_ff_fat_partition_format(vol_num);
		}
	}
	else
	{
		api_ff_fat_vol[vol_num].ff_disk->xStatus.bIsMounted = pdTRUE;
		/* Freertos+FAT implements a virtual filesystem. With FF_FS_Add a mounted partition
		 * will appear in the root directory as another directory. */
		if( 0 == FF_FS_Add(api_ff_fat_vol[vol_num].disk_path, api_ff_fat_vol[vol_num].ff_disk) ) ret = RET_ERR;
	}

	return ret;
}

/*
 * @brief unmounts a determinated disk.
 * @param disk_path the path of the disk to unmount.
 * @return RET_OK if success. If returns RET_ERR_BUSY is because a file descriptor
 * is opened in that partition and must to be closed before unmount.
 * */
ret_code_t API_FF_FAT_Unmount(char *disk_path)
{
	ret_code_t ret = RET_OK;
	FF_Error_t ff_fat_ret = FF_ERR_NONE;
	uint8_t vol_num = 0;
	if( NULL == disk_path ) return RET_ERR_NULL_POINTER;

	vol_num = api_ff_fat_dp_to_vn(disk_path); /* Translate the disk path to the volume number. */

	if( API_FF_FAT_VOLUMES_TABLE_SIZE < vol_num ) return RET_ERR_INVALID_PARAMS; // Path not valid!

	if( pdFALSE == api_ff_fat_vol[vol_num].ff_disk->xStatus.bIsMounted ) return RET_OK; // Its already unmounted!

	ff_fat_ret = FF_Unmount(api_ff_fat_vol[vol_num].ff_disk);
	if( FF_ERR_NONE != ff_fat_ret )
	{
		if( (FF_ERR_IOMAN_ACTIVE_HANDLES | FF_UNMOUNT) == ff_fat_ret )
		{
			API_FF_FAT_LOG_WARNING("%s: there are files opened or handles active\n", __func__);
			ret = RET_ERR_BUSY;
		}
		else
		{
			API_FF_FAT_LOG_ERROR("%s: error: %s\n", __func__, FF_GetErrMessage(ff_fat_ret));
			ret = RET_ERR;
		}
	}
	else
	{
		api_ff_fat_vol[vol_num].ff_disk->xStatus.bIsMounted = pdFALSE;
	}
	return ret;
}

/*
 * @brief get the status of a filesystem dtermiend by disk_path
 * @param disk_path the path where the disk is mounted
 * @param stat a pointer to the struct that will store the stats of the fs
 * @return RET_OK if success
 * */
ret_code_t API_FF_FAT_Statvfs(char *disk_path, API_FF_FAT_Statvfs_t *stat)
{
	ret_code_t ret = RET_OK;
	FF_Error_t ff_fat_err = FF_ERR_NONE;
	uint8_t vol_num = 0;
	if( (NULL == disk_path) || (NULL == stat) ) return RET_ERR_NULL_POINTER;
	vol_num = api_ff_fat_dp_to_vn(disk_path); /* Translate the disk path to the volume number. */
	if( API_FF_FAT_VOLUMES_TABLE_SIZE < vol_num ) return RET_ERR_INVALID_PARAMS; // Path not valid!

	FF_IOManager_t * p_iomanger = NULL;
	p_iomanger = api_ff_fat_vol[vol_num].ff_disk->pxIOManager;
	if( NULL == p_iomanger ) return RET_ERR_NULL_POINTER;

	if( p_iomanger->xPartition.ulFreeClusterCount == 0ul )
	{
		FF_LockFAT(p_iomanger);
		p_iomanger->xPartition.ulFreeClusterCount = FF_CountFreeClusters(p_iomanger, &ff_fat_err);
		FF_UnlockFAT(p_iomanger);
	}

	stat->total_sectors = p_iomanger->xPartition.ulSectorsPerCluster * p_iomanger->xPartition.ulNumClusters;
	stat->sector_size = p_iomanger->usSectorSize;
	stat->free_sectors = p_iomanger->xPartition.ulSectorsPerCluster *p_iomanger->xPartition.ulFreeClusterCount;

	return ret;
}

// Private functions to link with sd driver -----------------------------------
/*
 * @brief initialize a disk in FreeRTOS+FAT. This function creates a IOManger or IOMAN
 * in a determinated FF_Disk_t struct. This IOMAN is responsible for, amongs other things,
 * buffering and caching both files and directory information.
 * @param vol_num the volume to be initialized.
 * @return RET_OK if success.
 * */
static ret_code_t api_ff_fat_disk_init(uint8_t vol_num)
{
	ret_code_t ret = RET_OK;
	uint32_t sectors_qty = 0;	/* Here will be stored the quantity of the sectors of the disk. */
	uint32_t sectors_size = 0;	/* Here will be stored the size of the sectors of the disk. */
	FF_Error_t xError = FF_ERR_NONE;
	FF_CreationParameters_t xParameters = {0};
	uint32_t xIOManagerCacheSize = 0;
	api_ff_fat_dresult_t dresult = API_FF_FAT_RES_OK;
	DSTATUS dstatus = 0;

	if( API_FF_FAT_VOLUMES_TABLE_SIZE < vol_num ) return RET_ERR_INVALID_PARAMS; // vol doesn't exists!

	// Initialize the media driver
	dstatus = api_ff_fat_vol[vol_num].drv->disk_initialize(vol_num);
	if( STA_NOINIT == dstatus ) return RET_ERR;

	// Get sectors quantity on the disk
	dresult = api_ff_fat_vol[vol_num].drv->disk_ioctl(vol_num, GET_SECTOR_COUNT, &sectors_qty);
	if( API_FF_FAT_RES_OK != dresult ) return RET_ERR;

	// Get sectors size of the disk
	dresult = api_ff_fat_vol[vol_num].drv->disk_ioctl(vol_num, GET_SECTOR_SIZE, &sectors_size);
	if( API_FF_FAT_RES_OK != dresult ) return RET_ERR;

	xIOManagerCacheSize = 4 * sectors_size;
	/* Check the validity of the xIOManagerCacheSize parameter. */
	configASSERT((xIOManagerCacheSize % sectors_size) == 0);		// TODO remove this asserts
	configASSERT((xIOManagerCacheSize >= (2 * sectors_size)));	// TODO remove this asserts

	/* Check if the ff drive is initialized and return if its initialized. */
	if( (api_ff_fat_vol[vol_num].ff_disk != NULL) &&
		(api_ff_fat_vol[vol_num].ff_disk->xStatus.bIsInitialised == pdTRUE) )
	{
		return RET_OK;
	}

    /* Allocate the FF_Disk_t struct of this volume */
	api_ff_fat_vol[vol_num].ff_disk = pvPortMalloc( sizeof( FF_Disk_t ) );
	if( NULL == api_ff_fat_vol[vol_num].ff_disk )
	{
		ret = RET_ERR_NULL_POINTER;
	}
	else
	{
		/* Start with every member of the structure set to zero. */
		memset( api_ff_fat_vol[vol_num].ff_disk, '\0', sizeof( FF_Disk_t ) );

		/* The number of sectors is recorded for bounds checking in the read and
		 write functions. */
		api_ff_fat_vol[vol_num].ff_disk->ulNumberOfSectors = sectors_qty;

		/* Assign the volume number to the FF_Disk_t structure. */
		api_ff_fat_vol[vol_num].ff_disk->pvTag = (void*) vol_num;

		/* Create the IO manager that will be used to control the disk –
		 the FF_CreationParameters_t structure completed with the required
		 parameters, then passed into the FF_CreateIOManager() function. */
		memset(&xParameters, 0, sizeof xParameters);
		xParameters.pucCacheMemory = NULL;
		xParameters.ulMemorySize = xIOManagerCacheSize;
		xParameters.ulSectorSize = sectors_size;
		xParameters.fnWriteBlocks = api_ff_fat_prv_write;
		xParameters.fnReadBlocks = api_ff_fat_prv_read;
		xParameters.pxDisk = api_ff_fat_vol[vol_num].ff_disk;
		xParameters.pvSemaphore = (void *) xSemaphoreCreateRecursiveMutex();
		xParameters.xBlockDeviceIsReentrant = pdTRUE;
		api_ff_fat_vol[vol_num].ff_disk->pxIOManager = FF_CreateIOManger(&xParameters, &xError);

		if( ( NULL != api_ff_fat_vol[vol_num].ff_disk->pxIOManager) &&
			(pdFALSE == FF_isERR(xError)) )
		{	/* Record that the disk has been initialised. */
			api_ff_fat_vol[vol_num].ff_disk->xStatus.bIsInitialised = pdTRUE;
		}
		else
		{	/* The disk structure was allocated, but the disk’s IO manager could
			 not be allocated, so free the disk again. */
			ret = api_ff_fat_disk_delete(api_ff_fat_vol[vol_num].ff_disk);
			if( RET_OK != ret )
			{
				/* TODO Handle this error. */
			}
			API_FF_FAT_LOG_INFO("%st: FF_CreateIOManger: %s\n",__func__,  (const char *) FF_GetErrMessage(xError));
			ret = RET_ERR;
			api_ff_fat_vol[vol_num].ff_disk->xStatus.bIsInitialised = pdFALSE;
		}
	}
	return ret;
}

/*
 * @brief a function to write sectors to a device. This is used by FreeRTOS+FAT.
 * @param pucSource Source of data to be written
 * @param ulSectorNumber The first sector being written to
 * @param ulSectorcount	The number of sectors to write
 * @param pxDisk Describes the disk being written to
 * @return FF_ERR_NONE if success
 * */
static int32_t api_ff_fat_prv_write(uint8_t *pucSource,			/* Source of data to be written. */
									uint32_t ulSectorNumber,	/* The first sector being written to. */
									uint32_t ulSectorCount,		/* The number of sectors to write. */
									FF_Disk_t *pxDisk)			/* Describes the disk being written to. */
{
	DRESULT res = RES_OK;
	uint32_t ff_res = FF_ERR_NONE; /* retval needed by FreeRTOS+FAT. */
	uint32_t vol_num = (uint32_t) pxDisk->pvTag;	/* Get the volume number from the pvTag of the struct. */
	if( API_FF_FAT_VOLUMES_TABLE_SIZE < vol_num )
	{	/* The volume number is invalid. */
		ff_res = FF_ERRFLAG;
	}
	else
	{
		res = api_ff_fat_vol[vol_num].drv->disk_write(vol_num, pucSource, ulSectorNumber, ulSectorCount);
		if( RES_OK != res ) ff_res = FF_ERR_IOMAN_DRIVER_FATAL_ERROR | FF_ERRFLAG;
	}
	return ff_res;
}

/*
 * @brief a function to read sectors from the device. This is used by FreeRTOS+FAT.
 * @param pucDestination Destination for data being read
 * @param ulSectorNumber Sector from which to start reading data
 * @param ulSectorcount	Number of sectors to read
 * @param pxDisk Describes the disk being read from
 * @return FF_ERR_NONE if success
 * */
static int32_t api_ff_fat_prv_read(	uint8_t *pucDestination,
									uint32_t ulSectorNumber,
									uint32_t ulSectorCount,
									FF_Disk_t *pxDisk)
{
	DRESULT res = RES_OK;
	uint32_t ff_res = FF_ERR_NONE; /* retval needed by FreeRTOS+FAT. */
	uint32_t vol_num = (uint32_t) pxDisk->pvTag;	/* Get the volume number from the pvTag of the struct. */
	if( API_FF_FAT_VOLUMES_TABLE_SIZE < vol_num )
	{	/* The volume number is invalid. */
		ff_res = FF_ERRFLAG;
	}
	else
	{
		res = api_ff_fat_vol[vol_num].drv->disk_read(vol_num, pucDestination, ulSectorNumber, ulSectorCount);
		if( RES_OK != res ) ff_res = FF_ERR_IOMAN_DRIVER_FATAL_ERROR | FF_ERRFLAG;
	}
	return ff_res;
}

/*
 * @brief deletes a disk.
 * @param pxDisk the FF_Disk_t struct of the disk to be deleted.
 * @return RET_OK if success.
 * */
static ret_code_t api_ff_fat_disk_delete(FF_Disk_t *pxDisk)
{
	ret_code_t ret = RET_OK;
	if( NULL == pxDisk ) return RET_ERR_NULL_POINTER;

	if( pdTRUE == pxDisk->xStatus.bIsInitialised )
	{
		if( NULL != pxDisk->pxIOManager ) FF_DeleteIOManager(pxDisk->pxIOManager);
		pxDisk->ulSignature = 0;
		pxDisk->xStatus.bIsInitialised = pdFALSE;
		vPortFree(pxDisk);
	}
	return ret;
}

/*
 * @brief converts a disk path to a volume number.
 * @param disk_path the path of the disk to retrieve its volume number.
 * @return the volume number of the disk.
 * */
uint8_t api_ff_fat_dp_to_vn(char *disk_path)
{
	uint8_t vol_num = API_FF_FAT_VOLUMES_TABLE_SIZE+1; // inits in an invalid volume number
	if(NULL == disk_path) return vol_num;

	for( int i = 0; i<API_FF_FAT_VOLUMES_TABLE_SIZE; i++ )
	{
		if( 0 == strcmp(api_ff_fat_vol[i].disk_path, disk_path ) )
		{
			vol_num = i;
			break;
		}
	}
	return vol_num;
}

/*
 * @brief partition and format a volume.
 * @param vol the volume number to be partitioned and formatted.
 * @return RET_OK if success.
 * */
ret_code_t api_ff_fat_partition_format(uint8_t vol)
{
	ret_code_t ret = RET_OK;
	FF_PartitionParameters_t xPartition;
	FF_Error_t xError;

	if( API_FF_FAT_VOLUMES_TABLE_SIZE < vol ) return RET_ERR_INVALID_PARAMS; // vol doesn't exists!

	/* Create a single partition that fills all available space on the disk. */
	memset( &xPartition, '\0', sizeof( xPartition ) );
	xPartition.ulSectorCount = api_ff_fat_vol[vol].ff_disk->ulNumberOfSectors;
	xPartition.ulHiddenSectors = API_FF_FAT_FORMAT_HIDDEN_SECTORS;
	xPartition.xPrimaryCount = 1;	/* Only one primary partition. */
	xPartition.eSizeType = eSizeIsQuota;

	/* Partition the disk */
	xError = FF_Partition( api_ff_fat_vol[vol].ff_disk, &xPartition );
	API_FF_FAT_LOG_INFO( "%s: %s\n", __func__,( const char * ) FF_GetErrMessage( xError ) );

	if( FF_isERR( xError ) == pdFALSE )
	{	/* Format the partition. Just create a partition with partition number 0. */
		char disk_label[API_FF_FAT_DRIVE_STR_MAX_LEN];
		strcpy(disk_label, (api_ff_fat_vol[vol].disk_path+1)); // the disk label will be the disk path without the "/"
		xError = FF_FormatDisk(api_ff_fat_vol[vol].ff_disk, 0, pdTRUE, pdTRUE, api_ff_fat_vol[vol].disk_path);
	API_FF_FAT_LOG_INFO( "%s: FF_Format: %s\n", __func__, ( const char * ) FF_GetErrMessage( xError ) );
	}

	return ret;
}

///**
// * @brief verify if any file is opened or if there is active handles on the
// * disk partition.
// * @param vol the volume where we will check if there is an active handle
// * @return RET_TRUE if there are, RET_FALSE if there not and other error codes
// * if there are an error.
// */
//static ret_code_t api_ff_fat_handles_active(uint8_t vol)
//{
//	ret_code_t ret = RET_FALSE;
//	FF_IOManager_t * p_iomanger = NULL;
//
//	if( API_FF_FAT_VOLUMES_TABLE_SIZE < vol ) return RET_ERR_INVALID_PARAMS; // vol doesn't exists!
//
//	p_iomanger = api_ff_fat_vol[vol].ff_disk->pxIOManager;
//	if( NULL != p_iomanger )
//	{
//		FF_PendSemaphore( p_iomanger->pvSemaphore );
//
//		    BaseType_t xResult;
//		    FF_Buffer_t * pxBuffer = p_iomanger->pxBuffers;
//		    FF_Buffer_t * pxLastBuffer = pxBuffer + p_iomanger->usCacheSize;
//
//		    for( ; ; )
//		    {
//		        if( pxBuffer->usNumHandles )
//		        {
//		            xResult = pdTRUE;
//		            break;
//		        }
//
//		        pxBuffer++;
//
//		        if( pxBuffer == pxLastBuffer )
//		        {
//		            xResult = pdFALSE;
//		            break;
//		        }
//		    }
//
//		    return xResult;
//{
//	        if( prvHasActiveHandles p_iomanger) != 0 )
//	        {	/* Active handles found on the cache. */
//	            API_FF_FAT_LOG_WARNING("%s: handles active on the cache of %s", __func__,api_ff_fat_vol[vol].disk_pathh);
//	            ret = true;
//	        }
//	        else if( p_iomanger->FirstFile != NULL )
//	        {	/* Open files in this partition. */
//	            API_FF_FAT_LOG_WARNING("%s: opened files on %s", __func__, disk_path);
//	            ret = true;
//	        }
//	    }
//		FF_ReleaseSemaphore( p_iomanger->pvSemaphore );
//	}
//	else
//	{
//		ret = RET_ERR_NULL_POINTER;
//	}
//	return ret;
//}
#endif
