/*
 * api_ff_fat.h
 *
 *  Created on: Jul 27, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef API_FF_FAT_H_
#define API_FF_FAT_H_


// Includes -------------------------------------------------------------------
#include "api_types.h"

/* User configuration */
#include "api_config.h"
#include "board_config.h"

/* Only include the content of the file on the build if FreeRTOS+FAT is used. */
#ifdef API_FS_USE_FF_FAT

/* FreeTOS+FAT headers. */
#include "ff_headers.h"
#include "ff_stdio.h"

// Macros & Constants  --------------------------------------------------------
#define API_FF_FAT_DRIVE_STR_MAX_LEN	7	/*!< Max length of the logical drive name string. */
#define API_FF_FAT_SD_DISK_PATH 	"/sd"	/*!< The SD disk will be mounted on "/sd". */
#define API_FF_FAT_FRAM0_DISK_PATH 	"/fram0"

#ifndef STA_NOINIT
#define STA_NOINIT		0x01 /* Drive not initialized */
#endif

#ifndef STA_NODISK
#define STA_NODISK		0x02 /* No medium in the drive */
#endif

#ifndef STA_PROTECT
#define STA_PROTECT		0x04 /* Write protected */
#endif

/* Control commands */
#ifndef CTRL_SYNC
#define CTRL_SYNC			0	/* Complete pending write process (needed at _FS_READONLY == 0) */
#endif

#ifndef GET_SECTOR_COUNT
#define GET_SECTOR_COUNT	1	/* Get media size (needed at _USE_MKFS == 1) */
#endif

#ifndef GET_SECTOR_SIZE
#define GET_SECTOR_SIZE		2	/* Get sector size (needed at _MAX_SS != _MIN_SS) */
#endif

#ifndef GET_BLOCK_SIZE
#define GET_BLOCK_SIZE		3	/* Get erase block size (needed at _USE_MKFS == 1) */
#endif

#ifndef CTRL_TRIM
#define CTRL_TRIM			4	/* Inform device that the data on the block of sectors is no longer used (needed at _USE_TRIM == 1) */
#endif

// Public variable declaration ------------------------------------------------
/* Results of Disk Functions */
typedef enum
{
	API_FF_FAT_RES_OK = 0,		/* 0: Successful */
	API_FF_FAT_RES_ERROR,		/* 1: R/W Error */
	API_FF_FAT_RES_WRPRT,		/* 2: Write Protected */
	API_FF_FAT_RES_NOTRDY,		/* 3: Not Ready */
	API_FF_FAT_RES_PARERR		/* 4: Invalid Parameter */
} api_ff_fat_dresult_t;

#ifndef DSTATUS
typedef uint8_t	DSTATUS;	/* Holding the status of the drive (STA_NOINIT, STA_NODISK, STA_PROTECT). */
#endif

typedef struct
{
	/*
	 * @brief used to initialize the disk
	 * @param uint8_t the drive number.
	 * @return 0 if success.
	 * */
	api_ff_fat_dresult_t	(*disk_initialize) (uint8_t);
	/*
	 * @brief used to get the status of the disk
	 * @param uint8_t the drive number.
	 * @return STA_NOINIT, STA_NODISK or STA_PROTECT.
	 * */
	api_ff_fat_dresult_t	(*disk_status) (uint8_t);
	/*
	 * @brief  reads Sector(s)
	 * @param  pdrv: Physical drive number (0..)
	 * @param  *buff: Data buffer to store read data
	 * @param  sector: Sector address (LBA)
	 * @param  count: Number of sectors to read (1..128)
	 * @return DRESULT: Operation result
	 * */
	api_ff_fat_dresult_t	(*disk_read)       (uint8_t, uint8_t*, uint32_t, uint32_t);
	/**
	  * @brief  Writes Sector(s)
	  * @param  pdrv: Physical drive number (0..) of the drive to write
	  * @param  *buff: Data to be written
	  * @param  sector: Sector address (LBA)
	  * @param  count: Number of sectors to write (1..128)
	  * @retval DRESULT: Operation result
	  */
	api_ff_fat_dresult_t	(*disk_write)      (uint8_t, const uint8_t*, uint32_t, uint32_t);
	/*
	 * @brief used to make miscellanous operations with the disk.
	 * @param uint8_t the drive number.
	 * @param uint8_t the command to be executed (CTRL_SYNC, GET_SECTOR_COUNT, GET_SECTOR_SIZE, GET_BLOCK_SIZE, CTRL_TRIM)
	 * @param void* buffer passed to store some values.
	 * @return RES_OK if success.
	 * */
	api_ff_fat_dresult_t	(*disk_ioctl) (uint8_t, uint8_t, void*);
}API_FF_FAT_Diskio_t;

typedef struct
{
	uint32_t free_sectors;
	uint32_t sector_size;
	uint32_t total_sectors;
}API_FF_FAT_Statvfs_t;

// Public function declaration ------------------------------------------------
ret_code_t API_FF_FAT_Init(void);
ret_code_t API_FF_FAT_Mount(char *disk_path);
ret_code_t API_FF_FAT_Unmount(char *disk_path);
ret_code_t API_FF_FAT_Statvfs(char *disk_path, API_FF_FAT_Statvfs_t *stat);

#endif
#endif /* API_FF_FAT_H_ */
