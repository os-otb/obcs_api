/*
 * api_fs.c
 *
 *  Created on: Jul 28, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */


// Includes -------------------------------------------------------------------
#include "api_fs.h"

// Private variables declaration ----------------------------------------------

// Public functions definition ------------------------------------------------
/*
 * @brief initalize the File System API.
 * @return RET_OK if success.
 * */
ret_code_t API_FS_Init(void)
{
	ret_code_t ret = RET_OK;
#ifdef API_FS_USE_FF_FAT
	ret = API_FF_FAT_Init();
#else
	ret = API_FATFS_Init();
#endif
	return ret;
}

/*
 * @brief mount a determinated disk partition.
 * @param disk_path the disk to be mounted.
 * @return 0 if success, -1 otherwise
 * */
int API_FS_mount(char *disk_path)
{
	int ret = 0;
	ret_code_t fs_ret = RET_OK;
	if( NULL == disk_path ) return -1;
#ifdef API_FS_USE_FF_FAT
	ret = API_FF_FAT_Mount(disk_path);
#else
	fs_ret = API_FATFS_Mount(disk_path);
#endif
	ret = (fs_ret == RET_OK) ? 0 : -1;
	return ret;
}

/*
 * @brief unmount a disk.
 * @param disk_path the disk to be unmounted.
 * @return 0 if success, -1 otherwise. A new return value
 * different for the common POSIX return values was added.
 * This is the RET_ERR_BUSY ret val, when the function return this
 * means that there area opened files/handles on the disks that
 * is being tried to umount.
 * */
int API_FS_umount(char *disk_path)
{
	int ret = 0;
	ret_code_t fs_ret = RET_OK;
	if( NULL == disk_path ) return -1;
#ifdef API_FS_USE_FF_FAT
	fs_ret = API_FF_FAT_Unmount(disk_path);
	if( RET_OK != fs_ret ) ret = -1;
	if( RET_ERR_BUSY == fs_ret ) ret = RET_ERR_BUSY;
#else
	ret_code_t fs_ret = RET_OK;
	fs_ret = API_FATFS_Umount(disk_path);
	ret = (fs_ret == RET_OK) ? 0 : -1;
#endif
	return ret;
}

/*
 * @brief used to open a file to perform various operations (read, write, etc).
 * If the file doesn't exist, this API creates one new.
 * @param file_name the path of the file from the current directory.
 * @param mode_of_operation:
 * 	- "r": Open only for reading only.
 * 	- "w": Creates a new file for writing only.
 * 	- "a": The file is opened only for appending (writing at the end of file).
 * 	- "r+": Open the file for reading and writing.
 * 	- "w+": Creates a new file for writing and reading.
 * 	- "a+": Open file for reading and appending.
 * 	@return NULL if fails or a pointer to the FILE struct.
 * 	@details this function allocates memory! Be sure to do a fclose after this!
 * */
API_FS_FILE *API_FS_fopen(char *file_name, char *mode_of_operation)
{
	API_FS_FILE* file = NULL;
	if( (NULL == file_name) || (NULL == mode_of_operation) ) return NULL;

#ifdef API_FS_USE_FF_FAT
	file = ff_fopen(file_name, mode_of_operation);
#else
	file = pvPortMalloc(sizeof(API_FS_FILE));
	ret_code_t ret = RET_OK;
	uint8_t opt = 0;
	switch(mode_of_operation[0])
	{
		case 'r':
			opt = FA_OPEN_EXISTING | FA_READ;
			if( '+' == mode_of_operation[1] ) opt |= FA_WRITE;
		break;
		case 'w':
			opt = FA_CREATE_ALWAYS | FA_WRITE;
			if( '+' == mode_of_operation[1] ) opt |= FA_READ;
		break;
		case 'a':
			opt = FA_OPEN_EXISTING | FA_OPEN_APPEND;
			if( '+' == mode_of_operation[1] ) opt |= FA_READ;
		break;
		default:
			ret = RET_ERR_INVALID_PARAMS;
		break;
	}

	if( RET_OK == ret )
	{
		ret = API_FATFS_Open(file, file_name, opt);
		if( RET_OK != ret ) vPortFree(file);	// Free the space if there is an error
	}
#endif
	return file;
}

/*
 * @brief closes the stream and flushes all buffers.
 * @param stream pointer to the FILE object that specifies the stream that must be
 * closed.
 * @return 0 if success, EOF (-1) if fails.
 * */
int API_FS_fclose(API_FS_FILE *stream)
{
	int ret = 0;
	if( NULL == stream ) return -1;
#ifdef API_FS_USE_FF_FAT
	ret = ff_fclose(stream);
#else
	ret_code_t fs_ret = RET_OK;
	fs_ret = API_FATFS_Close(stream);
	ret = (fs_ret == RET_OK) ? 0 : -1;
	vPortFree(stream);	// Free the memory!
#endif
	return ret;
}

/*
 * @brief writes data from the array pointed by ptr to the given stream.
 * @param ptr pointer to the array of elements to be written
 * @param size size in bytes of each element to be written
 * @param nmemb humber of elements, each one with a size of 'size' bytes
 * @param stream pointer to a FILE object that specifies an output stream
 * @return the total number of elements successfully written to the file. If
 * the return value is different from the nmemb parameter there is an error.
 * */
size_t API_FS_fwrite(void *ptr, size_t size, size_t nmemb, API_FS_FILE *stream)
{
	size_t ret = 0;
	if( (NULL == ptr) || (NULL == stream) ) return ret;
#ifdef API_FS_USE_FF_FAT
	ret = ff_fwrite(ptr, size, nmemb, stream);
#else
	ret_code_t fs_ret = RET_OK;
	fs_ret = API_FATFS_Write(stream, (uint8_t*) ptr, size*nmemb, (uint32_t*) &ret);

#endif
	return ret;
}

/*
 * @brief reads data from the given stream into the array pointed by ptr.
 * @param ptr pointer to a block of memory with a minimum size of 'size'*'nmemb' bytes
 * @param size size in bytes of each element to be readed
 * @param nmemb humber of elements, each one with a size of 'size' bytes
 * @param stream pointer to a FILE object that specifies the input stream
 * @return the total number of elements successfully readed from the file. If
 * the return value is different from the nmemb parameter there is an error.
 * */
size_t API_FS_fread(void *ptr, size_t size, size_t nmemb, API_FS_FILE *stream)
{
	size_t ret = 0;
	if( (NULL == ptr) || (NULL == stream) ) return ret;
#ifdef API_FS_USE_FF_FAT
	ret = ff_fread(ptr, size, nmemb, stream);
#else
	ret_code_t fs_ret = RET_OK;
	fs_ret = API_FATFS_Read(stream, (uint8_t*) ptr, nmemb*size, (uint32_t*) &ret);
	ret = (RET_OK != fs_ret) ? -1 : ret;	// If the return value is different from RET_OK return -1 always!
#endif
	return ret;
}

/*
 * @brief create a new directory with name path.
 * @param path absolute path of the dir to be created.
 * @return 0 if success, -1 if fails.
 * */
int API_FS_mkdir(char *path)
{
	int ret = 0;
	if( NULL == path ) return -1;
#ifdef API_FS_USE_FF_FAT
	ret = ff_mkdir(path);
#else
	ret_code_t fs_ret = RET_OK;
	fs_ret = API_FATFS_Mkdir(path);
	ret = (RET_OK != fs_ret) ? -1 : 0;
#endif
	return ret;
}

/*
 * @brief returns information about a file.
 * @param path the path of the file.
 * @param stat a pointer to a struct where the info of the file will be stored.
 * @return 0 if success, -1 if fails.
 * */
int API_FS_fstat(char *path, API_FS_fstat_t *stat)
{
	int ret = 0;
	if( (NULL == path) || (stat == NULL) ) return -1;
#ifdef API_FS_USE_FF_FAT
	FF_Stat_t fstat = {0};
	ret = ff_stat(path, &fstat);
	if( 0 == ret )
	{
		stat->fsize = fstat.st_size;
		stat->ftime = 0;//fstat.st_mtime;
	}
#else
	ret_code_t fs_ret = RET_OK;
	FILINFO finfo = {0};
	fs_ret = API_FATFS_Fstat(path, &finfo);
	if( RET_OK == fs_ret )
	{
		stat->fsize = finfo.fsize;
		stat->ftime = finfo.ftime;
	}
	else
	{
		fs_ret = -1;
	}
#endif
	return ret;
}

/*
 * @brief returns information about a mounted filesystem.
 * @param path the path where the filesystem is mounted.
 * @return 0 if success, -1 if fails.
 * */
int API_FS_statvfs(char *path, API_FS_statvfs_t *stat)
{
	int ret = 0;
	ret_code_t fs_ret = RET_OK;
	if( (NULL == path) || (stat == NULL) ) return -1;
#ifdef API_FS_USE_FF_FAT
	API_FF_FAT_Statvfs_t ff_fat_stat = {0};
	fs_ret = API_FF_FAT_Statvfs(path, &ff_fat_stat);
	if( RET_OK == fs_ret )
	{
		stat->free_sectors = ff_fat_stat.free_sectors;
		stat->sector_size = ff_fat_stat.sector_size;
		stat->total_sectors = ff_fat_stat.total_sectors;
	}
#else
	API_FATFS_Statvfs_t fatfs_stat = {0};
	fs_ret = API_FATFS_Statvfs(path, &fatfs_stat);
	if( RET_OK == fs_ret )
	{
		stat->free_sectors = fatfs_stat.free_sectors;
		stat->sector_size = fatfs_stat.sector_size;
		stat->total_sectors = fatfs_stat.total_sectors;
	}

#endif
	ret = (RET_OK != fs_ret) ? -1 : 0;
	return ret;
}

/*
 * @brief returns the read/write pointer of the file to the beginning.
 * */
void API_FS_rewind(API_FS_FILE *stream)
{
#ifdef API_FS_USE_FF_FAT
	ff_rewind(stream);
#else

#endif
}

/*
 * @brief stores a line from an opened file in a buffer. Will stop 
 * if reads size-1 chars, or a newline char is found '\n', or an 
 * EOF is found. 
 * @param s the buffer where the line will be stored.
 * @param size the function will read size-1 chars from the file.
 * @param stream the file descriptor. 
 * @return a non NULL value if success. 
 * */
char *API_FS_fgets(char *s, int size, API_FS_FILE *stream)
{
	char *buff = NULL;
	if( (NULL == stream) || (NULL == s) ) return NULL;
#ifdef API_FS_USE_FF_FAT
	buff = ff_fgets(s, size, stream);	
#else

#endif
	return buff;
}

/*
 * @brief tests the end-of-file indicator of the stream pointed by
 * stream. 
 * @param stream the file descriptor.
 * @return 0 if the EOF is set. Otherwise, any other value is returned.. 
 * */
int API_FS_feof(API_FS_FILE *stream)
{
	int ret = 0;
	if( NULL == stream ) return NULL;
#ifdef API_FS_USE_FF_FAT
	ret = ff_feof(stream);	
#else

#endif
	return ret;
}

/*
 * @brief rename a file.
 * @param old the actual **absoulte path** of the file.
 * @param old the new **absoulte path** of the file.
 * @return 0 if success. Otherwise, -1 is returned.
 * @details if a file with the new name actually exists, this function
 * removes that file and replaces it with the file that its handling. 
 * */
int API_FS_rename(char *old, char *new)
{
	int ret = 0;
	if( (NULL == old) || (NULL == new) ) return NULL;
#ifdef API_FS_USE_FF_FAT
	ret = ff_rename(old, new, pdTRUE);	
#else

#endif
	return ret;
}
