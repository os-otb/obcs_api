/*
 * api_fs.h
 *
 *  Created on: Jul 28, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef API_FS_H_
#define API_FS_H_

// Includes -------------------------------------------------------------------
#include "api_types.h"

/* User configuration */
#include "api_config.h"
#include "board_config.h"

#ifdef API_FS_USE_FATFS
#include "api_fatfs.h"
#endif

#ifdef API_FS_USE_FF_FAT
#include "api_ff_fat.h"
#endif

// Macros & Constants ---------------------------------------------------------
#ifdef API_FS_USE_FATFS
#define API_FS_SD_DISK_PATH		"0:/"
#define API_FS_FRAM0_DISK_PATH	"1:/"
#define API_FS_FRAM1_DISK_PATH	"2:/"
#define API_FS_FRAM2_DISK_PATH	"3:/"
#endif

#ifdef API_FS_USE_FF_FAT
#define API_FS_DRIVE_STR_MAX_LEN	API_FF_FAT_DRIVE_STR_MAX_LEN
#define API_FS_SD_DISK_PATH			API_FF_FAT_SD_DISK_PATH
#define API_FS_FRAM0_DISK_PATH		API_FF_FAT_FRAM0_DISK_PATH
//#define API_FS_FRAM1_DISK_PATH	"/FRAM1"
//#define API_FS_FRAM2_DISK_PATH	"/FRAM3"
#define API_FS_FILE_NAME_MAX_LEN		32	/*!< must be lower than the maximum set on the FreeRTOS+FAT config. */
#endif

// Public variables declaration -----------------------------------------------
/* This API acts as an abstraction layer of FATFS or FreeRTOS+FAT. The API_FS_FILE
 * is used to use the correct FILE descriptor based in which FAT stack is used. */
/* If FATFS is used, then use the FIL file typedef. */
#ifdef API_FS_USE_FATFS
typedef FIL	API_FS_FILE;
#endif

/* If FreeRTOS+FAT is used, then use the FF_FILE file typedef. */
#ifdef API_FS_USE_FF_FAT
typedef FF_FILE API_FS_FILE;
#endif

/*
 * @brief structure used to store information about a file.
 * */
typedef struct
{
	uint64_t fsize;	/* !< File size. */
	uint32_t ftime;	/* !< Modified time. */
} API_FS_fstat_t;

/*
 * @brief structure used to store information about the filesystem.
 * */
typedef struct
{
	uint32_t total_sectors;	/* Total sectors in a fs. */
	uint32_t free_sectors;	/* Free sectors on the fs. */
	uint32_t sector_size;	/* Size of each sector on the fs. */
} API_FS_statvfs_t;

// Public functions declaration -----------------------------------------------
ret_code_t API_FS_Init(void);

/* This API got some functions definition to handle the underlaying filesystem.
 * The application code can use this functions to perform operations in a disk
 * previously initialized. */
int API_FS_mkdir(char *path);
API_FS_FILE *API_FS_fopen(char *file_name, char *mode_of_operation);
int API_FS_fclose(API_FS_FILE *stream);
size_t API_FS_fread(void *ptr, size_t size, size_t nmemb, API_FS_FILE *stream);
size_t API_FS_fwrite(void *ptr, size_t size, size_t nmemb, API_FS_FILE *stream);
int API_FS_mount(char *disk_path);
int API_FS_umount(char *disk_path);
int API_FS_fstat(char *path, API_FS_fstat_t *stat);
int API_FS_statvfs(char *path, API_FS_statvfs_t *stat);
void API_FS_rewind(API_FS_FILE *stream);
char *API_FS_fgets(char *s, int size, API_FS_FILE *stream);
int API_FS_feof(API_FS_FILE *stream);
int API_FS_rename(char *old, char *new);

#endif /* API_FS_H_ */
