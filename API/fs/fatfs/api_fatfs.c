/*
 * api_fatfs.c
 *
 *  Created on: May 23, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */


// Include --------------------------------------------------------------------
#include "api_fatfs.h"

/* Only include the content of the file on the build if FATFS is used. */
#ifdef API_FS_USE_FATFS

#include "ffconf.h"
#include "board.h"

// Macros and constants -------------------------------------------------------
#define f_unmount(path) f_mount(NULL, path, 0) /*!< Macro to unregister a work area. */

#define ptr_SD_DISK_DRIVER		(&SD_Disk_Driver)	/*!< SD disk driver that will be linked to FATFS. */
#define API_FATFS_MAX_VOLUMES	(_VOLUMES)	/*!< For now, only supports the SD. TODO: add support for more volumes.*/

#define API_FATFS_DISK_PATH_MAX_LEN	(4)	/*!< max length of the path of the disks. */

// Private variables declaration ----------------------------------------------
typedef struct
{
	struct{
		char disk_path[4];	/*!< Disk logical drive path. This is filled by FATFS_LinkDriver. */
		FATFS disk_fatfs;	/*!< File system object for disk logical drive. */
		const Diskio_drvTypeDef *drv; /*!< Driver used for this volume. */
	}volume[API_FATFS_MAX_VOLUMES];
}api_fatfs_t;

// Private variables definition -----------------------------------------------
extern Diskio_drvTypeDef SD_Disk_Driver;	/* !< Driver for a SD card. */

#define API_FATFS_INIT_VOLUME(dr) {.disk_path = 0, .disk_fatfs = 0, .drv = dr }
static api_fatfs_t api_fatfs =
{
	.volume =
	{
		API_FATFS_INIT_VOLUME(ptr_SD_DISK_DRIVER),
	}
};
#define API_FATFS_VOLUMES_TABLE_SIZE	(sizeof(api_fatfs.volume) / sizeof(api_fatfs.volume[0]))

// Public functions definition  -----------------------------------------------
/*
 * @brief inits FATFS linking the low level drivers
 * (see board_sd_diskio for example) to the FATFS layer. Also, stores the path
 * of the drive on the api_fatfs struct.
 * @return RET_OK if success.
 * */
ret_code_t API_FATFS_Init(void)
{
	ret_code_t ret = RET_OK;

	/* FatFS: Link the drivers to FATFS. */
	for(int i = 0; i < API_FATFS_VOLUMES_TABLE_SIZE; i++)
	{
		ret = FATFS_LinkDriver(api_fatfs.volume[i].drv, api_fatfs.volume[i].disk_path);
	}

	return ret;
}

#if 0
/*
 * @brief test if filesystem is good or not.
 * This "test" checks:
 * 1) The total and available space of the filesystem.
 * 2) Creates file named API_FATFS_HEALTH_FILE_NAME and writes to it the
 * API_FATFS_HEALTH_FILE_TEXT and a timestamp.
 * @return RET_OK if filesystem is ok.
 * */
ret_code_t API_FATFS_Health_Test(API_FATFS_Info_T *fs_info)
{
	ret_code_t ret = RET_OK;
	FRESULT fatfs_ret = FR_OK; /* To hold the return values of the FatFs functions. */
	FIL health_file;		/* Test file. */
	uint32_t wbytes = 0;		/* File write counts. */
    DWORD free_clusters = 0;	/* Free clusters on fs. */
    FATFS *get_free_fs;
    char str_buff[256] = {0};
    char str_ts[API_DATETIME_TS_STR_LEN];

    if( NULL == fs_info ) ret = RET_ERR_NULL_POINTER;

    /* First mount the drive. */
	fatfs_ret = f_mount(&api_fatfs.disk_fatfs, api_fatfs.disk_path, 0);
	if( FR_OK == fatfs_ret )
	{
	    /* Then get the avaiable space of the fs. */
	    fatfs_ret = f_getfree(api_fatfs.disk_path, &free_clusters, &get_free_fs);
		if( FR_OK == fatfs_ret )
		{
			/* Save in the fs_info struct the available space. */
			fs_info->total_sectors = ((get_free_fs->n_fatent - 2) * get_free_fs->csize)/2;
		    fs_info->free_sectors = (free_clusters * get_free_fs->csize)/2;

		    /* Create the directory where the health file will be. Also, with the return
		     * value of mkdir, can check if the directory exists already. */
		   	fatfs_ret = f_mkdir(API_FATFS_HEALTH_FILE_DIR);
			if( (FR_OK == fatfs_ret) || (FR_EXIST == fatfs_ret) )
			{
				/* Create the health file. If the file already exists, overwrite it. */
				fatfs_ret = f_open(&health_file, API_FATFS_HEALTH_FILE_PATH, FA_CREATE_ALWAYS | FA_WRITE);
				if( FR_OK == fatfs_ret )
				{
					/* Write the data to health file. */
					API_Datetime_Get_Timestamp(str_ts, sizeof(str_ts));
					sprintf(str_buff,"%s: healthy fs check\r\n", str_ts);
					fatfs_ret = f_write(&health_file, str_buff, sizeof(str_buff), (void *)&wbytes);
					f_close(&health_file);
				}
			}
		}

		f_unmount(api_fatfs.disk_path);		/* Unregister work area after use it. */
	}

	ret = (FR_OK == fatfs_ret) ? RET_OK : RET_ERR;

	return ret;
}
#endif

/*
 * @brief mount a disk specified by disk_path.
 * @param disk_path path of the disk that will be mounted.
 * @return RET_OK if success.
 * */
ret_code_t API_FATFS_Mount(char* disk_path)
{
	ret_code_t ret = RET_ERR_INVALID_PARAMS;
	FRESULT fatfs_ret = FR_OK; /* To hold the return values of the FatFs functions. */
	if( NULL == disk_path ) return RET_ERR_NULL_POINTER;

	/* Look if the drive_path queried exists. */
	for(int i = 0; i < API_FATFS_VOLUMES_TABLE_SIZE; i++)
	{
		if( 0 == strcmp(disk_path, api_fatfs.volume[i].disk_path) )
		{
			ret = RET_OK;
			fatfs_ret = f_mount(&api_fatfs.volume[i].disk_fatfs, disk_path, 0);
			if( FR_OK != fatfs_ret ) ret = RET_ERR;
		}
	}
	return ret;
}

/*
 * @brief unmount a disk specified by disk_path.
 * @param disk_path path of the disk that will be mounted.
 * @return RET_OK if success.
 * */
ret_code_t API_FATFS_Umount(char* disk_path)
{
	ret_code_t ret = RET_ERR_INVALID_PARAMS;
	FRESULT fatfs_ret = FR_OK; /* To hold the return values of the FatFs functions. */
	if( NULL == disk_path ) return RET_ERR_NULL_POINTER;

	/* Look if the drive_path queried exists. */
	for(int i = 0; i < API_FATFS_VOLUMES_TABLE_SIZE; i++)
	{
		if( 0 == strcmp(disk_path, api_fatfs.volume[i].disk_path) )
		{
			ret = RET_OK;
			fatfs_ret = f_unmount(disk_path);
			if( FR_OK != fatfs_ret ) ret = RET_ERR;
		}
	}
	return ret;
}

/*
 * @brief  Close a file.
 * @param  p_file pointer to a file struct.
 *
 * @return RET_OK if success.
 *
 */
ret_code_t API_FATFS_Close(FIL* p_file)
{
	ret_code_t ret = RET_ERR_INVALID_PARAMS;
	FRESULT fatfs_ret = FR_OK; /* To hold the return values of the FatFs functions. */
	if( NULL == p_file ) return RET_ERR_NULL_POINTER;

	fatfs_ret = f_close(p_file);
	if( FR_OK != fatfs_ret ) ret = RET_ERR;

	return ret;
}

/**
  * @brief  Open a file.
  * @param  p_file pointer to a file struct.
  * @param  name_file name of the file that will be opened.
  * @param  opt option to the open action (read, write, see the typedef ot this parameter).
  *
  * @return RET_OK if success.
  *
  */
ret_code_t API_FATFS_Open(FIL* p_file, char *name_file, uint8_t opt)
{
	ret_code_t ret = RET_OK;
	FRESULT fatfs_ret = FR_OK; /* To hold the return values of the FatFs functions. */
	if( (NULL == p_file) || (NULL == name_file) ) return RET_ERR_NULL_POINTER;

	fatfs_ret = f_open(p_file, name_file, opt);
	if( FR_OK != fatfs_ret ) ret = RET_ERR;
	return ret;
}

/**
  * @brief  get the information about a file.
  * @param  path_file path of the file queried.
  * @param  info_file struct where the info of the file will be stored.
  * @return	RET_OK if success.
  */
ret_code_t API_FATFS_Fstat(char* path_file, FILINFO* info_file)
{
	ret_code_t ret = RET_OK;
	FRESULT fatfs_ret = FR_OK;
	if( (NULL == path_file) || (NULL == info_file) ) return RET_ERR_NULL_POINTER;

	fatfs_ret = f_stat(path_file, info_file);
	if( FR_OK != fatfs_ret ) ret = RET_ERR;
	return ret;
}

/*
 * @brief read a quantity of data from the file and store it in a buffer.
 * @param file pointer to the FIL struct to read.
 * @param buffer pointer to the buffer where the data will be stored.
 * @param buffer_size size of the buffer where the data will be stored (in bytes).
 * @param read_size var where the size of the data readed will be stored (in bytes).
 * @return RET_OK if success.
 * */
ret_code_t API_FATFS_Read(FIL *file, uint8_t *buffer, uint32_t buffer_size, uint32_t *read_size)
{
	ret_code_t ret = RET_OK;
	FRESULT fatfs_ret = FR_OK; /* To hold the return values of the FatFs functions. */
	if( (NULL == file) || (NULL == buffer) || (NULL == read_size) ) return RET_ERR_NULL_POINTER;

	fatfs_ret = f_read(file, buffer, buffer_size, (UINT*) read_size);
	if( FR_OK != fatfs_ret ) ret = RET_ERR;
	return ret;
}

/*
 * @brief write a buffer of bytes into a file.
 * @param file pointer to the FIL struct to write.
 * @param buffer pointer to the data to be written.
 * @param buffer_size number of bytes to write.
 * @param bytes_written the number of bytes written.
 * @return RET_OK if success.
 * */
ret_code_t API_FATFS_Write(FIL *file, uint8_t *buffer, uint32_t buffer_size, uint32_t *bytes_written)
{
	ret_code_t ret = RET_OK;
	FRESULT fatfs_ret = FR_OK; /* To hold the return values of the FatFs functions. */
	if( (NULL == file) || (NULL == buffer) || (NULL == bytes_written) ) return RET_ERR_NULL_POINTER;

	fatfs_ret = f_write(file, buffer, buffer_size, (UINT*) bytes_written);
	if( FR_OK != fatfs_ret ) ret = RET_ERR;
	return ret;
}

/*
 * @brief write a buffer of bytes into a file.
 * @param file pointer to the FIL struct to write.
 * @return RET_OK if success.
 * */
ret_code_t API_FATFS_Mkdir(char *path)
{
	ret_code_t ret = RET_OK;
	FRESULT fatfs_ret = FR_OK; /* To hold the return values of the FatFs functions. */
	if( NULL == path ) return RET_ERR_NULL_POINTER;

	fatfs_ret = f_mkdir(path);
	if( (FR_OK != fatfs_ret) && (FR_EXIST != fatfs_ret) ) ret = RET_ERR;
	return ret;
}

/*
 * @brief read a quantity of data from the file and store it in a buffer.
 *
 * @param dir pointer to the DIR struct. This will hold the data of the dir.
 * @param dir_name the name of the dir from the root directory.
 *
 * @return RET_OK if success.
 *
 * */
ret_code_t API_FATFS_Open_Dir(DIR *dir, char *dir_name)
{
	ret_code_t ret = RET_OK;
	FRESULT fatfs_ret = FR_OK; /* To hold the return values of the FatFs functions. */
	if( (NULL == dir) || (NULL == dir_name) ) return RET_ERR_NULL_POINTER;

	fatfs_ret = f_opendir(dir, dir_name);
	if( FR_OK != fatfs_ret ) ret = RET_ERR;

	return ret;
}

/*
 * @brief get the total and free sectors on the filesystem.
 * @param disk_path the path of the disk.
 * @param stat struct where the info of the fs will be stored.
 * @return RET_OK if success.
 * */
ret_code_t API_FATFS_Statvfs(char *disk_path, API_FATFS_Statvfs_t *stat)
{
	ret_code_t ret = RET_OK;
	FRESULT fatfs_ret = FR_OK; /* To hold the return values of the FatFs functions. */
	FATFS *get_free_fs;
	uint32_t free_clusters = 0;
	if( (NULL == disk_path) || (NULL == stat) ) return RET_ERR_INVALID_PARAMS;

	fatfs_ret = f_getfree(disk_path, &free_clusters, &get_free_fs);
	if( FR_OK == fatfs_ret )
	{
		/* Multiply the number of clusters by the sectors per cluster to get
		 * the number of sectors on the file system. */
		stat->total_sectors = ((get_free_fs->n_fatent - 2) * get_free_fs->csize);
		/* Multiple the number of free cluster by the sectors per cluster to
		 * get the number of free sectors on the file system. */
		stat->free_sectors = (free_clusters * get_free_fs->csize);
		/* Store sector size. */
		stat->sector_size = get_free_fs->ssize;
	}
	else
	{
		ret = RET_ERR;
	}
	return ret;
}

#if 0
/*
 * @brief mount sd disk.
 *
 * @param unmount set to false if wants to unmount the disk.
 *
 * @return RET_OK if success.
 *
 * */
ret_code_t API_FATFS_Mount_SD(bool unmount)
{
	ret_code_t ret = RET_OK;
	FRESULT fatfs_ret = FR_OK; /* To hold the return values of the FatFs functions. */

	if( false == unmount ) f_unmount(api_fatfs.disk_path);
	else
	{
		fatfs_ret = f_mount(&api_fatfs.disk_fatfs, api_fatfs.disk_path, 0);
		if( FR_OK != fatfs_ret ) ret = RET_ERR;
	}

	return ret;
}

/*
 * @brief unmount a file system specified by disk_path.
 * @param disk_path path of the filesystem that must be unmounted
 * @return RET_OK if success.
 * */
ret_code_t API_FATFS_Umount(char* disk_path)
{
	ret_code_t ret = RET_OK;
	if( NULL == disk_path ) return RET_ERR_NULL_POINTER;

	if( 0 != strcmp(disk_path, api_fatfs.disk_path) ) ret = RET_ERR_INVALID_PARAMS;
	else f_unmount(api_fatfs.disk_path);

	return ret;
}
#endif
/**
  * @brief  Gets Time from datetime API.
  * @param  None
  * @retval Time in DWORD
  */
DWORD get_fattime(void)
{
	DWORD fattime = 0;
	ret_code_t ret = RET_OK;
	struct tm timestamp = {0};

	ret = API_Datetime_Get_Timestamp_Struct(&timestamp);
	if( RET_OK != ret )
	{
		/* TODO handle error. */
	}

	timestamp.tm_year += 1900; // This value is the year -1900.
	timestamp.tm_year -= 1980; // FATFS get_fattime requires that the year have got an offset of -1980

	timestamp.tm_mon += 1; // FATFS requires that the month must be from 1 to 12.
	timestamp.tm_sec /= 2; // FATFS requires that the seconds must be divided two.

	fattime = ( (((uint8_t) timestamp.tm_year)	<< 25)	|
				(((uint8_t) timestamp.tm_mon)	<< 21)	|
				(((uint8_t) timestamp.tm_mday)	<< 16)	|
				(((uint8_t) timestamp.tm_hour)	<< 11)	|
				(((uint8_t) timestamp.tm_min)	<< 5)	|
				(((uint8_t) timestamp.tm_sec)	<< 0));

	return fattime;
}

#endif
