/*
 * api_fatfs.h
 *
 *  Created on: Jul 28, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef API_FATFS_H_
#define API_FATFS_H_

// Includes -------------------------------------------------------------------
#include "api_types.h"

/* User configuration */
#include "api_config.h"
#include "board_config.h"

/* Only include the content of the file on the build if FATFS is used. */
#ifdef API_FS_USE_FATFS
#include "ff.h"

// Macros & Constants ---------------------------------------------------------
#define API_FATFS_FILE_NAME_MAX_SIZE 20

// Public variables definition ------------------------------------------------
/* File struct used by the application. */
#if 0
typedef struct
{
	FILINFO finfo;
	FIL file;
} api_fatfs_file_t;

typedef enum
{
	API_FATFS_OPEN_OPTION_OVER_WRITE = FA_CREATE_ALWAYS | FA_WRITE, /*!< Overwrite & write file. */
	API_FATFS_OPEN_OPTION_READ = FA_OPEN_EXISTING | FA_READ, /* !< Only read existing file. */
} api_fatfs_open_option_t;

typedef struct
{
	uint32_t total_sectors;	/* Total drive space in kB. */
	uint32_t free_sectors;	/* Space available in drive in kB. */
}API_FATFS_Info_T;
#endif
typedef struct
{
	uint32_t total_sectors;	/* Total sectors in a fs. */
	uint32_t free_sectors;	/* Free sectors on the fs. */
	uint32_t sector_size;
} API_FATFS_Statvfs_t;

// Public functions declaration -----------------------------------------------
ret_code_t API_FATFS_Init(void);
ret_code_t API_FATFS_Mount(char* disk_path);
ret_code_t API_FATFS_Umount(char* disk_path);
ret_code_t API_FATFS_Open(FIL* p_file, char *name_file, uint8_t opt);
ret_code_t API_FATFS_Close(FIL* p_file);
ret_code_t API_FATFS_Read(FIL *file, uint8_t *buffer, uint32_t buffer_size, uint32_t *read_size);
ret_code_t API_FATFS_Write(FIL *file, uint8_t *buffer, uint32_t buffer_size, uint32_t *bytes_written);
ret_code_t API_FATFS_Mkdir(char *path);
ret_code_t API_FATFS_Statvfs(char *disk_path, API_FATFS_Statvfs_t *stat);
ret_code_t API_FATFS_Fstat(char* path_file, FILINFO* info_file);

#endif
#endif /* API_FATFS_H_ */
