/*
 * api_flash.h
 *
 *  Created on: Jul 28, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef API_FLASH_H_
#define API_FLASH_H_

// Includes -------------------------------------------------------------------
#include "api_types.h"

// Public variable declaration ------------------------------------------------
/* Enum with the sectors of the flash that can be written. */
typedef enum
{
	API_FLASH_SECTOR_0 = 0,
	API_FLASH_SECTOR_1,
	API_FLASH_SECTOR_2,
	API_FLASH_SECTOR_3,
	API_FLASH_SECTOR_4,
	API_FLASH_SECTOR_5,
	API_FLASH_SECTOR_6,
	API_FLASH_SECTOR_7,
	API_FLASH_SECTOR_8,
	API_FLASH_SECTOR_9,
	API_FLASH_SECTOR_10,
	API_FLASH_SECTOR_11,
	API_FLASH_MAX_SECTORS
} api_flash_sector_t;

// Public variable declaration ------------------------------------------------
ret_code_t API_Flash_Get_Sectors_Total_Size(uint16_t sectors_mask, uint32_t *sectors_total_size);
ret_code_t API_FLASH_Erase_Sectors(uint16_t sectors_mask);
ret_code_t API_FLASH_Write_From(uint16_t sectors_mask, uint32_t offset, uint8_t *buffer, uint32_t buffer_size);
ret_code_t API_FLASH_Read_From(uint16_t sectors_mask, uint32_t offset, uint32_t *buffer, uint32_t buffer_size);
bool API_FLASH_Is_There_A_Valid_App(int nsector);

#endif /* API_FLASH_H_ */
