/*
 * api_flash.c
 *
 *  Created on: Apr 30, 2021
 *  	Author: Lucas Mancini
 *      Author: Julian Rodriguez aka Marifante 
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Include --------------------------------------------------------------------
#include "api_flash.h"
#include "board.h"

#include "api_iface_board.h"

// Macros & constants ---------------------------------------------------------
#define API_FLASH_SECTOR_N_MAX 11
#define API_FLASH_SECTOR_N_MIN 11

#define API_FLASH_LOG_ENABLED			1	/*<! Enables module log. */

#define	LOG_TAG							"api_flash"	/*<! Tag assigned to logs for this module. */

#if API_FLASH_LOG_ENABLED
	#define API_FLASH_LOG_ERROR(format, ...)		LOG_ERROR(LOG_TAG, format, ##__VA_ARGS__)
	#define API_FLASH_LOG_WARNING(format, ...)	LOG_WARNING(LOG_TAG, format, ##__VA_ARGS__)
	#define API_FLASH_LOG_INFO(format, ...)		LOG_INFO(LOG_TAG, format, ##__VA_ARGS__)
	#define API_FLASH_LOG_DEBUG(format, ...)		LOG_DEBUG(LOG_TAG, format, ##__VA_ARGS__)
#else
	#define API_FLASH_LOG_ERROR(...)
	#define API_FLASH_LOG_WARNING(...)
	#define API_FLASH_LOG_INFO(...)
	#define API_FLASH_LOG_DEBUG(...)
#endif

// Private variables declaration ----------------------------------------------

// Private variables definition -----------------------------------------------

// Private functions declaration ----------------------------------------------

// Public functions definition  -----------------------------------------------
/*
 * @brief get the total size of the sectors queried with the mask.
 *
 * @param sectors_mask mask composed with the macro api_flash_sector_t. This
 * mask represent the sectors that will be queried.
 * @sectors_total_size var where the total size of the sectors will be stored.
 *
 * @return RET_OK if success.
 *
 * */
ret_code_t API_Flash_Get_Sectors_Total_Size(uint16_t sectors_mask, uint32_t *sectors_total_size)
{
	ret_code_t ret = RET_OK;
	if( NULL == sectors_total_size )
	{
		ret = RET_ERR_NULL_POINTER;
	}
	else
	{
		*sectors_total_size = 0; /* Clear the value. */
		/* Scan the mask to check what sectors size must be queried. */
		for(int i = 0; i< API_FLASH_MAX_SECTORS; i++)
		{
			if( 0 != (sectors_mask & (1 << i)) )
			{
				/* This sector was queried!. */
				*sectors_total_size += Board_Flash_Get_Sector_Size(i); /* Add the size of the sector to this var. */
			}
		}
	}
	return ret;
}

/*
 * @brief erase the sectors represented by the mask passed as argument,
 * @param sectors_mask mask composed with the macro api_flash_sector_t. This
 * mask represent the sectors that will be queried.
 * @return RET_OK if success.
 * */
ret_code_t API_FLASH_Erase_Sectors( uint16_t sectors_mask )
{
	ret_code_t ret = RET_OK;
	uint32_t base_address = 0;

	Board_FLASH_Unlock(); /* Unlock the flash. */

	for(int i = 0; i< API_FLASH_MAX_SECTORS; i++)
	{	/* Loop in the mask to erase the sectors choosen. */
		if( 0 != (sectors_mask & (1 << i)) )
		{	/* This sector will be erased! Just for debug, print the address! */
			base_address = Board_FLASH_Get_Sector_Address(i);
			API_FLASH_LOG_DEBUG("%s: erasing sector %d starting from 0x%X", __func__, i, base_address);
			ret = Board_FLASH_Erase_Sector(i, false);
			if( RET_OK != ret )
			{	/* If there is an error break and return. */
				break;
			}
		}
	}
	Board_FLASH_Lock(); /* Lock the flash again. */

	return ret;
}

/*
 * @brief write in the flash in the first sector of the sectors chosen with the sectors_mask
 * with an offset chosen by the user.
 *
 * @param sectors_mask sectors chosen.
 * @param offset offset address from the first sector.
 * @param buffer data that will be written.
 * @param buffer_size the size of the data that will be written.
 *
 * @return RET_OK if success.
 *
 * */
ret_code_t API_FLASH_Write_From(uint16_t sectors_mask, uint32_t offset, uint8_t *buffer, uint32_t buffer_size)
{
	ret_code_t ret = RET_OK;
	api_flash_sector_t first_sector = 0;
	uint32_t base_address = 0;

	if( NULL == buffer )
	{
		ret = RET_ERR_NULL_POINTER;
	}
	else
	if( 0 == sectors_mask )
	{
		/* If the mask is all zeros, then return an error. This means that no sector will be
		 * programmed. */
		ret = RET_ERR_INVALID_PARAMS;
	}
	else
	{
		/* Search the first of the sectors passed: */
		for(int i = 0; i< API_FLASH_MAX_SECTORS; i++)
		{
			/* Shift to the right the mask and find the least significant bit that is a '1'.
			 * This is the least significant bit. */
			sectors_mask = sectors_mask >> 1;
			if( 1 == (sectors_mask & 0x1) )
			{
				/* The first sector was found! */
				first_sector = i+1;
				break;
			}
		}

		/* Just for debug print the sector. */
		base_address = Board_FLASH_Get_Sector_Address(first_sector);
		API_FLASH_LOG_DEBUG("%s: writing sectors starting from sector %d (mask=0x%X, base_addr=0x%X)", __func__, first_sector, sectors_mask, base_address);

		/* Unlock the flash. */
		Board_FLASH_Unlock();
		/* Write in the address: ADDR_OF_FIRST_SECTOR+OFFSET the data passed in the
		 * buffer. */
		for(int i = 0; i< buffer_size; i++)
		{
			ret = Board_FLASH_Write_Offset(first_sector, BOARD_FLASH_WR_BYTE, buffer[i], offset+i, false);
			if(RET_OK != ret )
			{
				break;
			}
		}
		/* Lock the flash again. */
		Board_FLASH_Lock();
	}

	return ret;
}

/*
 * @brief read words from the first sector of the sectors chosen with the sectors_mask
 * with an offset chosen by the user.
 *
 * @param sectors_mask sectors chosen.
 * @param offset offset address from the first sector.
 * @param buffer where the words readed will be stored.
 * @param buffer_size the size in words of the data that will be readed.
 *
 * @return RET_OK if success.
 * */
ret_code_t API_FLASH_Read_From(uint16_t sectors_mask, uint32_t offset, uint32_t *buffer, uint32_t buffer_size)
{
	ret_code_t ret = RET_OK;
	api_flash_sector_t first_sector = 0;

	if( NULL == buffer )
	{
		ret = RET_ERR_NULL_POINTER;
	}
	else
	if( 0 == sectors_mask )
	{
		/* If the mask is all zeros, then return an error. This means that no sector will be
		 * programmed. */
		ret = RET_ERR_INVALID_PARAMS;
	}
	else
	{
		/* Search the first of the sectors passed: */
		for(int i = 0; i< API_FLASH_MAX_SECTORS; i++)
		{
			/* Shift to the right the mask and find the least significant bit that is a '1'.
			 * This is the least significant bit. */
			sectors_mask = sectors_mask >> 1;
			if( 1 == (sectors_mask & 0x1) )
			{
				/* The first sector was found! */
				first_sector = i+1;
				break;
			}
		}
		/* Unlock the flash. */
		Board_FLASH_Unlock();
		/* Read in the address: ADDR_OF_FIRST_SECTOR+OFFSET the data passed in the
		 * buffer. */
		for(int i = 0; i< buffer_size; i++)
		{
			ret = Board_FLASH_Read_Offset(first_sector, &buffer[i], offset+i, false);
			if(RET_OK != ret )
			{
				break;
			}
		}
		/* Lock the flash again. */
		Board_FLASH_Lock();
	}

	return ret;
}


/*
 * @brief check if there is a valid application in a sector.
 *
 * @param the sector where the check will be done.
 *
 * @details this checks if the first word of the sector points
 * to the end of the SRAM memory of the MCU. The first word of the
 * sector must be the initial value of the Main Stack Pointer, and this
 * initial value must be the end of the SRAM memory (ARM Cortex-M have
 * a descendent stack scheme.
 *
 * */
bool API_FLASH_Is_There_A_Valid_App(int nsector)
{
	bool res = false;
	uint32_t app_address = 0;
	app_address = Board_FLASH_Get_Sector_Address(nsector);
	res = (*((uint32_t*) app_address) != BOARD_MCU_SRAM_END) ? false : true;
	return res;
}
