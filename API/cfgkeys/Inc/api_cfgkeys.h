/*
 * api_veeprom.h
 *
 *  Created on: Aug 25, 2021
 *      Author: marifante
 */

#ifndef API_CFG_KEYS_H_
#define API_CFG_KEYS_H_

// Includes -------------------------------------------------------------------
#include "api_types.h"
#include "api_config_default.h"

// Macros & Constants ---------------------------------------------------------
#define API_CFGKEYS_DEFINE_TABLE_ITEM(k, v) {.keys=k, .val=v}

// Public variables declaration -----------------------------------------------
/**
 * @brief this is the type used by the config_keys.
 * The key is an integer number. The keys doesn't have any string identificator
 * because isn't needed, with the key is enough.
 * Then, each config key can hold a uint8_t, an uint32_t, a float or a string
 * of API_CFGKEYS_STR_VAL_MAX_LEN value. This last one can be configured trough
 * the api_config_default.h file.
 */
typedef struct
{
	int key;
	union
	{
		uint8_t	u8;
		uint32_t u32;
		float fl;
//		char str[API_CFGKEYS_STR_VAL_MAX_LEN];
	}val;
} api_cfgkey_t;

// Public functions declarations ----------------------------------------------
ret_code_t api_cfgkeys_init(api_cfgkey_t *keys_table, uint32_t keys_number);
ret_code_t api_cfgkeys_reset_default_cfg(void);

#endif /* API_CFG_KEYS_H_ */
