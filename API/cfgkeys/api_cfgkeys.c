/*
 * api_cfgkeys.c
 *
 *	This module handles a file that contains different configurations of
 *	the device.
 *	Each line of the config file has the following format:
 *	<key>,<value>
 *	0,0
 *	1,120
 *	2,405
 *	3,1506
 *
 *	The format is very simple, just a key-value par separated by a "," character
 *	and each line separated by a \n. It's like a CSV (Comma Separated Value) format.
 * 
 *  Created on: Aug 25, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Include --------------------------------------------------------------------
#include "api_cfgkeys.h"

/* Uses of other APIs */
#include "api_fs.h"

// Macros & constants ---------------------------------------------------------
#define API_CFGKEYS_KEY_VAL_SEPARATOR	':'	/*!< In the file, the key-value pairs are separated with an ':'. */
#define API_CFGKEYS_ABS_PATH API_CFGKEYS_DIR "/" API_CFGKEYS_FILE	/*!< Absolute path of the config file. */

#define API_CFG_KEYS_LOG_ENABLED			1	/*<! Enables module log. */

#define	LOG_TAG							"api_cfgkeys"	/*<! Tag assigned to logs for this module. */

#if API_CFG_KEYS_LOG_ENABLED
	#define API_CFG_KEYS_LOG_ERROR(format, ...)		LOG_ERROR(LOG_TAG, format, ##__VA_ARGS__)
	#define API_CFG_KEYS_LOG_WARNING(format, ...)	LOG_WARNING(LOG_TAG, format, ##__VA_ARGS__)
	#define API_CFG_KEYS_LOG_INFO(format, ...)		LOG_INFO(LOG_TAG, format, ##__VA_ARGS__)
	#define API_CFG_KEYS_LOG_DEBUG(format, ...)		LOG_DEBUG(LOG_TAG, format, ##__VA_ARGS__)
#else
	#define API_CFG_KEYS_LOG_ERROR(...)
	#define API_CFG_KEYS_LOG_WARNING(...)
	#define API_CFG_KEYS_LOG_INFO(...)
	#define API_CFG_KEYS_LOG_DEBUG(...)
#endif

// Private variables declaration ----------------------------------------------

// Private variables definition -----------------------------------------------
extern api_cfgkey_t *api_cfgkeys_table;		/* !< current table. */
extern api_cfgkey_t *api_cfgkeys_def_table;	/* <! default table. */

// Private functions declaration ----------------------------------------------
/* This functions must be defined in the api_config_default.h. Were leaved a this
 * abstraction level because we want to unit-test this module separatedly. */
extern int API_CFGKEYS_mkdir(char *path);
extern API_CFGKEYS_FILE_T *API_CFGKEYS_fopen(char *file_name, char *mode_of_operation);
extern int API_CFGKEYS_fclose(API_CFGKEYS_FILE_T *stream);
extern size_t API_CFGKEYS_fread(void *ptr, size_t size, size_t nmemb, API_CFGKEYS_FILE_T *stream);
extern size_t API_CFGKEYS_fwrite(void *ptr, size_t size, size_t nmemb, API_CFGKEYS_FILE_T *stream);
extern int API_CFGKEYS_fstat(char *path, API_CFGKEYS_FSTAT_T *stat);
extern int API_CFGKEYS_statvfs(char *path, API_CFGKEYS_STATVFS_T *stat);
extern char *API_CFGKEYS_fgets(char *s, int size, API_CFGKEYS_STATVFS_T *stream);
extern int *API_CFGKEYS_feof(API_CFGKEYS_FILE_T *stream);
extern int *API_CFGKEYS_rename(char *old, char *new);

ret_code_t api_cfgkey_get_key_line(int key, char *buff, size_t buff_size, API_CFGKEYS_FILE_T file);

// Public functions definition  -----------------------------------------------
/**
 * @brief init this API.
 * This function reads the API_CFGKEYS_FILE and copies the values on the file
 * on the struct pointed by api_cfgkeys_table. If this struct is null, means
 * there is a coding error, the struct must be defined by the user in its code.
 */
ret_code_t api_cfgkeys_init(void)
{
	ret_code_t ret = RET_OK;
	API_CFGKEYS_FSTAT_T fstat = {0};

	if( 0 == API_CFGKEYS_mkdir(API_CFGKEYS_DIR) )
	{
		if( 0 == API_CFGKEYS_fstat(API_CFGKEYS_ABS_PATH, &fstat) )
		{
			ret = api_cfgkeys_get_config_from_file();
			if( RET_ERR == ret )
			{	/* The file doesn't have the correct format! recreate from default.*/
				API_CFG_KEYS_LOG_WARNING("%s: bad on %s, creating from default...", __function__, API_CFGKEYS_ABS_PATH);
				ret = api_cfgkeys_reset_default_cfg();
			}
		}
		else
		{	/* We couldn't find this file in the device! we will use the default values,
			 * the user must to define this values in its code. */
			API_CFG_KEYS_LOG_WARNING("%s: error finding %s, creating from default...", __function__, API_CFGKEYS_ABS_PATH);
			ret = api_cfgkeys_reset_default_cfg();
		}
	}
	else
	{
		API_CFG_KEYS_LOG_ERROR("%s: can't create %s folder!", __function__, API_CFGKEYS_DIR);
		ret = RET_ERR;
	}

	return ret;
}

/**
 * @brief reset to default the configuration file. If the file exists, this
 * function overwrites it.
 */
ret_code_t api_cfgkeys_reset_default_cfg(void)
{
	ret_code_t ret = RET_OK;

	return ret;
}

// Private functions definition -----------------------------------------------
/**
 * @brief set the value to a uint32_t-value config key. This function set the
 * value in persistent.
 * @param key_id the key that we want to set.
 * @param val the value that we are setting to this key.
 * @return RET_OK if success.
 */
ret_code_t api_cfgkeys_set_u32(int key_id, uint32_t val)
{
	ret_code_t ret = RET_OK;
	char file_line[API_CFGKEYS_FILE_LINE_MAX_LEN] = {0};
	if( 0 < sprintf(file_line, "%d,%d", key_id, val) )
	{
		
	}
	return ret;
}

/**
 * @brief set the value to a float-value config key. This function set the
 * value in persistent.
 * @param key_id the key that we want to set.
 * @param val the value that we are setting to this key.
 * @return RET_OK if success.
 */
ret_code_t api_cfgkeys_set_float(int key_id, float val)
{
	ret_code_t ret = RET_OK;
	char file_line[API_CFGKEYS_FILE_LINE_MAX_LEN] = {0};
	if( 0 < sprintf(file_line, "%d,%f", key_id, val) )
	{

	}

	return ret;
}

/**
 * @brief get the value of a key. This key must have an uint32_t value to 
 * decode it correctly
 * @param key_id the key we want to bring its value.
 * @param val a buffer where we will store the value of that key.
 * @return RET_OK if success.
 */
ret_code_t api_cfgkeys_get_u32(int key_id, uint32_t *val)
{
	ret_code_t ret = RET_OK;
	API_CFGKEYS_FILE_T file = NULL;
	char file_line[API_CFGKEYS_FILE_LINE_MAX_LEN] = {0};
	char *str_key = NULL, str_val = NULL;
	if( NULL == val ) return RET_ERR_NULL_POINTER;

	file = API_CFGKEYS_fopen(API_CFGKEYS_ABS_PATH, "w");
	if( NULL != file )
	{	/* Get the line of the file that have the given key. */
		ret = api_cfgkey_get_key_line(key_id, file_line, sizeof(file_line), file);
		if( RET_OK == ret )
		{
			str_key = strtok(file_line, API_CFGKEYS_KEY_VAL_SEPARATOR);
			if( NULL != str_key )
			{
				*val = atoi(file_line);
			}
			else
			{
				file_line[API_CFGKEYS_FILE_LINE_MAX_LEN-1] = '\0';
				API_CFG_KEYS_LOG_ERROR("%s: cang parse entry! id=%d, line=%s", __func__, key_id, file_line);
			}
		}
		else
		{
			API_CFG_KEYS_LOG_ERROR("%s: cfgkey not found in %s ! entry=%s", __func__, API_CFGKEYS_ABS_PATH, buff);
		}

		if( 0 != API_CFGKEYS_fclose(file) )
		{
			API_CFG_KEYS_LOG_ERROR("%s: error closing %s!", __func__, API_CFGKEYS_ABS_PATH, buff);
		}
	}
	else
	{
		API_CFG_KEYS_LOG_ERROR("%s: can't open %s ", __func__, API_CFGKEYS_ABS_PATH);
	}
	return ret;
}

/**
 * @brief get the value of a key. This key must have a float value to 
 * decode it correctly
 * @param key_id the key we want to bring its value.
 * @param val a buffer where we will store the value of that key.
 * @return RET_OK if success.
 */
ret_code_t api_cfgkeys_get_float(int key_id, float *val)
{
	ret_code_t ret = RET_OK;
	API_CFGKEYS_FILE_T file = NULL;
	char file_line[API_CFGKEYS_FILE_LINE_MAX_LEN] = {0};
	char *str_key = NULL, str_val = NULL;
	if( NULL == val ) return RET_ERR_NULL_POINTER;

	file = API_CFGKEYS_fopen(API_CFGKEYS_ABS_PATH, "w");
	if( NULL != file )
	{	/* Get the line of the file that have the given key. */
		ret = api_cfgkey_get_key_line(key_id, file_line, sizeof(file_line), file);
		if( RET_OK == ret )
		{
			str_key = strtok(file_line, API_CFGKEYS_KEY_VAL_SEPARATOR);
			if( NULL != str_key )
			{
				*val = atof(file_line);
			}
			else
			{
				file_line[API_CFGKEYS_FILE_LINE_MAX_LEN-1] = '\0';
				API_CFG_KEYS_LOG_ERROR("%s: cang parse entry! id=%d, line=%s", __func__, key_id, file_line);
			}
		}
		else
		{
			API_CFG_KEYS_LOG_ERROR("%s: cfgkey not found in %s! entry=%s", __func__, API_CFGKEYS_ABS_PATH, buff);
		}

		if( 0 != API_CFGKEYS_fclose(file) )
		{
			API_CFG_KEYS_LOG_ERROR("%s: error closing %s!", __func__, API_CFGKEYS_ABS_PATH, buff);
		}
	}
	else
	{
		API_CFG_KEYS_LOG_ERROR("%s: can't open %s ", __func__, API_CFGKEYS_ABS_PATH);
	}
	return ret;
}

/**
 * @brief get the line of the file where there is a given key.
 * @param key the key to look for in the file.
 * @param buff a buffer where will be stored the line of the file.
 * @param buff_size the size of the buffer passed as argument.
 * @param file the file descriptor of the config file.
 * @return RET_OK if success and RET_ERR if there is not a line with the given 
 * key in the file.
 */
ret_code_t api_cfgkey_get_key_line(int key, char *buff, size_t buff_size, API_CFGKEYS_FILE_T file)
{
	ret_code_t ret = RET_ERR;
	char str_key[API_CFGKEYS_FILE_LINE_MAX_LEN] = {0};
	if( (NULL == buff) || (NULL == file) ) return RET_ERR_NULL_POINTER;

	/* Get line by line until we got the line with a desired key. */
	do
	{
		str_key = strtok(buff, API_CFGKEYS_KEY_VAL_SEPARATOR);
		if( NULL != str_key )
		{
			if( key == atoi(str_key) )
			{
				API_CFG_KEYS_LOG_INFO("%s: cfgkey found! entry=%s", __func__, buff);
				ret = RET_OK;
				break;
			}
		}
		else
		{
			buff[API_CFGKEYS_FILE_LINE_MAX_LEN-1] = '\0';
			API_CFG_KEYS_LOG_ERROR("%s: corrupted entry of config file! entry=%s", __func__, buff);
		}
	} while( 0 == API_CFGKEYS_fgets(buff, buff_size, file) );
	
	return ret;
}

/**
 * @brief replace a line in the config file.
 * @param old_line the line to replaced.
 * @param new_line the line that will replace the old one.
 * @return RET_OK if success.
 */
ret_code_t api_cfgkey_replace_line(char *new_line, char *old_line)
{
	ret_code_t ret = RET_OK;
	API_CFGKEYS_FILE_T *old_file = NULL;
	API_CFGKEYS_FILE_T *new_file = NULL;
	char file_line[API_CFGKEYS_FILE_LINE_MAX_LEN] = {0};
	if( (NULL == new_line) || (NULL == old_line) || (NULL == file) ) return RET_ERR_NULL_POINTER;


	if( 0 == API_CFGKEYS_rename(API_CFGKEYS_ABS_PATH, API_CFGKEYS_ABS_PATH ".old") )
	{ /* First replace the file */

	}

	old_file = API_CFGKEYS_fopen(API_CFGKEYS_ABS_PATH, "r");
	if( NULL != old_file )
	{	
		
		/* Iterate over the file to get the value */
		do
		{
			if( 0 != strcmp(old_line, new_line) ) break;
		} while( API_CFGKEYS_fgets(file_line, sizeof(file_line), old_file) );
		

		if( 0 != API_CFGKEYS_fclose(old_file) )
		{
			API_CFG_KEYS_LOG_ERROR("%s: error closing %s!", __func__, API_CFGKEYS_ABS_PATH);
		}
	}


	return ret;
}