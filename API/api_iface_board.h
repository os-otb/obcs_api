/*
 * api_iface_board.h
 *
 *	This function defines some interfaces between the Board
 *	and the API.
 *	The API only knows about "ports" and the underliying board
 *	must to be configured in order to set that ports to the correct
 *	peripherals on the hardware.
 *
 *  Created on: Jul 28, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef API_IFACE_BOARD_H_
#define API_IFACE_BOARD_H_

// Includes -------------------------------------------------------------------
#include "api_fs.h"

// MCU propierties ------------------------------------------------------------
#define BOARD_MCU_SRAM_BASE		0x20000000
#define BOARD_MCU_SRAM_SIZE		128*1024	/*!< STM32F407xx has 118 KB of SRAM. */
#define BOARD_MCU_CCM_RAM_SIZE	64*1024		/*!< STM32F407xx has 118 KB of CCM RAM. */
#define BOARD_MCU_SRAM_END	(BOARD_MCU_SRAM_BASE + BOARD_MCU_SRAM_SIZE)

// API FS ---------------------------------------------------------------------
#if 0
/*
 * Return value compatible with FATFS functions and with
 * the FreeRTOS+FAT abstraction layer implementation in this
 * API.
 */
typedef enum
{
	DISKIO_RES_OK = 0,		/* 0: Successful */
	DISKIO_RES_ERROR,		/* 1: R/W Error */
	DISKIO_RES_WRPRT,		/* 2: Write Protected */
	DISKIO_RES_NOTRDY,		/* 3: Not Ready */
	DISKIO_RES_PARERR		/* 4: Invalid Parameter */
} Diskio_DRESULT_t;
#endif
typedef api_ff_fat_dresult_t Diskio_DRESULT_t;

#ifndef DSTATUS
typedef uint8_t	DSTATUS;	/* Holding the status of the drive (STA_NOINIT, STA_NODISK, STA_PROTECT). */
#endif

#ifndef STA_NOINIT
#define STA_NOINIT		0x01 /* Drive not initialized */
#endif

#ifndef STA_NODISK
#define STA_NODISK		0x02 /* No medium in the drive */
#endif

#ifndef STA_PROTECT
#define STA_PROTECT		0x04 /* Write protected */
#endif

/* Control commands */
#ifndef CTRL_SYNC
#define CTRL_SYNC			0	/* Complete pending write process (needed at _FS_READONLY == 0) */
#endif

#ifndef GET_SECTOR_COUNT
#define GET_SECTOR_COUNT	1	/* Get media size (needed at _USE_MKFS == 1) */
#endif

#ifndef GET_SECTOR_SIZE
#define GET_SECTOR_SIZE		2	/* Get sector size (needed at _MAX_SS != _MIN_SS) */
#endif

#ifndef GET_BLOCK_SIZE
#define GET_BLOCK_SIZE		3	/* Get erase block size (needed at _USE_MKFS == 1) */
#endif

#ifndef CTRL_TRIM
#define CTRL_TRIM			4	/* Inform device that the data on the block of sectors is no longer used (needed at _USE_TRIM == 1) */
#endif

// API Temp Sensors -----------------------------------------------------------
typedef struct
{
	float temp;
	uint32_t sensor_id;
}api_temp_sensor_measure_t;

#endif /* OBCS_API_API_API_IFACE_BOARD_H_ */
