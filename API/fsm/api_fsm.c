/*
 * api_fsm.h
 *
 *  Created on: Aug 13, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Includes -------------------------------------------------------------------
#include "api_fsm.h"

// Public functions definition ------------------------------------------------
/*
 * @brief process an event incoming event.
 * @param fsm a pointer to the fsm struct that stores all the information of the fsm.
 * @param event the event that is being processed.
 * @param ev_argv and argument that the event is passing to the on_entry handler of the 
 * state.
 * @return RET_OK if success.
 * @details this function will change to another state if the event is valid. Also,
 * will execute the on_exit() function of the actual state and the on_entry() function
 * of the next state. In detail, this functions loops through the transition maps of the
 * fsm and looks if the processed event could change the actual state to another one.
 * */
ret_code_t api_fsm_process_event(api_fsm_t *fsm, uint32_t event, void *ev_argv)
{
	ret_code_t ret = RET_OK;
	if( NULL == fsm ) return RET_ERR_NULL_POINTER;
	bool valid_transition_found = false;

	for( int i = 0; i < fsm->transitions_qty; i++ )
	{	/* First look for the possible transitions of this state. */
		if( fsm->transition_map[i].from_id == fsm->actual_state->id )
		{	/* Check if the event of the transition table corresponds 
			 * to the processed event. */
			if( fsm->transition_map[i].trigg_event == event )
			{	/* The event is valid for this state! then transition to next state.
				 * First execute the on_exit() function of the current state. */
				valid_transition_found = true;
				if( NULL != fsm->actual_state->on_exit ) fsm->actual_state->on_exit();

				for( int j = 0; j < fsm->states_qty; j++ )
				{	/* Look for the ID of the next state and assing it to the actual state. */
					if( fsm->state_table[j].id == fsm->transition_map[i].to_id )
					{
						fsm->previous_state = fsm->actual_state; /* Save the previous state. */
						fsm->actual_state = &(fsm->state_table[j]);
						/* Now execute the entry function of the state. */
						if( NULL != fsm->actual_state->on_entry ) fsm->actual_state->on_entry(ev_argv);
						break; /* Now stop looking in the transition map. */
					}
				}
				/* If the transition was done, break.*/
				if( true == valid_transition_found ) break; 
			}
		}
	}
	return ret;
}

/*
 * @brief executes the ongoing function of the actual state of the fsm.
 * @param fsm the fsm to be handled.
 * @param argv an argument to be passed to the ongoing.
 * @return RET_OK if success.
 * */
ret_code_t api_fsm_work(api_fsm_t *fsm, void *argv)
{
	ret_code_t ret = RET_OK;
	if( (NULL != fsm) && (NULL != fsm->actual_state) )
	{
		/* Could happen that the state doesn't have an
		 * ongoing action. For this reason, take as valid if
		 * handler is null. */
		if( (NULL != fsm->actual_state->ongoing) )
		{
			ret = fsm->actual_state->ongoing(argv);
		}		
	}
	else
	{		
		ret = RET_ERR_NULL_POINTER;
	}
	return ret;
}
