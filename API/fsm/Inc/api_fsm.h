/*
 * api_fsm.h
 *
 *  Created on: Aug 13, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef API_FSM_H_
#define API_FSM_H_

// Includes -------------------------------------------------------------------
#include "api_types.h"

// Public variables declaration -----------------------------------------------
/*
 * @brief this is an useful struct to define states in a fsm.
 * @details the user can define an array of states and then assign a pointer to that
 * array to the api_fsm_t struct of that state machine.
 * */
typedef struct
{
	uint32_t id;							/*!< An identificator for the state. */
	ret_code_t (*on_entry)(void *argv);		/*!< Function executed when the fsm enters to the state. */
	ret_code_t (*ongoing)(void *argv);		/*!< Function executed periodically when the fsm is in the state. */
	ret_code_t (*on_exit)(void);		/*!< Function executed when the fsm exits to the state. */
} api_fsm_state_t;

/*
 * @brief use the next var to populate a state with its handlers and its ID.
 * */
#define API_FSM_DEFINE_STATE(i, entry, going, exit) { .id=i, .on_entry=entry, .ongoing=going, .on_exit=exit }

/*
 * @brief this var is designed to give a format to register how a state can transition to
 * another state when an event is present.
 * @details the user can define an array of transitions and then assign a pointer to that
 * array to the api_fsm_t struct of that state machine.
 * */
typedef struct
{
	uint32_t from_id;			/*!< The ID of the origin state. */
	uint32_t trigg_event;		/*!< The event hat is triggering the transition. */
	uint32_t to_id;				/*!< The ID of the destiny state. */
} api_fsm_transition_t;

/*
 * @brief use the next var to populate an item of a transition map.
 * */
#define API_FSM_DEFINE_TRANSITION_MAP_ITEM(from, trig, to) { .from_id=from, .trigg_event=trig, .to_id=to }

/*
 * @brief this var will handle all the information of the fsm.
 * */
typedef struct
{
	api_fsm_state_t *state_table;			/*!< A table with the possible states and the triggering events of that states. */
	size_t states_qty;						/*!< The number of elements! of the states table (how many states have the fsm?). */
	api_fsm_state_t *actual_state;			/*!< The actual state of the fsm. */
	api_fsm_state_t *previous_state;		/*!< The previous state of the fsm. It's useful to operate over self-transitions on a state. */
	api_fsm_transition_t *transition_map;	/*!< A table with the legal transitions on the fsm. */
	size_t transitions_qty;					/*!< The number of elements! the map with the transitions (how many transitions have the fsm?). */
} api_fsm_t;

// Public functions declaration -----------------------------------------------
ret_code_t api_fsm_process_event(api_fsm_t *fsm, uint32_t event, void *arg);
ret_code_t api_fsm_work(api_fsm_t *fsm, void *argv);

#endif /* API_FSM_H_ */
