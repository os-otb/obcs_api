/*
 * api_ram_errors.h
 *
 *  Created on: Sep 25, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef API_RAM_ERRORS_H_
#define API_RAM_ERRORS_H_

// Includes -------------------------------------------------------------------
#include "api_types.h"

// Public functions declaration -----------------------------------------------
ret_code_t api_ram_errors_init(void);

#endif /* OBCS_API_API_RAM_ERRORS_INC_API_RAM_ERRORS_H_ */
