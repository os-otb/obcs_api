/*
 * api_ram_errors.c
 *
 *  Created on: Sep 25, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Include --------------------------------------------------------------------
#include "api_ram_errors.h"
#include "board.h"
int Memories_Buffer_On[BOARD_23K256_MAX_RAMS_USED];


// Public functions definition ------------------------------------------------
ret_code_t api_ram_errors_init(void)
{
	ret_code_t ret = RET_OK;
	board_23k256_init();
	return ret;
}

void api_ram_errors_setup(int *buffer_num_mem)
{


   board_verifify_ram(buffer_num_mem,9);


  for (int c=0; c<9;c++)
  {
  if (buffer_num_mem[c]!=-1){
	  board_write_ram (buffer_num_mem[c]);

 }
 }



}









