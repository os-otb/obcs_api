/*
 * api_init.c
 *
 *  Created on: Jul 29, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Include --------------------------------------------------------------------
#include "api_common.h"
#include "board.h"

// Macros & constants ---------------------------------------------------------

// Private variables declaration ----------------------------------------------

// Private variables definition -----------------------------------------------

// Private functions declaration ----------------------------------------------

// Public functions definition  -----------------------------------------------
/**
 * @brief Initialize the board peripherals.
 * @return RET_OK if success.
 **/
ret_code_t API_Board_Init(void)
{
	ret_code_t ret = RET_OK;

	/* When the User Code inits the API, the API inits the Board. */
	ret = Board_Init();
	if( RET_OK != ret )
	{
		/* TODO Handle error. */
	}
	return ret;
}

/*
 * NOTE: just define this because the heap implementation we use doesn't define this.
 * */
size_t xPortGetMinimumEverFreeHeapSize( void )
{
	return 0;
}
// Private functions definition -----------------------------------------------


