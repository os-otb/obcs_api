/*
 * api_common.h
 *
 *  Created on: Jul 29, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef API_COMMON_H_
#define API_COMMON_H_

// Includes -------------------------------------------------------------------
#include "api_types.h"

// Public functions declaration  ----------------------------------------------
ret_code_t API_Board_Init(void);

#endif /* API_COMMON_H_ */
