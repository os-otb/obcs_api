/*
 * FreeRTOS_definitions.c
 *
 * 	This module have some functions that must be defined for FreeRTOS.
 * 	Some of this functions was defined in cmsis-rots abstraction layer.
 *
 *  Created on: May 9, 2021
 *      Author: Julian Rodriguez aka Marifante 
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Includes ------------------------------------------------------------------
#include "api_common.h"
#include "rtos.h"

// Macros & Constants --------------------------------------------------------
#define FREERTOS_DEFS_LOG_ENABLED			1	/*<! Enables module log. */

#define	LOG_TAG							"freertos_defs"	/*<! Tag assigned to logs for this module. */

#if FREERTOS_DEFS_LOG_ENABLED
	#define FREERTOS_DEFS_LOG_ERROR(format, ...)		LOG_ERROR(LOG_TAG, format, ##__VA_ARGS__)
	#define FREERTOS_DEFS_LOG_WARNING(format, ...)		LOG_WARNING(LOG_TAG, format, ##__VA_ARGS__)
	#define FREERTOS_DEFS_LOG_INFO(format, ...)			LOG_INFO(LOG_TAG, format, ##__VA_ARGS__)
	#define FREERTOS_DEFS_LOG_DEBUG(format, ...)		LOG_DEBUG(LOG_TAG, format, ##__VA_ARGS__)
#else
	#define FREERTOS_DEFS_LOG_ERROR(...)
	#define FREERTOS_DEFS_LOG_WARNING(...)
	#define FREERTOS_DEFS_LOG_INFO(...)
	#define FREERTOS_DEFS_LOG_DEBUG(...)
#endif

// Public functions definition -----------------------------------------------
#if (configSUPPORT_STATIC_ALLOCATION == 1)
/* External Idle and Timer task static memory allocation functions */
extern void vApplicationGetIdleTaskMemory  (StaticTask_t **ppxIdleTaskTCBBuffer,  StackType_t **ppxIdleTaskStackBuffer,  uint32_t *pulIdleTaskStackSize);
extern void vApplicationGetTimerTaskMemory (StaticTask_t **ppxTimerTaskTCBBuffer, StackType_t **ppxTimerTaskStackBuffer, uint32_t *pulTimerTaskStackSize);

/*
  vApplicationGetIdleTaskMemory gets called when configSUPPORT_STATIC_ALLOCATION
  equals to 1 and is required for static memory allocation support.
*/
__attribute__((weak)) void vApplicationGetIdleTaskMemory (StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize) {
  /* Idle task control block and stack */
  static StaticTask_t Idle_TCB;
  static StackType_t  Idle_Stack[configMINIMAL_STACK_SIZE];

  *ppxIdleTaskTCBBuffer   = &Idle_TCB;
  *ppxIdleTaskStackBuffer = &Idle_Stack[0];
  *pulIdleTaskStackSize   = (uint32_t)configMINIMAL_STACK_SIZE;
}

/*
  vApplicationGetTimerTaskMemory gets called when configSUPPORT_STATIC_ALLOCATION
  equals to 1 and is required for static memory allocation support.
*/
__attribute__((weak)) void vApplicationGetTimerTaskMemory (StaticTask_t **ppxTimerTaskTCBBuffer, StackType_t **ppxTimerTaskStackBuffer, uint32_t *pulTimerTaskStackSize) {
  /* Timer task control block and stack */
  static StaticTask_t Timer_TCB;
  static StackType_t  Timer_Stack[configTIMER_TASK_STACK_DEPTH];

  *ppxTimerTaskTCBBuffer   = &Timer_TCB;
  *ppxTimerTaskStackBuffer = &Timer_Stack[0];
  *pulTimerTaskStackSize   = (uint32_t)configTIMER_TASK_STACK_DEPTH;
}
#endif


// Work-around to use open-ocd debugger:
#ifdef __GNUC__
#define USED __attribute__((used))
#else
#define USED
#endif

const int USED uxTopUsedPriority = configMAX_PRIORITIES - 1;

/**
 * @brief Warn user if pvPortMalloc fails.
 *
 * Called if a call to pvPortMalloc() fails because there is insufficient
 * free memory available in the FreeRTOS heap.  pvPortMalloc() is called
 * internally by FreeRTOS API functions that create tasks, queues, software
 * timers, and semaphores.  The size of the FreeRTOS heap is set by the
 * configTOTAL_HEAP_SIZE configuration constant in FreeRTOSConfig.h.
 *
 */
void vApplicationMallocFailedHook()
{
	FREERTOS_DEFS_LOG_ERROR( "%s: ERROR: Malloc failed to allocate memory", __func__ );

	/* Loop forever */
	for( ; ; )
	{
		asm("NOP");
	}
}
