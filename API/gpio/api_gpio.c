/*
 * api_debug_leds.c
 *
 *  Created on: May 4, 2021
 *      Author: Julian Rodriguez aka Marifante 
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Include --------------------------------------------------------------------
#include "api_gpio.h"
#include "board.h"

#include "board_config.h"

// Macros & constants ---------------------------------------------------------

// Private variables declaration ----------------------------------------------

// Private variables definition -----------------------------------------------

// Private functions declaration ----------------------------------------------

// Public functions definition  -----------------------------------------------
ret_code_t API_GPIO_Toggle(API_GPIO_Port_Id_T gpio)
{
	ret_code_t ret = RET_OK;

	if(gpio < API_GPIO_MAX)	ret = Board_GPIO_Toggle(gpio);
	else ret = RET_ERR_INVALID_PARAMS;

	return ret;
}

/**
 * @brief set the state of a GPIO.
 * @param gpio the gpio to set its state. Must be included in the list previously
 * defined by the user in the board layer.
 * @param state true == HIGH, false == LOW.
 * @return RET_OK if success.
 */
ret_code_t API_GPIO_Set_State(API_GPIO_Port_Id_T gpio, bool state)
{
	ret_code_t ret = RET_OK;

	if(gpio < API_GPIO_MAX)	ret = Board_GPIO_Set_State(gpio, state);
	else ret = RET_ERR_INVALID_PARAMS;

	return ret;
}

/**
 * @brief set the state of a GPIO.
 * @param gpio the gpio to get its state. Must be included in the list previously
 * defined by the user in the board layer.
 * @param state var that will contain the state of the gpio, true == HIGH, false == LOW.
 * @return RET_OK if success.
 */
ret_code_t API_GPIO_Get_State(API_GPIO_Port_Id_T gpio, bool *state)
{
	ret_code_t ret = RET_OK;

	if(gpio < API_GPIO_MAX)	ret = Board_GPIO_Get_State(gpio, state);
	else ret = RET_ERR_INVALID_PARAMS;

	return ret;
}
