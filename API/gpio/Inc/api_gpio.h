/*
 * api_leds.h
 *
 *  Created on: Jul 28, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef API_GPIO_H_
#define API_GPIO_H_

// Includes -------------------------------------------------------------------
#include "api_types.h"
#include "api_iface_board.h"

// Public variables declaration -----------------------------------------------

// Public functions declaration -----------------------------------------------
ret_code_t API_GPIO_Toggle(API_GPIO_Port_Id_T gpio);
ret_code_t API_GPIO_Set_State(API_GPIO_Port_Id_T gpio, bool state);
ret_code_t API_GPIO_Get_State(API_GPIO_Port_Id_T gpio, bool *state);

#endif /* OBCS_API_API_LEDS_INC_API_LEDS_H_ */
