/*
 * api_logger.c
 *
 *  Created on: Aug 2, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Includes -------------------------------------------------------------------
#include "api_logger.h"
#include "board.h"

/* Uses of other APIs */
#include "api_datetime.h"

// Macros & constants ---------------------------------------------------------
/* @brief logger task configuration parameters. */
#define API_LOGGER_TASK_DELAY_MS			50			/*<! in ms, delay of the task. */
#define API_LOGGER_TASK_STACK_SIZE			1024
#define API_LOGGER_TASK_PRIOTY				osPriorityNormal

/* @brief circular buffer configuration parameters. */
#define API_LOG_MSG_MAX_SIZE				(BOARD_USART_MAX_PACKET_SIZE)	/*!< Max length of a single log print sent to serial log api. */
#define MAX_LOGS_IN_BUFFER					(10)									/*!< Max number of logs in the buffer at the same time. */
#define API_LOGGER_BUFFER_SIZE				(API_LOG_MSG_MAX_SIZE * MAX_LOGS_IN_BUFFER)	/*!< Size of the buffer of the logger. */
#define API_LOG_HEADER_MAX_SIZE				(20)		/*!< Max size (in bytes) of the header of the log. */
#define API_LOG_TAIL						"\r\n"		/*!< Tail of each log string sent. */
#define API_LOG_TAIL_MAX_SIZE				sizeof(API_LOG_TAIL)
#define API_LOG_PAYLOAD_MAX_SIZE			(API_LOG_MSG_MAX_SIZE-API_LOG_TAIL_MAX_SIZE-API_LOG_HEADER_MAX_SIZE) /* !< Max length of the log payload sent by another tasks.*/

#define CIRC_BUFFER_SEPARATOR_CHAR		3 /*!< ASCII 3 = EOT (End Of Text).This character separates different elements in the circular buffer. */

// Private variables declaration ----------------------------------------------
/* @brief circular buffer struct, the buffer of the logger is circular, this
 * is more fast and less memory consuming. */
typedef struct
{
	char buffer[API_LOGGER_BUFFER_SIZE]; /* !< "Buffer" of the circular buffer*/
	int head;
	int tail;
} circ_buffer_t;

typedef struct
{
	struct
	{
		char aux_buffer[API_LOG_MSG_MAX_SIZE]; /* !< Used to conform the result msg sent as log (head+payload+tail).*/
		char payload_buffer[API_LOG_PAYLOAD_MAX_SIZE]; /* !< Used to process the log data from another tasks.*/
		char header[API_LOG_HEADER_MAX_SIZE];
		char tail[API_LOG_TAIL_MAX_SIZE];
	}print;

	SemaphoreHandle_t mutex; /* !< To access to the print log there is a mutex.*/
	circ_buffer_t circ_buffer;
	TaskHandle_t task_handle;

} api_logger_t;

/* @brief return values for internal functions of logger module. */
typedef enum
{
	API_LOGGER_RET_BUFFER_EMPTY	= -2,
	API_LOGGER_RET_BUFFER_FULL = -1,
	API_LOGGER_RET_OK = 0
} api_logger_ret_code_t;


// Private variables definition -----------------------------------------------
static api_logger_t api_logger;

// Private functions declaration ----------------------------------------------
void api_logger_task(void *argument);
static api_logger_ret_code_t api_logger_circ_buffer_push(char data);
static api_logger_ret_code_t api_logger_circ_buffer_pop(char *data);
static api_logger_ret_code_t api_logger_get_circ_buffer_status(void);
static void api_logger_print_initial_message(void);

// Public functions definition  -----------------------------------------------
ret_code_t API_Logger_Init(void *arg)
{
	ret_code_t ret = RET_ERR_NULL_POINTER;
	// Create mutex for circular buffer
	MUTEX_INIT(api_logger.mutex); // Create buffer mutex
	if( api_logger.mutex != NULL )
	{
		if( xTaskCreate(	api_logger_task, "api_logger",
							API_LOGGER_TASK_STACK_SIZE,
							NULL,
							API_LOGGER_TASK_PRIOTY,
							&api_logger.task_handle ) == pdPASS )
		{	/* If the task can't be created the task handle will be NULL. */
			ret = RET_OK;
		}
		else
		{
			api_logger.task_handle = NULL;
		}
	}

	return ret;
}

void API_Logger_Print(char *tag, API_Logger_Level_T level, char *format, ...)
{
	if( (api_logger.mutex == NULL) || (format == NULL) || (tag == NULL)) return;

	/* Take mutex or block until it is free or a timeout is reached. */
	if( pdFALSE == xSemaphoreTake( api_logger.mutex, MS_TO_TICKS(50) ) ) return;

	/* Initialize variable arguments. */
	va_list arg; // Declare a list that store a variable number of arguments
	va_start(arg, format); // Store all arguments after format in the list

	char *traverse = NULL;
	/* Search if there is a '%' char on the print string. If there isn't then
	 * there is not any argument on it. */
	traverse = strchr(format, '%');
	if( NULL != traverse )
	{
		if( 0 > vsnprintf(api_logger.print.payload_buffer, API_LOG_PAYLOAD_MAX_SIZE, format, arg) )
		{
			/* TODO handler error. */
		}
	}
	else
	{
		/* There aren't arguments on the string. Only copy the string to the
		 * aux buffer (fast method if there isn't any argument on the log). */
		if(api_logger.print.payload_buffer != strncpy(api_logger.print.payload_buffer, format, API_LOG_PAYLOAD_MAX_SIZE))
		{
			/* TODO handle error. */
		}
	}

	va_end(arg);

	// TODO implement this if there is time
#if 0
	/* Check if the log have an \n at the end and deletes it. */
	char *aux_ptr = NULL;
	aux_ptr = strrchr(api_logger.print.payload_buffer, '\n');
	if( NULL == aux_ptr )
	{
		/* If there is a newline char, check if there is a carriage return before
		 * CR+LR. */
		if( *(aux_ptr-1) == '\r' )
		{
			aux_ptr = aux_ptr -1;
		}
		*aux_ptr = '\0';
	}
#endif

	/* Get the time stamp from datetime API. */
	char ts[API_DATETIME_TS_STR_LEN];
	API_Datetime_Get_Timestamp(ts, sizeof(ts));

	/* Format string with the log level, the system ms and add a LF+CR. */
	int log_length = snprintf(	api_logger.print.aux_buffer,
								API_LOG_MSG_MAX_SIZE,
								"%c[%s]|%s|%s\r\n",
								level,
								ts, // Register time of the log
								tag, // TAG to print to log
								api_logger.print.payload_buffer ); // Message of the log.

	for( int i = 0; i<(log_length+1); i++ ) // Push byte-to-byte on the logger buffer
	{
		if( API_LOGGER_RET_OK != api_logger_circ_buffer_push( api_logger.print.aux_buffer[i] ) )
		{
			xSemaphoreGive( api_logger.mutex ); // Release the mutex
			return;
		}

		/* if the buffer is full, then drop the actual data.
		@TODO	Analyze if it is better to hold the actual data and drop the older.
				Think of this as a tail-bitting snake.*/
	}

	/* Add the separator char to log. This is used to identify the elements in the buffer when
	 * the logger task is popping logs from buffer. */
	if( API_LOGGER_RET_OK != api_logger_circ_buffer_push( CIRC_BUFFER_SEPARATOR_CHAR ) )
	{
		xSemaphoreGive( api_logger.mutex ); // Release the mutex
		return;
	}

	xSemaphoreGive( api_logger.mutex ); // Release the mutex
}

// Private functions definition -----------------------------------------------

/**
 *
 * @brief The logger has a (not-big) FIFO buffer. When another module wants to
 * print a log through the logger must take the mutex of the circular buffer,
 * then push the data to the buffer and give the mutex. When the Scheduler
 * gives time to the logger, this task pop data from the FIFO buffer and send it
 * through uart.
 *
 * */
void api_logger_task(void *argument)
{
	char task_buffer[API_LOG_MSG_MAX_SIZE]; // The task can pop only one log per loop
	int task_buffer_index = 0;
	ret_code_t ret = RET_OK;
	memset(task_buffer, 0, sizeof(task_buffer));

	api_logger_print_initial_message();

	while(true)
	{
		// Take semaphore (or wait for it) to pop a string from it
		xSemaphoreTake( api_logger.mutex, portMAX_DELAY );

		/* If circular buffer is not empty & there is not a currently string in
		 * the task buffer then pop a string from the circular buffer and save it
		 * to task buffer. The task knows when have a full string in its own task
		 * buffer when reaches a CIRC_BUFFER_SEPARATOR_CHAR when is popping the
		 * circular buffer. This char separates strings in the circ buffer.
		 *   */
		while(	(API_LOGGER_RET_BUFFER_EMPTY != api_logger_get_circ_buffer_status()) &&
				(CIRC_BUFFER_SEPARATOR_CHAR != task_buffer[task_buffer_index]))
		{
			api_logger_circ_buffer_pop( &task_buffer[task_buffer_index] );

			if( CIRC_BUFFER_SEPARATOR_CHAR == task_buffer[task_buffer_index] )
				break;
			else
			{
				task_buffer_index++;
				if( task_buffer_index >= (API_LOG_MSG_MAX_SIZE-1) )
					break;
			}
		}

		// Release mutex, so another tasks can push data to circular buffer
		xSemaphoreGive( api_logger.mutex );

		/* If the taskBufferIndex points to an element with CIRC_BUFFER_SEPARATOR_CHAR
		 * so a string must be sended. The task send it and waits until the UART
		 * finishes the transfer. Meanwhile, the circular buffer can be pushed with
		 * logs that belongs to another tasks. */
		if( CIRC_BUFFER_SEPARATOR_CHAR == task_buffer[task_buffer_index] )
		{
			while(RET_ERR_BUSY == Board_USART_TX_Is_Busy(API_USART_SERIAL_LOG))
			{
				/* Wait until the serial transference is complete. */
			}

			ret = Board_USART_Send_Packet(API_USART_SERIAL_LOG, task_buffer, strlen(task_buffer));
			if(RET_OK != ret)
			{
				/* TODO handle error. */
			}

			/* When the transference is finished clean the buffer with the log.
			 * NOTE: if this buffer is cleared before the transference is finished then
			 * log will be lost */
			task_buffer_index = 0;
			memset(task_buffer, 0, sizeof(task_buffer)); // Clean the task buffer in every loop
		}
	}
}

/**
 *
 * @brief circular buffer push function. Push a byte it into the buffer and increment
 * the head (write pointer).
 *
 **/
static api_logger_ret_code_t api_logger_circ_buffer_push(char data)
{
	api_logger_ret_code_t ret = API_LOGGER_RET_OK;
    int next;

    next = api_logger.circ_buffer.head + 1;  // Next is where head will point to after this write.
    if (next >= sizeof(api_logger.circ_buffer.buffer))
    {
        next = 0;	// If it is the max, completes one round
    }

    if (next == api_logger.circ_buffer.tail)  // If the head + 1 == tail, circular buffer is full
    {
    	ret = API_LOGGER_RET_BUFFER_FULL;
    }
    else
    {
    	api_logger.circ_buffer.buffer[api_logger.circ_buffer.head] = data;  // Load data and then move
    	api_logger.circ_buffer.head = next; 				            // Head to next data offset.
    }
    return ret;  // Return success to indicate successful push.
}

/**
 *
 * @brief circular buffer pop function. Takes a byte from the buffer and increment
 * the tail (read pointer).
 *
 **/
static api_logger_ret_code_t api_logger_circ_buffer_pop( char *data )
{
	api_logger_ret_code_t ret = API_LOGGER_RET_OK;
    int next;

    if(api_logger.circ_buffer.head == api_logger.circ_buffer.tail)  // If the head == tail, we don't have any data
    {
    	ret = API_LOGGER_RET_BUFFER_EMPTY;
    }
    else
    {
        next = api_logger.circ_buffer.tail + 1;  // Next is where tail will point to after this read.
        if(next >= sizeof(api_logger.circ_buffer.buffer))
        {
        	next = 0;
        }

        *data = api_logger.circ_buffer.buffer[api_logger.circ_buffer.tail];  // Read data and then move
        api_logger.circ_buffer.tail = next; 				             // tail to next offset.
    }

    return ret;
}

/**
 *
 * @brief checks if the buffer is empty.
 *
 * @return LOGGER_RET_BUFFER_EMPTY if the buffer is empty and LOGGER_RET_BUFFER_FULL if it is full.
 * Returns LOGGER_RET_OK if the buffer have data but is not full.
 *
 **/
static api_logger_ret_code_t api_logger_get_circ_buffer_status(void)
{
	api_logger_ret_code_t ret = API_LOGGER_RET_OK;

    if(api_logger.circ_buffer.head == api_logger.circ_buffer.tail)  // If the head == tail, we don't have any data
    {
    	ret = API_LOGGER_RET_BUFFER_EMPTY;
    }
    else
    if(api_logger.circ_buffer.head + 1 == api_logger.circ_buffer.tail)  // If the head + 1 == tail, circular buffer is full
    {
    	ret = API_LOGGER_RET_BUFFER_FULL;
    }

    return ret;
}

/**
 * @brief print the initial message through the UART.
 */
static void api_logger_print_initial_message(void)
{
	while(RET_ERR_BUSY == Board_USART_TX_Is_Busy(API_USART_SERIAL_LOG))
	{
		/* Wait until the serial transference is complete. */
	}
	Board_USART_Send_Packet(API_USART_SERIAL_LOG, API_LOGGER_INITIAL_MSG_TITLE, strlen(API_LOGGER_INITIAL_MSG_TITLE));
}
