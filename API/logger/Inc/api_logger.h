/*
 * api_logger.h
 *
 *  Created on: Aug 2, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef API_LOGGER_H_
#define API_LOGGER_H_

// Include --------------------------------------------------------------------
#include "api_types.h"

// Macros & constants ---------------------------------------------------------
typedef enum
{
	LOG_LEVEL_INFO = 		'I',
	LOG_LEVEL_WARNING =		'W',
	LOG_LEVEL_DEBUG =		'D',
	LOG_LEVEL_ERROR =		'E',
} API_Logger_Level_T;

#define API_LOG_IMPL(tag, format, log_level, ...) {API_Logger_Print(tag, log_level, format, ##__VA_ARGS__);}

/**
 * @brief macros used to log prints in anothers modules, just define TAG as a
 * string in another modules.
 **/
#define LOG_INFO(tag, format, ...)		API_LOG_IMPL(tag, format, LOG_LEVEL_INFO, ##__VA_ARGS__)
#define LOG_WARNING(tag, format, ...)	API_LOG_IMPL(tag, format, LOG_LEVEL_WARNING, ##__VA_ARGS__)
#define LOG_DEBUG(tag, format, ...)		API_LOG_IMPL(tag, format, LOG_LEVEL_DEBUG, ##__VA_ARGS__)
#define LOG_ERROR(tag, format, ...)		API_LOG_IMPL(tag, format, LOG_LEVEL_ERROR, ##__VA_ARGS__)

// Public functions declaration -----------------------------------------------
//TaskHandle_t API_Logger_Init(void *arg);
ret_code_t API_Logger_Init(void *arg);

/* @brief print a desired string through the log medium. */
void API_Logger_Print(char *tag, API_Logger_Level_T level, char *format, ...);

#endif /* API_LOGGER_H_ */
