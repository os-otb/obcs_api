/*
 * api_osotb_rx.c
 *
 *	This API implements the OSOTB protocol reception. A variant of the 
 *  ndjson protocol, used to communicate a payload with a host. 
 *  In this module, we have the parsing of the received cjson objects.
 *
 *  Created on: Jul 11, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Include --------------------------------------------------------------------
#include "api_osotb_commands.h"

/* Othe APIs includes */
#ifndef UNIT_TEST
#include "api_logger.h"
#endif

/* cJSON includes */
#include "cJSON.h"
#include "cJSON_Utils.h"

//#include "rtos.h"

// Macros & constants ---------------------------------------------------------
#define API_OSOTB_RX_LOG_ENABLED			1	/*<! Enables module log. */

#define	LOG_TAG		"api_osotb_rx"	/*<! Tag assigned to logs for this module. */

#if API_OSOTB_RX_LOG_ENABLED

#ifdef UNIT_TEST
#ifdef UNIT_TESTS_LOGS_ENABLED
	#define API_OSOTB_RX_LOG_ERROR(format, ...)		printf(format, ##__VA_ARGS__)
	#define API_OSOTB_RX_LOG_WARNING(format, ...)		printf(format, ##__VA_ARGS__)
	#define API_OSOTB_RX_LOG_INFO(format, ...)			printf(format, ##__VA_ARGS__)
	#define API_OSOTB_RX_LOG_DEBUG(format, ...)		printf(format, ##__VA_ARGS__)
#else
	#define API_OSOTB_RX_LOG_ERROR(...)
	#define API_OSOTB_RX_LOG_WARNING(...)
	#define API_OSOTB_RX_LOG_INFO(...)
	#define API_OSOTB_RX_LOG_DEBUG(...)
#endif
#else
	#define API_OSOTB_RX_LOG_ERROR(format, ...)		LOG_ERROR(LOG_TAG, format, ##__VA_ARGS__)
	#define API_OSOTB_RX_LOG_WARNING(format, ...)	LOG_WARNING(LOG_TAG, format, ##__VA_ARGS__)
	#define API_OSOTB_RX_LOG_INFO(format, ...)		LOG_INFO(LOG_TAG, format, ##__VA_ARGS__)
	#define API_OSOTB_RX_LOG_DEBUG(format, ...)		LOG_DEBUG(LOG_TAG, format, ##__VA_ARGS__)
#endif
#else
	#define API_OSOTB_RX_LOG_ERROR(...)
	#define API_OSOTB_RX_LOG_WARNING(...)
	#define API_OSOTB_RX_LOG_INFO(...)
	#define API_OSOTB_RX_LOG_DEBUG(...)
#endif

// Private variables declaration -----------------------------------------------

// Private variables definition -----------------------------------------------

// Private functions declaration ----------------------------------------------
ret_code_t api_osotb_get_resource_from_header(cJSON *json_header, char *res_name);
ret_code_t api_osotb_get_method_from_header(cJSON *json_header, char *method_name);
ret_code_t api_osotb_get_payload_ptr(cJSON *json_root, char *payload_str);
ret_code_t api_osotb_process_proto_version_from_header(cJSON *json_header);

// Public functions definition  -----------------------------------------------
/*
 * @brief parse the command and iterate through the list of known commands
 * to see if the received command is known or not. If the received command
 * is known, calls its handler.
 * @param api_osotb_rx the api_osotb_rx struct that stores the table
 * with the resources/methods/callbacks needed.
 * @param buffer_packet the packet to process.
 * @param buffer_packet_size the size of the buffer where the packet is stored.
 * @param buffer_response a buffer where the proper response for the incomming packet
 * will be stored.
 * @return RET_OK if the packet have a well known format and the server have a handler for that
 * method targetting to this resource.
 * */
ret_code_t api_osotb_rx_process_pkt(api_osotb_rx_t *api_osotb_rx, char *buffer_packet, size_t buffer_packet_size, char *buffer_response)
{
	if( (NULL == buffer_packet) || (NULL == buffer_response) || (NULL == api_osotb_rx) ) return RET_ERR_NULL_POINTER;
	ret_code_t ret = RET_OK;
	cJSON *json_root = NULL, *json_header = NULL;
	char resource_str[API_OSOTB_RES_NAME_MAX_LEN] = {0};
	char method_str[API_OSOTB_METHOD_NAME_MAX_LEN] = {0};
	char payload_str[API_OSOTB_PAYLOAD_MAX_LEN] = {0};
	char payload_response[API_OSOTB_PAYLOAD_MAX_LEN];

	/* Check if the payload received from the TCP message contains a 
	 * stringified JSON. */
	API_OSOTB_RX_LOG_DEBUG("%s: pkt=%s", __func__, buffer_packet);
	json_root = cJSON_Parse(buffer_packet);
	if( NULL == json_root ) ret = RET_ERR_INVALID_PARAMS; // The buffer received is not a valid ndjson payload
	else
	{
		json_header = cJSON_GetObjectItem(json_root, API_OSOTB_JSON_HEADER_KEY);
		if( NULL == json_header ) ret = RET_ERR_INVALID_PARAMS; // The buffer received doesn't have a header object inside
		else
		{
			ret = api_osotb_get_resource_from_header(json_header, resource_str);
			ret = api_osotb_get_method_from_header(json_header, method_str);			
			ret = api_osotb_get_payload_ptr(json_root, payload_str);
			if( RET_OK == ret )
			{	/* Iterate in the table if there is a handler for this combination of
				 * RESOURCE|METHOD. */
				API_OSOTB_RX_LOG_INFO("%s: method=%s, res=%s, pay=%s", __func__, method_str, resource_str, payload_str);

				int c = 0;
				for(c = 0; c<api_osotb_rx->table_size; c++)
				{
					if( ( 0 == strcmp(api_osotb_rx->table[c].resource, resource_str )) &&
						( 0 == strcmp(api_osotb_rx->table[c].method, method_str) ) )
					{	/* Copy the resource to the argument buffer (in this way, could be used later for the caller function). */

						if( (RET_OK == ret) && (NULL != api_osotb_rx->table[c].handler) )
						{ /* Execute the handler defined by the user code. This handler must define the payload that will 
						   * be sent to the client in the response. */
							api_osotb_rx->table[c].handler(payload_str, payload_response);
							break;
						}
					}
				}
				if( c >= api_osotb_rx->table_size )
				{
					API_OSOTB_RX_LOG_WARNING("%s: no handler for method=%s, res=%s", __func__, method_str, resource_str);
					ret = RET_ERR_INVALID_PARAMS;
				}
			}
		}
		cJSON_Delete(json_root);
	}

	// Now, we need to make the response!
    if( RET_ERR_INVALID_PARAMS == ret )
	{
		sprintf(buffer_response, API_OSOTB_GEN_RESPONSE, API_OSOTB_PROTOCOL_VERSION, API_OSOTB_REPLY_404, "N/A", "N/A");
	}
	else if( RET_OK == ret )
	{
		sprintf(buffer_response, API_OSOTB_GEN_RESPONSE, API_OSOTB_PROTOCOL_VERSION, API_OSOTB_REPLY_200, resource_str, payload_response);
	}
    else
	{	/* Unknown ret value or RET_ERR from api_osotb_rx_process_pkt. */
		sprintf(buffer_response, API_OSOTB_GEN_RESPONSE, API_OSOTB_PROTOCOL_VERSION, API_OSOTB_REPLY_500, "N/A", "N/A");
	}

	return ret;
}

// Private functions definition -----------------------------------------------
/*
 * @brief parse the resource of the command received.
 * @param json_header must be the json object of the header.
 * @param res_name buffer where the name of the resource string will be stored.
 * @return RET_OK if success.
 * */
ret_code_t api_osotb_get_resource_from_header(cJSON *json_header, char *res_name)
{
	if( (NULL == json_header) || (NULL == res_name)) return RET_ERR_NULL_POINTER;
	ret_code_t ret = RET_OK;
	cJSON *res_item = NULL;
	res_item = cJSON_GetObjectItem(json_header, API_OSOTB_JSON_HEADER_RESOURCE_KEY);
	if( NULL == res_item ) ret = RET_ERR_INVALID_PARAMS;
	else
	{
		if( strlen(res_item->valuestring) < API_OSOTB_RES_NAME_MAX_LEN ) strcpy(res_name, res_item->valuestring);
		else ret = RET_ERR;
	}
	return ret;
}

/*
 * @brief parse the method of the command received.
 * @param json_header must be the json object of the header.
 * @param method_name a buffer where the method name string will be stored. 
 * @return RET_OK if success.
 * */
ret_code_t api_osotb_get_method_from_header(cJSON *json_header, char *method_name)
{
	if( (NULL == json_header) || (NULL == method_name) ) return RET_ERR_NULL_POINTER;
	ret_code_t ret = RET_OK;
	cJSON *res_item = NULL;
	res_item = cJSON_GetObjectItem(json_header, API_OSOTB_JSON_HEADER_METHOD_KEY);
	if( NULL == res_item ) ret = RET_ERR_INVALID_PARAMS;
	else
	{
		if( strlen(res_item->valuestring) < API_OSOTB_METHOD_NAME_MAX_LEN ) strcpy(method_name, res_item->valuestring);
		else ret = RET_ERR;
	}
	return ret;
}

/**
 * @brief get a pointer to the payload from the root json of the packet.
 * @param json_root the root json of the packet.
 * @param payload_ptr this will be stored a pointer to the payload string.
 * @return RET_OK if success.
 */
ret_code_t api_osotb_get_payload_ptr(cJSON *json_root, char *payload_str)
{
	if( NULL == json_root ) return RET_ERR_NULL_POINTER;
	ret_code_t ret = RET_OK;
	cJSON *pay_item = NULL;
	
	pay_item = cJSON_GetObjectItem(json_root, API_OSOTB_JSON_PAYLOAD_KEY);
	if( NULL == pay_item ) ret = RET_ERR_INVALID_PARAMS;
	else
	{
		if( strlen(pay_item->valuestring) < API_OSOTB_PAYLOAD_MAX_LEN ) strcpy(payload_str, pay_item->valuestring);
		else ret = RET_ERR;
	}

	return ret;
}

#if 0 // This is a WIP were we can process the version of the protocol 
///*
// * @brief checks the version of the protocol of the payload received.
// *
// * @param json_header must be the json object of the header.
// *
// * @return RET_ERR_INVALID_PARAMS if the packet received doesn't have any protocol version
// * or if the protocol version is greater than the actual. RET_ERR if the protocol
// * version is equal or lesser than the actual.
// *
// * */
//ret_code_t api_osotb_process_proto_version_from_header(cJSON *json_header)
//{
//	ret_code_t ret = RET_OK;
//	cJSON *json_temp = NULL;
//	char *str_proto_version[API_OSOTB_OSOTB_HEADER_VER_VALUE_STR_LEN+5]; // cJSON recommends allocate 5 bytes more
//	int pkt_proto_version = 0;
//
//	if( NULL == json_header )
//	{
//		ret = RET_ERR_NULL_POINTER;
//	}
//	else
//	{
//		json_temp = cJSON_GetObjectItem(json_header, API_OSOTB_JSON_HEADER_VER_KEY);
//		if( NULL == json_temp )
//		{
//			ret = RET_ERR_INVALID_PARAMS; // The cmd received doesn't have a version key, is invalid
//		}
//		else
//		{
//			if( 1 == cJSON_PrintPreallocated(json_temp, str_proto_version, API_OSOTB_OSOTB_HEADER_VER_VALUE_STR, 0) )
//			{
//				pkt_proto_version = atoi(str_proto_version);
//				if( pkt_proto_version > atoi(API_OSOTB_PROTOCOL_VERSION) )
//				{
//					/* If the packet received have a protocol version greater than the actual used by the code
//					 * then will be an error. */
//				}
//			}
//			else
//			{
//				ret = RET_ERR; // Was a problem parsing the protocol version
//			}
//		}
//	}
//	return ret;
//}
#endif
