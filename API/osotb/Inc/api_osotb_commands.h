/*
 * api_osotb_replies.h
 *
 *  Created on: Jul 14, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef API_OSOTB_COMMANDS_H_
#define API_OSOTB_COMMANDS_H_

// Includes -------------------------------------------------------------------
#include "api_types.h"
#include "api_osotb_types.h"

// Private variables declaration ----------------------------------------------

// Private functions declaration ----------------------------------------------
ret_code_t api_osotb_rx_process_pkt(api_osotb_rx_t *api_osotb_rx, char *buffer_packet, size_t buffer_packet_size, char *buffer_response);
ret_code_t api_osotb_rx_init(api_osotb_rx_handler_table_item_t *rx_table, uint32_t rx_table_size);

// Resources & methods definitions --------------------------------------------
// cJSON object definitions 
#define API_OSOTB_HEADER_VER_VALUE_STR_LEN	4		/* !< The length of the value of the version key in the header. */
#define API_OSOTB_JSON_DELIMITER_CHAR		'\n' 	/* !< This is the character that delimites the JSON on the payloads. */
#define API_OSOTB_JSON_HEADER_KEY			"header"/* !< The key of the header in the root json. */ 
#define API_OSOTB_JSON_HEADER_VER_KEY		"ver"	/* !< The key of the version field inside the header object. */
#define API_OSOTB_JSON_HEADER_METHOD_KEY	"method"/* !< The key of the method field inside the header object. */
#define API_OSOTB_JSON_HEADER_RESOURCE_KEY	"resource"	/* !< The key of the resource field inside the header object. */
#define API_OSOTB_JSON_PAYLOAD_KEY		"payload"	/* !< The key of the payload in the root json. */

// Common responses to requests -----------------------------------------------
/* Maximum length in bytes of the reply of the OSOTB sv. */
//#define API_OSOTB_SV_RESPONSE_MAX_LEN (API_OSOTB_PAYLOAD_MAX_LEN+API_OSOTB_JSON_KEYS_MAX_LEN+API_OSOTB_RES_NAME_MAX_LEN+API_OSOTB_METHOD_NAME_MAX_LEN)
#define API_OSOTB_SV_RESPONSE_MAX_LEN 300

/* A general OSOTB reply that can be filled with a sprintf() type function. */
#define API_OSOTB_GEN_RESPONSE "{\"header\":{\"ver\":\"%s\",\"method\":\"RESPONSE\",\"status\":\"%s\",\"resource\":\"%s\"},\"payload\":\"%s\"}\n"

/* Success responses. */
#define API_OSOTB_REPLY_200   "200 OK"

/* Client error responses. */
#define API_OSOTB_REPLY_404   "404 Not Found"

/* Server error responses. */
#define API_OSOTB_REPLY_500   "500 Internal Server Error"

#endif
