/*
 * api_osotb_replies.h
 *
 *  Created on: Jul 14, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef API_OSOTB_TYPES_H_
#define API_OSOTB_TYPES_H_

// Macros & constants ---------------------------------------------------------
#define API_OSOTB_PROTOCOL_VERSION	"100"	/* !< The version of the payload <-> host protocol actually used. */

/* @brief the methods that can be handled in the OSOTB protocol. The methods
 * are defined by the protocol. */
#define API_OSOTB_METHOD_GET		"GET"
#define API_OSOTB_METHOD_PUT		"PUT"
#define API_OSOTB_METHOD_RESPONSE	"RESPONSE"
#define API_OSOTB_MAX_METHODS	    3 /*!< Max number of methods handled by the protocol. */

#define API_OSOTB_JSON_KEYS_MAX_LEN				10		/* !< The maximun length of the keys of the json objects in this protocol.*/
#define API_OSOTB_RES_NAME_MAX_LEN				16		/* !< The maximun length of the resources in this protocol.*/
#define API_OSOTB_METHOD_NAME_MAX_LEN			8		/* !< The maximun length of the methods in this protocol.*/
#define API_OSOTB_PAYLOAD_MAX_LEN				120		/* !< The maximum length of the valued of the payload field in this protocol. */

#define API_OSOTB_RX_HANDLER_TABLE_SET_ITEM(m, r, rh) {.method=m, .resource=r, .handler=rh}

// Public types definition ----------------------------------------------------
/* @brief the function pointer used to develop the RX handlers of OSOTB. */
typedef ret_code_t (*api_osotb_rx_handle_t)(char *payload, char *payload_response); /*!< Function pointer var declaration. */

/* @brief an entry to the rx handlers commands table. */
typedef struct
{
	char resource[API_OSOTB_RES_NAME_MAX_LEN];	/*!< The name (string) of the resource. */
	char method[API_OSOTB_METHOD_NAME_MAX_LEN];	/*!< The name (string) of the method. */
	api_osotb_rx_handle_t handler;
} api_osotb_rx_handler_table_item_t;

typedef struct
{
	api_osotb_rx_handler_table_item_t *table;
	uint32_t table_size;
}api_osotb_rx_t;

#endif
