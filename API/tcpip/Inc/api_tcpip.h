/*
 * api_tcpip.h
 *
 *  Created on: Jul 28, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef API_TCPIP_H_
#define API_TCPIP_H_

// Includes -------------------------------------------------------------------
#include "api_types.h"
#include "rtos.h"

// Macros & Constants ---------------------------------------------------------
#define API_TCPIP_TCP_SERVER_BUFFER_SIZE ipconfigTCP_MSS /*!< Size (in bytes) of the receive buffer of the tcp server. */
#define API_TCPIP_TCP_SERVER_MAX_CLIENTS 1 /*!< Max number of clients connected to the tcp server. */

// Public functions declarations ----------------------------------------------
TaskHandle_t API_TCPIP_Init( void *arg );
ret_code_t API_TCPIP_Create_TCP_Server(	uint32_t port, TickType_t receive_timeout );
ret_code_t API_TCPIP_TCP_Server_Wait_Connection( uint8_t *child_number );
ret_code_t API_TCPIP_TCP_Server_Receive( char *rx_buffer, size_t rx_buffer_size, uint8_t child_number, uint32_t *bytes_received);
void API_TCPIP_TCP_Server_Close_Socket( void );
ret_code_t API_TCPIP_TCP_Server_Close_Child_Socket( uint8_t child_number );
ret_code_t API_TCPIP_Listen_TCP_Server( BaseType_t max_clients );

#endif /* API_TCPIP_H_ */
