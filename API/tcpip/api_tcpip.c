/*
 * tcpip.c
 *
 *  Created on: May 23, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Include --------------------------------------------------------------------
#include "api_tcpip.h"
#include "board.h"

// Macros and constants -------------------------------------------------------
/* The default IP and MAC address used by the demo.  The address configuration
defined here will be used if ipconfigUSE_DHCP is 0, or if ipconfigUSE_DHCP is
1 but a DHCP server could not be contacted.  See the online documentation for
more information.  http://www.FreeRTOS.org/tcp */
static const uint8_t ucIPAddress[ 4 ] = { configIP_ADDR0, configIP_ADDR1, configIP_ADDR2, configIP_ADDR3 };
static const uint8_t ucNetMask[ 4 ] = { configNET_MASK0, configNET_MASK1, configNET_MASK2, configNET_MASK3 };
static const uint8_t ucGatewayAddress[ 4 ] = { configGATEWAY_ADDR0, configGATEWAY_ADDR1, configGATEWAY_ADDR2, configGATEWAY_ADDR3 };
static const uint8_t ucDNSServerAddress[ 4 ] = { configDNS_SERVER_ADDR0, configDNS_SERVER_ADDR1, configDNS_SERVER_ADDR2, configDNS_SERVER_ADDR3 };

/* Default MAC address configuration. */
const uint8_t ucMACAddress[ 6 ] = { configMAC_ADDR0, configMAC_ADDR1, configMAC_ADDR2, configMAC_ADDR3, configMAC_ADDR4, configMAC_ADDR5 };

// Private variables declaration ---------------------------------------------
typedef struct
{
	Socket_t socket;	/* Socket that handles the client connections to a server. */
	uint8_t number;	/* Number to identify the client. */
}child_t;

typedef struct
{
	struct
	{
		struct freertos_sockaddr xBindAddress;	/*!< Got the addresses of the socket (port). */
		Socket_t xListeningSocket;	/*!< TCP Server that will be listening. */
		socklen_t xSize;			/*!< Size of the socket of the TCP Server. */
		TickType_t xReceiveTimeOut;	/*!< Receive timeout of the server. */
		BaseType_t xBacklog;		/*!< Maximum number of clients connected to the server. */
		child_t clients[API_TCPIP_TCP_SERVER_MAX_CLIENTS];	/*!< The TCP+IP stack creates a child socket for every connected client. */
		int clients_qty;			/*!< Number of the clients connected. */
	}tcp_server;

}api_tcpip_t;

// Private variables definition -----------------------------------------------
api_tcpip_t api_tcpip;

// Private functions declaration ----------------------------------------------

// Public functions definition  -----------------------------------------------
/**
 *
 * @brief init TCP-IP stack.
 *
 * @return the task handle of the task that impkement the IP stack.
 */
TaskHandle_t API_TCPIP_Init( void *arg )
{
	TaskHandle_t ip_task_handle = NULL;
	memset(&api_tcpip, 0, sizeof(api_tcpip));

	/* Initialise the network interface.

	***NOTE*** Tasks that use the network are created in the network event hook
	when the network is connected and ready for use (see the definition of
	vApplicationIPNetworkEventHook() below).  The address values passed in here
	are used if ipconfigUSE_DHCP is set to 0, or if ipconfigUSE_DHCP is set to 1
	but a DHCP server cannot be	contacted. */
//	FreeRTOS_printf( ( "FreeRTOS_IPInit\n" ) ); // TODO see how to implement debug through ethernet
	if( pdPASS == FreeRTOS_IPInit( ucIPAddress, ucNetMask, ucGatewayAddress, ucDNSServerAddress, ucMACAddress ) )
	{
		ip_task_handle = xGet_xIPTaskHandle();
	}

	return ip_task_handle;
}

/**
 *
 * @brief create a TCP server.
 *
 * @param port				port of the server socket.
 * @param max_client		the maximum number of simutaneous clients connected.
 * @param receive_timeout	the timeout used in API_TCPIP_TCP_Server_Wait_Connection (in ticks of the RTOS).
 *
 * @return RET_OK if success.
 *
 */
ret_code_t API_TCPIP_Create_TCP_Server(	uint32_t port,
													TickType_t receive_timeout )
{
	ret_code_t ret = RET_OK;
	BaseType_t rtos_ret = pdFREERTOS_ERRNO_NONE;
	api_tcpip.tcp_server.xReceiveTimeOut = receive_timeout;

	/* Attempt to open the socket. */
	api_tcpip.tcp_server.xListeningSocket = FreeRTOS_socket(	FREERTOS_AF_INET,
																	FREERTOS_SOCK_STREAM,  /* SOCK_STREAM for TCP. */
																	FREERTOS_IPPROTO_TCP );
	/* Check the socket was created. */
	if( FREERTOS_INVALID_SOCKET != api_tcpip.tcp_server.xListeningSocket )
	{
		/* If FREERTOS_SO_RCVBUF or FREERTOS_SO_SNDBUF are to be used with
		FreeRTOS_setsockopt() to change the buffer sizes from their default then do
		it here!.  (see the FreeRTOS_setsockopt() documentation. */

		/* If ipconfigUSE_TCP_WIN is set to 1 and FREERTOS_SO_WIN_PROPERTIES is to
		be used with FreeRTOS_setsockopt() to change the sliding window size from
		its default then do it here! (see the FreeRTOS_setsockopt()
		documentation. */

		/* Set a time out so accept() will just wait for a connection. */
		rtos_ret = FreeRTOS_setsockopt(	api_tcpip.tcp_server.xListeningSocket,
										0,
										FREERTOS_SO_RCVTIMEO,
										&api_tcpip.tcp_server.xReceiveTimeOut,
										sizeof( api_tcpip.tcp_server.xReceiveTimeOut ) );
		if( pdFREERTOS_ERRNO_NONE == rtos_ret )
		{
			/* Fill in the buffer and window sizes that will be used by the socket. */
			WinProperties_t xWinProps;
			xWinProps.lTxBufSize = ipconfigTCP_TX_BUFFER_LENGTH;
			xWinProps.lTxWinSize = configTCP_SERVER_TX_WINDOW_SIZE;
			xWinProps.lRxBufSize = ipconfigTCP_RX_BUFFER_LENGTH;
			xWinProps.lRxWinSize = configTCP_SERVER_RX_WINDOW_SIZE;
			rtos_ret = FreeRTOS_setsockopt(	api_tcpip.tcp_server.xListeningSocket,
											0,
											FREERTOS_SO_WIN_PROPERTIES,
											( void * ) &xWinProps,
											sizeof( xWinProps ) );
			if( pdFREERTOS_ERRNO_NONE == rtos_ret )
			{
				/* Set the listening port. */
				api_tcpip.tcp_server.xBindAddress.sin_port = ( uint16_t ) port;
				api_tcpip.tcp_server.xBindAddress.sin_port = FreeRTOS_htons( api_tcpip.tcp_server.xBindAddress.sin_port );
				/* Bind the socket to the port that the client RTOS task will send to. */
				rtos_ret = FreeRTOS_bind(	api_tcpip.tcp_server.xListeningSocket,
											&(api_tcpip.tcp_server.xBindAddress),
											sizeof( api_tcpip.tcp_server.xBindAddress ) );
				if( pdFREERTOS_ERRNO_NONE != rtos_ret )
				{
					ret = RET_ERR;
				}
			}
			else
			{
				ret = RET_ERR;
			}
		}

		/* If the server socket was created but there was an error making its configuration or
		 * binding it to a port, close it. */
		if( RET_OK != ret )
		{
			API_TCPIP_TCP_Server_Close_Socket();
		}
	}
	else
	{
		ret = RET_ERR;
	}

	return ret;
}

/**
 *
 * @brief set the TCP server in listening mode.
 *
 * @param max_client		the maximum number of simutaneous clients connected.
 *
 * @return RET_OK if success.
 *
 */
ret_code_t API_TCPIP_Listen_TCP_Server( BaseType_t max_clients )
{
	ret_code_t ret = RET_OK;
	BaseType_t rtos_ret = pdFREERTOS_ERRNO_NONE;
	api_tcpip.tcp_server.xBacklog = max_clients;
	/* Set the socket into a listening state so it can accept connections.
	The maximum number of simultaneous connections is limited to max_clients.
	API ref: https://www.freertos.org/FreeRTOS-Plus/FreeRTOS_Plus_TCP/API/listen.html. */
	rtos_ret = FreeRTOS_listen(	api_tcpip.tcp_server.xListeningSocket,
								api_tcpip.tcp_server.xBacklog );
	if( pdFREERTOS_ERRNO_NONE != rtos_ret )
	{
		ret = RET_ERR;
	}
	return ret;
}

/**
 *
 * @brief wait until a client connects to the the tcp server. If no connections
 * are received in the timeout previously set then return err.
 * The FreeRTOS+TCP stack is configured to automatically create a child socket to
 * handle any connections accepted on a listening TCP/IP. Child sockets inherit
 * the buffer sizes and sliding window sizes of their parent sockets.
 *
 * @param child_number this API returns the number of the child that has been connected.
 *
 * @return RET_OK if success.
 *
 */
ret_code_t API_TCPIP_TCP_Server_Wait_Connection( uint8_t *child_number )
{
	ret_code_t ret = RET_OK;
	Socket_t child_socket;
	struct freertos_sockaddr client_address;

	if( API_TCPIP_TCP_SERVER_MAX_CLIENTS <= api_tcpip.tcp_server.clients_qty )
	{
		/* Max number of childs (clients connected) reached!. */
		ret = RET_ERR;
	}
	else
	if( NULL == child_number )
	{
		ret = RET_ERR_NULL_POINTER;
	}
	else
	{
		child_socket = FreeRTOS_accept(	api_tcpip.tcp_server.xListeningSocket,
										&client_address,
										&api_tcpip.tcp_server.xSize);

		if( NULL == child_socket )
		{
			// The timeout was reached!
			ret = RET_ERR_TIMEOUT;
		}
		else
		if( FREERTOS_INVALID_SOCKET == child_socket )
		{
			// API_TCPIP_Create_TCP_Server wasnt called first!
			ret = RET_ERR;
		}

		if( RET_OK == ret )
		{
			/* If OK then a client was connected to the socket!
			 * Save the socket on the struct.
			 * TODO: For now, just can handle one client at a time.
			 * */
			api_tcpip.tcp_server.clients[api_tcpip.tcp_server.clients_qty].socket = child_socket;
			*child_number = api_tcpip.tcp_server.clients_qty; // Return the child number to the application
			api_tcpip.tcp_server.clients_qty++;
		}
	}

	return ret;
}

/**
 *
 * @brief wait for a timeout (previously set) for messages from the client.
 *
 * @param rx_buffer buffer where the data will be stored.
 * @param rx_buffer_size size of the buffer where the data will be stored.
 * @param child_number the client from which data is expected.
 *
 * @return RET_OK if there is data sent by the client.
 *
 */
ret_code_t API_TCPIP_TCP_Server_Receive( char *rx_buffer, size_t rx_buffer_size, uint8_t child_number, uint32_t *bytes_received )
{
	ret_code_t ret = RET_OK;
	if( API_TCPIP_TCP_SERVER_MAX_CLIENTS < child_number )
	{
		ret = RET_ERR;
	}
	else
	if( (NULL == rx_buffer) || (NULL == api_tcpip.tcp_server.clients[child_number].socket) || (NULL == bytes_received))
	{
		ret = RET_ERR_NULL_POINTER;
	}
	else
	{
		char cRxedData[API_TCPIP_TCP_SERVER_BUFFER_SIZE];
		BaseType_t lBytesReceived;

		/* Receive another block of data into the cRxedData buffer. */
		lBytesReceived = FreeRTOS_recv( api_tcpip.tcp_server.clients[child_number].socket,
										cRxedData,
										API_TCPIP_TCP_SERVER_BUFFER_SIZE,
										0 );

		if( lBytesReceived > 0 )
		{
			/* Data was received, return it. */
			if( lBytesReceived > rx_buffer_size )
			{
				// Limit data to prevent an overflow.
				lBytesReceived = rx_buffer_size;
			}
			memcpy(rx_buffer, cRxedData, lBytesReceived);
			*bytes_received = lBytesReceived;
		}
		else if( lBytesReceived == 0 )
		{
			/* No data was received, but FreeRTOS_recv() did not return an error.
			Timeout? */
			ret = RET_ERR_TIMEOUT;
		}
		else
		{
			/* Error (maybe the connected socket already shut down the socket?). */
			ret = RET_ERR;
		}
	}

	return ret;
}

/**
 *
 * @brief shutdown and close the tcp server socket.
 *
 */
void API_TCPIP_TCP_Server_Close_Socket( void )
{
	char cRxedData[8]; // Just a dummy buffer to use FreeRTOS_recv

	/* First make a gracefully shutdown on the connected socket. */
	FreeRTOS_shutdown(	 api_tcpip.tcp_server.xListeningSocket,
						FREERTOS_SHUT_RDWR );

	while( FREERTOS_EINVAL !=
			FreeRTOS_recv( 	api_tcpip.tcp_server.xListeningSocket,
							&cRxedData,
							API_TCPIP_TCP_SERVER_BUFFER_SIZE,
							0 ) )
	{
		/* Wait until FreeRTOS_recv returns FREERTOS_EINVAL to verify that
		 * the socket was shutdown correctly.*/
	}

	FreeRTOS_closesocket(api_tcpip.tcp_server.xListeningSocket);
}

/**
 *
 * @brief shutdown and close a child socket. This child sockets handles
 * the incoming tcp connections.
 *
 * @param child_number the client socket to close.
 *
 */
ret_code_t API_TCPIP_TCP_Server_Close_Child_Socket( uint8_t child_number )
{
	ret_code_t ret = RET_OK;

	if( API_TCPIP_TCP_SERVER_MAX_CLIENTS < child_number )
	{
		ret = RET_ERR;
	}
	else
	if( NULL == api_tcpip.tcp_server.clients[child_number].socket )
	{
		ret = RET_ERR_NULL_POINTER;
	}
	else
	{
		char cRxedData[8]; // Just a dummy buffer to use FreeRTOS_recv
		/* First make a gracefully shutdown on the connected socket. */
		FreeRTOS_shutdown(	api_tcpip.tcp_server.clients[child_number].socket,
							FREERTOS_SHUT_RDWR );

		while(	FREERTOS_EINVAL !=
				FreeRTOS_recv( 	api_tcpip.tcp_server.clients[child_number].socket,
								&cRxedData,
								API_TCPIP_TCP_SERVER_BUFFER_SIZE,
								0 ) )
		{
			/* Wait until FreeRTOS_recv returns FREERTOS_EINVAL to verify that
			 * the socket was shutdown correctly.*/
		}

		FreeRTOS_closesocket(api_tcpip.tcp_server.clients[child_number].socket);
		api_tcpip.tcp_server.clients_qty--;
	}

	return ret;
}
