/*
 * api_pd_ctrl.c
 *
 *  Created on: Aug 28, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Includes -------------------------------------------------------------------
#include "api_pd_ctrl.h"

// Macros & Constants ---------------------------------------------------------

// Public functions definition ------------------------------------------------
/*
 * @brief init pd control API.
 *
 */
ret_code_t api_pd_ctrl_init(void)
{
	ret_code_t ret = RET_OK;
	ret = pi_pd_ctrl_turn_off_all_pd();
	return ret;
}

ret_code_t api_pd_ctrl_turn_on(int pd_id)
{

}

ret_code_t api_pd_ctrl_turn_off(int pd_id)
{

}

// Private functions definition -----------------------------------------------

