/*
 * api_pd_ctrl.h
 *
 *  Created on: Aug 28, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef API_PD_CTRL_H_
#define API_PD_CTRL_H_

// Public variables declaration -----------------------------------------------
typedef struct
{
	int pd_id;
	int pin;
}api_pd_t;

#endif /* API_PD_CTRL_H_ */
