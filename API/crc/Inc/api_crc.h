/*
 * api_crc.h
 *
 *  Created on: Oct 17, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef API_CRC_H_
#define API_CRC_H_

// Includes -------------------------------------------------------------------
#include "api_types.h"

// Public functions declaration -----------------------------------------------
uint16_t api_crc16_ccitt_byte(uint16_t crc_in, uint8_t data);
uint16_t api_crc16_ccitt_buf(const uint8_t * pbuf, uint16_t len);

#endif /* API_CRC_H_ */
