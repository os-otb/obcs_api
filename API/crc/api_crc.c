/*
 * api_crc.c
 *
 *  Created on: Oct 17, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Includes -------------------------------------------------------------------
#include <crc/Inc/api_crc.h>

// Public functions definition ------------------------------------------------
/**
 * @brief calculate CRC16 CCITT of one byte. It's a 16 bit CRC with
 * polynomial x^16 + x^12 + x^5 + 1
 * @param crc_in the CRC before (0 for first step).
 * @param data byte for crc calculation.
 * @return the CRC16 value
 */
uint16_t api_crc16_ccitt_byte(uint16_t crc_in, uint8_t data)
{
	crc_in  = (uint8_t)(crc_in >> 8)|(crc_in << 8);
	crc_in ^=  data;
	crc_in ^= (uint8_t)(crc_in & 0xff) >> 4;
	crc_in ^= (crc_in << 8) << 4;
	crc_in ^= ((crc_in & 0xff) << 4) << 1;

	return crc_in;
}

/**
 * @brief calculate the CRC16 CCITT of a buffer.
 * @param pbuf the data to which we will calculate the crc.
 * @param len the length of the data
 * @return the CRC16 value
 */
uint16_t api_crc16_ccitt_buf(const uint8_t * pbuf, uint16_t len)
{
	uint16_t crc = 0;
	int i = 0;
	for(i = 0; i < len; i++)
	{
		crc = api_crc16_ccitt_byte(crc, pbuf[i]);
	}
	return crc;
}
