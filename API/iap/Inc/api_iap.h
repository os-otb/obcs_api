/*
 * api_iap.h
 *
 *  Created on: Jul 28, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef API_IAP_H_
#define API_IAP_H_

// Includes -------------------------------------------------------------------
#include "api_types.h"

// Private functions declaration ----------------------------------------------
/*
 * @brief this are the slots of APPs that the API can handle.
 * Each slot is a defined flash sector (or sectors) where an APP
 * can be flashed.
 * */
typedef enum
{
	API_IAP_APP0_SLOT = 0,
	API_IAP_APP1_SLOT,
	API_IAP_MAX_SLOTS,	/*!< max quantity of app slots. */
}api_iap_app_slot_t;

// Public functions declaration -----------------------------------------------
ret_code_t api_iap_init(void);
ret_code_t api_iap_program(api_iap_app_slot_t app_slot, char *filename);
ret_code_t api_iap_check_bootloader_condition(bool *cond);
ret_code_t api_iap_jump_to_app(api_iap_app_slot_t app_slot);
bool api_iap_app_has_good_integrity(api_iap_app_slot_t app_slot);
api_iap_app_slot_t api_iap_get_default_app(void);
ret_code_t api_iap_set_default_app(api_iap_app_slot_t app_slot);

#endif /* API_IAP_H_ */
