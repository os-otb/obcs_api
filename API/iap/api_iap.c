/*
 * api_iap.c
 *
 *  Created on: Ago 27, 2021
 *  	Author: Julian Rodriguez
 *
 *      Author: Julian Rodriguez aka Marifante 
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Include --------------------------------------------------------------------
#include "api_iap.h"
#include "api_iface_board.h"
#include "board.h"

#include "board_config.h"

/* Uses of other APIs */
#include "api_gpio.h"
#include "api_flash.h"
#include "api_fs.h"

// Macros & constants ---------------------------------------------------------
#define API_IAP_LOG_ENABLED			1	/*<! Enables module log. */

#define	LOG_TAG							"api_iap"	/*<! Tag assigned to logs for this module. */

#if API_IAP_LOG_ENABLED
	#define API_IAP_LOG_ERROR(format, ...)		LOG_ERROR(LOG_TAG, format, ##__VA_ARGS__)
	#define API_IAP_LOG_WARNING(format, ...)	LOG_WARNING(LOG_TAG, format, ##__VA_ARGS__)
	#define API_IAP_LOG_INFO(format, ...)		LOG_INFO(LOG_TAG, format, ##__VA_ARGS__)
	#define API_IAP_LOG_DEBUG(format, ...)		LOG_DEBUG(LOG_TAG, format, ##__VA_ARGS__)
#else
	#define API_IAP_LOG_ERROR(...)
	#define API_IAP_LOG_WARNING(...)
	#define API_IAP_LOG_INFO(...)
	#define API_IAP_LOG_DEBUG(...)
#endif

#define API_IAP_RAM_BUFFER_SIZE     (4096)

#define API_IAP_COND1_GPIO 	API_GPIO_COND1
#define API_IAP_COND2_GPIO	API_GPIO_COND2
#define API_IAP_COND3_GPIO	API_GPIO_COND3

// Private variables declaration ----------------------------------------------
typedef void(*pfunction)(void); /*!< Function pointer var declaration. */

typedef struct
{
	uint8_t ram_buffer[API_IAP_RAM_BUFFER_SIZE];
}api_iap_t;

// Private variables definition -----------------------------------------------
static api_iap_t self_;

// Private functions declaration ----------------------------------------------
static ret_code_t api_iap_flash_binary_from_file(api_iap_app_slot_t app_slot, char *abs_path);

// Public functions definition  -----------------------------------------------
/**
 * @brief init iap API.
 * */
ret_code_t API_IAP_Init(void)
{
	memset(&self_, 0, sizeof(self_));
	return RET_OK;
}

/**
 * @brief start iap process. This function opens the file that contains the
 * binary of the application and erase the sectors that will be written in
 * a future with this binary.
 * @param app_slot the app_slot to flash (TODO: for now, only works with one slot
 * and this param is ignored).
 * @param abs_path the abosolute path to the binary file..
 * @return RET_OK if success.
 * TODO: add to process the another app slots too!
 * */
ret_code_t api_iap_program(api_iap_app_slot_t app_slot, char *abs_path)
{
	if( NULL == abs_path ) return RET_ERR_NULL_POINTER;
	if( API_IAP_MAX_SLOTS < app_slot ) return RET_ERR_INVALID_PARAMS;
	uint32_t flash_sectors_total_size = 0, app_sectors_mask;
	ret_code_t ret = RET_OK;
	int fs_ret = 0;

	app_sectors_mask = (app_slot == API_IAP_APP0_SLOT) ? API_IAP_APP0_SECTORS_LOCATION : API_IAP_APP1_SECTORS_LOCATION;
	API_FS_fstat_t fstat = {0};
	fs_ret = API_FS_fstat(abs_path, &fstat);
	if( 0 == fs_ret )
	{	/* Get the sum of the sizes of the sectors that will be flashed. */
		ret = API_Flash_Get_Sectors_Total_Size(app_sectors_mask, &flash_sectors_total_size);
		if( RET_OK == ret )
		{	/* Check if the total size of the sectors is greater than the size of the binary that
			 * will be written. */
			if( fstat.fsize <= flash_sectors_total_size )
			{	/* Erase the sectors that will be written. */
				ret = API_FLASH_Erase_Sectors(app_sectors_mask);
				if( RET_OK == ret )
				{
					ret = api_iap_flash_binary_from_file(app_slot, abs_path);
					if( RET_OK != ret )
					{
						API_IAP_LOG_ERROR("%s: error flashing binary from file! (ret=%d)", __func__, ret);
					}
				}
				else
				{
					API_IAP_LOG_ERROR("%s: cannot erase sectors 0x%h! ", __func__, app_sectors_mask);
				}
			}
		}
	}
	else
	{
		ret = RET_ERR;
		API_IAP_LOG_ERROR("%s: err getting info about binary file %s", __func__, abs_path);
	}

	return ret;
}

/**
 * @brief check the condition to start in bootloader mode. The condition
 * will be true when 2 or more GPIOS of the API_IAP_CONDx_GPIO
 * are in LOW state.
 * @param cond var where a true is stored if the bootloader condition
 * is true. A false is set otherwise.
 * @return RET_OK if success.
 * */
ret_code_t api_iap_check_bootloader_condition(bool *cond)
{

	ret_code_t ret = RET_OK;
	bool state = false;
	uint8_t low_count = 0;

	if( NULL == cond )
	{
		ret = RET_ERR_NULL_POINTER;
	}
	else
	{
		*cond = false;
		ret = API_GPIO_Get_State(API_IAP_COND1_GPIO, &state);
		if( RET_OK == ret )
		{
			if(false == state) low_count++;
			ret = API_GPIO_Get_State(API_IAP_COND2_GPIO, &state);
			if( RET_OK == ret )
			{
				if(false == state) low_count++;
				ret = API_GPIO_Get_State(API_IAP_COND3_GPIO, &state);
				if( RET_OK == ret )
				{
					if(false == state) low_count++;
					if(2 <= low_count) *cond = true;
				}
			}
		}
	}

	return ret;
}

/*
 * @brief jump to application located in the flash.
 * @details The function carries out the following operations.
 * */
ret_code_t api_iap_jump_to_app(api_iap_app_slot_t app_slot)
{
	ret_code_t ret = RET_ERR;
	uint32_t app_address = 0;
	uint32_t jump_address = 0;
	pfunction jump = NULL;
	api_flash_sector_t app_first_sector = 0;

	if( API_IAP_MAX_SLOTS > app_slot )
	{
		app_first_sector = (API_IAP_APP1_SLOT == app_slot) ? API_IAP_APP1_FIRST_SECTOR_LOC : API_IAP_APP0_FIRST_SECTOR_LOC;

		if( false == API_FLASH_Is_There_A_Valid_App(app_first_sector) )
		{
			ret = RET_ERR;
		}
		else
		{
			app_address = Board_FLASH_Get_Sector_Address(app_first_sector);
			jump_address = *((__IO uint32_t*)(app_address + 4));

			jump = (pfunction) jump_address; /* The +4 is to give an offset of 1 word. */

			Board_Prepare_For_IAP_Jump(app_address);

			jump(); /* Jump to user application. */

			while(true)
			{
				/* Never must reach here!. */
			}
		}
	}

	return ret;
}

/**
 * @brief get the default app slot. The bootloader will jump to the default app slot
 * automatically (if has good integrity) when inits.
 * @return API_IAP_APP0_SLOT or API_IAP_APP1_SLOT. This always will succes because
 * if a non-valid default app is readed from the flash, will writea valid one.
 * If any error is detected, the API will return the API_IAP_APP0_SLOT as
 * the default app. And if no app_slot is stored in the flash, the
 * API_IAP_APP0_SLOT will be burned into flash.
 *
 * @details the default app slot is stored in the internal FLASH of the MCU. The
 * user can select the address of where this value is stored configuring the
 * values API_IAP_DEFAULT_APP_SLOT_FLASH_SECTOR and
 * API_IAP_DEFAULT_APP_SLOT_FLASH_SECTOR_OFFSET.
 * The app slot is stored as triplicated in the flash, so, a voting system
 * is implemented in the read operation!. If the APP1 slot is stored as default then
 * the word 0xXXFFFFFF will be the first of the API_IAP_DEFAULT_APP_SLOT_FLASH_SECTOR. If the
 * APP0 slot is stored as default then the word 0xXX000000 will be the first of the
 * API_IAP_DEFAULT_APP_SLOT_FLASH_SECTOR.
 *
 */
api_iap_app_slot_t api_iap_get_default_app(void)
{
	ret_code_t ret = RET_OK;
	api_iap_app_slot_t def_slot = API_IAP_APP0_SLOT;
	uint32_t word_from_flash;

	int app0_slot_votes = 0, app1_slot_votes = 0;
	/* Read the word in the flash where the default app slot is stored.
	 * Remember that this value is stored by triplicated, so we must vote
	 * if it is the APP0 slot or the APP1 slot. */
	ret = API_FLASH_Read_From(	(1 << API_IAP_DEFAULT_APP_SLOT_FLASH_SECTOR),
								API_IAP_DEFAULT_APP_SLOT_FLASH_SECTOR_OFFSET,
								&word_from_flash, 1);
	if( RET_ERR != ret )
	{
		/* Vote! */
		if( 0 == (word_from_flash & 0xFF) ) app0_slot_votes++;
		else if( 0xFF == (word_from_flash & 0xFF) ) app1_slot_votes++;

		if( 0 == (word_from_flash & 0xFF00) ) app0_slot_votes++;
		else if( 0xFF00 == (word_from_flash & 0xFF00) ) app1_slot_votes++;

		if( 0 == (word_from_flash & 0xFF0000) ) app0_slot_votes++;
		else if( 0xFF0000 == (word_from_flash & 0xFF0000) ) app1_slot_votes++;

		def_slot = (app0_slot_votes > app1_slot_votes) ? API_IAP_APP0_SLOT : API_IAP_APP1_SLOT;

		if( app0_slot_votes == app1_slot_votes )
		{	/* This only can happen in a weird situation where the FLASH is in an
		 	 * unstable situation. Here just reflash with the APP0 for simplicity. */
			API_IAP_LOG_WARNING("%s: the default app slot bits was in a weird condition (0x%H) setting APP%d as defualt!",__func__, (0x00FFFFFF & word_from_flash), API_IAP_APP0_SLOT);
			ret = api_iap_set_default_app(API_IAP_APP0_SLOT);
			if( RET_OK != ret )
			{
				API_IAP_LOG_ERROR("%s: error reading the default app slot and writing the ROM default (%lu)!",__func__, API_IAP_APP0_SLOT);
			}
			else
			{
				def_slot = API_IAP_APP0_SLOT;
			}
		}
	}
	return def_slot;
}


/**
 * @brief set the default app slot. The bootloader will jump to the default app slot
 * automatically (if has good integrity) when inits.
 * @param app_slot app_slot that will be set as default.
 * @return RET_OK if success.
 *
 * @details the app slot is stored as triplicated in the flash, so, a voting system
 * can be implemented in the read operation!. If the APP1 slot is stored as default then
 * the word 0xXXFFFFFF will be the first of the API_IAP_DEFAULT_APP_SLOT_FLASH_SECTOR. If the
 * APP0 slot is stored as default then the word 0xXX000000 will be the first of the
 * API_IAP_DEFAULT_APP_SLOT_FLASH_SECTOR.
 */
ret_code_t api_iap_set_default_app(api_iap_app_slot_t app_slot)
{
	ret_code_t ret = RET_OK;
	if( (app_slot != API_IAP_APP0_SLOT) && (app_slot != API_IAP_APP1_SLOT) ) return RET_ERR_INVALID_PARAMS;

	uint8_t buff[3] = {0};
	if( app_slot == API_IAP_APP1_SLOT ) memset(buff, 0xFF, sizeof(buff));

	/* Erase the flash sector before writing the default app slot into it. Some MCUs
	 * only support sector erase... (as STM32 MCUs). */
	ret = API_FLASH_Erase_Sectors(1 << API_IAP_DEFAULT_APP_SLOT_FLASH_SECTOR);
	if( RET_OK == ret )
	{
		ret = API_FLASH_Write_From(	(1 << API_IAP_DEFAULT_APP_SLOT_FLASH_SECTOR),
									API_IAP_DEFAULT_APP_SLOT_FLASH_SECTOR_OFFSET,
									buff, sizeof(buff));
	}

	return ret;
}


/**
 * @brief checks if a determined app slot has good integrity.
 * @param app_slot the app slot to check its integrity.
 * @return true if the app has good integrity.
 * TODO: this must be moved to the api_flash.
 */
bool api_iap_app_has_good_integrity(api_iap_app_slot_t app_slot)
{
	bool ret = false;
	if( API_IAP_MAX_SLOTS > app_slot )
	{
		ret = API_FLASH_Is_There_A_Valid_App(app_slot);
	}
	return ret;
}

// Private functions definition -----------------------------------------------
/**
 * @brief burn a file to the flash memory.
 * @param app_slot the app slot where the binary will be flashed.
 * @param abs_path the absolute path to the binary file.
 * @return RET_OK if succes.
 * @details this function assumes that the disk is mounted.
 */
ret_code_t api_iap_flash_binary_from_file(api_iap_app_slot_t app_slot, char *abs_path)
{
	if( NULL == abs_path ) return RET_ERR_NULL_POINTER;
	if( API_IAP_MAX_SLOTS < app_slot ) return RET_ERR_INVALID_PARAMS;

	ret_code_t ret = RET_OK;
	bool read_flag = true;
	uint32_t read_size = 0x00, tmp_read_size = 0x00, offset = 0, app_sectors_mask = 0;
	app_sectors_mask = (app_slot == API_IAP_APP0_SLOT) ? API_IAP_APP0_SECTORS_LOCATION : API_IAP_APP1_SECTORS_LOCATION;

	API_FS_FILE *program_file = NULL;
	program_file = API_FS_fopen(abs_path, "r");
	if( NULL != program_file )
	{
		read_size = API_IAP_RAM_BUFFER_SIZE; /* Starts in the buffer size. */
		while( true == read_flag )
		{
			/* Read maximum "BUFFERSIZE" Kbyte from the selected file  */
			read_size = API_FS_fread(self_.ram_buffer, sizeof(self_.ram_buffer[0]), read_size, program_file);

			/* Break if there is an error reading the file or if there isn't more data to read. */
			if( 0 >= read_size ) break;

			/* Temp variable to hold the size of the data readed from the file. */
			tmp_read_size = read_size;

			/* If the size of the data readed is less than the buffer, this means that the end of the file
			 * has been reached (read data < "BUFFERSIZE" Kbyte). */
			if(tmp_read_size < API_IAP_RAM_BUFFER_SIZE) read_flag = false; // At the end of the program binary file

			ret = API_FLASH_Write_From(app_sectors_mask, offset, self_.ram_buffer, read_size);
			if( RET_OK != ret )
			{	/* Break if there is an error writing to the flash. */
				break;
			}
			else
			{	/* Update last programmed address value */
				offset += tmp_read_size;
			}
		}
		if( 0 != API_FS_fclose(program_file) ) ret = RET_ERR;
	}
	return ret;
}
