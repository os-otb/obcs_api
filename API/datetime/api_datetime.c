/*
 * api_datetime.c
 *
 *	This API stores the current date and time in a struct. The tasks
 *	of the project can use a "get" to get the current timestamp.
 *
 *  Created on: Jun 26, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */


// Include --------------------------------------------------------------------
#include "api_datetime.h"
#include "board.h"

// Macros & constants ---------------------------------------------------------
/* Default time stamp of the module. The timestamp of the module can be
 * re-synced with a function. */
#define API_DATETIME_DEFAULT_YEAR	2000	/*!< Default year of the timestamp when it is initialized. */
#define API_DATETIME_DEFAULT_MONTH	0		/*!< Default month of the timestamp when it is initialized. */
#define API_DATETIME_DEFAULT_DATE	0		/*!< Default day of the timestamp when it is initialized. */
#define API_DATETIME_DEFAULT_DOW	0		/*!< Default Day Of the Week of the timestamp when it is initialized. */
#define API_DATETIME_DEFAULT_HOUR	00		/*!< Default hour of the timestamp when it is initialized. */
#define API_DATETIME_DEFAULT_MIN	00		/*!< Default minute of the timestamp when it is initialized. */
#define API_DATETIME_DEFAULT_SEC	00		/*!< Default seconds of the timestamp when it is initialized. */

/* RTOS Timer. */
#define API_DATETIME_OS_TIMER_NAME		"api_datetime_tmr"
#define API_DATETIME_OS_TIMER_RES_MS	1000	/*!< Resolution (in ms) of the timer that updates the timestamp. */

/* RTOS Mutex. */
#define API_DATETIME_MUTEX_UNLOCK()     MUTEX_UNLOCK(api_datetime.mutex_handle)
#define API_DATETIME_MUTEX_LOCK()       MUTEX_LOCK(api_datetime.mutex_handle)

// Private variables declaration ----------------------------------------------
/*
 * @brief used to identify each day of the week.
 * */
typedef enum
{
	SUNDAY = 0,
	MONDAY,
	TUESDAY,
	WEDNESDAY,
	THURSDAY,
	FRIDAY,
	SATURDAY = 6,
} day_of_week_t;

/*
 * @brief used to identify each month of the year.
 * */
typedef enum
{
	JANUARY = 0,
	FEBRUARY,
	MARCH,
	APRIL,
	MAY,
	JUNE,
	JULY,
	AUGUST,
	SEPTEMBER,
	OCTOBER,
	NOVEMBER,
	DECEMBER = 11
} month_t;

typedef struct
{
	struct tm data; /*!< Struct defined in time.h (standard C library) that holds the information of the timestamp. */
	uint32_t timestamp;	/*!< Will be incremented each API_DATETIME_OS_TIMER_RES_MS. */

	SemaphoreHandle_t mutex_handle;	/*!< Mutex to manage the resource access of this module. */
	TimerHandle_t timer_handle;	/*!< Software timer that will increase the timestamp. */
} api_datetime_t;

// Private variables definition -----------------------------------------------
api_datetime_t api_datetime;
const uint8_t api_datetime_days_per_month[] = {31,28,31,30,31,30,31,31,30,31,30,31};

// Private functions declaration ----------------------------------------------
static void api_datetime_timer_cb(TimerHandle_t xTimer);
static bool api_datetime_update_time(void);
static void api_datetime_update_date(void);

// Public functions definition  -----------------------------------------------
/*
 * @brief init datetime API. The time will not increase until the scheduler of the
 * RTOS has begun.
 *
 * */
ret_code_t API_Datetime_Init(void)
{
	ret_code_t ret = RET_OK;
	memset(&api_datetime, 0, sizeof(api_datetime));

	/* Set default timestamp: */
	api_datetime.data.tm_year = API_DATETIME_DEFAULT_YEAR-1900;
	api_datetime.data.tm_mon = API_DATETIME_DEFAULT_MONTH;
	api_datetime.data.tm_mday = API_DATETIME_DEFAULT_DATE;
	api_datetime.data.tm_wday = API_DATETIME_DEFAULT_DOW;
	api_datetime.data.tm_hour = API_DATETIME_DEFAULT_HOUR;
	api_datetime.data.tm_min = API_DATETIME_DEFAULT_MIN;
	api_datetime.data.tm_sec = API_DATETIME_DEFAULT_SEC;

	/* Create mutex to share this resource among various tasks. */
	MUTEX_INIT(api_datetime.mutex_handle); /* Create mutex for datetime module. */

	/* Create 1s timer. */
	api_datetime.timer_handle = xTimerCreate(	API_DATETIME_OS_TIMER_NAME,
												MS_TO_TICKS(API_DATETIME_OS_TIMER_RES_MS),
												pdTRUE, (void*)0, api_datetime_timer_cb);
	if( NULL == api_datetime.timer_handle )
	{
		ret = RET_ERR;
	}
	else
	{
		/* Start the timer. If this is called before the scheduler starts, the timer
		 * will not be started until the scheduler begun. */
		if( pdPASS != xTimerStart( api_datetime.timer_handle, 0 ) )
		{
			ret = RET_ERR;
		}
	}

	return ret;
}

/*
 * @brief get the actual timestamp parsed in a string extended form.
 * See ISO-8601 extended format.
 * @param buff where the timestamp will be stored as a string
 * */
ret_code_t API_Datetime_Get_Timestamp(char *buff, size_t buff_size)
{
	ret_code_t ret = RET_OK;

	API_DATETIME_MUTEX_UNLOCK();
	if( NULL == buff )
	{
		ret = RET_ERR_NULL_POINTER;
	}
	else
	{
		/* Because we dont have strftime, we make a "little" format to
		 * ISO 8601 with sprintf. */
		sprintf(buff, "%04d-%02d-%02dT%02d:%02d:%02d",
				api_datetime.data.tm_year+1900,
				api_datetime.data.tm_mon,
				api_datetime.data.tm_mday,
				api_datetime.data.tm_hour,
				api_datetime.data.tm_min,
				api_datetime.data.tm_sec);
	}

	API_DATETIME_MUTEX_LOCK();
	return ret;
}

/*
 * @brief get the actual timestamp parsed in a string basic form.
 * See ISO-8601 basic format.
 * @param buff where the timestamp will be stored as a string
 * */
ret_code_t API_Datetime_Get_Timestamp_Basic(char *buff, size_t buff_size)
{
	ret_code_t ret = RET_OK;

	API_DATETIME_MUTEX_UNLOCK();
	if( NULL == buff )
	{
		ret = RET_ERR_NULL_POINTER;
	}
	else
	{
		/* Because we dont have strftime, we make a "little" format to
		 * ISO 8601 with sprintf. */
		sprintf(buff, "%04d%02d%02dT%02d%02d%02d",
				api_datetime.data.tm_year+1900,
				api_datetime.data.tm_mon,
				api_datetime.data.tm_mday,
				api_datetime.data.tm_hour,
				api_datetime.data.tm_min,
				api_datetime.data.tm_sec);
	}

	API_DATETIME_MUTEX_LOCK();
	return ret;
}

/*
 * @brief get the actual timestamp in the form of an tm_struct form.
 *
 * @param ts_struct where the timestamp data will be stored.
 * */
ret_code_t API_Datetime_Get_Timestamp_Struct(struct tm *ts_struct)
{
	ret_code_t ret = RET_OK;

	API_DATETIME_MUTEX_UNLOCK();
	if( NULL == ts_struct )
	{
		ret = RET_ERR_NULL_POINTER;
	}
	else
	{
		if( ts_struct != memcpy(ts_struct, &api_datetime.data, sizeof(api_datetime.data)))
		{
			ret = RET_ERR;
		}
	}

	API_DATETIME_MUTEX_LOCK();
	return ret;
}

// Private functions definition -----------------------------------------------
/*
 * @brief periodic callback function invoked every API_DATETIME_OS_TIMER_RES_MS
 *
 * */
static void api_datetime_timer_cb(TimerHandle_t xTimer)
{
	API_DATETIME_MUTEX_UNLOCK();

	// increment timestamp
	api_datetime.timestamp++;

	// update date and time
	if(true == api_datetime_update_time())
	{
		api_datetime_update_date();
	}

	API_DATETIME_MUTEX_LOCK();
}

/*
 * @brief update time in the module struct (seconds, minutes and hours).
 *
 * @return true if there is a day change; false otherwise.
 *
 * */
static bool api_datetime_update_time(void)
{
	bool day_overflow = false;

	api_datetime.data.tm_sec++;   /* First increment seconds. */

	/* Then check if seconds overflows. */
	if( api_datetime.data.tm_sec > 59 )
	{
		api_datetime.data.tm_sec = 0;
		api_datetime.data.tm_min++; /* If seconds overflows increment minutes. */
		/* Then check if minutes overflow. */
		if(api_datetime.data.tm_min > 59)
		{
			api_datetime.data.tm_min = 0;
			api_datetime.data.tm_hour++;	/* If minutes overflow, increment hours. */
			/* Then check if hours overflows. */
			if(api_datetime.data.tm_hour > 23)
			{
				api_datetime.data.tm_sec = 0;
				api_datetime.data.tm_min = 0;
				api_datetime.data.tm_hour = 0;
				day_overflow = true; /* If hours overflow, then there is a day overflow!. */
			}
		}
	}
	return day_overflow;
}

/*
 * @brief update the date in the datetime module struct (day of week, date, month & year).
 *
 * */
static void api_datetime_update_date(void)
{
	bool leap_year;
	uint8_t days_in_month_aux;

	/* Check if leap year. ("Año bisiesto" in spanish). */
	leap_year = (api_datetime.data.tm_year % 4) ? false : true;

	/* Increment the date. */
	api_datetime.data.tm_mday++;

	/* Update day of the week. */
	api_datetime.data.tm_wday++;
	/* Checks if day overflows. */
	if(SATURDAY < api_datetime.data.tm_wday)
	{
		/* Overflows! Go back to sunday. */
		api_datetime.data.tm_wday = SUNDAY;
	}

	/* The aux var is just to take in count the exception of the
	 * leap year. */
	days_in_month_aux = api_datetime_days_per_month[api_datetime.data.tm_mon];
	/* Check special consideration for leap year. February have
	 * 29 days in leap year. */
	if((true == leap_year) && (FEBRUARY == api_datetime.data.tm_mon))
	{
		days_in_month_aux = 29;	/* Extra day in February. */
	}

	/* Check if day overflows. */
	if(api_datetime.data.tm_mday > days_in_month_aux)
	{
		api_datetime.data.tm_mday = 1;
		api_datetime.data.tm_mon++; /* Day overflows, then increment the month. */
		/* Check if month overflows. */
		if(api_datetime.data.tm_mon > DECEMBER)
		{
			api_datetime.data.tm_mon = 1;
			api_datetime.data.tm_year++; /* Month overflows, then increment the year. */
		}
	}
}

#ifdef API_FS_USE_FF_FAT
/*
 * @brief equivalent of time() : returns the number of seconds after 1-1-1970.
 * @details it is used by FreeRTOS+FAT. If FreeRTOS+FAT it is used and the time
 * is supported, then this must be included. */
time_t FreeRTOS_time( time_t * pxTime )
{
	time_t time = 0;
	API_DATETIME_MUTEX_UNLOCK();
	time = mktime(&api_datetime.data);
	API_DATETIME_MUTEX_LOCK();
	if( NULL == pxTime ) *pxTime = time;
	return time;
}
#endif

