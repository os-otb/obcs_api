/*
 * api_datetime.h
 *
 *  Created on: Jul 28, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef API_DATETIME_H_
#define API_DATETIME_H_

// Includes -------------------------------------------------------------------
#include "api_types.h"
#include "time.h"

// Macros & Constants  --------------------------------------------------------
#define API_DATETIME_TS_STR_LEN	(19+1)	/*!< String length taking in count the null terminating char of timestamp (ISO-8601)*/

// Public variable declaration ------------------------------------------------
#ifndef _TIME_H_
/* This is defined in the standard time.h library. To maintain a "portability"
 * of code, the same structures and functions that are in said library were
 * defined and used. Trying to import the time.h file of the tool vendor in
 * the build makes conflict with the printf-stdarg.c used for FreeRTOS+TCP. */
struct tm
{
	int	tm_sec;		/*!< Seconds (0-60). */
	int	tm_min;		/*!< Minutes (0-59). */
	int	tm_hour;	/*!< Hours (0-23). */
	int	tm_mday;	/*!< Day of the month (1-31). */
	int	tm_mon;		/*!< Month (0-11.) */
	int	tm_year;	/*!< Year - 1900. */
	int	tm_wday;	/*!< Day of the week (0-6, Sunday = 0). */
	int	tm_yday;	/*!< Day in the year (0-365, 1 Jan = 0). */
	int	tm_isdst;	/*!< Daylight saving time. */
#ifdef __TM_GMTOFF
	long	__TM_GMTOFF;
#endif
#ifdef __TM_ZONE
	const char *__TM_ZONE;
#endif
};
#endif /*TIME_H*/

// Public variable declaration ------------------------------------------------
ret_code_t API_Datetime_Get_Timestamp(char *buff, size_t buff_size);
ret_code_t API_Datetime_Get_Timestamp_Basic(char *buff, size_t buff_size);
ret_code_t API_Datetime_Init(void);
ret_code_t API_Datetime_Get_Timestamp_Struct(struct tm *ts_struct);

#endif /* OBCS_API_API_DATETIME_INC_API_DATETIME_H_ */
