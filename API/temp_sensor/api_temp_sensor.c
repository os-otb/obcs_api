/*
 * api_temp_sensor.c
 *
 * With this API we can sense some values of determinated temperature sensors
 * and the create a string with all the results.
 * In the lower layers, this API expects to use a digital filter and the 
 * digital filter to use is leaved to the lower layers because the functions
 * of the digital filters can depend on the MCU used. Also, if the filter vary
 * this API not.
 *
 *  Created on: Apr 30, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Includes -------------------------------------------------------------------
#include "api_temp_sensor.h"
/* Other APIs include. */
#include "api_datetime.h"

/* Board config. */
#include "board.h"
#include "board_config.h"

/* Default config. */
#include "api_config_default.h"

// Macros & Constants ---------------------------------------------------------

// Private variables declaration ----------------------------------------------

// Private variables definition -----------------------------------------------
extern const api_temp_sensor_t api_temp_sensor[BOARD_TEMP_SENSORS_MAX];

// Public functions definition ------------------------------------------------
/**
 * @brief initialize the temp sensors api. 
 */
ret_code_t api_temp_sensor_init(void)
{
    ret_code_t ret = RET_OK;
    ret = board_lmt85_init();
    return ret;
}

/**
 * @brief measure every sensor that was configured and make a report in a string
 * with the temperatures and an ID. 
 */
ret_code_t api_temp_sensor_measure(char *report)
{
    if( report == NULL ) return RET_ERR_NULL_POINTER;
    ret_code_t ret = RET_OK;
    char aux_str[32];
    api_temp_sensor_measure_t measures[BOARD_TEMP_SENSORS_MAX];
    int measures_qty = sizeof(measures)/sizeof(measures[0]);
	char timestamp_str[API_DATETIME_TS_STR_LEN];

	sprintf(report, "TEMP:"); // empty the string to make the future strcats

	ret = API_Datetime_Get_Timestamp(timestamp_str, sizeof(timestamp_str));
	if( RET_OK == ret )
	{
		sprintf(aux_str, "[%s]:", timestamp_str);
		strcat(report, aux_str);
	}

    ret = board_lmt85_get_temp(measures, measures_qty);
    // Even if the measure was erroneous, the report will be made

	for(int i = 0; i < measures_qty; i++)
	{	/* Iterate over the table of sensor tags to see if the ID corresponds
		 * to a TAG. */
    	for(int c = 0; c < BOARD_TEMP_SENSORS_MAX; c++)
    	{
    		if( api_temp_sensor[i].id == measures[c].sensor_id )
    		{	// FIXME: we need to see how to print a float with pritnf-stdarg.c
                sprintf(aux_str, "%s=%6ld|", api_temp_sensor[i].tag, (int) measures[c].temp);
                strcat(report, aux_str);
                break;
    		}
    	}
	}
	strcat(report, "\n\0");


    return ret;
}
