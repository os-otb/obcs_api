
/*
 * api_temp_sensor.h
 *
 * With this API we can sense some values of determinated temperature sensors
 * and the create a string with all the results.
 * In the lower layers, this API expects to use a digital filter and the 
 * digital filter to use is leaved to the lower layers because the functions
 * of the digital filters can depend on the MCU used. Also, if the filter vary
 * this API not.
 *
 *  Created on: Apr 30, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef API_TEMP_SENSOR_H_
#define API_TEMP_SENSOR_H_

// Includes -------------------------------------------------------------------
#include "api_types.h"

// Includes -------------------------------------------------------------------
// Macros & Constants ---------------------------------------------------------
#define API_TEMP_SENSOR_ID_TAG(i, t) {.id=i, .tag=t}

// Public variables declaration -----------------------------------------------
typedef struct
{
    uint32_t id;    /*!< An ID for this temp sensor. */
	char tag[2];    /*!< A two character tag for the sensor. */
} api_temp_sensor_t;

// Public functions declaration -----------------------------------------------
ret_code_t api_temp_sensor_measure(char *report);
ret_code_t api_temp_sensor_init(void);

#endif
