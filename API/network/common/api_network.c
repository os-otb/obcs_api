/*
 * FreeRTOSPlusTCP_definitions.c
 *
 *  Created on: May 24, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Includes ------------------------------------------------------------------
#include "board.h"
#include "api_network.h"

// External functions declaration (must be defined by application) ------------
extern ret_code_t API_Network_Down_Event_Cb(void);
extern ret_code_t API_Network_Up_Event_Cb(API_Network_Data_Net_Up_Event_t net_info);
extern ret_code_t API_Network_Ping_Response_Cb(API_Network_Data_Ping_Response_t ping_response_info);

// Public functions definition -----------------------------------------------
/* @brief called by FreeRTOS+TCP TCP/IP stack when the network connects or disconnects.
 * Disconnect events are only received if implemented in the MAC driver.
 * @see https://www.freertos.org/FreeRTOS-Plus/FreeRTOS_Plus_TCP/API/vApplicationIPNetworkEventHook.html
 * */
void vApplicationIPNetworkEventHook( eIPCallbackEvent_t eNetworkEvent )
{
	API_Network_Data_Net_Up_Event_t net_info;

	switch( eNetworkEvent )
	{
	case eNetworkDown:
		if( NULL != API_Network_Down_Event_Cb ) API_Network_Down_Event_Cb();
		break;
	case eNetworkUp:
		/* Notify to net task that the device was connected to the network and send
		 * to him the network information. */
		FreeRTOS_GetAddressConfiguration(	&net_info.ulIPAddress, &net_info.ulNetMask,
											&net_info.ulGatewayAddress,
											&net_info.ulDNSServerAddress );
		if( NULL != API_Network_Up_Event_Cb ) API_Network_Up_Event_Cb(net_info);
		break;
	default:
		/* TODO handle unexpected event. */
		break;
	}
}

/* @brief called by FreeRTOS+TCP TCP/IP stack when receives an ICMP Echo Reply to an
 * ICMP Echo Request generated calling FreeRTOS_SendPingRequest(). Long story short:
 * is the callback called when the stack have the result of an Echo Request sent by
 * the device.
 * @see https://www.freertos.org/FreeRTOS-Plus/FreeRTOS_Plus_TCP/API/vApplicationPingReplyHook.html
 * */
void vApplicationPingReplyHook( ePingReplyStatus_t eStatus,
                                uint16_t usIdentifier )
{
	API_Network_Data_Ping_Response_t ping_response_info;
	ping_response_info.echo_reply_id = usIdentifier;
	ping_response_info.status = eStatus;
	/* Provide a stub for this function. */
	if( NULL != API_Network_Ping_Response_Cb ) API_Network_Ping_Response_Cb(ping_response_info);
}

/* @brief used by TCP/IP stack of FreeRTOSPlus+TCP to generate random numbers
 * for sequence numbers of some protocols.
 * */
BaseType_t xApplicationGetRandomNumber( uint32_t *pulValue )
{
	BaseType_t xReturn;
	uint32_t ulValue;
	if( pulValue == NULL )
	{
		xReturn = pdFAIL;
		return xReturn;
	}

	if( RET_OK == Board_Random_Number_Generate(&ulValue) )
	{
		xReturn = pdPASS;
		*pulValue = ulValue;
	}
	else
	{
		xReturn = pdFAIL;
	}
	return xReturn;
}

/* @brief used by TCP/IP stack of FreeRTOSPlus+TCP to generate random numbers
 * for sequence numbers of some protocols.
 * */
uint32_t ulApplicationGetNextSequenceNumber( 	uint32_t ulSourceAddress,
												uint16_t usSourcePort,
												uint32_t ulDestinationAddress,
												uint16_t usDestinationPort )
{
    ( void ) ulSourceAddress;
    ( void ) usSourcePort;
    ( void ) ulDestinationAddress;
    ( void ) usDestinationPort;

	uint32_t ulValue;

	if( RET_OK != Board_Random_Number_Generate(&ulValue) )
	{
		/* TODO Handle this error. */
	}

	return ulValue;
}
