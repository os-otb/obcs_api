/*
 * api_network.h
 *
 *  Created on: Aug 11, 2021
 *      Author: marifante
 */

#ifndef API_NETWORK_H_
#define API_NETWORK_H_

// Includes -------------------------------------------------------------------
#include "api_types.h"
#include "rtos.h"

// Public variables declaration -----------------------------------------------
typedef struct
{
	uint32_t ulIPAddress;
	uint32_t ulNetMask;
	uint32_t ulGatewayAddress;
	uint32_t ulDNSServerAddress;
}API_Network_Data_Net_Up_Event_t;

typedef struct
{
	uint16_t echo_reply_id;
	ePingReplyStatus_t status;
}API_Network_Data_Ping_Response_t;

#endif /* OBCS_API_API_NETWORK_INC_API_NETWORK_H_ */
