# Network Module
This module uses the FreeRTOS+TCP TCP/IP stack to build some servers. The servers suported are an FTP server and an OSOTB server. Both servers uses the same core TCP/IP engine and only differs in a "work" function. 
Each TCP server have a task that executes this "work" function after a select() has expired or if a select() event has ocurred.

## FTP Server
To use this server, the project must use the FreeRTOS+FAT library. This library can be included in the configuration. Also, a storage device driver must be specified and its certain drivers must be included in the Board layer of the OBCs API. 

## OSOTB Server
This is a custom server that can process some commands and acts in consecuence. Is like a lite HTTP or CoAP server that have resources and methods. A client can make request to the resources that the server has. In this case, the most important methods of the protocol are:
* GET: query the value of a resource.
* PUT: set a value on a resource.
