/*
 * api_ftp.h
 *
 *  Created on: Jul 26, 2021
 *      Author: julian
 */

#ifndef API_FTP_H_
#define API_FTP_H_

// Includes -------------------------------------------------------------------
#include "api_types.h"

// Public functions declaration -----------------------------------------------
ret_code_t API_FTP_Server_Create(void);
ret_code_t API_FTP_Server_Work(void);

#endif /* OBCS_API_API_FTP_API_FTP_H_ */
