/*
 * tcpip.c
 *
 *  Created on: May 23, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Include --------------------------------------------------------------------
#include "api_ftp.h"
#include "board.h"

/* Private includes */
#include "../priv_inc/FreeRTOS_TCP_server.h"
#include "../priv_inc/FreeRTOS_server_private.h"
#include "api_config.h"

// Macros and constants -------------------------------------------------------
#define API_FTP_LOG_ENABLED			1	/*<! Enables module log. */

#define	LOG_TAG							"api_ftp"	/*<! Tag assigned to logs for this module. */

#if API_FTP_LOG_ENABLED
	#define API_FTP_LOG_ERROR(format, ...)		LOG_ERROR(LOG_TAG, format, ##__VA_ARGS__)
	#define API_FTP_LOG_WARNING(format, ...)	LOG_WARNING(LOG_TAG, format, ##__VA_ARGS__)
	#define API_FTP_LOG_INFO(format, ...)		LOG_INFO(LOG_TAG, format, ##__VA_ARGS__)
	#define API_FTP_LOG_DEBUG(format, ...)		LOG_DEBUG(LOG_TAG, format, ##__VA_ARGS__)
#else
	#define API_FTP_LOG_ERROR(...)
	#define API_FTP_LOG_WARNING(...)
	#define API_FTP_LOG_INFO(...)
	#define API_FTP_LOG_DEBUG(...)
#endif

// Private variables declaration ---------------------------------------------
typedef struct
{
	struct xTCP_SERVER *sv;	/* Pointer to a TCP server struct defined by FreeRTOS Plus TCP. */
}api_ftp_t;

// Private variables definition -----------------------------------------------
static api_ftp_t api_ftp;

// Private functions declaration ----------------------------------------------

// Public functions definition  -----------------------------------------------
/**
 * @brief create a FTP server.
 * @param port port of the server socket.
 * @return RET_OK if success.
 * */
ret_code_t API_FTP_Server_Create(void)
{
	ret_code_t ret = RET_ERR;
	struct xSERVER_CONFIG sv_cfg = {
			.eType = eSERVER_FTP,
			.xPortNumber = API_FTP_SERVER_PORT,
			.xBackLog = API_FTP_SERVER_MAX_CLIENTS,
			.pcRootDir = API_FTP_SERVER_ROOT_DIR
	};

	/* First make sure the root dir of the FTP server exists. */
	if( 0 == API_FS_mkdir(API_FTP_SERVER_ROOT_DIR) )
	{
		api_ftp.sv = FreeRTOS_CreateTCPServer(&sv_cfg, 1);
		if( NULL == api_ftp.sv ) ret = RET_ERR_NULL_POINTER;
		else ret = RET_OK;
	}

	return ret;
}

/*
 * @brief function that must be called periodically to execute the procedures.
 * that must to accomplish the FTP server.
 * @return RET_OK if success.
 * */
ret_code_t API_FTP_Server_Work(void)
{
	ret_code_t ret = RET_OK;
	if( NULL == api_ftp.sv ) return RET_ERR_NULL_POINTER;
	FreeRTOS_TCPServerWork(api_ftp.sv, MS_TO_TICKS(API_FTP_SERVER_WORK_BLOCKTIME_MS));

	return ret;
}

/*
 * @brief callback that can be implemented by the application. This callback is executed every time
 * that the server receives a file.
 * @param file_name the name of the file received.
 * */
__attribute__((weak)) void api_ftp_file_received_cb(const char *file_name, char *reply_msg)
{

}

/*
 * @brief if ipconfigFTP_HAS_RECEIVED_HOOK is set to one, when a file is received, this callback is executed.
 * @param pcFileName the name of the file received.
 * @param ulRecvBytes the quantity of bytes received (appox filesize).
 * @param pxClient the client that is sending to us this file.
 * */
void vApplicationFTPReceivedHook( const char *pcFileName, uint32_t ulSize, struct xFTP_CLIENT *pxFTPClient )
{
	char reply_msg[API_FTP_REPLY_MSG_MAX_LEN] = {0};
	if( (NULL != pxFTPClient) && (NULL != pcFileName) )
	{
		api_ftp_file_received_cb(pcFileName, reply_msg);
		if(NULL != reply_msg) vFTPReplyMessage(pxFTPClient, reply_msg);
	}
}
