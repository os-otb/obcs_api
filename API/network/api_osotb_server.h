/*
 * api_osotb.h
 *
 *  Created on: Jul 14, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef API_OSOTB_H_
#define API_OSOTB_H_

// Includes -------------------------------------------------------------------
#include "api_types.h"
#include "api_osotb_types.h"

// Public functions declaration -----------------------------------------------
ret_code_t api_osotb_server_create( api_osotb_rx_t *api_osotb_rx );
ret_code_t api_osotb_server_work(void);

#endif /* API_OSOTB_H_ */
