/*
 * api_osotb_sv.c
 *
 *	This API implements a OSOTB server. A variant of the ndjson protocol,
 *	used to communicate a payload with a host.
 *
 *  Created on: Jul 11, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Include --------------------------------------------------------------------
#include "api_osotb_server.h"
#include "api_osotb_commands.h"

/* Private includes */
#include "../priv_inc/FreeRTOS_TCP_server.h"
#include "../priv_inc/FreeRTOS_server_private.h"
#include "api_config.h"

/* Other APIs includes. */
#include "api_logger.h"

/* FreeRTOS Protocol includes. */
#include "../priv_inc/FreeRTOS_TCP_server.h"
#include "../priv_inc/FreeRTOS_server_private.h"

// Macros & constants ---------------------------------------------------------
#define API_OSOTB_SV_LOG_ENABLED			1	/*<! Enables module log. */

#define	LOG_TAG							"api_osotb_sv"	/*<! Tag assigned to logs for this module. */

#if API_OSOTB_SV_LOG_ENABLED

#ifdef UNIT_TEST
#ifdef UNIT_TESTS_LOGS_ENABLED
	#define API_OSOTB_SV_LOG_ERROR(format, ...)		printf(format, ##__VA_ARGS__)
	#define API_OSOTB_SV_LOG_WARNING(format, ...)		printf(format, ##__VA_ARGS__)
	#define API_OSOTB_SV_LOG_INFO(format, ...)			printf(format, ##__VA_ARGS__)
	#define API_OSOTB_SV_LOG_DEBUG(format, ...)		printf(format, ##__VA_ARGS__)
#else
	#define API_OSOTB_SV_LOG_ERROR(...)
	#define API_OSOTB_SV_LOG_WARNING(...)
	#define API_OSOTB_SV_LOG_INFO(...)
	#define API_OSOTB_SV_LOG_DEBUG(...)
#endif
#else
	#define API_OSOTB_SV_LOG_ERROR(format, ...)	LOG_ERROR(LOG_TAG, format, ##__VA_ARGS__)
	#define API_OSOTB_SV_LOG_WARNING(format, ...)	LOG_WARNING(LOG_TAG, format, ##__VA_ARGS__)
	#define API_OSOTB_SV_LOG_INFO(format, ...)		LOG_INFO(LOG_TAG, format, ##__VA_ARGS__)
	#define API_OSOTB_SV_LOG_DEBUG(format, ...)	LOG_DEBUG(LOG_TAG, format, ##__VA_ARGS__)
#endif
#else
	#define API_OSOTB_SV_LOG_ERROR(...)
	#define API_OSOTB_SV_LOG_WARNING(...)
	#define API_OSOTB_SV_LOG_INFO(...)
	#define API_OSOTB_SV_LOG_DEBUG(...)
#endif

/* Ascii character definitions. */
#define osotbASCII_CR	13  /*!< Carriage return ascii char. */
#define osotbASCII_LF   10  /*!< New line ascii char. */

/* A way to identify t*/
#define pcCOMMAND_BUFFER	pxClient->pxParent->pcCommandBuffer

// Private functions declaration -----------------------------------------------
static portINLINE BaseType_t IsDigit( char cChar );
static BaseType_t prvSendReply( Socket_t xSocket, const char *pcBuffer, BaseType_t xLength );

// Private variables declaration -----------------------------------------------
typedef struct
{
	struct xTCP_SERVER *sv;	/* Pointer to a TCP server struct defined by FreeRTOS Plus TCP. */
	api_osotb_rx_t *api_osotb_rx;	/*!< Pointer to the struct needed to handle incoming osotb packets. */
}api_osotb_server_t;

// Private variables definition -----------------------------------------------
static api_osotb_server_t api_osotb_server;

// Public functions definition -------------------------------------------------
/**
 * @brief create an OSOTB server.
 * @param api_osotb_rx a pointer to the api_osotb_rx struct that holds the resources/methods/handlers
 * table and its size.
 * @return RET_OK if success.
 * */
ret_code_t api_osotb_server_create( api_osotb_rx_t *api_osotb_rx )
{
	ret_code_t ret = RET_ERR;
	if( NULL == api_osotb_rx ) return ret;

    struct xSERVER_CONFIG sv_cfg = {
            .eType = eSERVER_OSOTB,
            .xPortNumber = API_OSOTB_SERVER_PORT,
            .xBackLog = API_OSOTB_SERVER_MAX_CLIENTS,
			.pcRootDir = "/", // IMPORTANT! this must be defined even if it nos used!
    };
    api_osotb_server.api_osotb_rx = api_osotb_rx;
    api_osotb_server.sv = FreeRTOS_CreateTCPServer(&sv_cfg, 1);
    if( NULL == api_osotb_server.sv ) ret = RET_ERR_NULL_POINTER;
    else ret = RET_OK;

	return ret;
}

/*
 * @brief function that must be called periodically to execute the procedures
 * that must to accomplish the OSOTB server.
 * @return RET_OK if success.
 * */
ret_code_t api_osotb_server_work(void)
{
	ret_code_t ret = RET_OK;
	if( NULL == api_osotb_server.sv ) return RET_ERR_NULL_POINTER;
	FreeRTOS_TCPServerWork(api_osotb_server.sv, MS_TO_TICKS(API_OSOTB_SERVER_WORK_BLOCKTIME_MS));
	return ret;
}

// Private functions definition -----------------------------------------------
/*
 * @brief OSOTB server work function. will be called by FreeRTOS_TCPServerWork(), 
 * after select has expired(). FD_ISSET will not be used.  This work function will 
 * always be called at regular intervals, and also after a select() event has occurred.
 */
BaseType_t xOSOTBClientWork( TCPClient_t *pxTCPClient )
{
    OSOTBClient_t *pxClient = ( OSOTBClient_t * ) pxTCPClient;
    BaseType_t xRc;

	/* Call recv() in a non-blocking way, to see if there is an OSOTB command
	sent to this server. */
	xRc = FreeRTOS_recv( pxClient->xSocket, ( void * )pcCOMMAND_BUFFER, sizeof( pcCOMMAND_BUFFER ), 0 );

	if( xRc > 0 )
	{
        ret_code_t ret = RET_OK;

		if( xRc < ( BaseType_t ) sizeof( pcCOMMAND_BUFFER ) )
		{
			pcCOMMAND_BUFFER[ xRc ] = '\0';
		}

		while( xRc && ( ( pcCOMMAND_BUFFER[ xRc - 1 ] == osotbASCII_CR ) || ( pcCOMMAND_BUFFER[ xRc - 1 ] == osotbASCII_LF ) ) )
		{
			pcCOMMAND_BUFFER[ --xRc ] = '\0';
		}

		/* In the response_buffer, will be stores the response to send to the client.*/
        char response_buffer[API_OSOTB_SV_RESPONSE_MAX_LEN] = {0}; // Here will be stored the response to the client

        ret = api_osotb_rx_process_pkt(api_osotb_server.api_osotb_rx, pcCOMMAND_BUFFER, strlen(pcCOMMAND_BUFFER), response_buffer);
        if( RET_OK != ret )
		{
        	API_OSOTB_SV_LOG_ERROR("%s: error processing incoming packet!", __func__);
		}

		if( 0 > prvSendReply( pxClient->xSocket, response_buffer, strlen(response_buffer)) )
		{
			API_OSOTB_SV_LOG_ERROR("%s: error sending reply! (%s)",__func__, response_buffer);
		}
	}
	else 
    if( xRc < 0 )
	{   /* The connection will be closed and the client will be deleted. */
		FreeRTOS_printf( ( "%s: xRc = %ld\n", __func__, xRc ) );
	}
	return xRc;
}

/**
 * @brief close a client socket.
 */
void vOSOTBClientDelete( TCPClient_t *pxTCPClient )
{
    FTPClient_t *pxClient = ( FTPClient_t * ) pxTCPClient;

	/* Close the FTP command socket */
	if( pxClient->xSocket != FREERTOS_NO_SOCKET )
	{
		FreeRTOS_FD_CLR( pxClient->xSocket, pxClient->pxParent->xSocketSet, eSELECT_ALL );
		FreeRTOS_closesocket( pxClient->xSocket );
		pxClient->xSocket = FREERTOS_NO_SOCKET;
	}
}

/**
 * @brief send a reply to the client. 
 * @param xSocket the client socket to send the reply.
 * @param pcBuffer the reply to send.
 * @param xLength the length of the reply to send.
 * @return The number of bytes actually sent. Zero when nothing could be sent
 *         or a negative error code in case an error occurred.
 */
static BaseType_t prvSendReply( Socket_t xSocket, const char *pcBuffer, BaseType_t xLength )
{
    BaseType_t xResult;

	if( xLength == 0 ) xLength = strlen( pcBuffer );

	xResult = FreeRTOS_send( xSocket, ( const void * )pcBuffer, ( size_t ) xLength, 0 );
	if( IsDigit( ( int ) pcBuffer[ 0 ] ) &&
		IsDigit( ( int ) pcBuffer[ 1 ] ) &&
		IsDigit( ( int ) pcBuffer[ 2 ] ) &&
		IsDigit( ( int ) pcBuffer[ 3 ] ) )
	{
		const char *last = pcBuffer + strlen( pcBuffer );
		int iLength;
		while( ( last > pcBuffer ) && ( ( last[ -1 ] == osotbASCII_CR ) || ( last[ -1 ] == osotbASCII_LF ) ) )
		{
			last--;
		}
		iLength = ( int )( last - pcBuffer );
		API_OSOTB_SV_LOG_INFO( "   %-*.*s", iLength, iLength, pcBuffer );
	}
	return xResult;
}

/**
 * @brief check if a char is a digit (a number).
 * @param cChar the character to check.
 * @return true if it is a number, false otherwise.
 */
static portINLINE BaseType_t IsDigit( char cChar )
{
    BaseType_t xResult;

	if( cChar >= '0' && cChar <= '9' )
	{
		xResult = pdTRUE;
	}
	else
	{
		xResult = pdFALSE;
	}
	return xResult;
}
