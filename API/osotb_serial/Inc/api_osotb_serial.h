/*
 * api_cli.h
 *
 *  Created on: Oct 8, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef INC_API_CLI_H_
#define INC_API_CLI_H_

// Includes -------------------------------------------------------------------
#include "api_types.h"

/* Uses of other APIs */
#include "api_osotb_commands.h"

// Macros & Constants ---------------------------------------------------------
#define API_OSOTB_SERIAL_PACKET_DELIMITER_CHAR '\n'

// Public variables declaration -----------------------------------------------

// Public functions declarations ----------------------------------------------
ret_code_t api_osotb_serial_init( api_osotb_rx_t *api_osotb_rx ); /*!< Used to initialize the serial OSOTB interface.. */

#endif /* INC_API_CLI_H_ */
