/*
 * api_osotb_serial.c
 *
 *  Created on: Oct 8, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Include --------------------------------------------------------------------
#include "api_osotb_serial.h"
#include "board.h"

/* Uses of other APIs */
#include "api_osotb_commands.h"
#include "api_crc.h"

// Macros & constants ---------------------------------------------------------
#define API_OSOTB_SERIAL_MAX_SEND_ERRORS		10		/*!< Max send errors that an osotb client/server can count. */
#define API_OSOTB_SERIAL_SEND_ERROR_TIMEOUT_MS 10000	/*!< The max time that can pass without receiving an ACK or a NACK for a packet sent. */
#define API_OSOTB_HEADER_SIZE					(1)
#define API_OSOTB_CRC_SIZE						(4)
#define API_OSOTB_TRAILER_SIZE					(API_OSOTB_CRC_SIZE+1)
#define API_OSOTB_SERIAL_PACKET_OVERHEAD		(API_OSOTB_HEADER_SIZE+API_OSOTB_TRAILER_SIZE)
#define API_OSOTB_SERIAL_PACKET_MAX_DATA_SIZE	BOARD_USART_MAX_PACKET_SIZE-API_OSOTB_SERIAL_PACKET_OVERHEAD
#define API_OSOTB_SERIAL_PACKET_MAX_SIZE		API_OSOTB_SERIAL_PACKET_MAX_DATA_SIZE+API_OSOTB_SERIAL_PACKET_OVERHEAD
#define API_OSOTB_SERIAL_TASK_DELAY_MS			50			/*<! in ms, delay of the task. */
#define API_OSOTB_SERIAL_TASK_STACK_SIZE		256 + API_OSOTB_SERIAL_PACKET_MAX_SIZE
#define API_OSOTB_SERIAL_TASK_PRIOTY			osPriorityNormal

#define API_OSOTB_SERIAL_BLOCK_TIME_MS			10		/*!< The OSOTB_SERIAL interpreter task will block this time waiting for a command. */

#define API_OSOTB_SERIAL_LOG_ENABLED			1	/*<! Enables module log. */

#define	LOG_TAG		"api_osotb_serial"	/*<! Tag assigned to logs for this module. */

#if API_OSOTB_SERIAL_LOG_ENABLED

#ifdef UNIT_TEST
#ifdef UNIT_TESTS_LOGS_ENABLED
	#define API_OSOTB_SERIAL_LOG_ERROR(format, ...)		printf(format, ##__VA_ARGS__)
	#define API_OSOTB_SERIAL_LOG_WARNING(format, ...)		printf(format, ##__VA_ARGS__)
	#define API_OSOTB_SERIAL_LOG_INFO(format, ...)			printf(format, ##__VA_ARGS__)
	#define API_OSOTB_SERIAL_LOG_DEBUG(format, ...)		printf(format, ##__VA_ARGS__)
#else
	#define API_OSOTB_SERIAL_LOG_ERROR(...)
	#define API_OSOTB_SERIAL_LOG_WARNING(...)
	#define API_OSOTB_SERIAL_LOG_INFO(...)
	#define API_OSOTB_SERIAL_LOG_DEBUG(...)
#endif
#else
	#define API_OSOTB_SERIAL_LOG_ERROR(format, ...)		LOG_ERROR(LOG_TAG, format, ##__VA_ARGS__)
	#define API_OSOTB_SERIAL_LOG_WARNING(format, ...)	LOG_WARNING(LOG_TAG, format, ##__VA_ARGS__)
	#define API_OSOTB_SERIAL_LOG_INFO(format, ...)		LOG_INFO(LOG_TAG, format, ##__VA_ARGS__)
	#define API_OSOTB_SERIAL_LOG_DEBUG(format, ...)		LOG_DEBUG(LOG_TAG, format, ##__VA_ARGS__)
#endif
#else
	#define API_OSOTB_SERIAL_LOG_ERROR(...)
	#define API_OSOTB_SERIAL_LOG_WARNING(...)
	#define API_OSOTB_SERIAL_LOG_INFO(...)
	#define API_OSOTB_SERIAL_LOG_DEBUG(...)
#endif

#define STX 0x2	/*!< Header byte for the osotb serial packets. */
#define ACK	((char) 0x6)	/*!< Acknowledge byte! */
#define NACK ((char) 0x15)	/*!< Not-Acknowledge byte! */

// Private variables declaration ----------------------------------------------
typedef struct
{
	TaskHandle_t task_handle;
	api_osotb_rx_t *api_osotb_rx; /*!< Pointer to the table that stores the resources/methods/handlers needed.. */
}api_osotb_serial_t;

// Private variables definition -----------------------------------------------
static api_osotb_serial_t api_osotb_serial;

// Private functions declaration ----------------------------------------------
static void api_osotb_serial_task( void *pvParameters );
static ret_code_t api_osotb_serial_poll_input( char *buff, size_t buff_size );
static ret_code_t api_osotb_serial_send_packet( char *buff, size_t buff_size );
static ret_code_t api_osotb_serial_send( char *buff, size_t buff_size );
static ret_code_t api_osotb_serial_receive_packet(	char *buff, size_t buff_size,
													size_t *data_length, uint16_t *received_crc );

// Public functions definition  -----------------------------------------------
/**
 * @brief initialize the OSOTB_SERIAL API. This function will create the task that handles
 * all the incoming commands.
 * @param api_osotb_rx a pointer to the osotb rx table with the resources/methods/handlers
 * that the serial interface will handle.
 */
ret_code_t api_osotb_serial_init( api_osotb_rx_t *api_osotb_rx )
{
	ret_code_t ret = RET_OK;
	if( NULL == api_osotb_rx ) return RET_ERR_NULL_POINTER;

	// Creates the task that processes this commands
	api_osotb_serial.api_osotb_rx = api_osotb_rx;

	if( xTaskCreate(	api_osotb_serial_task, "api_osotb_serial",
						API_OSOTB_SERIAL_TASK_STACK_SIZE,
						NULL,
						API_OSOTB_SERIAL_TASK_PRIOTY,
						&api_osotb_serial.task_handle ) != pdPASS )
	{
		ret = RET_ERR;
	}
	else
	{
		API_OSOTB_SERIAL_LOG_INFO("%s: OSOTB serial interface initialized!", __func__);
	}

	return ret;
}

// Private functions definition -----------------------------------------------
/*
 * @brief Task that provides the input and output for the FreeRTOS+OSOTB_SERIAL command
 * interpreter.
 */
static void api_osotb_serial_task( void *pvParameters )
{
	char input_buffer[ API_OSOTB_SERIAL_PACKET_MAX_SIZE ];
	char response[ API_OSOTB_SERIAL_PACKET_MAX_SIZE ];

	( void ) pvParameters; /* Just to prevent compiler warnings. */

	while(true)
	{
		// Get the firt string received from
		if( RET_OK == api_osotb_serial_poll_input(input_buffer, sizeof(input_buffer)) )
		{	/*	Process the packet received. */
			if( RET_OK != api_osotb_rx_process_pkt(api_osotb_serial.api_osotb_rx, input_buffer, strlen(input_buffer), response) )
			{
				API_OSOTB_SERIAL_LOG_ERROR("%s: error processing incoming packet!", __func__);
			}

			if( RET_OK != api_osotb_serial_send_packet(response, strlen(response)) )
			{
				API_OSOTB_SERIAL_LOG_ERROR("%s: error sending output!", __func__);
			}
		}
	}

	vTaskDelete( NULL );
}

// Public functions definition ------------------------------------------------
/**
 * @brief block until an OSOTB packet is received from the serial interface.
 * @param buff a buffer where will be stored the data if it is processed.
 * This packet will be stored as a string.
 * @param buff_size the max size of the buffer.
 * the task that executes it.
 * @return true if a command has been received or false otherwise
 */
static ret_code_t api_osotb_serial_poll_input( char *buff, size_t buff_size )
{
	ret_code_t ret = RET_OK;
	bool valid_packet_received = false;
	size_t data_length;
	uint16_t received_crc;

	while( valid_packet_received == false )
	{
		memset(buff, 0, buff_size);
		ret = api_osotb_serial_receive_packet(buff, buff_size, &data_length, &received_crc);
		if( RET_OK == ret )
		{	/* Verify the CRC! */
			uint16_t crc_calculated = api_crc16_ccitt_buf((uint8_t*) buff, data_length);
			char confirmation;
			if( crc_calculated != received_crc )
			{	/* If CRC is not valid then send a NACK and continue polling. */
				confirmation = NACK;
				api_osotb_serial_send(&confirmation, sizeof(confirmation));
			}
			else
			{	/*If CRC is valid then send an ACK and return to process the packet. */
				confirmation = ACK;
				api_osotb_serial_send(&confirmation, sizeof(confirmation));
				valid_packet_received = true;
			}
		}
		else
		{
			API_OSOTB_SERIAL_LOG_ERROR("%s: error polling input!", __func__);
		}
	}

	return ret;
}

/**
 * @brief receive a serial osotb packet through the serial port.
 * @param buff where the data of the packet will be stored.
 * @param buff_size the max size of the buffer passed as param.
 * @param data_length the length of the data in the packet.
 * @param received_crc the crc that comes in the packet.
 * @return RET_OK if success.
 */
static ret_code_t api_osotb_serial_receive_packet(	char *buff, size_t buff_size,
													size_t *data_length, uint16_t *received_crc )
{
	if( ( buff == NULL ) || (data_length == NULL) || (received_crc == NULL) ) return RET_ERR_NULL_POINTER;
	ret_code_t ret = RET_OK;
	int buff_loc = 0;
	bool packet_received = false, stx_received = false, delimiter_char_received = false;
	char rx_char;
	int crc_bytes_received = 0;
	*data_length = 0; // clear the var!
	*received_crc = 0; // clear the var!

	while( packet_received == false )
	{
		ret = Board_USART_Get_Char(API_USART_SERIAL_LOG, &rx_char, true);
		if( RET_EMPTY == ret ) vTaskDelay(MS_TO_TICKS(API_OSOTB_SERIAL_BLOCK_TIME_MS));
		else
		if( RET_OK == ret )
		{
			if( false == stx_received )
			{	/* The first byte always must be a STX (Start of Text). */
				if( rx_char == STX ) stx_received = true;
			}
			else
			{
				if( false == delimiter_char_received )
				{
					if( buff_loc >= buff_size )
					{	/* Preventing buffer overflow! */
						ret = RET_ERR;
						API_OSOTB_SERIAL_LOG_ERROR("%s: packet is too big! (%d)", __func__, buff_loc);
						break;
					}
					else
					{	/* A char must be intered in the buffer!. */
						buff[buff_loc] = rx_char;
						if( buff[buff_loc] == API_OSOTB_SERIAL_PACKET_DELIMITER_CHAR )
						{	// If the delimiter char is found then wait for CRC
							delimiter_char_received = true;
							*data_length = buff_loc; // don't include the delimiter in the data_length
						}
						else
						{
							buff_loc++;
						}
					}
				}
				else
				{	/* Now wait for the CRC16. */
					if( crc_bytes_received == 0 ) *received_crc |= (rx_char << 8) & 0xFF00;
					else
					if( crc_bytes_received == 1 )
					{
						*received_crc |= (rx_char << 0) & 0x00FF;
						packet_received = true;
					}
					crc_bytes_received++;
				}
			}
		}
		else
		{	// Error!!
			API_OSOTB_SERIAL_LOG_ERROR("%s: error receiving bytes!!", __func__);
			break;
		}
	}
	return ret;
}

/**
 * @brief send an OSOTB packet through the serial interface.
 * @param buff the data to send in the osotb packet.
 * @param buff_size, the size of the buffer that holds the data.
 * @return RET_OK if success.
 */
static ret_code_t api_osotb_serial_send_packet( char *buff, size_t buff_size )
{
	if( NULL == buff ) return RET_ERR_NULL_POINTER;
	ret_code_t ret = RET_OK;

	char output_packet[API_OSOTB_SERIAL_PACKET_MAX_SIZE];
	output_packet[0] = STX;
	memcpy(&output_packet[1], buff, buff_size);

	/* Generate and copy crc to output buffer. */
	uint16_t crc = api_crc16_ccitt_buf((uint8_t*) buff, buff_size-1); // The -1 is for the new line character at the end!!
	output_packet[1+buff_size] = (crc >> 8) & 0xFF;
	output_packet[1+buff_size+1] = (crc >> 0) & 0xFF;

	size_t packet_size = 1+buff_size+sizeof(crc);

	/* Send the packet and wait until the confirmation! */
	uint32_t send_errors = 0, t0, now;
	bool ack_received = false;
	char confirmation;
	while( (send_errors < API_OSOTB_SERIAL_MAX_SEND_ERRORS) && (ack_received == false) )
	{
		t0 = TICKS_TO_MS(xTaskGetTickCount()); // count before sending the packet
		now = t0;
		ret = api_osotb_serial_send( output_packet, packet_size );
		if( RET_OK == ret )
		{	// Wait for the ACK or the NACK until timeout.
			while( API_OSOTB_SERIAL_SEND_ERROR_TIMEOUT_MS > (now - t0) )
			{
				now = TICKS_TO_MS(xTaskGetTickCount());
				if( RET_EMPTY != Board_USART_Get_Char(API_USART_SERIAL_LOG, &confirmation, true) )
				{
					if( confirmation == NACK )
					{	// If a nack is received then increment send error counter and re-send
						send_errors++;
						break;
					}
					else
					if( confirmation == ACK )
					{	// If an ack is received then stop sending!
						ack_received = true;
						break;
					}
				}
			}
			if( API_OSOTB_SERIAL_SEND_ERROR_TIMEOUT_MS <= (now - t0) ) send_errors++;
		}
	}

	if( send_errors >= API_OSOTB_SERIAL_MAX_SEND_ERRORS ) ret = RET_ERR;
	return ret;
}


/**
 * @brief send a buffer through the serial port.
 * @param buff the buffer to send
 * @param buff_size the size of the buffer to send
 * @return RET_OK if success
 * */
static ret_code_t api_osotb_serial_send( char *buff, size_t buff_size )
{
	ret_code_t ret = RET_OK;
	while(RET_ERR_BUSY == Board_USART_TX_Is_Busy(API_USART_SERIAL_LOG))
	{
		/* Wait until a serial transference is complete. */
	}

	ret = Board_USART_Send_Packet(API_USART_SERIAL_LOG, buff, buff_size);
	if(RET_OK != ret)
	{
		/* TODO handle error. */
	}
	return ret;
}
