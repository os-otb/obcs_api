/*
 * api_ymodem.c
 *
 *  Implementation of ymodem protocol specially designed to be used with
 *  FreeRTOS.
 *
 *  Reference for YMODEM protocol: http://pauillac.inria.fr/~doligez/zmodem/ymodem.txt
 *
 *  Created on: Oct 14, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Include --------------------------------------------------------------------
#include "api_ymodem.h"
#include "board.h"

#include "api_ymodem_priv.h"

// Macros & constants ---------------------------------------------------------
/* @brief logger task configuration parameters. */
#define API_YMODEM_TASK_STDIN_BUFFER_LEN	BOARD_USART_MAX_PACKET_SIZE
#define API_YMODEM_TASK_STDOUT_BUFFER_LEN	BOARD_USART_MAX_PACKET_SIZE
#define API_YMODEM_TASK_DELAY_MS			50			/*<! in ms, delay of the task. */
#define API_YMODEM_TASK_STACK_SIZE		512 + API_XMODEM_TASK_STDIN_BUFFER_LEN
#define API_YMODEM_TASK_PRIOTY			osPriorityNormal

#define API_YMODEM_BLOCK_TIME_MS			10		/*!< The OSOTB_SERIAL interpreter task will block this time waiting for a command. */

#define API_YMODEM_LOG_ENABLED			1	/*<! Enables module log. */

#define	LOG_TAG		"api_ymodem"	/*<! Tag assigned to logs for this module. */

#if API_YMODEM_LOG_ENABLED

#ifdef UNIT_TEST
#ifdef UNIT_TESTS_LOGS_ENABLED
	#define API_YMODEM_LOG_ERROR(format, ...)		printf(format, ##__VA_ARGS__)
	#define API_YMODEM_LOG_WARNING(format, ...)		printf(format, ##__VA_ARGS__)
	#define API_YMODEM_LOG_INFO(format, ...)			printf(format, ##__VA_ARGS__)
	#define API_YMODEM_LOG_DEBUG(format, ...)		printf(format, ##__VA_ARGS__)
#else
	#define API_YMODEM_LOG_ERROR(...)
	#define API_YMODEM_LOG_WARNING(...)
	#define API_YMODEM_LOG_INFO(...)
	#define API_YMODEM_LOG_DEBUG(...)
#endif
#else
	#define API_YMODEM_LOG_ERROR(format, ...)		LOG_ERROR(LOG_TAG, format, ##__VA_ARGS__)
	#define API_YMODEM_LOG_WARNING(format, ...)	LOG_WARNING(LOG_TAG, format, ##__VA_ARGS__)
	#define API_YMODEM_LOG_INFO(format, ...)		LOG_INFO(LOG_TAG, format, ##__VA_ARGS__)
	#define API_YMODEM_LOG_DEBUG(format, ...)		LOG_DEBUG(LOG_TAG, format, ##__VA_ARGS__)
#endif
#else
	#define API_YMODEM_LOG_ERROR(...)
	#define API_YMODEM_LOG_WARNING(...)
	#define API_YMODEM_LOG_INFO(...)
	#define API_YMODEM_LOG_DEBUG(...)
#endif

// Private functions declaration ----------------------------------------------
ret_code_t api_ymodem_get_char(uint8_t *data, int timeout_s);
ret_code_t api_ymodem_send_char(uint8_t data);
ret_code_t api_ymodem_calculate_crc16(uint16_t *crc, uint8_t *data, size_t size);
uint16_t api_ymodem_update_crc16(uint16_t crcIn, uint8_t byte);

// Public functions definition ------------------------------------------------
/**
 * @brief receive a file using the ymodem protocol.
 * @param filepath a buffer where will be stored the path of the file received.
 * @return RET_OK if success
 */
static ret_code_t api_ymodem_receive_file( char *file_path )
{
	ret_code_t ret = RET_OK;

	return ret;
}

/**
 * @brief receive a file using the ymodem protocol.
 * @param filepath a buffer where will be stored the path of the file received.
 * @return RET_OK if success
 */
static ret_code_t api_ymodem_send_file( char *file_path )
{
	ret_code_t ret = RET_OK;

	return ret;
}

// Private functions definition -----------------------------------------------

typedef enum
{
	API_YMODEM_RECEIVE_PKT_HEADER = 0,
	API_YMODEM_RECEIVE_PKT_DATA,
	API_YMODEM_RECEIVE_PKT_CRC,
	API_YMODEM_RECEIVE_PKT_FINISH,
	API_YMODEM_RECEIVE_MAX_STATES			/*!< last one. */
}api_ymodem_receive_pkt_state_t;

typedef enum
{
	API_YMODEM_ABORT_BY_SENDER = -1, /*!< A sender aborts the transmssion. */
	API_YMODEM_END_OF_TRANSMISSION = 0, /*!< A sender sends an END of transmission. */

} api_ymodem_receive_packet_ret_code_t;

typedef enum
{
	API_YMODEM_UNKNOWN_PACKET = -1,
	API_YMODEM_DATA_PACKET = 0,

}api_ymodem_packet_type_t;

typedef struct
{
	uint8_t header[3];
	uint8_t data[1024];
	uint8_t crc[2];
	uint32_t data_len;
	api_ymodem_packet_type_t type;
}api_ymodem_packet_t;

typedef enum
{
	API_YMODEM_RECEIVE_PKT_OK = 0,
	API_YMODEM_RECEIVE_PKT_END_OF_TRANSMISSION,
	API_YMODEM_RECEIVE_PKT_ABORT_BY_SENDER,
	API_YMODEM_RECEIVE_PKT_ABORT_BY_RECEIVER,
	API_YMODEM_RECEIVE_PKT_TIMEOUT,
	API_YMODEM_RECEIVE_PKT_ERR,
	API_YMODEM_RECEIVE_PKT_MAX_RET_CODES
} api_ymodem_receive_pkt_ret_t;

/**
 * @brief receive an ymodem packet from an USART. The funcions blocks until we receive
 * a packet from the serial port.
 * @param buff buffer where the packet will be stored.
 * @param buff_size size of the buffer.
 * @param timeout_s the max time to wait for a char to come.
 * @param packet_length here will be stored the packet length.
 * @param check_crc
 * @return RET_OK if packet is received.
 */
static ret_code_t api_ymodem_receive_packet( api_ymodem_packet_t *pkt, uint32_t timeout_s )
{
	if( NULL == buff ) return RET_ERR_NULL_POINTER;
	ret_code_t ret = RET_OK;
	api_ymodem_receive_pkt_ret_t ret_rx_pkt = API_YMODEM_RECEIVE_PKT_OK;
	api_ymodem_receive_pkt_state_t fsm_state = API_YMODEM_RECEIVE_PKT_HEADER;
	uint8_t received_byte;
	int received_bytes_qty = 0, packet_size;

	while( fsm_state < API_YMODEM_RECEIVE_PKT_FINISH )
	{
		ret = api_ymodem_get_char(&received_byte, timeout_s);
		if( RET_OK == ret )
		{
			received_bytes_qty++; // a byte was received!
			switch( fsm_state )
			{
				/* In this state we will process the header of the packet. */
				case API_YMODEM_RECEIVE_PKT_HEADER:
					pkt->header[received_bytes_qty-1] = received_byte;
					/* The first byte of the packet defines its type. */
					if( 1 == received_bytes_qty )
					{
						switch( pkt->header[received_bytes_qty-1] )
						{
							case SOH:	/*!< Packet of 128 bytes. */
								packet_size = PACKET_SIZE;
								break;
							case STX:	/*!< Packet of 1024 bytes. */
								packet_size = PACKET_1K_SIZE;
								break;
							case EOT:	/*!< end of transmission received. */
								ret_rx_pkt = API_YMODEM_RECEIVE_PKT_END_OF_TRANSMISSION;
								fsm_state = API_YMODEM_RECEIVE_PKT_FINISH;
								break;
							default:
								ret_rx_pkt = API_YMODEM_RECEIVE_PKT_ERR;
								fsm_state = API_YMODEM_RECEIVE_PKT_FINISH;
						}
					}

					/* If the first two bytes are CA then is an abort by the sender.  */
					if( (2 == received_bytes_qty) &&
						(pkt->header[received_bytes_qty-2] == CA) &&
						(pkt->header[received_bytes_qty-1] == CA))
					{
						ret_rx_pkt = API_YMODEM_RECEIVE_PKT_ABORT_BY_SENDER;
						fsm_state = API_YMODEM_RECEIVE_PKT_FINISH;
					}

					/* Check if the frame number byte (2) and the inverted
					 * frame number byte (3) are the inverse: */
					if( (3 == received_bytes_qty) &&
						(pkt->header[received_bytes_qty-1] != ((pkt->header[received_bytes_qty] ^ 0xFF) & 0xFF)) )
					{	/* The bytes are not he inverse of each other! error in the transmission. */
						ret_rx_pkt = API_YMODEM_RECEIVE_PKT_ERR;
						fsm_state = API_YMODEM_RECEIVE_PKT_FINISH;
					}
					else
					{	/* Advance to next state ot get data! */
						fsm_state = API_YMODEM_RECEIVE_PKT_DATA;
					}
					break;

				/* In this state we will store the data of the packet. */
				case API_YMODEM_RECEIVE_PKT_DATA:
					pkt->data[received_bytes_qty-1-sizeof(pkt->header)] = received_byte;
					if( received_bytes_qty > (packet_size + sizeof(pkt->header)) )
					{
						fsm_state = API_YMODEM_RECEIVE_PKT_CRC;
					}
					break;

				/* Verify the CRC of the packet. */
				case API_YMODEM_RECEIVE_PKT_CRC:
					pkt->crc[received_bytes_qty-1-sizeof(pkt->header)-packet_size] = received_byte;
					if( 1 == received_bytes_qty-1-sizeof(pkt->header)-packet_size )
					{	/* Second byte of the crc has been received! Calculate the crc*/
						uint16_t crc = ((pkt->crc[0] << 8) & 0xFF00 || (pkt->crc[1] & 0x00FF) );
						uint16_t computed_crc;
						if( RET_OK != api_ymodem_calculate_crc(&computed_crc, pkt->data, packet_size) ) ret_rx_pkt = API_YMODEM_RECEIVE_PKT_ERR;
						else
						{
							if( crc != computed_crc ) ret_rx_pkt = API_YMODEM_RECEIVE_PKT_ERR;
							else ret_rx_pkt = API_YMODEM_RECEIVE_PKT_OK;
						}
						fsm_state = API_YMODEM_RECEIVE_PKT_FINISH;
					}
					break;

				default:
					ret_rx_pkt = API_YMODEM_RECEIVE_PKT_ERR;
					fsm_state = API_YMODEM_RECEIVE_PKT_FINISH;
					API_YMODEM_LOG_ERROR("%s: unrecognized state (%d)",__func__, fsm_state);
					break;
			}
		}
		else
		if( RET_ERR_TIMEOUT == ret )
		{
			fsm_state = API_YMODEM_RECEIVE_PKT_FINISH;
			ret_rx_pkt = API_YMODEM_RECEIVE_PKT_TIMEOUT;
			API_YMODEM_LOG_ERROR("%s: timeout receiving a packet!", __func__);
		}
	}
	return ret_rx_pkt;
}


/**
  * @brief  Receive a file using the ymodem protocol
  * @param  buf: Address of the first byte
  * @return RET_OK if success.
  */
ret_code_t api_ymodem_receive( uint8_t *buf )
{
	api_ymodem_packet_t packet;
	uint8_t *file_ptr, *buf_ptr;
	int32_t i, j, packet_length, session_done, file_done, packets_received, errors, session_begin, size = 0;

	uint8_t file_name[FILE_NAME_LENGTH], file_size[FILE_SIZE_LENGTH];
	uint8_t packet_data[PACKET_1K_SIZE];

	for(session_done = 0, errors = 0, session_begin = 0; ;)
	{
		for(packets_received = 0, file_done = 0, buf_ptr = buf; ;)
		{	/* Receive packets until the file is received. */
			switch( api_ymodem_receive_packet(&packet, NAK_TIMEOUT_S) )
			{
				case API_YMODEM_RECEIVE_PKT_ABORT_BY_SENDER:
					api_ymodem_send_char(ACK);
					return RET_ERR;
					break;

				case API_YMODEM_RECEIVE_PKT_END_OF_TRANSMISSION:
					api_ymodem_send_char(ACK);
					file_done = 1;
					break;

				case API_YMODEM_RECEIVE_PKT_OK:
					if( (packet.header[PACKET_SEQNO_INDEX] & 0xff) != (packets_received & 0xff) ) api_ymodem_send_char(NAK);
					else
					{
						if( packets_received == 0 )	/* Receive first packet! */
						{	/* The first packet have the filename and the file size. */
							if( packet.data[0] != 0 )
							{	/* Filename packet has valid data */
								for( i = 0, file_ptr = packet.data; (*file_ptr != 0) && (i < FILE_NAME_LENGTH);) file_name[i++] = *file_ptr++;
								file_name[i++] = '\0';
								for( i = 0, file_ptr++; (*file_ptr != ' ') && (i < FILE_SIZE_LENGTH);) file_size[i++] = *file_ptr++;
								file_size[i++] = '\0';
								size = atoi(file_size);

								api_ymodem_send_char(ACK);
								api_ymodem_send_char(CRC16);
							}
							else
							{	/* Filename packet is empty, end session */
								api_ymodem_send_char(ACK);
								file_done = 1;
								session_done = 1;
								break;
							}
						}
						else
						{	/* Data packet (packets from 1 and above). */
							memcpy(packet.data, packet_data, packet.data_len);
							api_ymodem_send_char(ACK);
						}
						packets_received ++;
						session_begin = 1;
					}
					break;

				case API_YMODEM_RECEIVE_PKT_ABORT_BY_RECEIVER:
					api_ymodem_send_char(CA);
					api_ymodem_send_char(CA);
					return RET_ERR;

				case API_YMODEM_RECEIVE_PKT_ERR:
				case API_YMODEM_RECEIVE_PKT_TIMEOUT:
				default: // error!
					if( session_begin > 0 ) errors ++;
					if( errors > MAX_ERRORS )
					{
						api_ymodem_send_char(CA);
						api_ymodem_send_char(CA);
						return RET_ERR;
					}
					api_ymodem_send_char(CRC16);
					break;
			}
			if(file_done != 0) break;
		}
		if(session_done != 0) break;
	}
	return RET_OK;
}


/**
 * @brief get a char from the serial port or wait timeout_s for a char.
 * @param data a buffer where the byte will be stored.
 * @param timeout_s the max time that we will wait for the byte.
 * @return RET_OK if succes or RET_ERR_TIMEOUT if timeout occurs.
 */
ret_code_t api_ymodem_get_char(uint8_t *data, int timeout_s)
{
	if( NULL == data ) return RET_ERR_NULL_POINTER;
	ret_code_t ret = RET_EMPTY; /* At first, assume that there is not any byte in the usart buffer. */
	int initial_time = xTaskGetTickCount();
	int now = initial_time;

	while( timeout_s > (now - initial_time) )
	{
		ret = Board_USART_Get_Char(API_USART_SERIAL_LOG, data, true);
		if( RET_OK == ret )
		{	/* byte was received! */
			break;
		}
		else
		if( RET_EMPTY != ret )
		{	/* error receiving byte! */
			API_YMODEM_LOG_ERROR("%s: error receiving byte!", __func__);
			break;
		}
		now = xTaskGetTickCount();
	}

	if( ret = RET_EMPTY ) ret = RET_ERR_TIMEOUT;
	return ret;
}

/**
 * @brief send a char through the serial port.
 * @param data the char to send.
 * @return RET_OK if success.
 */
ret_code_t api_ymodem_send_char(uint8_t data)
{
	ret_code_t ret = RET_OK;
	ret = Board_USART_Send_Packet(API_USART_SERIAL_LOG, data, 1);
	return ret;
}

/**
 * @brief calculate the YMODEM crc over a buffer.
 * @param expected_crc the desired crc (i.e. the crc received in the packet).
 * @param data the buffer were we will calculate the crc.
 * @param size the size of the buffer
 * @param crc the value of the crc16.
 * @return RET_OK if success.
 */
ret_code_t api_ymodem_calculate_crc16(uint16_t *crc, uint8_t *data, size_t size)
{
	if( (NULL == buff ) || (NULL == crc) ) return RET_ERR_NULL_POINTER;
	ret_code_t ret = RET_OK;
	uint32_t crc_computed = 0;

	const uint8_t* data_end = data+size;
	while( data < data_end )
	{
		crc_computed = api_ymodem_update_crc16(crc_computed,*data++);
	}

	crc_computed = api_ymodem_update_crc16(crc_computed,0);
	crc_computed = api_ymodem_update_crc16(crc_computed,0);

	*crc =  crc_computed&0xffffu;
	return ret;
}

/**
  * @brief  Update CRC16 for input byte
  * @param  CRC input value
  * @param  input byte
  * @retval None
  */
uint16_t api_ymodem_update_crc16(uint16_t crcIn, uint8_t byte)
{
	uint32_t crc = crcIn;
	uint32_t in = byte|0x100;
	do
	{
		crc <<= 1;
		in <<= 1;
		if(in&0x100)	++crc;
		if(crc&0x10000)	crc ^= 0x1021; // xor with polynomial
	} while(!(in&0x10000));
	return crc&0xffffu;
}
