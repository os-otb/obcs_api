/*
 * api_base.h
 *
 *  Created on: May 1, 2021
 *      Author: Julian Rodriguez aka Marifante 
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef API_H_
#define API_H_

// Includes  ------------------------------------------------------------------

/* APIs */
#include <api_osotb_server.h>
#include <crc/Inc/api_crc.h>
#include "api_common.h"
#include "api_fsm.h"
#include "api_network.h"
#include "api_ftp.h"
#include "api_datetime.h"
#include "api_flash.h"
#include "api_fs.h"
#include "api_tcpip.h"
#include "api_gpio.h"
#include "api_iap.h"
#include "api_osotb_serial.h"
#include "api_logger.h"
#include "api_temp_sensor.h"

#endif /* API_H_ */
